﻿import shutil
import os.path
from distutils.dir_util import copy_tree
import time
import glob
import re
  


@then("“S07_MaskEditor” 프로젝트 파일이 TA에 열렸는지 확인")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.frame_tab.bt_round_tab7.Drag(94, 36, 0, 8)
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.h5, "QtText", cmpEqual, "E:/Regression_Test/Prefix_File/S07_MaskEditor/S07_MaskEditor.tcpro")

@when("Preview 패널에서 Hep G2{arg} 파일을 선택하고 Toolbox의 Mask editor 버튼을 누른다.")
def step_impl(param1):
  frame = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base
  frame.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(126, 52)
  frame.bt_round_tool4.Click(57, 7)

@then("Hep G2{arg} 파일로 Mask Editor 탭이 열렸는지 확인")
def step_impl(param1):
  Regions.RenderWindow.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget)

@when("ID manager 패널의 New 버튼을 클릭하고 팝업에 ‘rt’를 입력한 뒤 키보드의 엔터 키를 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  scrollArea = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.leftLayout.IDManagerPanel.panel_base.panel_contents.bt_round_gray500.ClickButton()
  tomoAnalysis_InterSeg_Plugins_NewIdDialog = tomoAnalysis.NewIdDialog
  lineEdit = tomoAnalysis_InterSeg_Plugins_NewIdDialog.input_high
  lineEdit.Click(224, 18)
  lineEdit.SetText("rt")
  tomoAnalysis_InterSeg_Plugins_NewIdDialog.bt_square_primary.ClickButton()

@then("ID manager 패널에 rt 라는 ID가 생겼는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea.qt_scrollarea_viewport.leftLayout.IDManagerPanel.panel_base.panel_contents.IDCombo, "wText", cmpEqual, "rt")

@when("Mask control 패널에서 Cell instance 옆 회색 동그라미를 클릭한다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.leftLayout.MaskControlPanel.panel_base.panel_contents.instStatus.ClickButton()

@then("Mask control 패널 Cell instance 옆 회색 동그라미가 파란색으로 바뀌었는지 확인")
def step_impl():
  Regions.instStatus.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea.qt_scrollarea_viewport.leftLayout.MaskControlPanel.panel_base.panel_contents.instStatus)

@then("Mask selector 패널의 Cell instance 버튼이 활성화 되었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.MaskSelectionPanel.panel_base.panel_contents.instBtn, "Enabled", cmpEqual, True)

@when("Mask selector 패널에서 Cell instance 아이콘을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.MaskSelectionPanel.panel_base.panel_contents.instBtn.ClickButton()

@then("Mask selector 패널에서 Cell instance 아이콘이 파랗게 하이라이트 되었는지 확인")
def step_impl():
  Regions.instBtn.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.MaskSelectionPanel.panel_base.panel_contents.instBtn)

@when("Drawing tool 패널에서 Paint 버튼을 누르고 size 칸에 100을 입력한 뒤 키보드의 엔터 키를 누른다.")
def step_impl():
  widget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.DrawingToolPanel.panel_base.panel_contents
  widget.PaintBtn.ClickButton()
  spinBox = widget.stackedWidget.BrushPage.BrushSizeSpin
  spinBox.qt_spinbox_lineedit.Drag(21, 19, -41, -1)
  spinBox.Keys("100")
  spinBox.wValue = 100
  spinBox.Keys("[Enter]")
  spinBox.wValue = 100

@when("Mask Editor 탭의 XY Slice 화면을 한 번 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Click(564, 360)

@when("Label tools 패널의 Add 버튼을 한 번 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.LabelControlPanel.panel_base.panel_contents.bt_round_gray500.ClickButton()

@when("Mask Editor 탭의 XY Slice 화면에서 빈 부분을 한 번 클릭한다.")
def step_impl():
    Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Click(660, 254)

@then("Mask Editor 탭의 XY Slice 화면에 빨간색, 초록색 동그라미가 하나씩 생겼는지 확인")
def step_impl():
  aqUtils.Delay(3000)
  Regions.MainWindow1.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow)

@then("Label tools 패널의 Label 칸에 2가 표시됐는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.LabelControlPanel.panel_base.panel_contents.LabelSpin, "wValue", cmpEqual, 2)

@when("Clean tool 패널의 Clean selected label 버튼을 누르고 XY Slice 화면의 녹색 동그라미를 클릭한다.")
def step_impl():
  tomoAnalysis_InterSeg_AppUI_MainWindow = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  tomoAnalysis_InterSeg_AppUI_MainWindow.CleanerPanel.panel_base.panel_contents.FlushSelBtn.ClickButton()
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Click(642, 277)

@then("Mask Editor 탭의 XY Slice 화면에서 녹색 동그라미가 사라졌는지 확인")
def step_impl():
  Regions.RenderWindow5.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow)

@when("History 패널의 Undo 버튼을 클릭한다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.leftLayout.HistoryPanel.panel_base.panel_contents.UndoBtn.ClickButton()

@then("Mask Editor 탭 XY Slice 화면에서 사라졌던 녹색 동그라미가 다시 생겼는지 확인")
def step_impl():
  Regions.RenderWindow1.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow)

@when("Mask selector 패널에서 파란색의 Cell instance 아이콘을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.MaskSelectionPanel.panel_base.panel_contents.instBtn.ClickButton()

@then("Mask Editor 탭 XY Slice 화면에서 마스크들이 사라졌는지 확인")
def step_impl():
  Regions.MainWindow2.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow)

@when("General 패널에서 Mask file download 버튼을 누르고 Cell Instance Mask에 체크한 후 Save 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  scrollArea = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.leftLayout.GeneralPanel.panel_base.panel_contents.SaveBtn.ClickButton()
  tomoAnalysis_InterSeg_Plugins_MaskSelectionDialog = tomoAnalysis.MaskSelectionDialog
  tomoAnalysis_InterSeg_Plugins_MaskSelectionDialog.InstRadio.ClickButton()
  tomoAnalysis_InterSeg_Plugins_MaskSelectionDialog.bt_square_primary.ClickButton()

@when("파일 경로와 이름을 건드리지 않고 키보드의 엔터 키를 누른다.")
def step_impl():
  Aliases.TomoAnalysis.dlgSaveMask.btn_S.ClickButton()

@then("Hep G2{arg} TCF 파일이 있는 폴더에 파일 이름 끝에 _rt가 붙은 .msk 확장자 파일이 생성됐는지 확인")
def step_impl(param1):
  explorer = Aliases.explorer
  explorer.wndShell_TrayWnd.ReBarWindow32.MSTaskSwWClass.MSTaskListWClass.Click(41, 11)
  wndCabinetWClass = explorer.wndCabinetWClass
  HWNDView = wndCabinetWClass.ShellTabWindowClass.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  UIItemsView = HWNDView.CtrlNotifySink2.ShellView.Item
  UIItemsView.Regression_Test.Item.DblClick(80, 12)
  UIItemsView.S_Test.Item.DblClick(56, 5)
  UIItemsView.S07_MaskEditor.Item.DblClick(64, 14)
  UIItemsView.Hep_G2_105.Item.DblClick(189, 15)
  aqObject.CheckProperty(Aliases.explorer.wndCabinetWClass.ShellTabWindowClass.DUIViewWndClassName.Item.CtrlNotifySink2.ShellView.Item.Hep_G2_105_MultiLabel_rt_msk.Item, "Value", cmpEqual, "20200611.164640.571.Hep G2-105_MultiLabel_rt.msk")
  wndCabinetWClass.Close()

@when("Mask Editor 탭 이름 옆의 x 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.TC_TabBar.CloseButton.Click(7, 8)

@when("General 패널에서 msk file upload 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  scrollArea = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.leftLayout.GeneralPanel.panel_base.panel_contents.LoadBtn.ClickButton()


@when("파일 탐색기에서 파일 이름 끝에 _rt가 붙은 .msk 확장자 파일을 더블 클릭한다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.dlgOpenMask.OpenFile("E:\\Regression_Test\\S_Test\\S07_MaskEditor\\20200611.164640.571.Hep G2-105\\20200611.164640.571.Hep G2-105_MultiLabel_rt.msk")

@when("Mask selector 패널에서 Cell instance 아이콘을 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.MaskSelectionPanel.panel_base.panel_contents.instBtn.ClickButton()

@when("Application Parameter 패널에서 RI Thresholding을 선택하고 Single RUN 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_gray.Click(59, 6)

@when("Hep G2{arg} 파일을 더블 클릭한다.")
def step_impl(param1):
  Aliases.TomoAnalysis.widget_popup.scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget2.panel_contents_image.DblClick(175, 77)

@when("Basic Analyzer 탭에서 Execute 버튼을 눌러 마스크를 생성한다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.buttonExecute.ClickButton()

@when("Basic Analyzer 탭에서 Mask Correction 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.correctionBtn.ClickButton()

@then("Mask Editor 탭에 Mask control, Mask selector 패널이 전부 활성화된 상태의 Hep G2{arg} 파일이 열렸는지 확인")
def step_impl(param1):
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.MaskSelectionPanel.panel_base.panel_contents, "QtText", cmpEqual, "")
  Regions.panel_contents3.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.MaskSelectionPanel.panel_base.panel_contents)
  Regions.panel_contents4.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea.qt_scrollarea_viewport.leftLayout.MaskControlPanel.panel_base.panel_contents)

@when("탭 ID manager 패널의 Copy 버튼을 누르고 팝업에 ‘rt’를 입력한 후 OK 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  scrollArea = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.leftLayout.IDManagerPanel.panel_base.panel_contents.bt_round_gray5002.ClickButton()
  tomoAnalysis_InterSeg_Plugins_NewIdDialog = tomoAnalysis.NewIdDialog
  lineEdit = tomoAnalysis_InterSeg_Plugins_NewIdDialog.input_high
  lineEdit.Click(125, 12)
  lineEdit.SetText("rt")
  tomoAnalysis_InterSeg_Plugins_NewIdDialog.bt_square_primary.ClickButton()

@when("Mask Selector 패널의 Cell instance 아이콘을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.MaskSelectionPanel.panel_base.panel_contents.instBtn.ClickButton()

@when("Drawing tool 패널의 Paint 버튼을 누르고 Label tools 패널의 Add 버튼을 누른 다음 Mask Editor 탭 XY Slice 화면의 빈 곳을 한 번 클릭한다.")
def step_impl():
  tomoAnalysis_InterSeg_AppUI_MainWindow = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  tomoAnalysis_InterSeg_AppUI_MainWindow.DrawingToolPanel.panel_base.panel_contents.PaintBtn.ClickButton()
  tomoAnalysis_InterSeg_AppUI_MainWindow.LabelControlPanel.panel_base.panel_contents.bt_round_gray500.ClickButton()
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.QtObject("QSplitter", "", 1).QtObject("QWidget", "", 3).QtObject("QWidget", "", 1).QtObject("TomoAnalysis::InterSeg::Plugins::InterSegRenderWindow2D", "", 1).QtObject("SoQtGLMgr").QtObject("SoQtGLWidget_InternalWidget").Window("Qt5157QWindowOwnDCIcon", "TomoAnalysis", 1).Click(378, 139)

@then("Mask Editor 탭 XY Slice 화면에 기존 마스크와 녹색 동그라미가 함께 생겨 있는지 확인")
def step_impl():
  Regions.RenderWindow2.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow)

@when("Clean tool 패널에서 Clean selected label 버튼을 누르고 XY Slice 화면의 녹색 동그라미를 클릭한다.")
def step_impl():
  tomoAnalysis_InterSeg_AppUI_MainWindow = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  tomoAnalysis_InterSeg_AppUI_MainWindow.CleanerPanel.panel_base.panel_contents.FlushSelBtn.ClickButton()
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis.Click(339, 148)

@then("Mask Editor 탭에서 XY Slice 화면의 녹색 동그라미가 사라졌는지 확인")
def step_impl():
  Regions.RenderWindow3.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow)

@when("History 패널에서 Undo 버튼을 클릭한다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.leftLayout.HistoryPanel.panel_base.panel_contents.UndoBtn.ClickButton()

@then("Mask Editor 탭의 XY Slice 화면에서 사라졌던 녹색 동그라미가 다시 생겼는지 확인")
def step_impl():
  Regions.RenderWindow4.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow)

@when("Mask selector 패널에서 파란색으로 표시된 Cell instance 아이콘을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.MaskSelectionPanel.panel_base.panel_contents.instBtn.ClickButton()

@then("Mask Editor 탭 XY Slice 화면에서 마스크가 모두 사라졌는지 확인")
def step_impl():
  Regions.TomoAnalysis48.Check(Aliases.TomoAnalysis.mainwindow.TomoAnalysis7)

@when("General 패널 Apply to Result 버튼을 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.leftLayout.GeneralPanel.panel_base.panel_contents.SaveLinkBtn.ClickButton()

@then("Mask Editor에서 수정한 마스크가 Basic Analyzer 탭 Mask Viewing 패널에 나타났는지 확인")
def step_impl():
  Regions.viewerPanel.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel)

@when("Application tab 바에서 Basic Analyzer와 Mask Editor 탭 이름 옆의 x 버튼을 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.TC_TabBar.CloseButton.Click(0, 5)

@when("Application Parameter에서 Batch Run 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  TC_AdaptiveTabWidget = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget
  TC_AdaptiveTabWidget.TC_TabBar.Click(108, 18)
  TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_primary.Click(60, 6)
  tomoAnalysis.fileChangeForm.bt_square_primary.ClickButton()

@then("조금 기다린 뒤 Report 탭이 열렸는지 확인")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  while tomoAnalysis.ProgressDialog.Exists :
   Delay(1000, "Waiting for window to close...");
  Regions.MainWindow3.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow)

@when("Report 탭 Select Parameter 패널에서 Projected area를 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.selectParameterWidget.selectParameterWidget.parametersGroupBox.RadioButton.ClickButton()

@when("Graph of individual cell 패널의 MIN 값을 450으로 설정한다.")
def step_impl():
  doubleSpinBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.widget.yMinInputBox
  doubleSpinBox.qt_spinbox_lineedit.Drag(50, 8, -71, -4)
  doubleSpinBox.Keys("450")
  doubleSpinBox.wValue = 450
  doubleSpinBox.Keys("[Enter]")
  doubleSpinBox.wValue = 450

@when("Report 탭 Graph of individual cell 패널에서 {arg}_{arg} 그래프를 누르고 Edit Mask 버튼을 누른다.")
def step_impl(param1, param2):
  widget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.widget
  widget.Widget.TCChartView.Widget.Click(527, 200)
  widget.correctBtn.ClickButton()

@when("Mask Editor 탭 ID manager 패널의 Copy 버튼을 누르고 팝업에서 ‘rt’를 입력한 후 OK 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  scrollArea = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.leftLayout.IDManagerPanel.panel_base.panel_contents.bt_round_gray5002.ClickButton()
  tomoAnalysis_InterSeg_Plugins_NewIdDialog = tomoAnalysis.NewIdDialog
  lineEdit = tomoAnalysis_InterSeg_Plugins_NewIdDialog.input_high
  lineEdit.Click(214, 5)
  lineEdit.SetText("rt")
  tomoAnalysis_InterSeg_Plugins_NewIdDialog.bt_square_primary.ClickButton()

@when("Mask Selector 패널 Cell instance 아이콘을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.MaskSelectionPanel.panel_base.panel_contents.instBtn.ClickButton()

@when("Drawing tool 패널의 Fill 버튼을 누르고 Mask Editor 탭 XY Slice의 빈 공간을 클릭한다.")
def step_impl():
  tomoAnalysis_InterSeg_AppUI_MainWindow = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  tomoAnalysis_InterSeg_AppUI_MainWindow.DrawingToolPanel.panel_base.panel_contents.FillBtn.ClickButton()
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.QtObject("QSplitter", "", 1).QtObject("QWidget", "", 3).QtObject("QWidget", "", 1).QtObject("TomoAnalysis::InterSeg::Plugins::InterSegRenderWindow2D", "", 1).QtObject("SoQtGLMgr").QtObject("SoQtGLWidget_InternalWidget").Window("Qt5157QWindowOwnDCIcon", "TomoAnalysis", 1).Click(771, 110)

@then("Mask Editor 탭 XY Slice 화면 전체에 마스크가 입혀졌는지 확인")
def step_impl():
  Regions.RenderWindow7.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow)

@when("Mask Selector 패널 Whole cell 아이콘을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.MaskSelectionPanel.panel_base.panel_contents.memBtn.ClickButton()

@when("Drawing tool 패널의 Fill 버튼을 누르고 Mask Editor 탭 XY Slice의 빈 부분을 클릭한다.")
def step_impl():
  tomoAnalysis_InterSeg_AppUI_MainWindow = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  tomoAnalysis_InterSeg_AppUI_MainWindow.DrawingToolPanel.panel_base.panel_contents.FillBtn.ClickButton()
  tomoAnalysis_InterSeg_AppUI_MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer.Widget.Splitter.Splitter.TomoAnalysis_Viewer_Plugins_ViewerRenderWindow2d2.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis7.Click(754, 161)

@when("Mask selector 패널에서 파란색으로 표시된 Whole cell 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.MaskSelectionPanel.panel_base.panel_contents.memBtn.ClickButton()

@then("Mask Editor 탭 XY Slice 화면에서 마스크가 전부 사라졌는지 확인")
def step_impl():
  Regions.RenderWindow8.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow)

@when("General 패널에서 Apply to Result 버튼을 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.leftLayout.GeneralPanel.panel_base.panel_contents.SaveLinkBtn.ClickButton()

@then("Report 탭에서 두 그래프 패널이 달라졌는지 확인")
def step_impl():
  Aliases.TomoAnalysis.DeleteDialog.bt_square_primary.ClickButton()
  Regions.graphReportPanel.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel)


@then("TA종료")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.Close()
