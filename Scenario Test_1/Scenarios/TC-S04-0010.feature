﻿Feature: Basic Analyzer AI Segmentation

  Scenario: Basic Analyzer의 AI Segmentation을 이용하여 single cells 혹은 organelles에 대한 마스크를 만들고 측정 값을 Export
Given 빈 TA가 열려있다.

When Select Project 탭의 Open 버튼을 누른다.

And “S04_BasicAnalyzerAIS” 프로젝트 파일을 선택한다.

Then “S04_BasicAnalyzerAIS” 프로젝트가 TA에 열렸는지 확인

When BA Application Parameter 패널의 제일 위 드롭다운 버튼에서 AI Segmentation을 클릭한다.

And Single RUN 버튼을 누른다.

And 팝업에서 HeLa-Rab7a-004 파일을 더블클릭한다.

Then Basic Analyzer 탭이 열렸는지 확인

When Basic Analyzer 탭의 Processing mode 패널 AI Inference 탭의 Execute 버튼을 클릭한다.

Then Basic Analyzer 탭의 Mask Viewing panel에 마스크가 생성되었는지 확인

And Basic Analyzer 탭의 Mask Viewing panel 상단에 Cell Instance, Whole Cell, Nucleus, Nucleolus, Lipid droplet 선택 버튼과 Multi-layered mask 스위치, Hide Mask 버튼, Opacity 조절 칸이 있는 툴박스가 생성되었는지 확인

And Basic Analyzer 탭의 AI Inference 탭의 Execute 버튼이 하이라이트 해제되고 Basic Measurement 탭의 Execute 버튼이 하이라이트 되었는지 확인

When 마우스를 Basic Analyzer 탭 Mask Viewing panel의 3D 마스크 캔버스 위로 가져가 클릭한 채로 마우스를 움직인다.

Then Basic Analyzer 탭 Mask Viewing panel의 3D 마스크가 회전하는지 확인

When Mask Viewing panel 상단 Mask 툴박스에서 Multi-layered mask 스위치를 켠다.

Then Basic Analyzer 탭 Mask Viewing panel에서 multi layer로 마스크가 표현됐는지 확인

When Basic Analyzer 탭 Basic Measurement 탭의 Execute 버튼을 누른다.

When Basic Analyzer 탭 위의 Measurement 팝업의 Export 버튼을 누른다.

And 저장 경로와 파일 이름을 지정한 뒤 저장 버튼을 누른다.

Then 지정한 경로에 생성된 .csv 파일의 데이터가 Measurement 탭의 데이터와 동일한지 확인한다.

When Basic Analyzer 탭의 Apply Parameter 버튼을 누른다.

Then TA에서 Basic Analyzer 탭이 Project Manager 탭으로 넘어갔는지 확인

When Batch Run 버튼을 누른다.

And File changes 팝업 Flush 탭의 Accept 버튼을 누른다.

And 프로세싱 바 팝업의 진행이 완료된다.

Then Report 탭이 열렸는지 확인

When Report 탭의 Graph theme 스위치 버튼을 누른다.

Then Report 탭의 두 그래프 패널 배경 색이 검은색으로 바뀌었는지 확인

When Report 탭 Select Parameter 패널에서 Projected area 체크박스에, Select orgamelle 패널에서 Lipid droplet 체크박스에 체크한다.

Then Report 탭 두 그래프 패널의 y축이 Projected area로, 두 그래프의 타이틀이 Lipid droplet으로 바뀌었는지 확인

When Report 탭 오른쪽 그래프 패널의 MIN 칸에 0.0000을 입력한다.

Then Report 탭 그래프들의 y축 최솟값이 0으로 설정되었는지 확인

When Report 탭 오른쪽 그래프 패널의 Capture 버튼을 누른다.

And 파일 탐색기에서 파일 경로와 이름을 입력한다.

Then Report 탭 오른쪽 그래프가 지정한 경로에 .png 파일로 저장되었는지 확인

When Report 탭 왼쪽 그래프 패널의 Capture 버튼을 누른다.

And 파일 탐색기에서 파일의 경로와 이름을 입력한다.

Then Report 탭 왼쪽 그래프가 지정한 경로에 .png 파일로 저장되었는지 확인

When Report 탭 Data Table 패널의 Case table의 Export 버튼을 누른다.

And 파일 탐색기에서 경로를 지정하고 ‘Case’ 파일을 저장한다.

And Report 탭 Data Table 패널의 Control table의 Export 버튼을 누른다.

And 파일 탐색기에서 경로를 지정하고 ‘Control’ 파일을 저장한다.

Then 지정한 경로에 ‘Case.csv’, ‘Control.csv’ 파일이 생성됐는지 확인

When Application tab 바의 Report 탭 이름 옆의 x 버튼을 클릭한다.

Then Report 탭이 종료되는지 확인