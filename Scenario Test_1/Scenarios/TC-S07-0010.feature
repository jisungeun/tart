﻿Feature: Mask Editor

  Scenario: Mask Editor를 이용하여 마스크 편집
Given 빈 TA가 열려있다.

When Select Project 탭의 Open 버튼을 누른다.

And “S07_MaskEditor” 프로젝트 파일을 선택한다.

Then “S07_MaskEditor” 프로젝트 파일이 TA에 열렸는지 확인

When Preview 패널에서 Hep G2-105 파일을 선택하고 Toolbox의 Mask editor 버튼을 누른다.

Then Hep G2-105 파일로 Mask Editor 탭이 열렸는지 확인

When ID manager 패널의 New 버튼을 클릭하고 팝업에 ‘rt’를 입력한 뒤 키보드의 엔터 키를 누른다.

Then ID manager 패널에 rt 라는 ID가 생겼는지 확인

When Mask control 패널에서 Cell instance 옆 회색 동그라미를 클릭한다.

Then Mask control 패널 Cell instance 옆 회색 동그라미가 파란색으로 바뀌었는지 확인

And Mask selector 패널의 Cell instance 버튼이 활성화 되었는지 확인

When Mask selector 패널에서 Cell instance 아이콘을 누른다.

Then Mask selector 패널에서 Cell instance 아이콘이 파랗게 하이라이트 되었는지 확인

When Drawing tool 패널에서 Paint 버튼을 누르고 size 칸에 100을 입력한 뒤 키보드의 엔터 키를 누른다.

And Mask Editor 탭의 XY Slice 화면을 한 번 클릭한다.

And Label tools 패널의 Add 버튼을 한 번 누른다.

And Mask Editor 탭의 XY Slice 화면에서 빈 부분을 한 번 클릭한다.

Then Mask Editor 탭의 XY Slice 화면에 빨간색, 초록색 동그라미가 하나씩 생겼는지 확인

And Label tools 패널의 Label 칸에 2가 표시됐는지 확인

When Clean tool 패널의 Clean selected label 버튼을 누르고 XY Slice 화면의 녹색 동그라미를 클릭한다.

Then Mask Editor 탭의 XY Slice 화면에서 녹색 동그라미가 사라졌는지 확인

When History 패널의 Undo 버튼을 클릭한다.

Then Mask Editor 탭 XY Slice 화면에서 사라졌던 녹색 동그라미가 다시 생겼는지 확인

When Mask selector 패널에서 Cell instance 아이콘을 누른다.

Then Mask Editor 탭 XY Slice 화면에서 마스크들이 사라졌는지 확인

When General 패널에서 Mask file download 버튼을 누르고 Cell Instance Mask에 체크한 후 Save 버튼을 누른다.

And 파일 경로와 이름을 건드리지 않고 키보드의 엔터 키를 누른다.

Then Hep G2-105 TCF 파일이 있는 폴더에 파일 이름 끝에 _rt가 붙은 .msk 확장자 파일이 생성됐는지 확인

When Mask Editor 탭 이름 옆의 x 버튼을 누른다.

And Preview 패널에서 Hep G2-105 파일을 선택하고 Toolbox의 Mask editor 버튼을 누른다.

And ID manager 패널의 New 버튼을 클릭하고 팝업에 ‘rt’를 입력한 뒤 키보드의 엔터 키를 누른다.

And General 패널에서 msk file upload 버튼을 누른다.

And 파일 탐색기에서 파일 이름 끝에 _rt가 붙은 .msk 확장자 파일을 더블 클릭한다.

And Mask selector 패널에서 Cell instance 아이콘을 선택한다.

Then Mask Editor 탭의 XY Slice 화면에 빨간색, 초록색 동그라미가 하나씩 생겼는지 확인2

When Mask Editor 탭 이름 옆의 x 버튼을 누른다.

And Application Parameter 패널에서 RI Thresholding을 선택하고 Single RUN 버튼을 누른다.

And Hep G2-105 파일을 더블 클릭한다.

And Basic Analyzer 탭에서 Execute 버튼을 눌러 마스크를 생성한다.

And Basic Analyzer 탭에서 Mask Correction 버튼을 누른다.

Then Mask Editor 탭에 Mask control, Mask selector 패널이 전부 활성화된 상태의 Hep G2-105 파일이 열렸는지 확인

When 탭 ID manager 패널의 Copy 버튼을 누르고 팝업에 ‘rt’를 입력한 후 OK 버튼을 누른다.

And Mask Selector 패널의 Cell instance 아이콘을 누른다.

And Drawing tool 패널의 Paint 버튼을 누르고 Label tools 패널의 Add 버튼을 누른 다음 Mask Editor 탭 XY Slice 화면의 빈 곳을 한 번 클릭한다.

Then Mask Editor 탭 XY Slice 화면에 기존 마스크와 녹색 동그라미가 함께 생겨 있는지 확인

When Clean tool 패널에서 Clean selected label 버튼을 누르고 XY Slice 화면의 녹색 동그라미를 클릭한다.

Then Mask Editor 탭에서 XY Slice 화면의 녹색 동그라미가 사라졌는지 확인

When History 패널에서 Undo 버튼을 클릭한다.

Then Mask Editor 탭의 XY Slice 화면에서 사라졌던 녹색 동그라미가 다시 생겼는지 확인

When Mask selector 패널에서 파란색으로 표시된 Cell instance 아이콘을 누른다.

Then Mask Editor 탭 XY Slice 화면에서 마스크가 모두 사라졌는지 확인

When General 패널 Apply to Result 버튼을 누른다.

Then Mask Editor에서 수정한 마스크가 Basic Analyzer 탭 Mask Viewing 패널에 나타났는지 확인

When Application tab 바에서 Basic Analyzer와 Mask Editor 탭 이름 옆의 x 버튼을 클릭한다.

And Application Parameter에서 Batch Run 버튼을 누른다.

Then 조금 기다린 뒤 Report 탭이 열렸는지 확인

When Report 탭 Select Parameter 패널에서 Projected area를 선택한다.

And Graph of individual cell 패널의 MIN 값을 450으로 설정한다.

And Report 탭 Graph of individual cell 패널에서 2_1 그래프를 누르고 Edit Mask 버튼을 누른다.

And Mask Editor 탭 ID manager 패널의 Copy 버튼을 누르고 팝업에서 ‘rt’를 입력한 후 OK 버튼을 누른다.

And Mask Selector 패널 Cell instance 아이콘을 누른다.

And Drawing tool 패널의 Fill 버튼을 누르고 Mask Editor 탭 XY Slice의 빈 공간을 클릭한다.

Then Mask Editor 탭 XY Slice 화면 전체에 마스크가 입혀졌는지 확인

When Mask Selector 패널 Whole cell 아이콘을 누른다.

And Drawing tool 패널의 Fill 버튼을 누르고 Mask Editor 탭 XY Slice의 빈 부분을 클릭한다.

And Mask selector 패널에서 파란색으로 표시된 Whole cell 버튼을 누른다.

Then Mask Editor 탭 XY Slice 화면에서 마스크가 전부 사라졌는지 확인

When General 패널에서 Apply to Result 버튼을 누른다.

Then Report 탭에서 두 그래프 패널이 달라졌는지 확인

Then TA종료