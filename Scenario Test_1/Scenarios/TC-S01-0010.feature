﻿Feature: Creating Project File

  Scenario: Project Manager 탭에서 TA의 Application 구동을 위한 Project 파일을 만들기
Given 빈 TA가 열려있다.

When Select Project 탭의 Create 버튼을 누른다.

And 프로젝트의 이름을 “S01_CreatingProjectFile”로 입력하고 저장 경로를 선택한 뒤 Create 버튼을 누른다.

Then PEP에 프로젝트가 생성됐는지 확인

And Operation Sequence 패널이 Import Data 탭으로 자동 진행됐는지 확인

When Import Data 탭의 Import 버튼을 누른다.

And 불러올 TCF 파일이 있는 폴더를 선택한다.

Then Preview 패널에 TCF 파일의 2D MIP 썸네일 이 잘 보이는지 확인

When Operation Sequence 패널 상단에서 Set Playground 탭 버튼을 클릭한다.

Then Operation Sequence 패널에 Set Playground 탭이 열렸는지 확인

When Set Playground 탭에서 키보드의 엔터 키를 누른다.

Then Operation Sequence 패널이 Create Hypercube 탭으로 자동 진행 되었는지 확인

When Create Hypercube 탭의 이름 입력 칸에 “Test”를 입력하고 키보드의 엔터 키를 누른다.

Then Playground canvas 패널에 Hypercube 아이콘이 생성됐는지 확인

And Operation Sequence 패널이 Create Cube 탭으로 자동 진행 되었는지 확인

When Create Cube 탭에서 Cube 이름 입력 칸에 “Case”를 입력하고  오른쪽 드롭다운 버튼에서 “Test” Hypercube를 선택한 후 아래의 OK 버튼을 누른다.

And Create Cube 탭에서 Cube 이름 입력 칸에 “Control”을 입력하고  키보드의 엔터 키를 누른다.

Then PEP와 Playground canvas에서 “Case”, “Control” 큐브가 생성되었는지 확인

When Operation Sequence 패널 상단에서 Link Data to Cube 탭 버튼을 클릭한다.

Then Operation Sequence 패널에 Link Data to Cube 탭이 열렸는지 확인

And Tooolbox의 Link to Cube, Unlink from Cube 두 버튼이 하이라이트 되었는지 확인

When Preview 패널에서 “Case” cube에 연결할 데이터를 모두 선택한다.

And Toolbox의 Link to Cube 버튼을 누르고 팝업에서 “Case”를 선택한 뒤 키보드의 엔터 키를 누른다.

Then Playground canvas 패널의 “Case” cube 아이콘 모양이 바뀌었는지 확인

And Preview 패널에서 “Case” cube에 연결된 TCF 썸네일에 cube 이름이 표시됐는지 확인

And Operation Sequence 패널이 Select Application 탭으로 자동 진행됐는지 확인

When Preview 패널에서 “Control” cube에 연결할 데이터를 모두 선택한다.

And Toolbox의 Link to Cube 버튼을 누르고 팝업에서 “Control”을 선택한 뒤 팝업의 Link 버튼을 누른다.

And Playground canvas 패널의 “Case” cube 아이콘 모양이 바뀌었는지 확인

And Preview 패널에서 “Case” cube에 연결된 TCF 썸네일에 cube 이름이 표시됐는지 확인

When Select Application 탭에서 Basic Analyzer와 “Test”를 선택하고 아래의 OK 버튼을 누른다.

Then Application Parameter 패널의 UI가 바뀌었는지 확인

