﻿Feature: 3D Visualizer

  Scenario: 비활성화 상태의 TFbox를 삭제한 뒤 다시 TFbox를 생성
  
Given 3D Visualizer 탭이 열려 있다.

And 3D Visualizer 탭 TF c anvas에 비활성화 상태의 TF box가 만들어져 있다.

When Preset 패널 하단의 Clear 버튼을 누른다.

And Preset 패널의 TF canvas의 하얀색 영역에서 마우스를 클릭한 채로 드래그한다.

And 팝업에서 빨간색을 지정하고 키보드의 엔터 키를 누른다.

Then View 패널에 빨간색 3D rendering이 생성되었는지 확인

Then TA종료