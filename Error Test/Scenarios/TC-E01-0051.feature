﻿Feature: Project Manager

  Scenario: Hypercube 생성 시 이름에 특수문자 입력 불가
Given 빈 TA가 열려있다.  
  
Given Create Hypercube 탭이 열려있다.

When Create Hypercube 탭의 hypercube 이름을 지정하는 칸에 "? , < > / ; : ' “ \ | [ { ] } ! @ # $ % ^ & * ( ) + ="를 키보드로 입력한다.

Then Create Hypercube 탭의 hypercube 이름을 지정하는 칸에 아무것도 입력되지 않았는지 확인