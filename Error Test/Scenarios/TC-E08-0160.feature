﻿Feature: 3D Visualizer

  Scenario: Movie maker 탭에서 Record 할 때 저장 경로에 한글 폴더가 포함되어 있으면 경고창
Given 3D Visualizer 탭의 View 패널에 Rendering이 생성되어 있다.

And Movie maker 탭에 Orbit 효과 옵션이 추가되어 있다.

When Movie maker 탭의 Record 버튼을 누르고 Multi-view 버튼을 누른다.

And 파일 탐색기에서 저장 경로를 “테스트 데이터” 폴더로 지정하고 이름을 입력한 뒤 저장 버튼을 누른다.

Then 3D Visualizer 탭에 “Target movie path contains non-english characters” 경고창이 나타났는지 확인

