﻿Feature: 3D Visualizer

  Scenario: 3D Visualizer 탭에서 Viewing tool 패널의 너비를 오른쪽으로 늘릴 때, 드롭다운 버튼과 선택 창 길이도 함께 늘어남
Given 3D Visualizer 탭이 열려 있다.

When 3D Visualizer 탭 View 패널과 Viewing tool 패널 사이의 경계에 마우스 포인터를 가져가서 클릭한 채로 오른쪽 끝까지 드래그한다.

And Viewing tool 패널의 Layout 드롭다운 버튼을 누른다.

Then Viewing tool 패널의 Layout 드롭다운 버튼의 너비와 클릭 시 나오는 Layout 선택 창의 너비가 같은지 확인

When Viewing tool 패널의 View 드롭다운 버튼을 누른다.

Then Viewing tool 패널의 View 드롭다운 버튼의 너비와 클릭 시 나오는 View 선택 창의 너비가 같은지 확인

Then TA종료