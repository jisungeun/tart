﻿Feature: Project Manager

  Scenario: 같은 이름의 Playground를 새로 생성하려 했을 때 생성되지 않음  
  
Given 빈 TA가 열려있다.  

Given ‘Playground’라는 이름의 playground가 생성되어 있다.

When Set playground 탭으로 이동한다.

And New Playground 버튼을 누른다.

And Playground의 이름을 ‘Playground’로 지정하고 OK 버튼을 누른다.

Then 에러 팝업이 나타나며 Playground가 생성되지 않음을 확인