﻿Feature: BA RI Thresholding

  Scenario: 생성된 RI 마스크를 캡쳐
 Given 빈 TA가 열려있다. 
  
Given Basic Analyzer 탭의 Mask Viewing panel에 RI 마스크가 생성되어 있다.

When Basic Analyzer 탭 screen shot 패널 Multi view 탭의 Capture 버튼을 누르고 파일 경로를 지정한다.

Then 지정한 경로에 Basic Analyzer 탭 Mask viewing panel 화면이 캡쳐됐는지 확인

Then TA종료