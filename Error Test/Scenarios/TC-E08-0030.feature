﻿Feature: 3D Visualizer

  Scenario: 3D Visualizer 탭에서 TF canvas를 분리했을 때, 팝업 이름이 제대로 출력
Given 3D Visualizer 탭이 열려 있다.

When Preset 패널 TF canvas 우상단의 겹쳐진 사각형 아이콘을 클릭한다.

Then 분리된 TF canvas 팝업 창의 이름이 2D Transfer Function Canvas로 출력됐는지 확인