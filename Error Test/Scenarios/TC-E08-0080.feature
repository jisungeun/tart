﻿Feature: 3D Visualizer

  Scenario: Visualizer 탭 Viewing tool 패널에서 Time Stamp가 생성되어 있을 때, time stamp의 색을 바꾸면 Change color 텍스트 앞의 색이 바뀜
Given 3D Visualizer 탭이 열려 있다.

When Viewing tool 패널의 View 드롭다운 버튼에서 TimeStamp 버튼을 누르고 Show TimeStamp 체크박스를 누른다.

Then Viewing tool 패널 View 드롭다운 버튼의 TimeStamp 버튼에서 Change Color 텍스트 앞에 흰 색 네모가 있는지 확인

When Viewing tool 패널의 View 드롭다운 버튼에서 TimeStamp 버튼을 누르고 Change Color 버튼을 누른다.

And 팝업에서 검은 색을 선택하고 OK 버튼을 누른다.

Then Viewing tool 패널 View 드롭다운 버튼의 TimeStamp 버튼에서 Change color 텍스트 앞에 검은 색 네모가 있는지 확인

Then TA종료한다