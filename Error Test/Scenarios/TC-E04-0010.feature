﻿Feature: Report

  Scenario: BA Application parameter 패널에서 RI Thresholding을 프로세서로 선택하고 Batch run 버튼을 눌러 RI Thresholding 버전 Report 탭을 엶
Given BA Application Parameter에서 프로세서로 RI Thresholding이 선택되어 있다.

When BA Application parameter 패널의 Batch Run 버튼을 누른다.

And 프로세싱 바 팝업의 진행이 완료된다.

Then 프로세싱 바 팝업과 Basic Analyzer 탭이 사라지는 지 확인

And table과 graph가 출력된 Report 탭이 화면에 나타나는지 확인

And Report[T] 탭의 Select Organelle 패널에 Whole Cell 체크박스 하나밖에 없는지 확인