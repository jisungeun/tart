﻿Feature: Project Manager

  Scenario: Select Application 탭에서 hypercube에 적용된 Application을 변경 시 Application Parameter 패널이 새 App에 따라 갱신
Given 빈 TA가 열려있다.  
  
Given BA Application Parameter에서 AI Segmentation이 프로세서로 선택되어 있다.

When Select Application 탭에서 Basic Analyzer가 적용된 Hypercube에 2D Image Gallery를 적용한다.

Then Application Parameter 패널에 2D Image Gallery의 UI가 나타났는지 확인