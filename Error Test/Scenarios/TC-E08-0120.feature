﻿Feature: 3D Visualizer

  Scenario: 3D Visualizer 탭이 열려 있을 때, 탭을 닫지 않고 다른 데이터에 대해 3D Visualizer를 실행
Given 3D Visualizer 탭이 열려 있다.

When Application tab 바에서 Project Manager 탭을 누른다.

And Preview 패널에서 새로운 TCF를 선택하고 3D Visualization 버튼을 누른다.

Then 기존 3D Visualizer 내용이 사라지고 빈 3D Visualizer가 새로 열리는지 확인