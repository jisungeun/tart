﻿Feature: BA AI Segmentation

  Scenario: Batch run 돌리는 중에 Cancel 버튼을 눌러 작업 취소
Given 빈 TA가 열려있다.  
  
Given BA Application Parameter에서 프로세서로 AI Segmentation이 선택되어 있다.

When Batch Run 버튼을 누른다.

And 프로세싱 바 팝업의 Cancel 버튼을 누른다.

Then Basic Analyzer 탭과 프로세싱 바 팝업이 종료됐는지 확인

Then TA종료