﻿Feature: Report[T]

  Scenario: Report[T] 탭 그래프의 X 축 범위 조절
Given Basic Analyzer[T] 탭 Batch Run setting 탭의 Processing mode 패널에서 RI Thresholding이 프로세서로 선택되어 있다.

When Batch Run setting 탭 Time points setting 패널에서 start 값을 '1', end 값을 ‘5’, interval 값을 '1'로 설정하고 Apply time points to all data 버튼을 누른 뒤 Processing mode 패널의 Execute Whole 버튼을 누른다.

And Report[T] 탭 그래프 패널 위의 Setting X axis의 MIN 값에 2, Max 값에 3을 입력한다.

Then Report[T] 탭의 그래프에 tp 2부터 4까지만 표시됐는지 확인

Then TA종료