﻿Feature: 3D Visualizer

  Scenario: 프로젝트를 열지 않고 3DV 실행 불가
Given TA의 Select Project 탭 화면이 나타나있다.

When Toolbox의 3D Visualization 버튼을 누른다.

Then ‘Select tcf files to view’ 라는 문구의 경고 팝업이 나타났는지 확인

Then TA종료

