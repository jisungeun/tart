﻿Feature: Project Manager

  Scenario: Playground에 생성된 Cube 이름이 길 때, List 패널 오른 쪽 드롭다운 버튼에 출력되는 cube 이름 뒤에 말줄임표 생성
Given 빈 TA가 열려있다.  
  
Given 프로젝트에 “12345678901234567890” 이라는 이름의 cube가 생성되어 있다.

When List 패널 우상단의 ‘Cube name :’ 텍스트 옆에 있는 드롭다운 버튼에서 “12345678901234567890” cube를 선택한다.

Then List 패널 드롭다운 버튼의 “12345678901234567890” cube 이름 뒷부분이 깔끔하게 생략되었는지 확인