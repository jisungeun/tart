﻿Feature: BAT RI Thresholding

  Scenario: BAT RI Thresholding Mask가 생성되어 있을 때, Mask Viewing panel 오른쪽의 2D HT 화면에서 마우스 휠로 Slice를 옮겨도 마스크가 깜빡거리지 않음
Given Basic Analyzer[T] 탭의 Mask Viewing panel에 RI 마스크가 생성되어 있다.

When Basic Analyzer[T] 탭 screen shot 패널 Multi view 탭의 Capture 버튼을 누르고 파일 경로를 지정한다.

Then 지정한 경로에 Basic Analyzer[T] 탭 Mask viewing panel 화면이 캡쳐됐는지 확인

Then TA종료