﻿Feature: Project Manager

  Scenario: 같은 폴더 안에 같은 이름의 프로젝트가 생성되지 않음
Given 빈 TA가 열려있다.  
  
Given “test” 라는 이름의 프로젝트가 열려 있다.

When Select Project 탭의 Create 버튼을 누르고 이름을 “test”로, Location을 열려 있는 프로젝트와 같은 경로로 설정한 후 Create 버튼을 누른다.

Then Warning 팝업이 뜨며 프로젝트 생성이 되지 않는지 확인