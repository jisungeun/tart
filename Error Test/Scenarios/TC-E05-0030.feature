﻿Feature: BAT AI Segmentation

  Scenario: Batch run 돌리는 중에 Cancel 버튼을 눌러 작업 취소
Given Basic Analyzer[T] 탭 Batch Run setting 탭의 Processing mode 패널에서 AI Segmentation이 프로세서로 선택되어 있다.

When Basic Analyzer[T] 탭의 Batch Run setting 탭 리스트에 존재하는 모든 TCF에 Time points를 설정하고 Processing mode 패널의 Execute Whole 버튼을 누른다.

And 프로세싱 바 팝업의 Cancel 버튼을 누른다.

Then 진행이 멈추고 Basic Analyzer[T] Batch Run setting 탭 화면이 나타나는지 확인

Then TA종료
