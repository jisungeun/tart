﻿Feature: Report[T]

  Scenario: BAT Application parameter 패널에서 RI Thresholding을 프로세서로 선택하고 Batch run 버튼을 눌러 RI Thresholding 버전 Report 탭을 엶
Given Basic Analyzer[T] 탭 Batch Run setting 탭의 Processing mode 패널에서 RI Thresholding이 프로세서로 선택되어 있다.

When Batch Run setting 탭 리스트에 존재하는 모든 TCF에 Time points를 “1, 2”로 설정하고 Processing mode 패널의 Execute Whole 버튼을 누른다.

And Batch Run setting 탭 위에 프로세싱 바 팝업의 진행이 완료될 때 까지 기다린다.

Then 프로세싱 바 팝업과 Basic Analyzer[T] 탭이 사라지는 지 확인

And Report[T] 탭이 화면에 나타나는지 확인

And Report[T] 탭에 app을 적용한 hypercube의 데이터에 따른 table과 graph가 각 패널에 출력 되었는지 확인