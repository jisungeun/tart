﻿Feature: 3D Visualizer

  Scenario: 3D Visualizer 탭의 Player/Movie maker 패널의 축소
Given 3D Visualizer 탭이 열려 있다.

When Movie maker 패널의 Add 버튼을 누르고 Orbit 버튼을 누른다.

And 3D Visualizer 탭의 Movie maker 패널과 View 탭의 경계선에 마우스 포인터를 가져간 뒤 클릭한 채로 아래로 끝까지 내린다.

Then Movie maker 패널의 세로 길이가 줄어드는 동안 탭의 텍스트가 서로 겹치지 않는지 확인