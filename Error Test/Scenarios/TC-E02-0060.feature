﻿Feature: BA AI Segmentation

  Scenario: Custom medium RI 적용
  
Given 빈 TA가 열려있다.  
  
Given “Hep G2-094” TCF 파일이 들어간 프로젝트 파일이 열려있다.

When Preview 패널에서 TCF 썸네일을 우클릭 하고 Create Simple Playground 버튼을 누른다.

And Select Application 탭에서 Basic Analyzer을 선택하고 OK를 누른다.

And Application Parameter에서 AI Segmentation을 선택한뒤, Custom Medium RI 체크박스를 클릭하고 Single run을 클릭한다.

And Single Run 팝업창에서 Hep G2-094 썸네일을 선택하고 Single RUN 버튼을 클릭한다.

And Basic Analyzer 탭에서 Processing mode 패널 AI Inference 탭의 Execute 버튼을 클릭한다.

And Basic Measurement 탭의 Execute 버튼을 클릭한다.

Then Measurement 팝업창 Whole Cell에서 cell1의 Dry mass(pg)값을 확인.

When Measurement 팝업창을 닫는다.

And Processing mode 패널의 Basic Measurement탭에서 Medium RI 입력 칸에 “1.25”를 입력한다.

And Basic Measurement 탭에서 Execute 버튼을 클릭한다.

Then Measurement 팝업창 Whole Cell에서 cell1의 Dry mass(pg) 값이 변했는지 확인

Then TA종료