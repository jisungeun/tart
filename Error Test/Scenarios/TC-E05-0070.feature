﻿Feature: BAT AI Segmentation

  Scenario: BAT Single run 팝업의 Cancel 버튼으로 팝업 닫기
Given Playground canvas에서 Basic Analyzer[T] 아이콘이 선택되어 있다.

When BAT Application Parameter에서 Single RUN 버튼을 누르고 Single run 팝업의 Cancel 버튼을 누른다.

Then Single run 팝업이 닫히는지 확인