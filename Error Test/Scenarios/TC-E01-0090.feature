﻿Feature: Project Manager

  Scenario: List 패널에서 TCF를 확인/정렬
Given 빈 TA가 열려있다.  
  
Given TCF의 썸네일이 보이는 Preview 패널이 열려 있다.

When List 패널을 누른다.

Then List 패널의 왼쪽 table에 불러진 TCF의 Data/Name/HT/FL/BF column의 정보가 누락 없이 나타나 있는지 확인

And List 패널의 왼쪽 table에 불러진 TCF의 Cube/Hypercube column은 비어있는지 확인

When List 패널에서 원하는 TCF 파일을 누른다.

Then List 패널에서 1개의 TCF 파일이 선택됐는지 확인

When 컨트롤 키를 누른 상태로 원하는 TCF 파일을 두 개 선택한다.

Then 두 개의 TCF 파일이 선택되었는지 확인

When List의 가장 위에 있는 TCF 파일을 누른다.

And 키보드의 Shift를 누른 상태로 List의 가장 아래에 있는 TCF파일을 누른다.

Then 클릭한 두 개의 파일 사이의 모든 TCF 파일이 선택되었는지 확인

When List 패널 HT column의 table header를 한 번 누른다.

Then HT 값에 대해 오름차순으로 List가 정렬되었는지 확인

When List 패널 HT column의 table header를 한 번 더 누른다.

Then HT 값에 대해 내림차순으로 List가 정렬되었는지 확인

Then TA종료