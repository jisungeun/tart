﻿Feature: 3D Visualizer
  Scenario: Axis grid 색 변경 및 이미지 export 시에 font size 정상 적용
  
  Given 3D Visualizer 탭이 열려 있다.
  When Viewing tool 패널 View 드롭다운 버튼의 Axis Grid에 마우스롤 갖다대고 Color(x)를 클릭하여 빨간색을 선택한다.
  Then 3D Visualizer 탭 View 패널의 3D 화면에 X축 숫자가 빨간색인 Axis grid가 생성됐는지 확인
  When Viewing tool 패널의 View 드롭다운에서 Axis grid에 마우스를 갖다대고 font size에 30을 입력한다.
  And Screen shot 패널 3D view 탭에서 Upsampling 수치를 3에 맞추고 Capture 버튼을 누른다.
  And 파일 탐색기에 경로를 지정하고 저장 버튼을 누른다.
  Then 지정한 경로에 생긴 스크린 샷 이미지 파일이 View 패널 3D 화면과 일치하는지 확인
  
Then TA종료