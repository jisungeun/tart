﻿Feature: 3D Visualizer

  Scenario: 3D Visualizer 탭에서 TF box를 우클릭하여 Rendering을 활성화/비활성화
Given Visualizer 탭이 열려 있다.

And 3D Visualizer 탭 View 패널에 rendering이 생성되어 있다.

When Preset 패널의 TF canvas에서 rendering의 TF box에 마우스 포인터를 가져간 뒤 우클릭한다.

Then 3D Visualizer 탭 View 패널에서 우클릭한 TF box의 rendering이 사라졌는지 확인

And Preset 패널 TF canvas에서 우클릭 한 TF box의 테두리가 점선으로 바뀌었는지 확인

When Preset 패널의 TF canvas에서 비활성화 된 TF box에 마우스 포인터를 가져간 뒤 우클릭한다.

Then 3D Visualizer 탭 View 패널에서 우클릭한 TF box의 rendering이 나타났는지 확인

And Preset 패널 TF canvas에서 우클릭 한 TF box의 테두리가 점선에서 실선으로 바뀌었는지 확인