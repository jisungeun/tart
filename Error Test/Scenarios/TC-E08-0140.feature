﻿Feature: 3D Visualizer

  Scenario: 3D Visualizer에서 형광 Opacity 조절 시 매 값마다 균일하게 조절
Given 3D Visualization 탭이 HT only 데이터로 열려 있다.

When 3D Visualization 탭을 종료한다.

And Preview 패널에서 HT+FL 데이터를 선택하고 3D visualization 버튼을 누른다.

And 3D Visualizer 탭이 열리면 Modality 패널에서 FL 체크박스를 누른다.

Then 3D Visualizer 탭 View 패널의 2D HT 화면들의 HT 이미지와 FL rendering이 정상적으로 출력 되는지 확인

When 3D Visualizer 탭 View 패널의 2D XY HT 화면에 마우스 포인터를 대고 마우스 휠을 아래로 5칸 내린다.

Then 3D Visualizer 탭 View 패널의 2D XY HT 이미지 slice가 변했는지 확인

Then TA종료