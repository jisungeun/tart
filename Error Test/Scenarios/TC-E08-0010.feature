﻿Feature: 3D Visualizer

  Scenario: 3D Visualizer에서 2D HT image Reset View
Given 3D Visualizer 탭이 열려 있다.

When Screen shot 패널의 겹쳐진 사각형 버튼을 눌러 팝업으로 뽑아낸다.

And 3D Visualizer 탭의 View 패널을 클릭하고 키보드의 Alt+F4 버튼을 누른다.

Then TA가 종료되면서 Screen shot 팝업도 함께 종료되는지 확인

