﻿Feature: Project Manager

  Scenario: Select Application 탭의 App을 적용할 hypercube를 선택하는 드롭다운 버튼에 하나의 hypercube 당 하나의 버튼만 생성
Given 빈 TA가 열려있다.  
  
Given Select Application 탭이 열려 있다.

And Playground에 Hypercube가 두 개 생성되어 있다.

When Select Application 탭에서 App을 적용할 hypercube를 선택하는 드롭다운 버튼을 클릭한다.

Then Hypercube 한 개당 Select Application 탭의 드롭다운에 하나의 버튼만 생성되었는지 확인