﻿"Feature: BAT AI Segmentation

  Scenario: BAT AI Segmentation Mask가 생성되어 있을 때, Mask Viewing panel 오른쪽의 2D HT 화면에서 마우스 휠로 Slice를 옮겨도 마스크가 깜빡거리지 않음
Given Basic Analyzer[T] Single Run 탭의 Mask Viewing panel에 3D 마스크가 생성되어 있다.

When Basic Analyzer[T] 탭 Mask Viewing panel의 2D HT 화면 위에 마우스 포인터를 올리고 마우스 휠을 마스크가 사라질 때까지 아래로 내린다.

And Basic Analyzer[T] 탭 Mask Viewing panel의 2D HT 화면 위에 마우스 포인터를 올리고 마우스 휠을 마스크가 사라질 때까지 위로 올린다.

Then Basic Analyzer[T] 탭 Mask Viewing panel의 2D HT 화면이 움직이는 동안 마스크가 사라졌다 생겼다 하지 않았는지 확인