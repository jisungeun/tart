﻿Feature: 3D Visualizer

  Scenario: 2D 선택 후 FL 기능 작동 및 HT Modality 선택 해제

Given HeLa-Rab5a-Rab7a-005 데이터로 3D Visualizer가 열려 있다.

When Display 패널에서 2D 버튼을 클릭하고 Modality 패널에서 FL 체크박스를 체크한다.

And Preset 패널의 FL 창에서 Green 체크박스를 선택 해제한다.

Then View 패널에 HT이미지와 함께 빨간색 형광만 표시 돼 있는지 확인

When Modality 패널에서 HT 체크박스를 체크하여 선택 해제한다.

Then View 패널에 HT이미지가 없고 형광색만 표시 돼 있는지 확인

Then TA종료