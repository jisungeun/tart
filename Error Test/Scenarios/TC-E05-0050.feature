﻿Feature: BAT AI Segmentation

  Scenario: BAT Batch Run Setting 탭에서 time point의 parameter에 0 입력 불가능
Given Basic Analyzer[T] 탭 Batch Run setting 탭의 Processing mode 패널에서 AI Segmentation이 프로세서로 선택되어 있다.

When Batch Run Setting 탭 Time points setting 패널에서 custom 체크박스를 체크하고 Apply time points to all data를 클릭한다.

Then Batch Run Setting 탭 왼쪽의 리스트에 있는 Counts 칸에 “1”이 입력 되는지 확인

When Time points setting 패널의 Selected time points가 빈칸이 되도록 모든 숫자를 지우고 Apply time points to all data를 클릭한다.

Then “Time step list is empty” Error 팝업 창이 뜨는지 확인

When Batch Run Setting 탭 왼쪽의 리스트에서 가장 위에 있는 파일을 하나 선택한다.

#And Time points setting 패널의 start 칸에 키보드로 '0'을 입력하고 키보드의 엔터 키를 누른다.

#Then Batch Run Setting 탭 Time points setting 패널의 start 칸에 '1'이 입력됐는지 확인

Then TA종료