﻿Feature: Project Manager

  Scenario: Project 생성 시 이름에 특수문자 입력 불가
Given 빈 TA가 열려있다.

Given TA의 Select Project 탭 화면이 나타나있다.

When Select Project의 Create 버튼을 누른다.

And New Project 팝업의 Project name 칸을 클릭하고 "? , < > / ; : ' “ \ | [ { ] } ! @ # $ % ^ & * ( ) + ="를 키보드로 누른다.

Then New Project 팝업의 Project name 칸에 아무 것도 입력되지 않았는지 확인