﻿Feature: BA RI Thresholding

  Scenario: Mask Viewing panel 상단의 Mask 툴박스에서 Opacity를 수치 입력으로 설정하여 마스크의 불투명도를 조절
Given 빈 TA가 열려있다.  
  
Given Basic Analyzer 탭의 Mask Viewing panel에 RI 마스크가 생성되어 있다.

When Basic Analyzer 탭의 Mask Viewing panel에 마스크의 Opacity 칸에 0.5를 입력한다.

Then Basic Analyzer 탭 Mask Viewing panel의 마스크가 투명해졌는지 확인

When Basic Analyzer 탭 Mask Viewing panel 상단 Mask 툴박스의 Opacity 칸 수치 입력창 옆의 위 화살표 버튼을 10번 누른다.

Then 화살표에 따라 Basic Analyzer 탭 Mask Viewing panel의 Opacity 수치 값이 0.60으로 올라감을 확인

And Basic Analyzer 탭 Mask Viewing panel의 마스크들이 점점 진해짐을 확인

When Basic Analyzer 탭 Mask Viewing panel 상단 Mask 툴박스의 Opacity 칸 수치 입력창 옆의 아래 화살표 버튼을 10번 누른다.

Then 화살표에 따라 Basic Analyzer 탭 Mask Viewing panel의 Opacity 수치 값이 0.50으로 내려감을 확인

And Basic Analyzer 탭 Mask Viewing panel의 마스크들이 점점 투명해짐을 확인

Then TA종료