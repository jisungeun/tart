﻿Feature: 3D Visualizer

  Scenario: 3D Visualizer 탭 Screen shot 패널에서 All plane 버튼을 누르면 파일 탐색기에 이전 이미지 저장 경로가 나타남
Given 3D Visualizer 탭이 열려 있다.

When Screen shot 패널의 3D View 탭에서 All axis 버튼을 누르고 파일 탐색기에서 임의의 폴더를 지정한 뒤 폴더 선택 버튼을 누른다.

And Screen shot 패널의 3D View 탭에서 All axis 버튼을 누르고 파일 탐색기에서 다른 임의의 폴더를 지정한 뒤 폴더 선택 버튼을 누른다.

Then 파일 탐색기 팝업에 두 번째로 All axis screen shot을 저장했던 폴더가 나타났는지 확인

Then TA종료