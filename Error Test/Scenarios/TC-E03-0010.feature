﻿Feature: BA RI Thresholding

  Scenario: BA RI Thresholding Application parameter 패널에서 Basic Measurement 탭의 table 너비를 늘려 각 parameter를 확인
  
Given 빈 TA가 열려있다.  
  
Given BA Application Parameter에서 RI Thresholding이 프로세서로 선택되어 있다.

When BA Application Parameter의 Basic Measurement에서 table header의 오른쪽 끝에서 마우스를 클릭한 채로 오른쪽으로 패널 끝까지 드래그 한다.

Then BA Application Parameter의 Basic Measurement에서 table의 너비가 늘어났는지 확인

And BA Application Parameter의 Basic Measurement에서 table의 각 값들이 잘리거나 줄이 밀리지 않고 한 줄로 출력 됐는지 확인