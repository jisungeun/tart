﻿Feature: Report[T]

  Scenario: Report[T] 탭 Data table에서 데이터를 선택
Given RI Thresholding 프로세서로 batch run 한 Report[T] 탭이 열려 있다.

When Report[T] 탭 Data Table 패널의 첫 번째 cube에서 첫 번째 데이터를 클릭한다.

Then Report[T] 탭 그래프 패널 위의 Current File 칸에 클릭한 데이터의 이름이 나타났는지 확인

Then TA종료