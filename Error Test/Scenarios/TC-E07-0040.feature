﻿Feature: Report[T]

  Scenario: graph 별모양 포인터 캡처 및 선택 해제
  
Given TomoAnalysis에 Report[T] 탭이 열려 있다.

When Report[T] 탭 Graph 패널에서 그래프 위의 점을 하나 클릭하고 Export graph 버튼을 클릭한다.

And 파일 탐색기에서 파일 경로와 파일 이름을 설정하고 저장 버튼을 누른다.

Then 저장된 png 파일에 별 모양 포인터가 깨지지 않고 그려져 있는지 확인

#When Report[T] 탭 Graph 패널에서 그래프 위의 빈 공간을 클릭한다.

#Then Report[T] 탭의 그래프 점 위에 그려졌던 별모양 포인터가 사라졌는지 확인
