﻿Feature: Mask Editor

  Scenario: Instance mask에서 2 이상의 label의 마스크를 수정한 뒤 Organelle 마스크를 수정

Given Mask Editor 탭에 2개의 Cell이 있는 TCF의 Cell instance Single Run 마스크가 ID ‘test’로 생성되어 있다.
When Mask selector 패널에서 Cell instance 버튼을 눌러 instance 마스크를 불러온다.
And Label tools 패널의 Label Picker 버튼을 누르고 초록색 마스크를 누른다.
And Drawing tool 패널의 Paint 버튼을 누르고 사이즈를 100으로 키운 뒤 XY Slice의 아무 곳이나 클릭하여 Label 2 마스크를 수정한다.
And Mask selector 패널에서 Whole cell 버튼을 눌러 organelle 마스크를 불러온다.
And Whole cell 마스크가 생성되어 있는 XY Slice의 아무 곳이나 클릭한다.
Then 클릭한 부분에 추가된Label 1 마스크가 빨간색인지 확인