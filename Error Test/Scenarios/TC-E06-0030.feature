﻿Feature: BAT RI Thresholding

  Scenario: BAT 탭에서 프로세서가 RI Thresholding일 때, 마스크가 생성되어 있지 않으면 Labeling 탭에서 Execute 버튼을 눌러도 마스크 수정이 안됨
Given Basic Analyzer[T] 탭에서 Use Labeling 체크박스가 체크되어 있다.

And Basic Analyzer[T] 탭의 Mask Viewing panel에 마스크가 생성되어 있지 않다.

When Basic Analyzer[T] 탭 Labeling 탭의 Select Option 드롭다운 버튼에서 Multi Label을 선택하고 Labeling 탭의 Execute 버튼을 누른다.

Then Basic Analyzer[T] 탭에서 경고창이 뜨며 아무런 변화도 없는지 확인

When Basic Analyzer[T] 탭 Labeling 탭의 Select Option 드롭다운 버튼에서 Largest Label을 선택하고 Labeling 탭의 Execute 버튼을 누른다.

Then Basic Analyzer[T] 탭에서 경고창이 나타나며 아무런 변화도 없는지 확인

And TA종료