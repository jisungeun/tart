﻿Feature: Report

  Scenario: Report 탭 Data table에서 데이터를 선택
 Given 빈 TA가 열려있다.   
  
Given RI Thresholding 프로세서로 batch run 한 Report 탭이 열려 있다.

When Report 탭 Data Table 패널의 첫 번째 cube에서 첫 번째 데이터를 클릭한다.

Then Graph of individual cell 패널 위의 Current File 칸에 클릭한 데이터의 이름이 나타났는지 확인

And Graph of individual cell 패널에서 선택한 데이터의 막대가 하이라이트 되었는지 확인

Then TA종료
