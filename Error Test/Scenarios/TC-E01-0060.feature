﻿Feature: Project Manager

  Scenario: Playground canvas 패널에서 Application 아이콘이 선택되어있지 않으면 Application Parameter가 비활성화
Given 빈 TA가 열려있다.  
  
Given Playground canvas 패널에서 Hypercube에 적용되어 있는 Basic Analyzer 아이콘이 선택되어 있다.

When Playground canvas에서 Hypercube 아이콘을 클릭한다.

Then Application Parameter 패널이 비활성화 되었는지 확인

When Playground canvas에서 Cube 아이콘을 클릭한다.

Then Application Parameter 패널이 비활성화 되었는지 확인

When Playground canvas의 빈 공간을 클릭한다.

Then Application Parameter 패널이 비활성화 되었는지 확인한다