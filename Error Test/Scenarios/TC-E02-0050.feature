﻿Feature: BA AI Segmentation

  Scenario: Single run 팝업의 Cancel 버튼으로 팝업 닫기
Given 빈 TA가 열려있다.
  
Given Operation sequence 패널의 Link Data to Cube 탭 화면이 열려 있다.
  
And cube에 TCF가 1개 연결되어 있다.

When Operation sequence 패널의 Select Application 탭에서 Basic Analyzer를 선택하고 OK 버튼을 누른다.

And Batch Run 버튼을 누른다.

And 프로세싱 바 팝업이 생기면 진행이 완료되고 팝업이 사라질 때 까지 기다린다.

Then Report 탭이 뜨는지 확인

When Project Manager 탭의 Preview 패널에서 cube에 연결되어있지 않은 TCF를 1개 클릭하고 Tool box의 Link to Cube를 클릭한다.

And Dialog 팝업창에서 Link 버튼을 클릭한다.

And Batch Run 버튼을 누른다.

Then File changes 창의 File Info탭에 File Name이 같은 2개의 리스트가 나타나는지 확인

Then TA종료