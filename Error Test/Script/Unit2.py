﻿import shutil
import os.path
from distutils.dir_util import copy_tree
import time
import glob
import re

@given("TCF의 썸네일이 보이는 Preview 패널이 열려 있다.")
def step_impl():

  dir4 =r'E:\Regression_Test\Prefix_File\PEP UI features\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\PEP UI features'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\PEP UI features'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation.Click(91, 26)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\PEP UI features\\PEP UI features.tcpro")

  
@when("List 패널을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.ClickTab("List")

@then("List 패널의 왼쪽 table에 불러진 TCF의 Data\\/Name\\/HT\\/FL\\/BF column의 정보가 누락 없이 나타나 있는지 확인")
def step_impl():
  Tables.contentsTableWidget5.Check()

@then("List 패널의 왼쪽 table에 불러진 TCF의 Cube\\/Hypercube column은 비어있는지 확인")
def step_impl():
  Tables.contentsTableWidget6.Check()

@when("List 패널에서 원하는 TCF 파일을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.detailsTab.DetailPanel.widget.contentsTableWidget.ClickCell("1", "Name")

@then("List 패널에서 1개의 TCF 파일이 선택됐는지 확인")
def step_impl():
  Regions.qt_scrollarea_viewport4.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.detailsTab.DetailPanel.widget.contentsTableWidget.QtObject("qt_scrollarea_viewport"))

@when("컨트롤 키를 누른 상태로 원하는 TCF 파일을 두 개 선택한다.")
def step_impl():
  widget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.detailsTab.DetailPanel.widget.contentsTableWidget.qt_scrollarea_viewport
  widget.Click(314, 44, skCtrl)
  widget.Click(313, 71, skCtrl)

@then("두 개의 TCF 파일이 선택되었는지 확인")
def step_impl():
  Regions.qt_scrollarea_viewport5.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.detailsTab.DetailPanel.widget.contentsTableWidget.QtObject("qt_scrollarea_viewport"))

@when("List의 가장 위에 있는 TCF 파일을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.detailsTab.DetailPanel.widget.contentsTableWidget.ClickCell("1", "HT")

@when("키보드의 Shift를 누른 상태로 List의 가장 아래에 있는 TCF파일을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.detailsTab.DetailPanel.widget.contentsTableWidget.qt_scrollarea_viewport.Click(263, 162, skShift)

@then("클릭한 두 개의 파일 사이의 모든 TCF 파일이 선택되었는지 확인")
def step_impl():
  Regions.qt_scrollarea_viewport6.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.detailsTab.DetailPanel.widget.contentsTableWidget.QtObject("qt_scrollarea_viewport"))

@when("List 패널 HT column의 table header를 한 번 누른다.")
def step_impl():
  tableWidget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.detailsTab.DetailPanel.widget.contentsTableWidget
  tableWidget.ClickColumnHeader("HT")

@then("HT 값에 대해 오름차순으로 List가 정렬되었는지 확인")
def step_impl():
  Tables.contentsTableWidget3.Check()

@when("List 패널 HT column의 table header를 한 번 더 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.detailsTab.DetailPanel.widget.contentsTableWidget.ClickColumnHeader("HT")

@then("HT 값에 대해 내림차순으로 List가 정렬되었는지 확인")
def step_impl():
  Tables.contentsTableWidget4.Check()

@when("Basic Analyzer 탭의 Mask Correction 버튼을 누른다.")
def step_impl():
  Delay(25000)
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.correctionBtn.ClickButton()

@when("ID manager 패널의 Copy 버튼을 누르고 팝업에 ‘e02’를 입력한 뒤 OK 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  scrollArea = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.leftLayout.IDManagerPanel.panel_base.panel_contents.bt_round_gray500.ClickButton()
  lineEdit = Aliases.TomoAnalysis.NewIdDialog.input_high
  lineEdit.Click(166, 20)
  lineEdit.SetText("e02")
  Aliases.TomoAnalysis.NewIdDialog.bt_square_primary.ClickButton()

@when("Mask selector 패널에서 Nucleus 버튼을 누른 뒤 Clean tool의 Flush slice 버튼을 누르고 XY slice 화면의 핵 마스크를 클릭한다.")
def step_impl():
  tomoAnalysis_mainwindow = Aliases.TomoAnalysis.mainwindow
  tomoAnalysis_InterSeg_AppUI_MainWindow = tomoAnalysis_mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  tomoAnalysis_InterSeg_AppUI_MainWindow.MaskSelectionPanel.panel_base.panel_contents.nucleBtn.ClickButton()
  tomoAnalysis = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis
  tomoAnalysis.Click(556, 249)
  for i in range(1, 8) :
    tomoAnalysis.MouseWheel(1)
  tomoAnalysis_InterSeg_AppUI_MainWindow.CleanerPanel.panel_base.panel_contents.CleanSelBtn.ClickButton()
  tomoAnalysis.Click(584, 315)

@when("Mask selector 패널에서 Nucleus 버튼을 다시 한 번 누른 뒤 Apply to Result 버튼을 누른다.")
def step_impl():
  tomoAnalysis_InterSeg_AppUI_MainWindow = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  tomoAnalysis_InterSeg_AppUI_MainWindow.MaskSelectionPanel.panel_base.panel_contents.nucleBtn.ClickButton()
  scrollArea = tomoAnalysis_InterSeg_AppUI_MainWindow.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.leftLayout.GeneralPanel.panel_base.panel_contents.SaveLinkBtn.ClickButton()

@when("Basic Analyzer 탭의 toolbox에서 Nucleus 버튼을 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.controlSocket.RadioButton.ClickButton()

@then("Basic Analyzer 탭 Mask Viewing panel에 아무런 마스크도 없는지 확인")
def step_impl():
  Regions.viewerPanel.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel.Splitter.Widget2)

@when("Basic Analyzer 탭의 Basic Measurement 탭에서 Execute 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterTable.buttonExecute.ClickButton()

@then("Measurement 팝업이 생겼는지 확인")
def step_impl():

  aqObject.CheckProperty(Aliases.TomoAnalysis.BasicAnalysisResultPanel, "QtText", cmpEqual, "Measurement")

@when("Basic Analyzer 탭의 Mask Viewing panel에 마스크의 Opacity 칸에 0.5를 입력한다.")
def step_impl():
  doubleSpinBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.maskOpacity
  doubleSpinBox.Keys("0.")
  doubleSpinBox.wValue = 0
  doubleSpinBox.Keys("5")
  doubleSpinBox.wValue = 0.5
  doubleSpinBox.Keys("[Enter]")
  doubleSpinBox.wValue = 0.5

@then("Basic Analyzer 탭 Mask Viewing panel의 마스크가 투명해졌는지 확인")
def step_impl():
  Regions.viewerPanel1.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel,False,False,1000)

@when("Basic Analyzer 탭 screen shot 패널 Multi view 탭의 Capture 버튼을 누르고 파일 경로를 지정한다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  TC_AdaptiveTabWidget = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.screenshotContainer.ScreenShotWidgetUI.panel_contents.tabWidget
  TC_AdaptiveTabWidget.TC_TabBar.Click(282, 33)
  TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.viewMultiTab.bt_round_gray700.ClickButton()
  tomoAnalysis = Aliases.TomoAnalysis
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  Aliases.TomoAnalysis.dlgSaveScreenShot.DUIViewWndClassName.Item.FloatNotifySink.ComboBox.Edit.SetText("E:\\Regression_Test\\20201229.105411.698.HeLa-Rab5a-Rab7a-003_multi-view_screenshot.png")
  Aliases.TomoAnalysis.dlgSaveScreenShot.btn_S.ClickButton()




@then("지정한 경로에 Basic Analyzer 탭 Mask viewing panel 화면이 캡쳐됐는지 확인")
def step_impl():
  explorer = Aliases.explorer
  explorer.wndShell_TrayWnd.ReBarWindow32.MSTaskSwWClass.MSTaskListWClass.Click(33, 13)
  HWNDView = explorer.wndCabinetWClass.ShellTabWindowClass.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  HWNDView.CtrlNotifySink2.ShellView.Item.Regression_Test.Item.DblClick(89, 16)
  aqObject.CheckProperty(Aliases.explorer.wndRegression_Test.Regression_Test.DUIViewWndClassName.Item.CtrlNotifySink.ShellView.Item.HeLa_Rab5a_Rab7a_003_multi_view_screenshot_png.Item, "Value", cmpEqual, "20201229.105411.698.HeLa-Rab5a-Rab7a-003_multi-view_screenshot.png")
  Aliases.explorer.wndRegression_Test.Close()

@given("Basic Analyzer Single Run 탭에 Hep G2{arg} 데이터의 RI 마스크가 열려 있다.")
def step_impl(param1):

  dir4 =r'E:\Regression_Test\Prefix_File\BA_borderkill\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\BA_borderkill'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BA_borderkill'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation.Click(91, 26)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  HWNDView = dlgOpenProject.DUIViewWndClassName.Item
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  UIItemsView = HWNDView.CtrlNotifySink2.ShellView.Item
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BA_borderkill\\BA_borderkill.tcpro")
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis_ProjectManager_AppUI_MainWindow = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  frame = tomoAnalysis_ProjectManager_AppUI_MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base
  frame.panel_contents.processorParamFrame.procCombo.ClickItem("RI Thresholding")
  frame.bt_square_gray.Click(91, 14)
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel = tomoAnalysis.widget_popup
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget.panel_contents_image.widget.panel_contents_image.Widget.Click(58, 66)
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.bt_square_primary.Click(19, 16)
  scrollArea = tomoAnalysis_ProjectManager_AppUI_MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.buttonExecute.ClickButton()


  
@when("Basic Analyzer 탭 Labeling 탭에서 Use Labeling을 체크하고 Option을 Multi Labels로 선택한 뒤 Labeling 탭의 Execute 버튼을 클릭한다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  TC_ParameterControl = scrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl
  TC_ParameterControl.TC_StrCheckBox.Click(15, 9)
  scrollBar.wPosition = 0
  TC_ParameterControl.buttonExecute.ClickButton()

@when("Basic Analyzer 탭 Border Kill 탭에서 Use Border Kill 체크박스를 체크하고 Border Kill 탭의 Execute를 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 325
  TC_ParamControl = scrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParamControl
  TC_ParamControl.TC_StrCheckBox.Click(3, 9)
  scrollBar.wPosition = 325
  TC_ParamControl.buttonExecute.ClickButton()

@when("Basic Analyzer 탭을 종료한 뒤 다시 Application parameter의 Single RUN 버튼을 누른 뒤 Hep G2{arg} 데이터를 선택한다.")
def step_impl(param1):
  tomoAnalysis = Aliases.TomoAnalysis
  TC_AdaptiveTabWidget = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget
  TC_AdaptiveTabWidget.TC_TabBar.CloseButton.Click(11, 11)
  TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_gray.Click(110, 18)
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel = tomoAnalysis.widget_popup
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget.panel_contents_image.widget.panel_contents_image.Widget.Click(104, 46)
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.bt_square_primary.Click(51, 17)

@then("빈 Basic Analyzer 탭이 열렸는지 확인")
def step_impl():
  Regions.viewerPanel2.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel)

@given("RI Thresholding 프로세서로 batch run 한 Report 탭이 열려 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir4 =r'E:\Regression_Test\Prefix_File\E03\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\E03'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\E03'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation.Click(91, 26)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\E03\\E03.tcpro")
  tomoAnalysis = Aliases.TomoAnalysis
  splitter = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter
  frame = splitter.operationPanel.panel_base
  widget = frame.widget_panel
  widget2 = widget.AddPGPanel.widget_panel2
  lineEdit = widget2.input_high
  lineEdit.Click(190, 24)
  lineEdit.SetText("PG")
  widget2.bt_round_operation.ClickButton()
  tomoAnalysis_ProjectManager_Plugins_HyperCubePanel = widget.HyperCubePanel
  lineEdit = tomoAnalysis_ProjectManager_Plugins_HyperCubePanel.input_high
  lineEdit.Click(86, 19)
  lineEdit.SetText("HC")
  tomoAnalysis_ProjectManager_Plugins_HyperCubePanel.bt_round_operation.ClickButton()
  widget.CubePanel.bt_round_operation.ClickButton()
  frame.frame_tab.bt_round_tab5.ClickButton()
  frame = splitter.previewPanel.panel_base
  frame2 = frame.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame
  frame2.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(164, 61)
  frame2.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget3.TC_TCF2DWidget.panel_contents_image.Widget.Click(102, 75)
  frame2.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget2.TC_TCF2DWidget.panel_contents_image.Widget.Click(151, 97)
  frame2.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget4.TC_TCF2DWidget.panel_contents_image.Widget.Click(111, 97)
  frame2.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget5.TC_TCF2DWidget.panel_contents_image.Widget.Click(67, 97)
  frame2.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget6.TC_TCF2DWidget.panel_contents_image.Widget.Click(54, 80)
  frame.bt_round_tool.Click(76, 13)
  tomoAnalysis.LinkDialog.bt_square_primary.ClickButton()
  widget.SelectAppPanel.bt_round_operation.ClickButton()
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.procCombo.ClickItem("RI Thresholding")
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_primary.Click(71, 29)
  tomoAnalysis = Aliases.TomoAnalysis
  while tomoAnalysis.ProgressDialog.Exists :
   Delay(1000, "Waiting for window to close...");

@when("Report 탭 Data Table 패널의 첫 번째 cube에서 첫 번째 데이터를 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.dataTable.tabWidget.qt_tabwidget_stackedwidget.DataTab.TableItemWidget.tableView.qt_scrollarea_viewport.Click(181, 23)

@then("Graph of individual cell 패널 위의 Current File 칸에 클릭한 데이터의 이름이 나타났는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.widget.fileName, "wText", cmpEqual, "20201229.105411.698.HeLa-Rab5a-Rab7a-003")

@then("Graph of individual cell 패널에서 선택한 데이터의 막대가 하이라이트 되었는지 확인")
def step_impl():
  Regions.graphReportPanel.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel)

@then("작업 중이던 time point 까지 정량 분석 결과 Measurement 탭이 생성됐는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.BaTimeResultPanel, "Enabled", cmpEqual, True)


@given("Basic Analyzer[T] 탭 Batch Run setting 탭의 Processing mode 패널에서 AI Segmentation이 프로세서로 선택되어 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir4 =r'E:\Regression_Test\Prefix_File\BAT\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\BAT'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BAT'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis_ProjectManager_AppUI_MainWindow = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  splitter = tomoAnalysis_ProjectManager_AppUI_MainWindow.mainSplitter.operationFrame.operationSplitter
  splitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation.Click(89, 25)
  tomoAnalysis.Menu.OpenFile("E:\\Regression_Test\\Prefix_File\\BAT\\BAT.tcpro")
  splitter = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter
  splitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(139, 106)
  splitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_primary.Click(59, 13)


@when("Basic Analyzer[T] 탭의 Batch Run setting 탭 리스트에 존재하는 모든 TCF에 Time points를 설정하고 Processing mode 패널의 Execute Whole 버튼을 누른다.")
def step_impl():
  tomoAnalysis_BasicAnalysisTime_Plugins_ParameterPanel = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.batchRunTab.batchParameterPanel
  groupBox = tomoAnalysis_BasicAnalysisTime_Plugins_ParameterPanel.timeGroup
  spinBox = groupBox.endTime
  spinBox.qt_spinbox_lineedit.Drag(32, 14, -64, -7)
  spinBox.Keys("2")
  spinBox.wValue = 2
  spinBox.Keys("[Enter]")
  spinBox.wValue = 2
  groupBox.applyAllBtn.ClickButton()
  tomoAnalysis_BasicAnalysisTime_Plugins_ParameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.buttonExecuteWhole.ClickButton()


@then("진행이 멈추고 Basic Analyzer[T] Batch Run setting 탭 화면이 나타나는지 확인")
def step_impl():
  while Aliases.TomoAnalysis.MessageBox12.Exists :
   Delay(1000, "Waiting for window to close...");
  Regions.ET_10.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget)

@when("Batch Run Setting 탭 Time points setting 패널의 start 칸에 키보드로 {arg}을 입력하고 키보드의 엔터 키를 누른다.")
def step_impl(param1):
  spinBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.batchRunTab.batchParameterPanel.timeGroup.startTime
  spinBox.qt_spinbox_lineedit.Drag(34, 15, -248, -8)
  spinBox.Keys("0[Enter]")
  spinBox.wValue = 1

@then("Batch Run Setting 탭 Time points setting 패널의 start 칸에 {arg}이 입력됐는지 확인")
def step_impl(param1):
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.batchRunTab.batchParameterPanel.timeGroup.startTime.qt_spinbox_lineedit, "wText", cmpEqual, "1")

@given("Basic Analyzer[T] 탭의 Mask Viewing panel에 RI 마스크가 생성되어 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir4 =r'E:\Regression_Test\Prefix_File\BA_borderkill\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  img = r'E:\Regression_Test\*.png'
  for f in glob.glob(img):
    os.remove(f)
  dir1 = r'E:\Regression_Test\Prefix_File\BA_borderkill'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BA_borderkill'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation.Click(91, 26)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  HWNDView = dlgOpenProject.DUIViewWndClassName.Item
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  UIItemsView = HWNDView.CtrlNotifySink2.ShellView.Item
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BA_borderkill\\BA_borderkill.tcpro")
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis_ProjectManager_AppUI_MainWindow = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  frame = tomoAnalysis_ProjectManager_AppUI_MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base
  frame.panel_contents.processorParamFrame.procCombo.ClickItem("RI Thresholding")
  frame.bt_square_gray.Click(91, 14)
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel = tomoAnalysis.widget_popup
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget.panel_contents_image.widget.panel_contents_image.Widget.Click(58, 66)
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.bt_square_primary.Click(19, 16)
  scrollArea = tomoAnalysis_ProjectManager_AppUI_MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.buttonExecute.ClickButton()

@when("Basic Analyzer[T] 탭 screen shot 패널 Multi view 탭의 Capture 버튼을 누르고 파일 경로를 지정한다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  TC_AdaptiveTabWidget = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.screenshotContainer.ScreenShotWidgetUI.panel_contents.tabWidget
  TC_AdaptiveTabWidget.TC_TabBar.Click(299, 17)
  TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.viewMultiTab.bt_round_gray700.ClickButton()
  Aliases.TomoAnalysis.dlgSaveScreenShot3.DUIViewWndClassName.Item.FloatNotifySink.ComboBox.Edit.SetText("E:\\Regression_Test\\20200609.153947.886.Hep G2-094_multi-view_screenshot.png")
  Aliases.TomoAnalysis.dlgSaveScreenShot3.btn_S.Click(-1)



@then("지정한 경로에 Basic Analyzer[T] 탭 Mask viewing panel 화면이 캡쳐됐는지 확인")
def step_impl():
  explorer = Aliases.explorer
  explorer.wndShell_TrayWnd.ReBarWindow32.MSTaskSwWClass.MSTaskListWClass.Click(19, 20)
  HWNDView = explorer.wndCabinetWClass.ShellTabWindowClass.DUIViewWndClassName.Item
  tv_ = HWNDView.CtrlNotifySink.NamespaceTreeControl.tv_
  tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  UIItemsView = HWNDView.CtrlNotifySink2.ShellView.Item
  UIItemsView.Regression_Test.Item.DblClick(90, 13)
  UIItemsView.Hep_G2_094_multi_view_screenshot_png.Item.DblClick(141, 9)
  Regions.ET_9.Check(Regions.CreateRegionInfo(Aliases.App.Item, 233, 141, 1456, 808, False))
  Aliases.ApplicationFrameHost.wndApplicationFrameWindow.Close()
  Aliases.explorer.wndRegression_Test.Close()

@given("RI Thresholding 프로세서로 batch run 한 Report[T] 탭이 열려 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir4 =r'E:\Regression_Test\Prefix_File\BAT\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\BAT'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BAT'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis_ProjectManager_AppUI_MainWindow = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  splitter = tomoAnalysis_ProjectManager_AppUI_MainWindow.mainSplitter.operationFrame.operationSplitter
  splitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation.Click(89, 25)
  tomoAnalysis.Menu.OpenFile("E:\\Regression_Test\\Prefix_File\\BAT\\BAT.tcpro")
  tomoAnalysis_ProjectManager_AppUI_MainWindow = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  tomoAnalysis_ProjectManager_AppUI_MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_primary.Click(70, 17)
  tomoAnalysis_BasicAnalysisTime_Plugins_ParameterPanel = tomoAnalysis_ProjectManager_AppUI_MainWindow.tabWidget.qt_tabwidget_stackedwidget.batchRunTab.batchParameterPanel
  spinBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.batchRunTab.batchParameterPanel.timeGroup.endTime
  lineEdit = spinBox.qt_spinbox_lineedit
  lineEdit.Drag(25, 14, -145, 13)
  lineEdit.Drag(35, 2, -212, 5)
  spinBox.Keys("2")
  spinBox.wValue = 2
  
  tomoAnalysis_BasicAnalysisTime_Plugins_ParameterPanel.timeGroup.applyAllBtn.ClickButton()
  tomoAnalysis_BasicAnalysisTime_Plugins_ParameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.buttonExecuteWhole.ClickButton()
  while tomoAnalysis.ProgressDialog.Exists :
   Delay(1000, "Waiting for window to close...");


@when("Report[T] 탭 Data Table 패널의 첫 번째 cube에서 첫 번째 데이터를 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.dataTable.tabWidget.qt_tabwidget_stackedwidget.DataTab.TableItemWidget.tableView.qt_scrollarea_viewport.Click(340, 25)

@then("Report[T] 탭 그래프 패널 위의 Current File 칸에 클릭한 데이터의 이름이 나타났는지 확인")
def step_impl():
  tomoAnalysis_Report_AppUI_MainWindow = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  widget = tomoAnalysis_Report_AppUI_MainWindow.graphReportPanel.graphWidget
  doubleSpinBox = widget.yAxisMinSpinBox
  lineEdit = doubleSpinBox.qt_spinbox_lineedit
  lineEdit.Drag(53, 15, -48, 2)
  lineEdit.Click(45, 10)
  lineEdit.Drag(48, 4, -40, 1)
  doubleSpinBox.Keys("1")
  doubleSpinBox.wValue = 1
  doubleSpinBox = widget.yAxisMaxSpinBox
  doubleSpinBox.qt_spinbox_lineedit.Drag(51, 15, -54, 0)
  doubleSpinBox.Keys("2")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.dataTable.tabWidget.qt_tabwidget_stackedwidget.DataTab.TableItemWidget.tableView.qt_scrollarea_viewport.Click(961, 26)
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.graphWidget.editMaskCombo, "wText", cmpEqual, "20210317.104159.036.RAW LPS-24h-002P001")

@when("Batch Run setting 탭 Time points setting 패널에서 start 값을 {arg}, end 값을 ‘{arg}’, interval 값을 {arg}로 설정하고 Apply time points to all data 버튼을 누른 뒤 Processing mode 패널의 Execute Whole 버튼을 누른다.")
def step_impl(param1, param2, param3):
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis_BasicAnalysisTime_Plugins_ParameterPanel = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.batchRunTab.batchParameterPanel
  groupBox = tomoAnalysis_BasicAnalysisTime_Plugins_ParameterPanel.timeGroup
  spinBox = groupBox.endTime
  spinBox.qt_spinbox_lineedit.Drag(17, 8, -76, 8)
  spinBox.Keys("5")
  spinBox.wValue = 5
  spinBox.Keys("[Enter]")
  spinBox.wValue = 5
  spinBox = groupBox.timeInterval
  spinBox.qt_spinbox_lineedit.Drag(38, 8, -214, 0)
  spinBox.Keys("1[Enter]")
  spinBox.wValue = 1
  groupBox.applyAllBtn.ClickButton()
  scrollArea = tomoAnalysis_BasicAnalysisTime_Plugins_ParameterPanel.groupBox.panel_contents.ScrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 140
  scrollArea.qt_scrollarea_viewport.Widget.buttonExecuteWhole.ClickButton()
  while tomoAnalysis.ProgressDialog.Exists :
   Delay(1000, "Waiting for window to close...");
  
@when("File Change 팝업에서 Accept 버튼을 누른다.")
def step_impl():
  raise NotImplementedError

@when("Report[T] 탭 그래프 패널 위의 Setting X axis의 MIN 값에 {arg}, Max 값에 3을 입력한다.")
def step_impl(param1):
  widget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.graphWidget
  lineEdit = widget.yAxisMinSpinBox.qt_spinbox_lineedit
  lineEdit.DblClick(29, 14)
  lineEdit.Drag(52, 11, -63, -4)
  spinBox = widget.xAxisMinSpinBox
  spinBox.qt_spinbox_lineedit.Drag(25, 15, -67, 0)
  spinBox.Keys("2")
  spinBox.wValue = 2
  spinBox.Keys("[Enter]")
  spinBox.wValue = 2
  spinBox = widget.xAxisMaxSpinBox
  spinBox.qt_spinbox_lineedit.Drag(18, 8, -34, 1)
  spinBox.Keys("3")
  spinBox.wValue = 3
  spinBox.Keys("[Enter]")
  spinBox.wValue = 3

@then("Report[T] 탭의 그래프에 tp 2부터 4까지만 표시됐는지 확인")
def step_impl():
  Regions.graphReportPanel1.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel)

@when("Screen shot 패널의 겹쳐진 사각형 버튼을 눌러 팝업으로 뽑아낸다.")
def step_impl():
  TC_TCDockWidget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget3
  TC_TCDockWidget.Click(328, 21)
  TC_TCDockWidget.qt_dockwidget_floatbutton.Click(9, 8)

@when("3D Visualizer 탭의 View 패널을 클릭하고 키보드의 Alt+F4 버튼을 누른다.")
def step_impl():
  mainWindow = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow
  mainWindow.panel_base.Click(603, 26)
  mainWindow.TC_TCDockWidget4.ViewingToolPanel.panel_contents.bt_tool_round.Keys("~[F4]")

@then("TA가 종료되면서 Screen shot 팝업도 함께 종료되는지 확인")
def step_impl():
  Delay(3000)
  if Aliases.TomoAnalysis.Exists:
    Log.Error("TA가 켜저 있음")
    Aliases.TomoAnalysis.mainwindow.Close()
  else :
    Log.Message("TA가 꺼저 있음")

@given("HeLa-Rab5a-Rab7a{arg} 데이터로 3D Visualizer가 열려 있다.")
def step_impl(param1):
  TestedApps.TomoAnalysis.Run()
  tomoAnalysis = Aliases.TomoAnalysis
  splitter = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter
  splitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation.Click(35, 37)
  HWNDView = tomoAnalysis.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  UIItemsView = HWNDView.CtrlNotifySink2.ShellView.Item
  UIItemsView.Regression_Test.Item.DblClick(86, 17)
  UIItemsView.Prefix_File.Item.DblClick(105, 15)
  UIItemsView.CtrlNotifySink2.ScrollBar.wPosition = 1697
  tomoAnalysis.Menu.OpenFile("E:\\Regression_Test\\Prefix_File\\E08\\E08.tcpro")
  frame = splitter.previewPanel.panel_base
  frame.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget2.TC_TCF2DWidget.panel_contents_image.Widget.Click(70, 50)
  frame.bt_round_tool3.Click(102, 15)

@when("Modality 패널에서 FL 체크박스를 클릭하고 Preset 패널에서 FL 탭을 클릭한다.")
def step_impl():
  aqUtils.Delay(5000)
  mainWindow = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow
  mainWindow.TC_TCDockWidget2.ModalityPanel.panel_contents.FLCheckBox.ClickButton(cbChecked)
  mainWindow.TC_TCDockWidget.panel_base.TabWidget.ClickTab("FL")

@when("Preset 패널 FL 탭의 테이블에서 Blue channel의 Max 칸을 더블 클릭하고 키보드로 100을 입력한 뒤 키보드의 엔터 키를 누른다.")
def step_impl():
  tableWidget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.FLChannelPanel.panel_contents.channelTableWidget
  tableWidget.ClickCell("1", "Max")
  tableWidget.Keys("100[Enter]")

@then("값을 입력한 칸에 100이 입력되지 않고 빈칸으로 남아있는지 확인")
def step_impl():
  Tables.channelTableWidget.Check()

@when("Screen shot 패널의 3D View 탭에서 All axis 버튼을 누르고 파일 탐색기에서 임의의 폴더를 지정한 뒤 폴더 선택 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget3.ScreenShotWidgetUI.panel_contents.tabWidget.TC_TabBar.Click(148, 19)
  buttonAllAxis = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget3.ScreenShotWidgetUI.panel_contents.tabWidget.qt_tabwidget_stackedwidget.view3DTab.bt_round_gray700
  buttonAllAxis.ClickButton()
  HWNDView = tomoAnalysis.DUIViewWndClassName2.Item
  Aliases.TomoAnalysis.dlg2.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  Aliases.TomoAnalysis.dlg2.DUIViewWndClassName.Item.CtrlNotifySink2.ShellView.Item.Regression_Test.Item.DblClick(43, 19)
  tomoAnalysis.btn_.ClickButton()



@when("Screen shot 패널의 3D View 탭에서 All axis 버튼을 누르고 파일 탐색기에서 다른 임의의 폴더를 지정한 뒤 폴더 선택 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget3.ScreenShotWidgetUI.panel_contents.tabWidget.TC_TabBar.Click(148, 19)
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget3.ScreenShotWidgetUI.panel_contents.tabWidget.qt_tabwidget_stackedwidget.view3DTab.bt_round_gray700.ClickButton()
  Aliases.TomoAnalysis.dlg2.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  Aliases.TomoAnalysis.dlg2.DUIViewWndClassName.Item.CtrlNotifySink2.ShellView.Item.Regression_Test.Item.DblClick(52, 11)
  tomoAnalysis.btn_.ClickButton()

@then("파일 탐색기 팝업에 두 번째로 All axis screen shot을 저장했던 폴더가 나타났는지 확인")
def step_impl():
  buttonAllAxis = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget3.ScreenShotWidgetUI.panel_contents.tabWidget.qt_tabwidget_stackedwidget.view3DTab.bt_round_gray700
  buttonAllAxis.Drag(55, 21, -2, 7)
  buttonAllAxis.ClickButton()
  aqObject.CheckProperty(Aliases.TomoAnalysis.dlg2.WorkerW.ReBarWindow32.AddressBandRoot.progress.BreadcrumbParent.toolbar_ERegressionTest, "WndCaption", cmpEqual, "주소: E:\\Regression_Test")
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.dlg2.btn_.ClickButton()

@when("Batch Run Setting 탭 Time points setting 패널에서 custom 체크박스를 체크하고 Apply time points to all data를 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.batchRunTab.batchParameterPanel.timeGroup.customTime.ClickButton(cbChecked)

  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.batchRunTab.batchParameterPanel.timeGroup.applyAllBtn.ClickButton()

@then("Batch Run Setting 탭 왼쪽의 리스트에 있는 Counts 칸에 “{arg}”이 입력 되는지 확인")
def step_impl(param1):
  Tables.tableWidget2.Check()

@when("Time points setting 패널의 Selected time points가 빈칸이 되도록 모든 숫자를 지우고 Apply time points to all data를 클릭한다.")
def step_impl():
  groupBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.batchRunTab.batchParameterPanel.timeGroup
  lineEdit = groupBox.timePoints
  lineEdit.Drag(98, 14, -170, 19)
  lineEdit.Click(63, 13)
  lineEdit.Click(63, 13)
  groupBox.customTime.ClickButton(cbChecked)
  lineEdit.Click(63, 20)
  lineEdit.Click(63, 20)
  lineEdit.SetText("")
  groupBox.applyAllBtn.ClickButton()

@then("“Time step list is empty” Error 팝업 창이 뜨는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.MessageBox10.qt_msgbox_label, "QtText", cmpEqual, "Time step list is empty")
  Aliases.TomoAnalysis.MessageBox10.qt_msgbox_buttonbox.buttonOk.ClickButton()
  
@when("Batch Run Setting 탭 왼쪽의 리스트에서 가장 위에 있는 파일을 하나 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.batchRunTab.batchListPanel.scrollArea.qt_scrollarea_viewport.Widget.BaTimeBatchListWidget.tableWidget.qt_scrollarea_viewport.CheckBox.ClickButton(cbChecked)

@when("Time points setting 패널의 start 칸에 키보드로 {arg}을 입력하고 키보드의 엔터 키를 누른다.")
def step_impl(param1):
  raise NotImplementedError

@when("MovIe maker 탭을 클릭하고 하단의 슬라이더를 오른쪽 끝까지 이동시킨다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.panel_base.TC_AdaptiveTabWidget.TC_TabBar.Click(199, 15)

  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.panel_base.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.RecordVideoPanel.scrollArea.qt_scrollarea_hcontainer.ScrollBar.wPosition = 5651

@then("Movie maker 탭의 시간 표기 눈금에서 오른쪽 끝에 있는 숫자가 “{arg}:{arg}”라고 표기 돼 있는지 확인")
def step_impl(param1, param2):
  Regions.ET_50.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.panel_base.TC_AdaptiveTabWidget)


@given("3D Visualizer 탭 TF canvas에 비활성화 상태의 TF box가 만들어져 있다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  widget = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.MainWindow.dockwidget_inner.Widget.QCanvas2D.scrollArea.qt_scrollarea_viewport.scrollAreaWidgetContents.CanvasWidget.Canvas.Widget
  widget.Drag(59, 200, 75, 46)
  Aliases.TomoAnalysis.ColorDialog.WellArray.Click(37, 83)
  Aliases.TomoAnalysis.ColorDialog.DialogButtonBox.buttonOk.ClickButton()
  widget.Click(104, 173)

@when("Preset 패널 하단의 Clear 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.bt_square_gary.Click(67, 14)

@when("Preset 패널의 TF canvas의 하얀색 영역에서 마우스를 클릭한 채로 드래그한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.MainWindow.dockwidget_inner.Widget.QCanvas2D.scrollArea.qt_scrollarea_viewport.scrollAreaWidgetContents.CanvasWidget.Canvas.Widget.Drag(90, 184, 105, 59)

@when("팝업에서 빨간색을 지정하고 키보드의 엔터 키를 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  wellArray = Aliases.TomoAnalysis.ColorDialog.WellArray
  wellArray.Click(45, 81)
  wellArray.Click(40, 83)
  Aliases.TomoAnalysis.ColorDialog.DialogButtonBox.buttonOk.ClickButton()

@then("View 패널에 빨간색 3D rendering이 생성되었는지 확인")
def step_impl():
  Regions.ET_09.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer, False, False, 2000)
  Regions.panel_base.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.panel_base)

@when("Preset 패널에서 TF canvas 우상단의 겹쳐진 사각형 아이콘을 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.qt_dockwidget_floatbutton.Click(5, 6)

@when("TA 창의 X 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.Close()

@then("TA 창과 2D TF Canvas 팝업이 모두 종료되었는지 확인")
def step_impl():
  
  Delay(10000)
  
  if Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.Exists == False :
    pass
  else :
    Log.Error("TA 창이 남아있음")
    
  if Aliases.TomoAnalysis.TC_TCDockWidget2.Exists == False :
    pass
  else :
    Log.Error("2D TF Canvas 팝업이 남아있음")

@when("Toolbox의 3D Visualization 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.bt_round_tool3.Click(69, 15)

@then("‘Select tcf files to view’ 라는 문구의 경고 팝업이 나타났는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.MessageBox11, "QtText", cmpEqual, "Select tcf files to view")
  Aliases.TomoAnalysis.MessageBox11.qt_msgbox_buttonbox.buttonOk.ClickButton()
  
  
@given("Mask Editor 탭에 2개의 Cell이 있는 TCF의 Cell instance Single Run 마스크가 ID ‘test’로 생성되어 있다.")
def step_impl():
  dir4 =r'E:\Regression_Test\Prefix_File\New_Project'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\New_Project'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\New_Project'
  copy_tree(dir2,dir1)
  TestedApps.TomoAnalysis.Run()
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation.Click(72, 13)
  dlgOpenProject = tomoAnalysis.Menu
  cbx_N = dlgOpenProject.cbx_N
  cbx_N.ComboBox.Edit.Click(113, 7)
  cbx_N.SetText("E:\\Regression_Test\\Prefix_File\\New_Project\\Project\\Project.tcpro")
  dlgOpenProject.btn_S.ClickButton()
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.AddTcfPanel.bt_round_operation.Click(136, 38)
  Aliases.TomoAnalysis.dlgOpenTcfFolder.edit_.SetText("E:\\Regression_Test\\TomoAnalysis test data\\HT only_test sample for SW team")
  Aliases.TomoAnalysis.btn_.Click(-1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis_ProjectManager_AppUI_MainWindow = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  frame = tomoAnalysis_ProjectManager_AppUI_MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base
  scrollArea = frame.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(107, 88)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis_ProjectManager_AppUI_MainWindow = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  splitter = tomoAnalysis_ProjectManager_AppUI_MainWindow.mainSplitter
  splitter2 = splitter.operationFrame.operationSplitter
  frame = splitter2.operationPanel.panel_base
  frame2 = frame.frame_tab
  frame2.bt_round_tab.ClickButton()
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis_ProjectManager_AppUI_MainWindow = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  splitter = tomoAnalysis_ProjectManager_AppUI_MainWindow.mainSplitter
  splitter2 = splitter.operationFrame.operationSplitter
  frame = splitter2.operationPanel.panel_base
  widget = frame.widget_panel
  widget.AddPGPanel.widget_panel2.bt_round_operation.ClickButton()
  widget.HyperCubePanel.bt_round_operation.ClickButton()
  widget.CubePanel.bt_round_operation.ClickButton()
  frame2 = splitter2.previewPanel.panel_base
  scrollArea = frame2.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(140, 97)
  frame.frame_tab.bt_round_tab5.ClickButton()
  frame2.bt_round_tool.Click(69, 11)
  tomoAnalysis.LinkDialog.bt_square_primary.ClickButton()
  widget.SelectAppPanel.bt_round_operation.ClickButton()
  splitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_gray.Click(113, 14)
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel = tomoAnalysis.widget_popup
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget.panel_contents_image.widget.panel_contents_image.Widget.Click(72, 63)
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.bt_square_primary.Click(60, 9)
  tomoAnalysis_BasicAnalysis_Plugins_ParameterPanel = tomoAnalysis_ProjectManager_AppUI_MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel
  tomoAnalysis_BasicAnalysis_Plugins_ParameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.buttonExecute.ClickButton()
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.correctionBtn.Click(-1)



@when("Mask selector 패널에서 Cell instance 버튼을 눌러 instance 마스크를 불러온다.")
def step_impl():
  raise NotImplementedError

@when("Label tools 패널의 Label Picker 버튼을 누르고 초록색 마스크를 누른다.")
def step_impl():
  raise NotImplementedError

@when("Drawing tool 패널의 Paint 버튼을 누르고 사이즈를 100으로 키운 뒤 XY Slice의 아무 곳이나 클릭하여 Label {arg} 마스크를 수정한다.")
def step_impl(param1):
  raise NotImplementedError

@when("Mask selector 패널에서 Whole cell 버튼을 눌러 organelle 마스크를 불러온다.")
def step_impl():
  raise NotImplementedError

@when("Whole cell 마스크가 생성되어 있는 XY Slice의 아무 곳이나 클릭한다.")
def step_impl():
  raise NotImplementedError

@then("클릭한 부분에 추가된Label {arg} 마스크가 빨간색인지 확인")
def step_impl(param1):
  raise NotImplementedError

@when("Display 패널에서 2D 버튼을 클릭하고 Modality 패널에서 FL 체크박스를 체크한다.")
def step_impl():
  mainWindow = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow
  mainWindow.TC_TCDockWidget5.DisplayPanel.panel_contents.display2DButton.ClickButton()
  mainWindow.TC_TCDockWidget2.ModalityPanel.panel_contents.FLCheckBox.ClickButton(cbChecked)

@when("Preset 패널의 FL 창에서 Green 체크박스를 선택 해제한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.ClickTab("FL")

  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.FLChannelPanel.panel_contents.widget_3.greenCheckBox.ClickButton(cbUnchecked)

@then("View 패널에 HT이미지와 함께 빨간색 형광만 표시 돼 있는지 확인")
def step_impl():
  Regions.ET_05.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@when("Modality 패널에서 HT 체크박스를 체크하여 선택 해제한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget2.ModalityPanel.panel_contents.HTCheckBox.ClickButton(cbUnchecked)

@then("View 패널에 HT이미지가 없고 형광색만 표시 돼 있는지 확인")
def step_impl():
  Regions.ET_06.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@given("to see if FL is right order-001P00Stitching 데이터로 3D Visualizer가 열려 있다.")
def step_impl():
  dir4 =r'E:\Regression_Test\Prefix_File\New_Project'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\New_Project'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\New_Project'
  copy_tree(dir2,dir1)
  TestedApps.TomoAnalysis.Run()
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation.Click(72, 13)
  dlgOpenProject = tomoAnalysis.Menu
  cbx_N = dlgOpenProject.cbx_N
  cbx_N.ComboBox.Edit.Click(113, 7)
  cbx_N.SetText("E:\\Regression_Test\\Prefix_File\\New_Project\\Project\\Project.tcpro")
  dlgOpenProject.btn_S.ClickButton()
  tomoAnalysis = Aliases.TomoAnalysis
  splitter = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter
  splitter.operationPanel.panel_base.widget_panel.AddTcfPanel.bt_round_operation.Click(84, 35)
  edit_ = Aliases.TomoAnalysis.dlgOpenTcfFolder.edit_
  edit_.Click(201, 18)
  edit_.SetText("E:\\Regression_Test\\E_Test")
  tomoAnalysis.btn_.ClickButton()
  frame = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base
  frame2 = frame.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame
  frame2.Click(194, 48)
  frame2.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(84, 65)
  frame.bt_round_tool3.Click(56, 12)





@when("Viewing tool 패널의 View dropdown에서 TimeStamp에 마우스를 갖다대고 Show TimeStamp를 클릭한다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget4.ViewingToolPanel.panel_contents.bt_tool_round2.ClickButtonXY(70, 16)
  NameMapping.Sys.TomoAnalysis.Menu2.QtMenu.Click("TimeStamp")
  tomoAnalysis.Menu4.CheckBox2.ClickButton(cbChecked)

@when("3D Visualizer 하단의 Player 창에서 Play 버튼을 클릭한다.")
def step_impl():
  raise NotImplementedError

@then("View 패널의 3D 화면 아래에 있는 TimeStamp에서 괄호 밖의 숫자가 변화하는지 확인")
def step_impl():
  raise NotImplementedError

@when("Viewing tool패널의 Layout 드롭다운에서 XY Plane을 클릭한다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  widget = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget4.ViewingToolPanel.panel_contents
  widget.bt_tool_round2.ClickButtonXY(85, 18)
  widget.bt_tool_round.ClickButtonXY(127, 10)
  tomoAnalysis.Menu4.RadioButton2.ClickButton()

@when("View 패널 오른쪽 상단의 설정 버튼을 클릭하고 ScalarBar에 마우스를 갖다대고 Show ScalarBar를 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer.Widget.TomoAnalysis_Viewer_Plugins_ViewerRenderWindow2d.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis.Click(1140, 16)
  Aliases.TomoAnalysis.Menu4.Check("ScalarBar|Show ScalarBar", True)

@when("View 패널 오른쪽 상단의 설정 버튼을 클릭하고 Level Window에서 RI Range 창의 왼쪽 아래에 있는 수치 박스에 마우스를 갖다대고 마우스 휠을 위로 올려 1.35에 맞춘다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer.Widget.TomoAnalysis_Viewer_Plugins_ViewerRenderWindow2d.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis.Click(1134, 18)
  widget = tomoAnalysis.Menu4.Widget
  widget.MouseWheel(1)
  widget.TC_RangeSlider.MouseWheel(1)
  doubleSpinBox = widget.DoubleSpinBox
  lineEdit = doubleSpinBox.qt_spinbox_lineedit
  lineEdit.MouseWheel(1)
  doubleSpinBox.wValue = 1.3327
  lineEdit.MouseWheel(1)
  doubleSpinBox.wValue = 1.3328
  lineEdit.MouseWheel(1)
  doubleSpinBox.wValue = 1.3329
  lineEdit.MouseWheel(1)
  doubleSpinBox.wValue = 1.333
  lineEdit.MouseWheel(1)
  doubleSpinBox.wValue = 1.3331
  lineEdit.MouseWheel(1)
  doubleSpinBox.wValue = 1.3331999999999999
  lineEdit.MouseWheel(1)
  doubleSpinBox.wValue = 1.3332999999999999
  lineEdit.MouseWheel(2)
  doubleSpinBox.wValue = 1.3334999999999999
  lineEdit.MouseWheel(1)
  doubleSpinBox.wValue = 1.3335999999999999
  lineEdit.MouseWheel(2)
  doubleSpinBox.wValue = 1.3337999999999999
  lineEdit.MouseWheel(1)
  doubleSpinBox.wValue = 1.3338999999999999
  lineEdit.MouseWheel(2)
  doubleSpinBox.wValue = 1.3340999999999998
  lineEdit.MouseWheel(1)
  doubleSpinBox.wValue = 1.3341999999999998
  lineEdit.MouseWheel(2)
  doubleSpinBox.wValue = 1.3343999999999998
  lineEdit.MouseWheel(1)
  doubleSpinBox.wValue = 1.3344999999999998
  lineEdit.MouseWheel(2)
  doubleSpinBox.wValue = 1.3346999999999998
  lineEdit.MouseWheel(1)
  doubleSpinBox.wValue = 1.3347999999999998
  lineEdit.MouseWheel(2)
  doubleSpinBox.wValue = 1.3349999999999997
  lineEdit.MouseWheel(1)
  doubleSpinBox.wValue = 1.3350999999999997
  lineEdit.MouseWheel(2)
  doubleSpinBox.wValue = 1.3352999999999997
  lineEdit.MouseWheel(1)
  doubleSpinBox.wValue = 1.3353999999999997
  lineEdit.MouseWheel(-1)
  doubleSpinBox.wValue = 1.3352999999999997
  lineEdit.MouseWheel(-1)
  doubleSpinBox.wValue = 1.3351999999999997
  lineEdit.MouseWheel(-1)
  doubleSpinBox.wValue = 1.3350999999999997
  lineEdit.MouseWheel(-1)
  doubleSpinBox.wValue = 1.3349999999999997
  lineEdit.MouseWheel(-1)
  doubleSpinBox.wValue = 1.3348999999999998
  lineEdit.MouseWheel(-1)
  doubleSpinBox.wValue = 1.3347999999999998
  lineEdit.MouseWheel(-2)
  doubleSpinBox.wValue = 1.3345999999999998
  lineEdit.MouseWheel(-1)
  doubleSpinBox.wValue = 1.3344999999999998
  lineEdit.MouseWheel(-1)
  doubleSpinBox.wValue = 1.3343999999999998
  lineEdit.MouseWheel(-3)
  doubleSpinBox.wValue = 1.3340999999999998
  lineEdit.MouseWheel(-1)
  doubleSpinBox.wValue = 1.3339999999999999
  lineEdit.MouseWheel(-1)
  doubleSpinBox.wValue = 1.3338999999999999
  lineEdit.MouseWheel(-2)
  doubleSpinBox.wValue = 1.3336999999999999
  lineEdit.MouseWheel(1)
  doubleSpinBox.wValue = 1.3337999999999999
  lineEdit.MouseWheel(2)
  doubleSpinBox.wValue = 1.3339999999999999
  lineEdit.MouseWheel(1)
  doubleSpinBox.wValue = 1.3340999999999998
  lineEdit.MouseWheel(2)
  doubleSpinBox.wValue = 1.3342999999999998
  lineEdit.MouseWheel(1)
  doubleSpinBox.wValue = 1.3343999999999998
  lineEdit.MouseWheel(1)
  doubleSpinBox.wValue = 1.3344999999999998
  lineEdit.MouseWheel(1)
  doubleSpinBox.wValue = 1.3345999999999998
  lineEdit.MouseWheel(1)
  doubleSpinBox.wValue = 1.3346999999999998
  lineEdit.MouseWheel(1)
  doubleSpinBox.wValue = 1.3347999999999998
  lineEdit.MouseWheel(1)
  doubleSpinBox.wValue = 1.3348999999999998
  lineEdit.MouseWheel(1)
  doubleSpinBox.wValue = 1.3349999999999997

@then("View 패널 오른쪽 하단의 ScalarBar 최솟값이 1.35인지 확인")
def step_impl():
  Regions.ET_07.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@when("Viewing tool 패널 View 드롭다운 버튼의 Axis Grid에 마우스롤 갖다대고 Color\\(x)를 클릭하여 빨간색을 선택한다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget4.ViewingToolPanel.panel_contents.bt_tool_round2.ClickButtonXY(132, 16)
  Sys.Desktop.Mousey += 120;
  Sys.Desktop.MouseDown(1,Sys.Desktop.Mousex,Sys.Desktop.Mousey)
  Sys.Desktop.MouseUp(1,Sys.Desktop.Mousex,Sys.Desktop.Mousey)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.Menu4.CheckBox3.ClickButton(-1)
  Sys.Desktop.Mousey += 80;
  Sys.Desktop.Mousex += 60;
  Sys.Desktop.MouseDown(1,Sys.Desktop.Mousex,Sys.Desktop.Mousey)
  Sys.Desktop.MouseUp(1,Sys.Desktop.Mousex,Sys.Desktop.Mousey)
  tomoAnalysis = Aliases.TomoAnalysis
  Aliases.TomoAnalysis.ColorDialog2.WellArray.Click(37, 83)
  Aliases.TomoAnalysis.ColorDialog2.DialogButtonBox.buttonOk.ClickButton()

@then("3D Visualizer 탭 View 패널의 3D 화면에 X축 숫자가 빨간색인 Axis grid가 생성됐는지 확인")
def step_impl():
  Regions.ET_08.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@when("Viewing tool 패널의 View 드롭다운에서 Axis grid에 마우스를 갖다대고 font size에 30을 입력한다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  widget = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget4.ViewingToolPanel.panel_contents
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget4.ViewingToolPanel.panel_contents.bt_tool_round2.ClickButtonXY(132, 16)
  Sys.Desktop.Mousey += 120;
  Sys.Desktop.Mousex += 60;
  Sys.Desktop.MouseDown(1,Sys.Desktop.Mousex,Sys.Desktop.Mousey)
  Sys.Desktop.MouseUp(1,Sys.Desktop.Mousex,Sys.Desktop.Mousey)
  spinBox = tomoAnalysis.Menu4.Widget.SpinBox
  spinBox.qt_spinbox_lineedit.Click(23, 13)
  widget.bt_tool_round.Keys("[BS][BS]30")
  spinBox.wValue = 30
  Delay(3000)
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.Click(950, 22)

@when("Screen shot 패널 3D view 탭에서 Upsampling 수치를 3에 맞추고 Capture 버튼을 누른다.")
def step_impl():
  TC_AdaptiveTabWidget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget3.ScreenShotWidgetUI.panel_contents.tabWidget
  TC_AdaptiveTabWidget.TC_TabBar.Click(168, 30)
  widget = TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.view3DTab
  spinBox = widget.volumeUpsampleSpin
  spinBox.qt_spinbox_lineedit.Drag(21, 14, -85, 0)
  spinBox.Keys("3")
  spinBox.wValue = 3
  widget.bt_round_gray7002.ClickButton()

@when("파일 탐색기에 경로를 지정하고 저장 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  comboBox = tomoAnalysis.DUIViewWndClassName2.Item.FloatNotifySink.ComboBox
  edit = comboBox.Edit
  Aliases.TomoAnalysis.dlgSaveScreenShot.DUIViewWndClassName.Item.FloatNotifySink.ComboBox.Edit.Click(21, 1)
  Aliases.TomoAnalysis.dlgSaveScreenShot.DUIViewWndClassName.Item.FloatNotifySink.ComboBox.Edit.SetText("E:\\Regression_Test\\test\\20201229.110524.646.HeLa-Rab5a-Rab7a-005_3D_screenshot.png")
  Aliases.TomoAnalysis.dlgSaveScreenShot.btn_S.ClickButton()


@then("지정한 경로에 생긴 스크린 샷 이미지 파일이 View 패널 3D 화면과 일치하는지 확인")
def step_impl():
  explorer = Aliases.explorer
  explorer.wndShell_TrayWnd.ReBarWindow32.MSTaskSwWClass.MSTaskListWClass.Click(33, 23)
  wndCabinetWClass = explorer.wndCabinetWClass
  HWNDView = wndCabinetWClass.ShellTabWindowClass.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  UIItemsView = HWNDView.CtrlNotifySink2.ShellView.Item
  UIItem = UIItemsView.Regression_Test
  UIProperty = UIItem.Item
  UIProperty.Click(107, 12)
  UIProperty.Click(107, 12)
  UIItem.Click(167, 12)
  UIProperty.DblClick(145, 11)
  UIItem = UIItemsView.test
  UIItem.Item.Click(67, 4)
  UIItem.Keys("[Enter]")
  UIItem = UIItemsView.HeLa_Rab5a_Rab7a_005_3D_screenshot_png
  UIItem.Item.Click(124, 17)
  UIItem.Keys("[Enter]")
  Regions.ET_20.Check(Regions.CreateRegionInfo(Aliases.App.Item, 390, 78, 1138, 928, False))
  wndCabinetWClass.Close()
  Aliases.ApplicationFrameHost.wndApplicationFrameWindow2.Close()

@then("Measurement 팝업창이 BA탭이 있는 창 위에 나타나는지 확인")
def step_impl():
  Regions.qt_scrollarea_viewport7.Check(Aliases.TomoAnalysis.BasicAnalysisResultPanel.tableWidget.qt_scrollarea_viewport, False, False , 2000)

@given("“Hep G2{arg}” TCF 파일이 들어간 프로젝트 파일이 열려있다.")
def step_impl(param1):
  dir4 =r'E:\Regression_Test\Prefix_File\BA_borderkill'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\BA_borderkill'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BA_borderkill'
  copy_tree(dir2,dir1)

  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation.Click(72, 13)
  dlgOpenProject = tomoAnalysis.Menu
  cbx_N = dlgOpenProject.cbx_N
  cbx_N.ComboBox.Edit.Click(113, 7)
  cbx_N.SetText("E:\\Regression_Test\\Prefix_File\\BA_borderkill\\BA_borderkill.tcpro")
  dlgOpenProject.btn_S.ClickButton()

@when("Preview 패널에서 TCF 썸네일을 우클릭 하고 Create Simple Playground 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.ClickR(109, 77)
  Aliases.TomoAnalysis.Menu6.Click(-1)

@when("Select Application 탭에서 Basic Analyzer을 선택하고 OK를 누른다.")
def step_impl():
  tomoAnalysis_ProjectManager_Plugins_SelectAppPanel = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectAppPanel
  tomoAnalysis_ProjectManager_Plugins_SelectAppPanel.dropdown_high2.ClickItem("Basic Analyzer")
  tomoAnalysis_ProjectManager_Plugins_SelectAppPanel.bt_round_operation.ClickButton()

@when("Application Parameter에서 AI Segmentation을 선택한뒤, Use Border Kill을 체크하고 Batch Run을 클릭한다.")
def step_impl():
  frame = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base
  frame2 = frame.panel_contents.processorParamFrame
  frame2.procCombo.ClickItem("AI Segmentation")
  scrollArea = frame2.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParamControl.TC_StrCheckBox.Click(15, 10)
  frame.bt_square_primary.Click(64, 3)

@when("Report 탭의 오른쪽, Graph of individual cell 패널에서 {arg}_1막대그래프를 선택하고 Edit Mask를 누른다.")
def step_impl(param1):
  widget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.widget
  widget.Widget.TCChartView.Widget.Click(280, 151)
  widget.correctBtn.ClickButton()

@then("Mask Editor 탭 Mask selector 패널의 5개 버튼이 모두 활성화 되는지 확인")
def step_impl():
  Regions.panel_contents.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.MaskSelectionPanel.panel_base.panel_contents)

@given("cube에 TCF가 1개 연결되어 있다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  frame = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base
  frame.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(171, 38)
  frame.bt_round_tool.Click(63, 9)
  tomoAnalysis.LinkDialog.bt_square_primary.ClickButton()

@when("Operation sequence 패널의 Select Application 탭에서 Basic Analyzer를 선택하고 OK 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectAppPanel.bt_round_operation.ClickButton()

@then("Report 탭이 뜨는지 확인")
def step_impl():
  Regions.ET_40.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.TC_TabBar)

@when("Project Manager 탭의 Preview 패널에서 cube에 연결되어있지 않은 TCF를 1개 클릭하고 Tool box의 Link to Cube를 클릭한다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  TC_AdaptiveTabWidget = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget
  TC_AdaptiveTabWidget.TC_TabBar.Click(139, 34)
  frame = TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base
  frame.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget3.TC_TCF2DWidget.panel_contents_image.Widget.Click(119, 70)
  frame.bt_round_tool.Click(31, 5)


@when("Dialog 팝업창에서 Link 버튼을 클릭한다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.LinkDialog.bt_square_primary.ClickButton()
  
@then("File changes 창의 File Info탭에 File Name이 같은 2개의 리스트가 나타나는지 확인")
def step_impl():
  Tables.addTable.Check()
  Aliases.TomoAnalysis.fileChangeForm.bt_square_gray.ClickButton()

@given("Operation sequence 패널의 Link Data to Cube 탭 화면이 열려 있다.")
def step_impl():
  dir_path = "E:\Regression_Test\Generated_File\E08"

  if os.path.exists(dir_path):
    shutil.rmtree(dir_path)
  TestedApps.TomoAnalysis.Run()
  tomoAnalysis = Aliases.TomoAnalysis
  widget = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel
  widget.SelectProjectPanel.bt_round_operation2.Click(152, 13)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis_ProjectManager_Plugins_NewProjectDialog = tomoAnalysis.NewProjectDialog
  lineEdit = tomoAnalysis_ProjectManager_Plugins_NewProjectDialog.input_high
  lineEdit.Click(200, 13)
  lineEdit.SetText("E08")
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.NewProjectDialog.pathButton.ClickButton()
  edit_ = tomoAnalysis.edit_
  Aliases.TomoAnalysis.dlg.edit_.SetText("E:\\Regression_Test\\Generated_File")
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.dlg.btn_.ClickButton()
  tomoAnalysis.NewProjectDialog.createButton.ClickButton()
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.AddTcfPanel.bt_round_operation.Click(155, 38)
  tomoAnalysis = Aliases.TomoAnalysis
  edit_ = Aliases.TomoAnalysis.dlgOpenTcfFolder.edit_
  edit_.Click(403, 2)
  edit_.Click(403, 10)
  edit_.SetText("E:\\Regression_Test\\F_Test\\F09-F12_Test")
  tomoAnalysis.btn_.ClickButton()
  frame = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base
  frame2 = frame.frame_tab
  frame2.bt_round_tab.ClickButton()
  widget = frame.widget_panel
  widget2 = widget.AddPGPanel.widget_panel2
  lineEdit = widget2.input_high
  lineEdit.Click(151, 29)
  lineEdit.SetText("test")
  widget2.bt_round_operation.ClickButton()
  buttonCreateHypercube = frame2.bt_round_tab3
  buttonCreateHypercube.ClickButton()
  tomoAnalysis_ProjectManager_Plugins_HyperCubePanel = widget.HyperCubePanel
  lineEdit = tomoAnalysis_ProjectManager_Plugins_HyperCubePanel.input_high
  lineEdit.Click(147, 21)
  buttonCreateHypercube.Keys("t")
  lineEdit.SetText("test")
  tomoAnalysis_ProjectManager_Plugins_HyperCubePanel.bt_round_operation.ClickButton()
  tomoAnalysis_ProjectManager_Plugins_CubePanel = widget.CubePanel
  lineEdit = tomoAnalysis_ProjectManager_Plugins_CubePanel.input_high
  lineEdit.Click(109, 34)
  lineEdit.SetText("test")
  tomoAnalysis_ProjectManager_Plugins_CubePanel.bt_round_operation.ClickButton()
  frame2.bt_round_tab5.ClickButton()








@when("프로세싱 바 팝업이 생기면 진행이 완료되고 팝업이 사라질 때 까지 기다린다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  while tomoAnalysis.ProgressDialog.Exists :
   Delay(1000, "Waiting for window to close...");

@when("Application Parameter에서 AI Segmentation을 선택한뒤, Custom Medium RI 체크박스를 클릭하고 Single run을 클릭한다.")
def step_impl():
  frame = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base
  frame2 = frame.panel_contents.processorParamFrame
  frame2.procCombo.ClickItem("AI Segmentation")
  scrollArea = frame2.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterTable.TC_StrCheckBox.Click(71, 8)
  frame.bt_square_gray.Click(101, 25)

@when("Single Run 팝업창에서 Hep G2{arg} 썸네일을 선택하고 Single RUN 버튼을 클릭한다.")
def step_impl(param1):
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel = Aliases.TomoAnalysis.widget_popup
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget.panel_contents_image.Click(163, 63)
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.bt_square_primary.Click(33, 12)

@when("Basic Analyzer 탭에서 Processing mode 패널 AI Inference 탭의 Execute 버튼을 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.buttonExecute.ClickButton()

@when("Basic Measurement 탭의 Execute 버튼을 클릭한다.")
def step_impl():
  Delay(30000)
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterTable.buttonExecute.ClickButton()

@then("Measurement 팝업창 Whole Cell에서 cell1의 Dry mass\\(pg)값이 {arg}인지 확인.")
def step_impl(param1):
  tableView = Aliases.TomoAnalysis.BasicAnalysisResultPanel.tableWidget
  Table = Tables.tableWidget4
  Tableval =  float(Table.Values[0, 0])
  
  if (Tables.tableWidget4.CompareWithObject(tableView)) :
    Tables.tableWidget4.Check()
    
  elif(float(param1)-1 < Tableval and Tableval < float(param1)+1) :
    Log.Message("오차가 있지만 범위에 들어감") 
    
  else :
    Log.Error("값이 틀림")

  
@when("Measurement 팝업창을 닫는다.")
def step_impl():
  Aliases.TomoAnalysis.BasicAnalysisResultPanel.Close()

@when("Processing mode 패널의 Basic Measurement탭에서 Medium RI 입력 칸에 “{arg}”를 입력한다.")
def step_impl(param1):
  doubleSpinBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterTable.ScalarUi.valueSpin
  doubleSpinBox.qt_spinbox_lineedit.Drag(42, 14, -84, -7)
  doubleSpinBox.Keys("1")
  doubleSpinBox.wValue = 1
  doubleSpinBox.Keys(".2")
  doubleSpinBox.wValue = 1.2
  doubleSpinBox.Keys("5")
  doubleSpinBox.wValue = 1.25
  doubleSpinBox.Keys("[Enter]")
  doubleSpinBox.wValue = 1.25

@when("Basic Measurement 탭에서 Execute 버튼을 클릭한다.")
def step_impl():
  Delay(10000)
  widget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab
  widget.viewerPanel.Splitter.Widget2.TomoAnalysis_BasicAnalysis_Plugins_BaRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis.Click(508, 372)
  widget.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterTable.buttonExecute.ClickButton()
  Delay(10000)

@then("Measurement 팝업창 Whole Cell에서 cell1의 Dry mass\\(pg) 값이 {arg}인지 확인")
def step_impl(param1):
  tableView = Aliases.TomoAnalysis.BasicAnalysisResultPanel.tableWidget
  Table = Tables.tableWidget6
  Tableval =  float(Table.Values[0, 0])
  
  if (Tables.tableWidget6.CompareWithObject(tableView)) :
    Tables.tableWidget6.Check()
    
  elif(float(param1)-1 < Tableval and Tableval < float(param1)+1) :
    Log.Message("오차가 있지만 범위에 들어감") 
    
  else :
    Log.Error("값이 틀림")

@given("TomoAnalysis에 Report[T] 탭이 열려 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir4 =r'E:\Regression_Test\Prefix_File\BAT\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\BAT'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BAT'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis_ProjectManager_AppUI_MainWindow = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  splitter = tomoAnalysis_ProjectManager_AppUI_MainWindow.mainSplitter.operationFrame.operationSplitter
  splitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation.Click(89, 25)
  tomoAnalysis.Menu.OpenFile("E:\\Regression_Test\\Prefix_File\\BAT\\BAT.tcpro")
  tomoAnalysis_ProjectManager_AppUI_MainWindow = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  tomoAnalysis_ProjectManager_AppUI_MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_primary.Click(70, 17)
  tomoAnalysis_BasicAnalysisTime_Plugins_ParameterPanel = tomoAnalysis_ProjectManager_AppUI_MainWindow.tabWidget.qt_tabwidget_stackedwidget.batchRunTab.batchParameterPanel
  spinBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.batchRunTab.batchParameterPanel.timeGroup.endTime
  lineEdit = spinBox.qt_spinbox_lineedit
  lineEdit.Drag(25, 14, -145, 13)
  lineEdit.Drag(35, 2, -212, 5)
  spinBox.Keys("2")
  spinBox.wValue = 2
  widget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.batchRunTab
  widget.batchListPanel.allCheckBox.ClickButton(cbChecked)
  tomoAnalysis_BasicAnalysisTime_Plugins_ParameterPanel = widget.batchParameterPanel
  groupBox = tomoAnalysis_BasicAnalysisTime_Plugins_ParameterPanel.timeGroup
  spinBox = groupBox.endTime
  spinBox.qt_spinbox_lineedit.Drag(26, 12, -32, 0)
  spinBox.Keys("2")
  spinBox.wValue = 2
  spinBox.Keys("[Enter]")
  spinBox.wValue = 2
  groupBox.applyAllBtn.ClickButton()
  tomoAnalysis_BasicAnalysisTime_Plugins_ParameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.buttonExecuteWhole.ClickButton()
  tomoAnalysis = Aliases.TomoAnalysis
  while tomoAnalysis.ProgressDialog.Exists :
   Delay(1000, "Waiting for window to close...");

@when("Report[T] 탭 Graph 패널에서 그래프 위의 점을 하나 클릭하고 Export graph 버튼을 클릭한다.")
def step_impl():
  file_path = r'E:\Regression_Test\test\untitled.png'
  if os.path.exists(file_path):
     os.remove(file_path)
  widget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.graphWidget
  widget.chartView.Widget.Click(98, 102)
  widget.exportGraphButton.ClickButton()

@when("파일 탐색기에서 파일 경로와 파일 이름을 설정하고 저장 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.dlgExportHypercubeGraph.DUIViewWndClassName.Item.FloatNotifySink.ComboBox.SetText("E:\\Regression_Test\\test\\untitled.png")
  Aliases.TomoAnalysis.dlgExportHypercubeGraph.btn_S.Click(-1)
  
@then("저장된 png 파일에 별 모양 포인터가 깨지지 않고 그려져 있는지 확인")
def step_impl():
  Regions.Widget5.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.graphWidget.chartView.Widget)

@when("Report[T] 탭 Graph 패널에서 그래프 위의 빈 공간을 클릭한다.")
def step_impl():
  widget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.graphWidget.chartView.Widget
  widget.Click(298, 142)
  widget.Click(400, 112)
  widget.Click(171, 49)
  widget.Click(633, 2)
  widget.Click(633, 25)
  widget.Click(613, 38)
  widget.Click(380, 178)
  widget.Click(390, 237)

@then("Report[T] 탭의 그래프 점 위에 그려졌던 별모양 포인터가 사라졌는지 확인")
def step_impl():
  raise NotImplementedError
