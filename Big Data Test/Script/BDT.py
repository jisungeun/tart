﻿import shutil
import os.path
from distutils.dir_util import copy_tree
import time
import glob
import re

@when("“S08_BasicAnalyzerAIS” 프로젝트 파일을 선택한다.")
def step_impl():
  dir4 =r'E:\Regression_Test\Scenario test_project sample\S08_BasicAnalyzerAIS\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Scenario test_project sample\S08_BasicAnalyzerAIS'
  dir2 = r'E:\Regression_Test\Scenario test_project sample\backup\S08_BasicAnalyzerAIS'
  copy_tree(dir2,dir1)
  dlgOpenProject = Aliases.TomoAnalysis.dlgOpenProject
  HWNDView = dlgOpenProject.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\Scenario test_project sample\\S08_BasicAnalyzerAIS\\S08_BasicAnalyzerAIS.tcpro")

  

@then("“S08_BasicAnalyzerAIS” 프로젝트가 TA에 열렸는지 확인")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.frame_tab.bt_round_tab7.ClickButton()
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.h5, "QtText", cmpEqual, "E:/Regression_Test/Scenario test_project sample/S08_BasicAnalyzerAIS/S08_BasicAnalyzerAIS.tcpro")

@when("팝업에서 RAW {arg}{arg} 파일을 더블클릭한다.")
def step_impl(param1, param2):
  Aliases.TomoAnalysis.widget_popup.scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget2.panel_contents_image.widget.panel_contents_image.Widget.DblClick(137, 69)

@then("Basic Analyzer 탭의 Mask Viewing panel에 마스크 두 개가 생성되었는지 확인")
def step_impl():
  Regions.BDT_01.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel, False, False, 1000)

@then("Basic Analyzer 탭의 Mask Viewing panel 상단에 Label 스위치, Whole Cell, Nucleus, Nucleolus, Lipid droplet 선택 버튼과 Multi-layered mask 스위치, Hide Mask 버튼, Opacity 조절 칸이 있는 툴박스가 생성되었는지 확인")
def step_impl():
  Regions.BAT_02.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel)

@when("Report 탭 Data Table 패널의 Cube1 table의 Export 버튼을 누른다.")
def step_impl():
  raise NotImplementedError

@when("파일 탐색기에서 경로를 지정하고 ‘Cube1’ 파일을 저장한다.")
def step_impl():
  raise NotImplementedError

@when("Report 탭 Data Table 패널의 Cube2 table의 Export 버튼을 누른다.")
def step_impl():
  raise NotImplementedError

@when("파일 탐색기에서 경로를 지정하고 ‘Cube2’ 파일을 저장한다.")
def step_impl():
  raise NotImplementedError

@then("지정한 경로에 ‘Cube1.csv’, ‘Cube2.csv’ 파일이 생성됐는지 확인")
def step_impl():
  raise NotImplementedError

@when("Basic Analyzer 탭 위에 Measurement 팝업을 종료한다")
def step_impl():
  Aliases.TomoAnalysis.BasicAnalysisResultPanel2.Close()

@when("“S09_BasicAnalyzerRIT” 프로젝트 파일을 선택한다.")
def step_impl():
  dir4 =r'E:\Regression_Test\Scenario test_project sample\S09_BasicAnalyzerRIT\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Scenario test_project sample\S09_BasicAnalyzerRIT'
  dir2 = r'E:\Regression_Test\Scenario test_project sample\backup\S09_BasicAnalyzerRIT'
  copy_tree(dir2,dir1)
  dlgOpenProject = Aliases.TomoAnalysis.dlgOpenProject
  HWNDView = dlgOpenProject.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\Scenario test_project sample\\S09_BasicAnalyzerRIT\\S09_BasicAnalyzerRIT.tcpro")

@then("“S09_BasicAnalyzerRIT” 프로젝트가 TA에 열렸는지 확인")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.frame_tab.bt_round_tab7.ClickButton()

  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.h5, "QtText", cmpEqual, "E:/Regression_Test/Scenario test_project sample/S09_BasicAnalyzerRIT/S09_BasicAnalyzerRIT.tcpro")

@when("팝업에서 {arg} 파일을 더블클릭한다.")
def step_impl(param1):
  raise NotImplementedError

@when("팝업에서 RAW2647009 파일을 더블클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.widget_popup.scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget.panel_contents_image.DblClick(159, 93)

@when("팝업에서 RAW2647003 파일을 더블클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.widget_popup.scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget2.panel_contents_image.widget.panel_contents_image.Widget.DblClick(137, 69)

@when("Basic Analyzer  탭의 UL Threshold 탭의 Execute 버튼을 누른다.")
def step_impl():
  raise NotImplementedError

@then("Basic Analyzer 탭 RI Thresholding Mask Viewing panel에 마스크가 생성되었는지 확인")
def step_impl():
  Regions.viewerPanel1.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel)

@when("프로세싱 바 팝업의 진행이 완료됨")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  while   Aliases.TomoAnalysis.ProgressDialog.Exists :
   Delay(1000, "Waiting for window to close...");

@when("“S10_BasicAnalyzerT” 프로젝트 파일을 선택한다.")
def step_impl():
  dir4 =r'E:\Regression_Test\Scenario test_project sample\S10_BasicAnalyzerT\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Scenario test_project sample\S10_BasicAnalyzerT'
  dir2 = r'E:\Regression_Test\Scenario test_project sample\backup\S10_BasicAnalyzerT'
  copy_tree(dir2,dir1)
  dlgOpenProject = Aliases.TomoAnalysis.dlgOpenProject
  HWNDView = dlgOpenProject.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")

  dlgOpenProject.OpenFile("E:\\Regression_Test\Scenario test_project sample\\S10_BasicAnalyzerT\\S10_BasicAnalyzerT.tcpro")

@then("“S10_BasicAnalyzerT” 프로젝트 파일이 TA에 열렸는지 확인")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.frame_tab.bt_round_tab7.ClickButton()
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.h5, "QtText", cmpEqual, "E:/Regression_Test\Scenario test_project sample/S10_BasicAnalyzerT/S10_BasicAnalyzerT.tcpro")

@when("팝업에서 RAW LPS-24h-002P010 파일을 더블 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.widget_popup.scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget2.panel_contents_image.widget.panel_contents_image.Widget.DblClick(102, 89)

@then("Basic Analyzer[T] 탭 Time points setting 패널의 Selected time points에 ‘{arg},{arg},{arg},{arg},{arg},{arg},{arg},{arg},{arg},{arg},{arg},{arg},{arg},{arg},{arg},{arg},{arg}’이 나타나는지 확인")
def step_impl(param1, param2, param3, param4, param5, param6, param7, param8, param9, param10, param11, param12, param13, param14, param15, param16, param17):
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup.timePoints, "wText", cmpEqual, "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17")

@then("Basic Analyzer[T] 탭의 Mask Viewing panel에 마스크가 만들어졌는지 확인")
def step_impl():
  Regions.singleRunTab.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab)

@then("화면의 Basic Analyzer[T] Single Run 탭에서 Project Manager 탭으로 넘어갔는지 확인")
def step_impl():
  Regions.BDT_03.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.TC_TabBar)

@when("Batch Run setting 탭의 Time points setting 패널에서 end 칸에 {arg}을 입력한다.")
def step_impl(param1):
  spinBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.batchRunTab.batchParameterPanel.timeGroup.endTime
  spinBox.qt_spinbox_lineedit.Drag(18, 18, -30, 3)
  spinBox.Keys("17")
  spinBox.wValue = 17
