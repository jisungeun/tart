﻿Feature: Basic Analyzer AI Segmentation

  Scenario: Basic Analyzer의 AI Segmentation을 이용하여 마스크를 만들고 측정값을 Export
Given 빈 TA가 열려있다.

When Select Project 탭의 Open 버튼을 누른다.

And “S08_BasicAnalyzerAIS” 프로젝트 파일을 선택한다.

Then “S08_BasicAnalyzerAIS” 프로젝트가 TA에 열렸는지 확인

When BA Application Parameter 패널의 제일 위 드롭다운 버튼에서 AI Segmentation을 클릭한다.

And Single RUN 버튼을 누른다.

And 팝업에서 RAW2647003 파일을 더블클릭한다.

Then Basic Analyzer 탭이 열렸는지 확인

When Basic Analyzer 탭의 Processing mode 패널 AI Inference 탭의 Execute 버튼을 클릭한다.

Then Basic Analyzer 탭의 Mask Viewing panel에 마스크 두 개가 생성되었는지 확인

When Basic Analyzer 탭 Basic Measurement 탭의 Execute 버튼을 누른다.

Then Basic Analyzer 탭 위에 Measurement 팝업이 생기는지 확인한다.

When Basic Analyzer 탭에서 Hide Result 버튼을 누른다.

Then Basic Anlayzer 탭에서 Measurement 팝업이 사라졌는지 확인

And Basic Anlayzer 탭의 Hide Result 버튼이 Show Result 버튼으로 바뀌었는지 확인

When Basic Analyzer 탭의 Apply Parameter 버튼을 누른다.

Then TA에서 Basic Analyzer 탭이 Project Manager 탭으로 넘어갔는지 확인

When Batch Run 버튼을 누른다.

And File changes 팝업 Flush 탭의 Accept 버튼을 누른다.

And 프로세싱 바 팝업의 진행이 완료된다.

Then Report 탭이 열렸는지 확인

When Application tab 바의 Report 탭 이름 옆의 x 버튼을 클릭한다.

Then Report 탭이 종료되는지 확인