﻿Feature: Basic Analyzer RI Thresholding

  Scenario: Basic Analyzer의 RI Thresholding을 이용하여 RI에 기반한 마스크를 만들고 분석
Given 빈 TA가 열려있다.

When Select Project 탭의 Open 버튼을 누른다.

And “S09_BasicAnalyzerRIT” 프로젝트 파일을 선택한다.

Then “S09_BasicAnalyzerRIT” 프로젝트가 TA에 열렸는지 확인

When BA Application Parameter 패널의 제일 위 드롭다운 버튼에서 RI Thresholding을 클릭한다.

And BA Application Parameter 패널의 Single RUN 버튼을 누른다.

And 팝업에서 RAW2647009 파일을 더블클릭한다.

Then Basic Analyzer 탭이 나타났는지 확인

When Basic Analyzer 탭의 UL Threshold 탭의 Execute 버튼을 누른다.

Then Basic Analyzer 탭 RI Thresholding Mask Viewing panel에 마스크가 생성되었는지 확인

When Basic Analyzer 탭의 Apply Parameter 버튼을 클릭한다.

Then 화면의 Basic Analyzer 탭이 Project Manager 탭으로 넘어갔는지 확인

When BA Application Parameter 패널에서 Batch Run 버튼을 누른다.

#And File changes 팝업 Flush 탭의 Accept 버튼을 누른다.

And 프로세싱 바 팝업의 진행이 완료됨

Then Report 탭이 나타났는지 확인

When Application tab 바의 Report 탭 이름 옆의 x 버튼을 클릭한다.

Then Report 탭이 종료되는지 확인