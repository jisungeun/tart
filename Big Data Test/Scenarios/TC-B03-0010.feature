﻿Feature: Basic Analyzer[T] AI Segmentation

  Scenario: Basic Analyzer[T]의 AI Segmentation을 이용하여 Time-Lapse 데이터에서 마스크를 만들고분석
Given 빈 TA가 열려있다.

When Select Project 탭의 Open 버튼을 누른다.

And “S10_BasicAnalyzerT” 프로젝트 파일을 선택한다.

Then “S10_BasicAnalyzerT” 프로젝트 파일이 TA에 열렸는지 확인

When BAT Application Parameter의 Single RUN 버튼을 누른다.

And 팝업에서 RAW LPS-24h-002P010 파일을 더블 클릭한다.

Then Basic Analyzer[T] 탭이 열리는지 확인

And Basic Analyzer[T] 탭 Time points setting 패널의 Selected time points에 ‘1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17’이 나타나는지 확인

When Time points가 지정된 Time points setting 패널에서 Apply time points 버튼을 누른다.

And Basic Analyzer[T] 탭에서 AI Inference 탭의 Execute 버튼을 클릭한다.

And 프로세싱 바 팝업이 생기면 진행이 완료되고 팝업이 사라질 때 까지 기다린다.

Then Basic Analyzer[T] 탭의 Mask Viewing panel에 마스크가 만들어졌는지 확인

When Basic Analyzer[T] 탭 Basic Measurement 탭의 Execute 버튼을 누른다.

And 프로세싱 바 팝업이 전부 진행될 때 까지 기다린다.

Then Basic Analyzer[T] 탭에서 프로세싱 바 팝업이 사라지고 Measurement 팝업이 생기는지 확인

When Basic Analyzer[T] 탭에서 Hide Result 버튼을 누른다.

Then Basic Analyzer[T] 탭 위의 Measurement 팝업이 없어졌는지 확인

And Basic Analyzer[T] 탭의 Hide Result 버튼이 Show Result 버튼으로 변경됐는지 확인

When Basic Analyzer[T] Single Run 탭에서 Apply Parameter 버튼을 누른다.

Then 화면의 Basic Analyzer[T] Single Run 탭에서 Project Manager 탭으로 넘어갔는지 확인

When BAT Application Parameter 패널에서 Batch RUN 버튼을 누른다.

Then Basic Analyzer[T] 탭에서 Batch Run Setting 탭이 열렸는지 확인

When Batch Run setting 탭의 Time points setting 패널에서 start 칸에 '1'을 입력한다.

And Batch Run setting 탭의 Time points setting 패널에서 end 칸에 '17'을 입력한다.

And Batch Run setting 탭의 Time points setting 패널에서 interval 칸에 '1'을 입력한다.

And Batch Run setting 탭에서 Time points setting 패널의 Apply time points to all data 버튼을 누른다.

And Basic Analyzer[T] Batch Run setting 탭의 Execute Whole 버튼을 누른다.

And File changes 팝업 Flush 탭의 Accept 버튼을 누른다.

And Batch Run setting 탭 위에 프로세싱 바 팝업이 나타나면 진행이 완료될 때 까지 기다린다.

Then 프로세싱 바 팝업과 Basic Analyzer[T] 탭이 사라지고 Report[T] 탭에 그래프와 table이 화면에 나타나는지 확인한다

When Application tab 바의 Report[T] 탭 이름 옆의 x 버튼을 클릭한다.

Then Report[T] 탭이 종료되는지 확인