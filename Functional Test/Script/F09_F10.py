﻿import shutil
import os.path
from distutils.dir_util import copy_tree
import time
import glob

@given("Playground canvas에서 Basic Analyzer 아이콘이 선택되어 있다.")
def step_impl():

  tomoAnalysis = Aliases.TomoAnalysis
  dir = r'E:\Regression_Test\Prefix_File\BA Application Parameter\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir3 = r'E:\Regression_Test\Prefix_File\BA Application Parameter\playground\test\Hypercube\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)
  dir4 = r'E:\Regression_Test\Prefix_File\BA Application Parameter\playground\test\Hypercube\*.rep'
  for f in glob.glob(dir4):
      os.remove(f)

  dir1 = r'E:\Regression_Test\Prefix_File\BA Application Parameter'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BA Application Parameter'
  copy_tree(dir2,dir1) 
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(137, 27)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BA Application Parameter\\BA Application Parameter.tcpro")

@given("Application Parameter 패널에 AI Segmentation이 선택되어 있다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.procCombo.ClickItem("AI Segmentation")

@when("Application Parameter 패널 SELECT PROCESSOR의 드롭다운 버튼을 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.procCombo.DropDown()

@when("RI Thresholding을 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.procCombo.ClickItem("RI Thresholding")

@then("Application Parameter의 UI가 달라지는지 확인1")
def step_impl():
  Regions.panel_contents.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@given("Application Parameter 패널에 RI Thresholding이 선택되어 있다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.procCombo.ClickItem("RI Thresholding")

@when("AI Segmentation을 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.procCombo.ClickItem("AI Segmentation")

@then("Application Parameter의 UI가 달라지는지 확인2")
def step_impl():
  Regions.panel_contents1.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
  
@when("Application Parameter 패널 SELECT PROCESSOR의 드롭다운 버튼에 마우스 포인터를 대고 마우스 휠을 아래로 내린다.")
def step_impl():
  frame = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame
  frame.h8.Click(129, 5)
  comboBox = frame.procCombo
  comboBox.DropDown()
  comboBox.CloseUp()
  comboBox.MouseWheel(-1)


@then("Application Parameter 패널 SELECT PROCESSOR에서 RI Thresholding이 선택되는지 확인")
def step_impl():
  Regions.panel_contents2.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents)

@then("Application Parameter의 UI가 달라지는지 확인")
def step_impl():
  Regions.panel_contents3.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents)



@when("Application Parameter 패널 SELECT PROCESSOR의 드롭다운 버튼에 마우스 포인터를 대고 마우스 휠을 위로 올린다.")
def step_impl():
  frame = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame
  frame.h8.Click(129, 5)
  comboBox = frame.procCombo
  comboBox.DropDown()
  comboBox.CloseUp()
  comboBox.MouseWheel(1)
  
@then("Application Parameter 패널 SELECT PROCESSOR에서 AI Segmentation이 선택되는지 확인")
def step_impl():
  Regions.panel_contents4.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents)

@given("BA Application Parameter에서 AI Segmentation이 프로세서로 선택되어 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()  
  tomoAnalysis = Aliases.TomoAnalysis
  dir = r'E:\Regression_Test\Prefix_File\BA Application Parameter\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir3 = r'E:\Regression_Test\Prefix_File\BA Application Parameter\playground\test\Hypercube\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)
  dir4 = r'E:\Regression_Test\Prefix_File\BA Application Parameter\playground\test\Hypercube\*.rep'
  for f in glob.glob(dir4):
      os.remove(f)
  dir1 = r'E:\Regression_Test\Prefix_File\BA Application Parameter'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BA Application Parameter'
  copy_tree(dir2,dir1) 
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(137, 27)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BA Application Parameter\\BA Application Parameter.tcpro")

@when("AI Segmentation을 프로세서로 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.procCombo.ClickItem("AI Segmentation")

@then("Basic Measurement의 table의 Preset row가 왼쪽부터 순서대로 Protein, Protein, Protein, Lipid로 선택되어 있는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.QtObject("Preset_0"), "wText", cmpEqual, "Protein")
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.QtObject("Preset_1"), "wText", cmpEqual, "Protein")
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.QtObject("Preset_2"), "wText", cmpEqual, "Protein")
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.QtObject("Preset_3"), "wText", cmpEqual, "Lipid")


@when("Application Parameter 패널 Whole cell column의 Preset 드롭다운 버튼에서 Protein을 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.QtObject("Preset_0").ClickItem("Protein")

@when("Application Parameter 패널 Nucleus column의 Preset 드롭다운 버튼에서 Protein을 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.QtObject("Preset_1").ClickItem("Protein")

@when("Application Parameter 패널 Nucleolus column의 Preset 드롭다운 버튼에서 Protein을 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.QtObject("Preset_2").ClickItem("Protein")

@when("Application Parameter 패널 Lipid droplet column의 Preset 드롭다운 버튼에서 Protein을 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.QtObject("Preset_3").ClickItem("Protein")

@then("네 column에서 모두 Baseline RI 값이 1.3337로, RII값이 0.19로 입력되는지 확인")
def step_impl():
  Tables.TableWidget46.Check()
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
  
@when("Application Parameter 패널 Whole cell column의 Preset 드롭다운 버튼에서 Hemoglobin을 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.QtObject("Preset_0").ClickItem("Hemoglobin")

@when("Application Parameter 패널 Nucleus column의 Preset 드롭다운 버튼에서 Hemoglobin을 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.QtObject("Preset_1").ClickItem("Hemoglobin")

@when("Application Parameter 패널 Nucleolus column의 Preset 드롭다운 버튼에서 Hemoglobin을 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.QtObject("Preset_2").ClickItem("Hemoglobin")

@when("Application Parameter 패널 Lipid droplet column의 Preset 드롭다운 버튼에서 Hemoglobin을 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.QtObject("Preset_3").ClickItem("Hemoglobin")

@then("네 column에서 모두 Baseline RI 값이 1.3337로, RII값이 0.15로 입력되는지 확인")
def step_impl():
  Tables.TableWidget47.Check()
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@when("Application Parameter 패널 Whole cell column의 Preset 드롭다운 버튼에서 Lipid를 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.QtObject("Preset_0").ClickItem("Lipid")

@when("Application Parameter 패널 Nucleus column의 Preset 드롭다운 버튼에서 Lipid를 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.QtObject("Preset_1").ClickItem("Lipid")

@when("Application Parameter 패널 Nucleolus column의 Preset 드롭다운 버튼에서 Lipid를 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.QtObject("Preset_2").ClickItem("Lipid")

@when("Application Parameter 패널 Lipid droplet column의 Preset 드롭다운 버튼에서 Lipid를 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.QtObject("Preset_3").ClickItem("Lipid")

@then("네 column에서 모두 Baseline RI 값이 1.3337로, RII값이 0.135로 입력되는지 확인")
def step_impl():
  Tables.TableWidget48.Check()
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)


@when("Application Parameter 패널 Basic Measurement의 table에서 모든 column의 Preset 드롭다운 버튼을 Customize로 선택한다.")
def step_impl():
  widget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport
  widget.Preset_Setter_0.ClickItem("Customize")
  widget.Preset_Setter_1.ClickItem("Customize")
  widget.Preset_Setter_2.ClickItem("Customize")
  widget.Preset_Setter_3.ClickItem("Customize")

@when("Baseline RI row의 cell을 클릭하고 키보드로 값을 직접 입력한 후 엔터키를 누른다.")
def step_impl():
  tableWidget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget
  tableWidget.ClickCell("Baseline RI", "Whole cell")
  tableWidget.Keys("1")
  lineEdit = tableWidget.qt_scrollarea_viewport.LineEdit
  lineEdit.Keys("[Enter]")
  tableWidget.ClickCell("Baseline RI", "Nucleus")
  tableWidget.Keys("2")
  lineEdit.Keys("[Enter]")
  tableWidget.ClickCell("Baseline RI", "Nucleolus")
  tableWidget.Keys("3")
  lineEdit.Keys("[Enter]")
  tableWidget.ClickCell("Baseline RI", "Lipid drolet")
  tableWidget.Keys("4")
  lineEdit.Keys("[Enter]")

@when("RII row의 cell을 클릭하고 키보드로 값을 직접 입력한 후 엔터키를 누른다.")
def step_impl():
  tableWidget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget
  tableWidget.ClickCell("RII", "Whole cell")
  tableWidget.Keys("1")
  lineEdit = tableWidget.qt_scrollarea_viewport.LineEdit
  lineEdit.Keys("[Enter]")
  tableWidget.ClickCell("RII", "Nucleus")
  tableWidget.Keys("2")
  lineEdit.Keys("[Enter]")
  tableWidget.ClickCell("RII", "Nucleolus")
  tableWidget.Keys("3")
  lineEdit.Keys("[Enter]")
  tableWidget.ClickCell("RII", "Lipid drolet")
  tableWidget.Keys("4")
  lineEdit.Keys("[Enter]")

@then("네 column에서 모두 Baseline RI 값과 RII값이 직접 입력한 값으로 변경되는지 확인")
def step_impl():
  Tables.TableWidget5.Check()
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@given("BA Application Parameter에서 RI Thresholding이 프로세서로 선택되어 있다.")
def step_impl():

  tomoAnalysis = Aliases.TomoAnalysis
  dir = r'E:\Regression_Test\Prefix_File\BA Application Parameter\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir3 = r'E:\Regression_Test\Prefix_File\BA Application Parameter\playground\test\Hypercube\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)
  dir4 = r'E:\Regression_Test\Prefix_File\BA Application Parameter\playground\test\Hypercube\*.rep'
  for f in glob.glob(dir4):
      os.remove(f)
  dir1 = r'E:\Regression_Test\Prefix_File\BA Application Parameter'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BA Application Parameter'
  copy_tree(dir2,dir1) 
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(137, 27)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BA Application Parameter\\BA Application Parameter.tcpro")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.procCombo.ClickItem("RI Thresholding")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 203
  comboBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.procCombo
  comboBox.DropDown()
  comboBox.CloseUp()


@when("‘Assign lower threshold’ 의 사이즈 바를 마우스로 왼쪽 끝까지 움직인다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl.ScalarUi.valueSlider.wPosition = 1
  scrollBar.wPosition = 0
  scrollBar.wPosition = 0
  scrollBar.wPosition = 0

@given("UL Threshold 버튼 맨 오른쪽의 화살표가 윗 방향\\(△)이다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea

  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  Regions.processorParamFrame.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame)

@when("UL Threshold 버튼을 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.panel_contents.bt_collapse2.CheckButton(cbUnchecked)
  Regions.processorParamFrame1.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame)

@then("UL Threshold 밑의 UI가 접히는지 확인")
def step_impl():
  Regions.processorParamFrame2.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame)

@then("UL Threshold 버튼 맨 오른쪽의 화살표가 아래 방향\\(▽)으로 바뀌는지 확인")
def step_impl():
  Regions.bt_collapse.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.panel_contents.bt_collapse2)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@then("‘Assign lower threshold’ 사이즈 바 옆의 수치가 1.0010으로 바뀌었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "1.0010")

@when("‘Assign lower threshold’ 의 사이즈 바를 마우스로 오른쪽 끝까지 움직인다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl.ScalarUi.valueSlider.wPosition = 438.4
  scrollBar.wPosition = 0

@then("‘Assign lower threshold’ 사이즈 바 옆의 수치가 2.0000으로 바뀌었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "2.0000")
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@when("‘Assign upper threshold’ 의 사이즈 바를 마우스로 왼쪽 끝까지 움직인다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl.ScalarUi2.valueSlider.wPosition = 358.4
  scrollBar.wPosition = 0

@then("‘Assign upper threshold’ 사이즈 바 옆의 수치가 1.0010으로 바뀌었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl.ScalarUi2.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "1.0010")

@when("‘Assign upper threshold’ 의 사이즈 바를 마우스로 오른쪽 끝까지 움직인다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl.ScalarUi2.valueSlider.wPosition = 1000
  scrollBar.wPosition = 0

@then("‘Assign upper threshold’ 사이즈 바 옆의 수치가 2.0000으로 바뀌었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl.ScalarUi2.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "2.0000")
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@when("‘Assign lower threshold’ 사이즈 바 옆의 칸에 값을 입력한다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  scrollBar.wPosition = 0
  scrollBar.wPosition = 0
  doubleSpinBox = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin
  lineEdit = doubleSpinBox.qt_spinbox_lineedit
  lineEdit.DblClick(27, 16)
  scrollBar.wPosition = 0
  scrollBar.wPosition = 0
  lineEdit.Drag(47, 11, -46, 6)
  doubleSpinBox.Keys("1")
  doubleSpinBox.wValue = 1

@when("‘Assign upper threshold’ 사이즈 바 옆의 칸에 값을 입력한다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  scrollBar.wPosition = 0
  doubleSpinBox = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl.ScalarUi2.valueSpin
  doubleSpinBox.qt_spinbox_lineedit.Drag(42, 14, -45, 0)
  doubleSpinBox.Keys("2")
  doubleSpinBox.wValue = 2

@then("입력한 값 대로 수치가 변경되었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl.ScalarUi2.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "2.0000")

@then("사이즈 바가 값에 따라 이동했는지 확인")
def step_impl():
  Regions.BA1.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@when("‘Assign lower threshold’ 수치 칸 옆의 윗 방향 화살표를 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin.wValue = 1.3501000000000001

@then("1과 {arg} 사이에서 ‘Assign lower threshold’의 수치가 올라가는지 확인")
def step_impl(param1):
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "1.3501")
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@when("‘Assign upper threshold’ 수치 칸 옆의 윗 방향 화살표를 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl.ScalarUi2.valueSpin.wValue = 1.4400999999999999

@then("1과 {arg} 사이에서  ‘Assign upper threshold’ 의 수치가 올라가는지 확인")
def step_impl(param1):
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl.ScalarUi2.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "1.4401")
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@when("‘Assign lower threshold’ 수치 칸 옆의 아래 방향 화살표를 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin.wValue = 1.3499000000000001

@then("1과 {arg} 사이에서 ‘Assign lower threshold’ 의 수치가 내려가는지 확인")
def step_impl(param1):
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "1.3499")
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@when("‘Assign upper threshold’ 수치 칸 옆의 아래 방향 화살표를 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl.ScalarUi2.valueSpin.wValue = 1.4399

@then("1과 {arg} 사이에서 ‘Assign upper threshold’ 의 수치가 내려가는지 확인")
def step_impl(param1):
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl.ScalarUi2.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "1.4399")
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@when("‘Assign lower threshold’의 UI에 마우스 포인터를 대고 마우스 휠을 위로 올린다.")
def step_impl():
  TC_CollapseWidget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4
  TC_CollapseWidget.panel_contents.bt_collapse.MouseWheel(1)
  TC_ParameterControl = TC_CollapseWidget.contentsFrame.TC_ParameterControl
  TC_ParameterControl.Label.MouseWheel(1)
  TC_ParameterControl.ScalarUi.valueSpin.wValue = 1.3501000000000001

@when("‘Assign upper threshold’ 의 UI에 마우스 포인터를 대고 마우스 휠을 위로 올린다.")
def step_impl():
  TC_ParameterControl = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl
  vlabel = TC_ParameterControl.Label2
  vlabel.MouseWheel(2)
  doubleSpinBox = TC_ParameterControl.ScalarUi2.valueSpin
  doubleSpinBox.wValue = 1.4401999999999999
  vlabel.MouseWheel(-1)
  doubleSpinBox.wValue = 1.4400999999999999

@then("1과 {arg} 사이에서 ‘Assign lower threshold’, ‘Assign upper threshold’ 의 수치가 올라가는지 확인")
def step_impl(param1):
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "1.3501")
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl.ScalarUi2.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "1.4401")
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@when("‘Assign lower threshold’의 UI에 마우스 포인터를 대고 마우스 휠을 아래로 내린다.")
def step_impl():
  TC_ParameterControl = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl
  TC_ParameterControl.Label.MouseWheel(-1)
  TC_ParameterControl.ScalarUi.valueSpin.wValue = 1.3499000000000001

@when("‘Assign upper threshold’ 의 UI에 마우스 포인터를 대고 마우스 휠을 아래로 내린다.")
def step_impl():
  TC_ParameterControl = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl
  TC_ParameterControl.Label2.MouseWheel(-1)
  TC_ParameterControl.ScalarUi2.valueSpin.wValue = 1.4399

@then("1과 {arg} 사이에서 ‘Assign lower threshold’, ‘Assign upper threshold’ 의 수치가 내려가는지 확인")
def step_impl(param1):
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "1.3499")
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl.ScalarUi2.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "1.4399")
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@given("Auto Threshold 버튼 맨 오른쪽의 화살표가 윗 방향\\(△)이다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  buttonAutoThreshold = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.panel_contents.bt_collapse2
  buttonAutoThreshold.ClickButton()
  scrollBar.wPosition = 0
  buttonAutoThreshold.ClickButton()
  Regions.bt_collapse32.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.panel_contents.bt_collapse)

@when("Auto Threshold 버튼을 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 26
  scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.panel_contents.bt_collapse2.ClickButton()

@then("Auto Threshold 밑의 체크박스가 접히는지 확인")
def step_impl():
  Regions.processorParamFrame3.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame)

@then("Auto Threshold 버튼 맨 오른쪽의 화살표가 아래 방향\\(▽)으로 바뀌는지 확인")
def step_impl():
  Regions.bt_collapse3.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.panel_contents.bt_collapse)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@given("Auto Threshold 버튼 맨 오른쪽의 화살표가 아래 방향\\(▽)이다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 26
  scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.panel_contents.bt_collapse.CheckButton(cbUnchecked)

@then("Auto Threshold 밑의 체크박스가 펼쳐지는지 확인")
def step_impl():
  Regions.processorParamFrame4.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame)

@then("Auto Threshold 버튼 맨 오른쪽의 화살표가 윗 방향\\(△)으로 바뀌는지 확인")
def step_impl():
  Regions.bt_collapse4.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.panel_contents.bt_collapse)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@given("BA Application Parameter 패널의 Auto lower threshold 체크박스가 비어있다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 140
  Regions.BA2.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.TC_StrCheckBox)


@when("Auto lower Threshold 체크박스를 클릭한다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 30
  scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParamControl.TC_StrCheckBox.Click(10, 9)

@then("‘Select Algorithm’ 문구와 알고리즘 드롭다운 버튼이 나타나는지 확인")
def step_impl():
  Regions.CollapseWidget.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget)

@then("Auto lower Threshold 체크박스가 하이라이트 되며 체크 되는지 확인")
def step_impl():
  Regions.BA3.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.TC_StrCheckBox)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@given("BA Application Parameter에서 Auto lower Threshold 체크박스가 체크되어 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()  
  tomoAnalysis = Aliases.TomoAnalysis
  dir = r'E:\Regression_Test\Prefix_File\BA Application Parameter\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir3 = r'E:\Regression_Test\Prefix_File\BA Application Parameter\playground\test\Hypercube\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)
  dir4 = r'E:\Regression_Test\Prefix_File\BA Application Parameter\playground\test\Hypercube\*.rep'
  for f in glob.glob(dir4):
      os.remove(f)
  dir1 = r'E:\Regression_Test\Prefix_File\BA Application Parameter'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BA Application Parameter'
  copy_tree(dir2,dir1) 
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(137, 27)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BA Application Parameter\\BA Application Parameter.tcpro")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.procCombo.ClickItem("RI Thresholding")
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.TC_StrCheckBox.Click(11, 13)



@then("‘Select Algorithm’ 문구와 알고리즘 드롭다운 버튼이 사라지는지 확인")
def step_impl():
  Regions.CollapseWidget1.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget)

@then("Auto lower Threshold 체크박스가  체크 해제 되는지 확인")
def step_impl():
  Regions.BA4.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.TC_StrCheckBox)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@when("Select Algorithm 드롭다운 버튼에서 Entropy를 선택한다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 86
  scrollArea2 = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea
  scrollArea2.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea2.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.StrComboBox.strCombo.ClickItem("Entropy")

@then("BA Application Parameter 패널 Auto Threshold 탭의 Select Algorithm 버튼에 Entropy가 선택되었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.StrComboBox.strCombo, "wText", cmpEqual, "Entropy")


@when("Select Algorithm 드롭다운 버튼에서 Moments Preserving을 선택한다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 86
  scrollArea2 = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea
  scrollArea2.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea2.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.StrComboBox.strCombo.ClickItem("Moment Preserving")

@then("BA Application Parameter 패널 Auto Threshold 탭의 Select Algorithm 버튼에 Moments Preserving이 선택되었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.StrComboBox.strCombo, "wText", cmpEqual, "Moment Preserving")

@given("BA Application Parameter 패널 Auto Threshold 탭의 Select Algorithm 버튼에 Moments Preserving이 선택되어 있다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea2 = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea
  TC_CollapseWidget = scrollArea2.qt_scrollarea_viewport.Widget.CollapseWidget
  TC_CollapseWidget.panel_contents.bt_collapse2.MouseWheel(-1)
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea2.qt_scrollarea_vcontainer.ScrollBar.wPosition = 60
  TC_CollapseWidget.contentsFrame.TC_ParameterControl.StrComboBox.strCombo.ClickItem("Moment Preserving")

@when("Select Algorithm 드롭다운 버튼에서 Otsu를 선택한다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea2 = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea
  scrollArea2.qt_scrollarea_vcontainer.ScrollBar.wPosition = 60
  scrollArea2.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.StrComboBox.strCombo.ClickItem("Otsu")

@then("BA Application Parameter 패널 Auto Threshold 탭의 Select Algorithm 버튼에 Otsu가 선택되었는지 확인")
def step_impl():
  Regions.BA6.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@given("BA Application Parameter 패널 Auto Threshold 탭의 Select Algorithm 버튼에 Otsu가 선택되어 있다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea2 = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea
  widget = scrollArea2.qt_scrollarea_viewport.Widget
  widget.CollapseWidget4.contentsFrame.TC_ParameterControl.ScalarUi2.valueSlider.MouseWheel(-1)
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea2.qt_scrollarea_vcontainer.ScrollBar.wPosition = 60
  widget.CollapseWidget.contentsFrame.TC_ParameterControl.StrComboBox.strCombo.ClickItem("Otsu")

@when("Select Algorithm 드롭다운 버튼에 마우스 포인터를 올리고 마우스 휠을 아래로 한 칸 내린다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea2 = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea
  TC_CollapseWidget = scrollArea2.qt_scrollarea_viewport.Widget.CollapseWidget
  TC_CollapseWidget.panel_contents.bt_collapse2.MouseWheel(-1)
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  scrollBar2 = scrollArea2.qt_scrollarea_vcontainer.ScrollBar
  scrollBar2.wPosition = 60
  TC_ParameterControl = TC_CollapseWidget.contentsFrame.TC_ParameterControl
  comboBox = TC_ParameterControl.StrComboBox.strCombo
  comboBox.DropDown()
  scrollBar.wPosition = 0
  scrollBar2.wPosition = 60
  comboBox.CloseUp()
  comboBox.MouseWheel(-1)

@given("BA Application Parameter 패널 Auto Threshold 탭의 Select Algorithm 버튼에 Entropy가 선택되어 있다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea2 = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea
  scrollArea2.qt_scrollarea_vcontainer.ScrollBar.wPosition = 60
  scrollArea2.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.StrComboBox.strCombo.ClickItem("Entropy")

@when("Select Algorithm 드롭다운 버튼에 마우스 포인터를 올리고 마우스 휠을 아래로 내린다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  scrollArea2 = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea
  scrollBar2 = scrollArea2.qt_scrollarea_vcontainer.ScrollBar
  scrollBar2.wPosition = 60
  TC_ParameterControl = scrollArea2.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl
  TC_StrComboBox = TC_ParameterControl.StrComboBox
  TC_StrComboBox.Click(108, 5)
  scrollBar.wPosition = 0
  scrollBar2.wPosition = 60
  comboBox = TC_StrComboBox.strCombo
  comboBox.DropDown()
  scrollBar.wPosition = 0
  scrollBar2.wPosition = 60
  comboBox.CloseUp()
  comboBox.MouseWheel(-1)

@when("Select Algorithm 드롭다운 버튼에 마우스 포인터를 올리고 마우스 휠을 위로 두 칸 이상 올린다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  scrollArea2 = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea
  scrollBar2 = scrollArea2.qt_scrollarea_vcontainer.ScrollBar
  scrollBar2.wPosition = 0
  widget = scrollArea2.qt_scrollarea_viewport.Widget
  TC_CollapseWidget = widget.CollapseWidget
  TC_ParameterControl = TC_CollapseWidget.contentsFrame.TC_ParameterControl
  vlabel = TC_ParameterControl.Label
  vlabel.Click(184, 0)
  TC_CollapseWidget.MouseWheel(1)
  TC_CollapseWidget.MouseWheel(1)
  TC_CollapseWidget.MouseWheel(1)
  widget.CollapseWidget4.contentsFrame.TC_ParameterControl.MouseWheel(-1)
  TC_ParameterControl.TC_StrCheckBox.MouseWheel(-1)
  scrollBar.wPosition = 0
  scrollBar2.wPosition = 78
  comboBox = TC_ParameterControl.StrComboBox.strCombo
  comboBox.DropDown()
  scrollBar.wPosition = 0
  scrollBar2.wPosition = 78
  comboBox.CloseUp()
  comboBox.MouseWheel(1)
  comboBox.MouseWheel(3)


@given("Labeling 버튼 맨 오른쪽의 화살표가 윗 방향\\(△)이다.")
def step_impl():
  Regions.bt_collapse5.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.panel_contents.bt_collapse2)

@when("버튼을 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.panel_contents.bt_collapse.ClickButton()

@then("Labeling 밑의 체크박스가 접히는지 확인")
def step_impl():
  Regions.bt_collapse6.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.panel_contents.bt_collapse)

@then("버튼 맨 오른쪽의 화살표가 아래 방향\\(▽)으로 바뀌는지 확인")
def step_impl():
  Regions.bt_collapse57.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.panel_contents.bt_collapse2)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
 
@given("BA Application Parameter 패널의 Use Labeling 체크박스가 비어있다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.panel_contents.bt_collapse.MouseWheel(-1)
  Regions.CollapseWidget2.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3)


@when("Use Labeling 체크박스를 클릭한다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  widget = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget
  widget.CollapseWidget.panel_contents.bt_collapse.MouseWheel(-1)
  widget.CollapseWidget.panel_contents.bt_collapse.MouseWheel(-1)
  widget.CollapseWidget.panel_contents.bt_collapse.MouseWheel(-1)
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 60
  widget.CollapseWidget3.contentsFrame.TC_ParameterControl.TC_StrCheckBox.Click(11, 8)

@then("‘Select Option’ 문구와 옵션 드롭다운 버튼이 나타나는지 확인")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  widget = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget
  widget.CollapseWidget.panel_contents.bt_collapse.MouseWheel(-1)
  widget.CollapseWidget.panel_contents.bt_collapse.MouseWheel(-1)
  widget.CollapseWidget.panel_contents.bt_collapse.MouseWheel(-1)
  Regions.Label.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.Label)

@then("‘Assign Size of Neglectable Particle’ 문구와 사이즈 바, 수치 입력 칸이 나타나는지 확인")
def step_impl():
  Regions.BA11.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl)

@then("Use Labeling 체크박스가 하이라이트 되며 체크 되는지 확인")
def step_impl():
  Regions.BA12.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.TC_StrCheckBox)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@given("BA Application Parameter에서 Use Labeling 체크박스가 체크되어 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()  
  tomoAnalysis = Aliases.TomoAnalysis
  dir = r'E:\Regression_Test\Prefix_File\BA Application Parameter\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir3 = r'E:\Regression_Test\Prefix_File\BA Application Parameter\playground\test\Hypercube\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)
  dir4 = r'E:\Regression_Test\Prefix_File\BA Application Parameter\playground\test\Hypercube\*.rep'
  for f in glob.glob(dir4):
      os.remove(f)
  dir1 = r'E:\Regression_Test\Prefix_File\BA Application Parameter'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BA Application Parameter'
  copy_tree(dir2,dir1) 
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(137, 27)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BA Application Parameter\\BA Application Parameter.tcpro")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.procCombo.ClickItem("RI Thresholding")
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  widget = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget
  widget.CollapseWidget.panel_contents.bt_collapse.MouseWheel(-1)
  widget.CollapseWidget3.contentsFrame.TC_ParameterControl.TC_StrCheckBox.Click(11, 8)



@then("‘Select Option’ 문구와 옵션 드롭다운 버튼이 사라지는지 확인")
def step_impl():
  Regions.CollapseWidget3.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3)

@then("‘Assign Size of Neglectable Particle’ 문구와 사이즈 바, 수치 입력 칸이 사라지는지 확인")
def step_impl():
  Regions.CollapseWidget4.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3)
  Regions.CollapseWidget5.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3)

@then("Use Labeling 체크박스가  체크 해제 되는지 확인")
def step_impl():
  Regions.BA13.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.TC_StrCheckBox)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@when("BA Application Parameter 패널의 Select Option 드롭다운 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  scrollArea = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 60
  scrollArea2 = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea
  scrollArea2.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  
@when("Largest Label 버튼을 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 150
  scrollArea2 = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea
  scrollArea2.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea2.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.StrComboBox.strCombo.ClickItem("Largest Label")

@then("BA Application Parameter 패널의 Select Option 버튼이 Largest Label 옵션으로 변경되었는지 확인")
def step_impl():
  Regions.strCombo.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.StrComboBox.strCombo)

  
@when("Multi Labels 버튼을 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 150
  scrollArea2 = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea
  scrollArea2.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea2.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.StrComboBox.strCombo.ClickItem("Multi Labels")

@then("BA Application Parameter 패널의 Select Option 버튼이 Multi Labels 옵션으로 변경되었는지 확인")
def step_impl():
  Regions.StrComboBox1.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.StrComboBox)

  
@given("BA Application Parameter 패널의 Select Option 버튼에 Multi Labels가 선택되어 있다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 150
  scrollArea2 = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea
  scrollArea2.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea2.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.StrComboBox.strCombo.ClickItem("Multi Labels")

@when("Select Option 드롭다운 버튼에 마우스 포인터를 올리고 마우스 휠을 아래로 내린다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 150
  scrollArea2 = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea
  scrollBar2 = scrollArea2.qt_scrollarea_vcontainer.ScrollBar
  scrollBar2.wPosition = 0
  TC_ParameterControl = scrollArea2.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl
  comboBox = TC_ParameterControl.StrComboBox.strCombo
  comboBox.DropDown()
  scrollBar.wPosition = 150
  scrollBar2.wPosition = 0
  comboBox.CloseUp()
  vlabel = TC_ParameterControl.Label
  comboBox.MouseWheel(-2)
  comboBox.MouseWheel(-1)

@given("BA Application Parameter 패널의 Select Option 버튼에 Largest Label가 선택되어 있다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 150
  scrollArea2 = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea
  scrollArea2.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea2.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.StrComboBox.strCombo.ClickItem("Largest Label")

@when("Select Option 드롭다운 버튼에 마우스 포인터를 올리고 마우스 휠을 위로 올린다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 150
  scrollArea2 = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea
  scrollBar2 = scrollArea2.qt_scrollarea_vcontainer.ScrollBar
  scrollBar2.wPosition = 0
  TC_ParameterControl = scrollArea2.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl
  vlabel = TC_ParameterControl.Label
  vlabel.Click(100, 17)
  scrollBar.wPosition = 150
  scrollBar2.wPosition = 0
  comboBox = TC_ParameterControl.StrComboBox.strCombo
  comboBox.DropDown()
  scrollBar.wPosition = 150
  scrollBar2.wPosition = 0
  comboBox.CloseUp()
  comboBox.MouseWheel(5)

@when("‘Assign Size of Neglectable Particle’ 밑의 사이즈 바를 마우스로 왼쪽 끝으로 움직인다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 150
  scrollArea2 = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea
  scrollBar2 = scrollArea2.qt_scrollarea_vcontainer.ScrollBar
  scrollBar2.wPosition = 0
  scrollArea2.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.ScalarUi.valueSlider.wPosition = 1
  scrollBar.wPosition = 150
  scrollBar2.wPosition = 0

@then("사이즈 바 옆의 수치가 0.1001로 변경되었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "0.1001")

@when("‘Assign Size of Neglectable Particle’ 밑의 사이즈 바를 마우스로 오른쪽 끝으로 움직인다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 150
  scrollArea2 = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea
  scrollBar2 = scrollArea2.qt_scrollarea_vcontainer.ScrollBar
  scrollBar2.wPosition = 0
  scrollArea2.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.ScalarUi.valueSlider.wPosition = 1000
  scrollBar.wPosition = 150
  scrollBar2.wPosition = 0

@then("사이즈 바 옆의 수치가 100.0000으로 변경되었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "100.0000")
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)


@when("‘Assign Size of Neglectable Particle’ 수치 칸 옆의 윗 방향 화살표를 한 번 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 150
  scrollArea2 = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea
  scrollArea2.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea2.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin.wValue = 25.0001

@then("‘Assign Size of Neglectable Particle’ 값이 25.0001로 바뀌는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "25.0001")


@when("‘Assign Size of Neglectable Particle’ 수치 칸 옆의 아래 방향 화살표를 한 번 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 150
  scrollArea2 = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea
  scrollArea2.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea2.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin.wValue = 24.9999

@then("‘Assign Size of Neglectable Particle’ 값이 24.9999로 바뀌는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "24.9999")
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@when("‘Assign Size of Neglectable Particle’ 의 UI에 마우스 포인터를 대고 마우스 휠을 위로 한 칸 올린다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 150
  scrollArea2 = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea
  scrollArea2.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  TC_ParameterControl = scrollArea2.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl
  doubleSpinBox = TC_ParameterControl.ScalarUi.valueSpin
  doubleSpinBox.qt_spinbox_lineedit.Click(27, 12)
  vlabel = TC_ParameterControl.Label2
  doubleSpinBox.MouseWheel(1)
  doubleSpinBox.wValue = 25.0001


@when("‘Assign Size of Neglectable Particle’ 의 UI에 마우스 포인터를 대고 마우스 휠을 아래로 한 칸 내린다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 150
  scrollArea2 = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea
  scrollArea2.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  TC_ParameterControl = scrollArea2.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl
  doubleSpinBox = TC_ParameterControl.ScalarUi.valueSpin
  doubleSpinBox.qt_spinbox_lineedit.Click(27, 12)
  vlabel = TC_ParameterControl.Label2
  doubleSpinBox.MouseWheel(-1)
  doubleSpinBox.wValue = 24.9999

@given("Basic Measurement 버튼 맨 오른쪽의 화살표가 윗 방향\\(△)이다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 150
  buttonBasicMeasurement = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.panel_contents.bt_collapse
  buttonBasicMeasurement.ClickButton()
  scrollBar.wPosition = 150
  buttonBasicMeasurement.ClickButton()

@when("Basic Measurement 버튼을 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 150
  buttonBasicMeasurement = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.panel_contents.bt_collapse
  buttonBasicMeasurement.ClickButton()
  
@then("Basic Measurement 밑의 UI가 접히는지 확인")
def step_impl():
  Regions.Widget33.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget)


@given("Basic Measurement 버튼 맨 오른쪽의 화살표가 아래 방향\\(▽)이다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 150
  scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.panel_contents.bt_collapse.ClickButton()

@then("Basic Measurement 밑의 UI가 펼쳐지는지 확인")
def step_impl():
  Regions.panel_contents9.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents)

@then("버튼 맨 오른쪽의 화살표가 윗 방향\\(△)으로 바뀌는지 확인")
def step_impl():
  Regions.panel_contents10.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@when("RI Thresholding을 프로세서로 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.procCombo.ClickItem("RI Thresholding")

@then("Basic Measurement의 table의 Preset row가 Protein으로 선택되어 있는지 확인")
def step_impl():
  frame = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame
  vlabel = frame.h8
  vlabel.MouseWheel(-1)
  vlabel.MouseWheel(-1)
  vlabel.MouseWheel(-1)
  widget = frame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget
  TC_CollapseWidget = widget.CollapseWidget4
  TC_CollapseWidget.panel_contents.bt_collapse.MouseWheel(-1)
  TC_ParameterControl = TC_CollapseWidget.contentsFrame.TC_ParameterControl
  TC_ParameterControl.ScalarUi.valueSlider.MouseWheel(-1)
  TC_ParameterControl.ScalarUi2.valueSlider.MouseWheel(-1)
  widget.CollapseWidget.MouseWheel(-2)
  Tables.TableWidget6.Check()
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@when("default column의 Preset 드롭다운 버튼에서 Protein을 선택한다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 150
  scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.Preset_Setter_0.ClickItem("Protein")

@then("Baseline RI 값이 1.3337로, RII값이 0.19로 입력되는지 확인")
def step_impl():
  Tables.TableWidget7.Check()
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@when("default column의 Preset 드롭다운 버튼에서 Hemoglobin을 선택한다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 150
  scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.Preset_Setter_0.ClickItem("Hemoglobin")

@then("Baseline RI 값이 1.3337로, RII값이 0.15로 입력되는지 확인")
def step_impl():
  Tables.TableWidget8.Check()
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@when("default column의 Preset 드롭다운 버튼에서 Lipid를 선택한다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 150
  scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.Preset_Setter_0.ClickItem("Lipid")

@then("Baseline RI 값이 1.3337로, RII값이 0.135로 입력되는지 확인")
def step_impl():
  Tables.TableWidget9.Check()
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@when("Basic Measurement의 table에서 default column의 Preset 드롭다운 버튼을 Customize로 선택한다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 150
  scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.Preset_Setter_0.ClickItem("Customize")

@when("Baseline RI row의 cell을 클릭하고 키보드로 값을 직접 입력하고 키보드의 엔터 키를 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 150
  tableWidget = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterTable.TableWidget
  tableWidget.ClickCell("Baseline RI", "default")
  tableWidget.Keys("1")
  tableWidget.qt_scrollarea_viewport.LineEdit.Keys("[Enter]")

@when("RII row의 cell을 클릭하고 키보드로 값을 직접 입력하고 키보드의 엔터 키를 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 200
  tableWidget = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterTable.TableWidget
  tableWidget.ClickCell("RII", "default")
  tableWidget.Keys("2")
  tableWidget.qt_scrollarea_viewport.LineEdit.Keys("[Enter]")

@then("Baseline RI 값과 RII값이 직접 입력한 값으로 변경되는지 확인")
def step_impl():
  Tables.TableWidget10.Check()
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@given("BA Application Parameter에서 프로세서로 AI Segmentation이 선택되어 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir = r'E:\Regression_Test\Prefix_File\BA Application Parameter\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir3 = r'E:\Regression_Test\Prefix_File\BA Application Parameter\playground\test\HC\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)
  dir1 = r'E:\Regression_Test\Prefix_File\BA Application Parameter'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BA Application Parameter'
  copy_tree(dir2,dir1) 
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(92, 19)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.WorkerW.ReBarWindow32.AddressBandRoot.progress.BreadcrumbParent.toolbar.ClickItemXY("‎Prefix_File", 22, 13, False)
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BA Application Parameter\\BA Application Parameter.tcpro")

@when("Single RUN 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_gray.Drag(111, 16, -7, 1)

@when("팝업에서 App이 연결된 hypercube 안의 TCF 중 Single RUN 하고 싶은 TCF를 더블 클릭한다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.widget_popup.scrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  scrollBar.wPosition = 0
  scrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget2.panel_contents_image.widget.panel_contents_image.Widget.DblClick(119, 77)

@then("Basic Analyzer 탭이 열리는지 확인")
def step_impl():
  Regions.tabWidget1.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget)

@then("Application tab 바에서 Project Manager 탭 이름의 하이라이트가 사라지고 Basic Analyzer 탭 이름에 하이라이트가 생겼는지 확인")
def step_impl():
  Regions.BA_SR2.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.TC_TabBar)

@then("Single Run 탭 이름에 하이라이트가 생겼는지 확인")
def step_impl():
  Regions.qt_tabwidget_tabbar.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_tabbar)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)


@when("팝업에서 App이 연결된 hypercube 안의 TCF 중 Single RUN 하고 싶은 TCF를 선택하고 팝업 안의 Single RUN 버튼을 누른다.")
def step_impl():
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel = Aliases.TomoAnalysis.widget_popup
  scrollArea = tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget2.panel_contents_image.widget.panel_contents_image.Widget.Click(68, 41)
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.bt_square_primary.Click(103, 12)

@when("Processing mode 패널의 드롭다운 버튼을 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.procCombo.DropDown()

  
@when("Single RUN 패널의 RI Thresholding을 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.procCombo.ClickItem("RI Thresholding")

@then("Basic Analyzer Single Run 탭의 Processing mode 패널에 RI Thresholding의 UI가 나타나는지 확인")
def step_impl():
  Regions.groupBox.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox)
  
@then("Basic Analyzer Single Run 탭의 Processing mode 패널에 AI Segmentation의 UI가 나타나는지 확인")
def step_impl():
  Regions.groupBox1.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox)


@given("Basic Analyzer Single Run 탭의 Processing mode 패널의 제일 위 드롭다운 버튼이 RI Thresholding으로 선택되어 있다.")
def step_impl():

  dir = r'E:\Regression_Test\Prefix_File\BA Application Parameter\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir3 = r'E:\Regression_Test\Prefix_File\BA Application Parameter\playground\test\HC\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)
  dir4 =r'E:\Regression_Test\Prefix_File\BA Application Parameter\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\BA Application Parameter'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BA Application Parameter'
  copy_tree(dir2,dir1) 
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(92, 19)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.WorkerW.ReBarWindow32.AddressBandRoot.progress.BreadcrumbParent.toolbar.ClickItemXY("‎Prefix_File", 22, 13, False)
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BA Application Parameter\\BA Application Parameter.tcpro")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_gray.Drag(111, 16, -7, 1)
  scrollArea = Aliases.TomoAnalysis.widget_popup.scrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  scrollBar.wPosition = 0
  scrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget.panel_contents_image.widget.panel_contents_image.Widget.DblClick(128, 48) 
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.procCombo.ClickItem("RI Thresholding")

@given("Basic Analyzer Single Run 탭의 Processing mode 패널의 제일 위 드롭다운 버튼이 AI Segmentation으로 선택되어 있다.")
def step_impl():

  dir = r'E:\Regression_Test\Prefix_File\BA Application Parameter\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir3 = r'E:\Regression_Test\Prefix_File\BA Application Parameter\playground\test\HC\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)
  dir1 = r'E:\Regression_Test\Prefix_File\BA Application Parameter'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BA Application Parameter'
  copy_tree(dir2,dir1) 
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(92, 19)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.WorkerW.ReBarWindow32.AddressBandRoot.progress.BreadcrumbParent.toolbar.ClickItemXY("‎Prefix_File", 22, 13, False)
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BA Application Parameter\\BA Application Parameter.tcpro")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_gray.Drag(111, 16, -7, 1)
  scrollArea = Aliases.TomoAnalysis.widget_popup.scrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  scrollBar.wPosition = 0
  scrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget.panel_contents_image.widget.panel_contents_image.Widget.DblClick(128, 48)

@when("Processing mode 패널의 드롭다운 버튼에 마우스 포인터를 올리고 마우스 휠을 아래로 내린다.")
def step_impl():
  groupBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox
  comboBox = groupBox.procCombo
  comboBox.DropDown()
  comboBox.CloseUp()
  comboBox.MouseWheel(-1)

@then("Processing mode 패널의 드롭다운 버튼이 RI Thresholding으로 변경되는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.procCombo, "wText", cmpEqual, "RI Thresholding")

@when("Processing mode 패널의 드롭다운 버튼에 마우스 포인터를 올리고 마우스 휠을 위로 올린다.")
def step_impl():
  groupBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox
  comboBox = groupBox.procCombo
  comboBox.DropDown()
  comboBox.CloseUp()
  comboBox.MouseWheel(1)
  comboBox.MouseWheel(1)
  
@then("Processing mode 패널의 드롭다운 버튼이 AI Segmentation으로 변경되는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.procCombo, "wText", cmpEqual, "AI Segmentation")

@when("팝업에서 App이 연결된 hypercube 안의 TCF 중 Single RUN 하고 싶은 TCF를 선택한다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.widget_popup.scrollArea
  scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget.panel_contents_image.widget.panel_contents_image.Widget.Click(76, 43)
  Aliases.TomoAnalysis.widget_popup.bt_square_primary.Click(45, 7)

@then("Basic Analyzer 탭에서 Processing mode 패널의 맨 위 드롭다운 버튼이 AI Segmentation으로 설정됐는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.procCombo, "wText", cmpEqual, "AI Segmentation")
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@then("Project Manager 탭의 BA Application Parameter 패널과 Basic Analyzer 탭의 Processing mode 패널의 두 Basic Measurement 값이 서로 일치하는지 확인")
def step_impl():
  Tables.TableWidget11.Check()
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@given("Basic Analyzer 탭에서 AI Segmentation이 프로세서로 선택되어 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir = r'E:\Regression_Test\Prefix_File\BA Application Parameter\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir3 = r'E:\Regression_Test\Prefix_File\BA Application Parameter\playground\test\HC\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)
  dir1 = r'E:\Regression_Test\Prefix_File\BA Application Parameter'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BA Application Parameter'
  copy_tree(dir2,dir1) 
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(92, 19)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.WorkerW.ReBarWindow32.AddressBandRoot.progress.BreadcrumbParent.toolbar.ClickItemXY("‎Prefix_File", 22, 13, False)
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BA Application Parameter\\BA Application Parameter.tcpro")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_gray.Drag(111, 16, -7, 1)
  scrollArea = Aliases.TomoAnalysis.widget_popup.scrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  scrollBar.wPosition = 0
  scrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget.panel_contents_image.widget.panel_contents_image.Widget.DblClick(128, 48)


@given("AI Inference 버튼 맨 오른쪽의 화살표가 윗 방향\\(△)이다.")
def step_impl():
  Regions.bt_collapse11.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.panel_contents.bt_collapse2)
  Regions.bt_collapse12.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.panel_contents.bt_collapse2)

@when("AI Inference 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.panel_contents.bt_collapse.ClickButton()

@then("AI Inference 버튼 밑의 UI가 접히는지 확인")
def step_impl():
  Regions.Widget2.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget)

@then("AI Inference 버튼 맨 오른쪽의 화살표가 아래 방향\\(▽)으로 바뀌는지 확인")
def step_impl():
  Regions.bt_collapse9.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.panel_contents.bt_collapse2)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@given("AI Inference 버튼 맨 오른쪽의 화살표가 아래 방향\\(▽)이다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.panel_contents.bt_collapse.ClickButton()

@then("AI Inference 버튼 밑의 UI가 펼쳐지는지 확인")
def step_impl():
  Regions.CollapseWidget7.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2)

@then("AI Inference 버튼 맨 오른쪽의 화살표가 윗 방향\\(△)으로 바뀌는지 확인")
def step_impl():
  Regions.bt_collapse10.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.panel_contents.bt_collapse2)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@then("AI Inference 탭의 Execute 버튼이 하이라이트 되었는지 확인")
def step_impl():
  Regions.CollapseWidget8.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@when("AI Inference 탭의 Execute 버튼을 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterControl.buttonExecute.ClickButton()

@then("Mask Viewing panel에 3D 마스크가 생성되었는지 확인")
def step_impl():
  Regions.TomoAnalysis41.Check(Aliases.TomoAnalysis.mainwindow.TomoAnalysis15)

@then("HT 이미지에 마스크가 생성되었는지 확인")
def step_impl():
  Regions.TomoAnalysis42.Check(Aliases.TomoAnalysis.mainwindow.TomoAnalysis4)

@then("Mask Viewing panel 상단에 Cell Instance, Whole Cell, Nucleus, Nucleolus, Lipid droplet 체크박스와 Cell-wise filter 스위치, Opacity 조절 버튼이 있는 툴박스가 생성되었는지 확인")
def step_impl():
  Regions.VizControlPanel3.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel)

@then("AI Inference 탭의 Execute 버튼이 하이라이트 해제되었는지 확인")
def step_impl():
  Regions.CollapseWidget34.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2)

@then("Basic Measurement 탭의 Execute 버튼이 하이라이트 되었는지 확인")
def step_impl():
  Regions.CollapseWidget35.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@given("Basic Analyzer 탭의 Mask Viewing panel에 3D 마스크가 생성되어 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir = r'E:\Regression_Test\Prefix_File\BA_Single_RUN\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir3 = r'E:\Regression_Test\Prefix_File\BA_Single_RUN\playground\test\Hypercube\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)
  dir4 =r'E:\Regression_Test\Prefix_File\BA_Single_RUN\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\BA_Single_RUN'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BA_Single_RUN'
  copy_tree(dir2,dir1) 
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(92, 19)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.WorkerW.ReBarWindow32.AddressBandRoot.progress.BreadcrumbParent.toolbar.ClickItemXY("‎Prefix_File", 22, 13, False)
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BA_Single_RUN\\BA_Single_RUN.tcpro")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_gray.Drag(111, 16, -7, 1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel = tomoAnalysis.widget_popup
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget.panel_contents_image.widget.panel_contents_image.Widget.Click(136, 89)
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.bt_square_primary.Click(52, 23)
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterControl.buttonExecute.ClickButton()

@when("마우스를 커서 모양이 바뀌는 3D 마스크 캔버스 위로 가져가 마우스 휠을 위로 올린다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TomoAnalysis15.MouseWheel(1)

@then("3D 마스크가 확대됨을 확인")
def step_impl():
  Regions.TomoAnalysis5.Check(Aliases.TomoAnalysis.mainwindow.TomoAnalysis15)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
  
@when("마우스를 커서 모양이 바뀌는 3D 마스크 캔버스 위로 가져가 마우스 휠을 아래로 내린다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TomoAnalysis15.MouseWheel(-1)

@then("3D 마스크가 축소됨을 확인")
def step_impl():
  Regions.TomoAnalysis6.Check(Aliases.TomoAnalysis.mainwindow.TomoAnalysis15)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
  
@given("Basic Analyzer Single Run 탭이 열려 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir = r'E:\Regression_Test\Prefix_File\BA_Single_RUN\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir3 = r'E:\Regression_Test\Prefix_File\BA_Single_RUN\playground\test\Hypercube\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)
  dir1 = r'E:\Regression_Test\Prefix_File\BA_Single_RUN'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BA_Single_RUN'
  copy_tree(dir2,dir1) 
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(92, 19)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.WorkerW.ReBarWindow32.AddressBandRoot.progress.BreadcrumbParent.toolbar.ClickItemXY("‎Prefix_File", 22, 13, False)
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BA_Single_RUN\\BA_Single_RUN.tcpro")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_gray.Drag(111, 16, -7, 1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel = tomoAnalysis.widget_popup
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget.panel_contents_image.widget.panel_contents_image.Widget.Click(136, 89)
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.bt_square_primary.Click(52, 23)


@when("Background Option의 Black 버튼을 클릭한다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TomoAnalysis.Click(771, 22)
  tomoAnalysis.mainwindow.TomoAnalysis.Click(850, 22)




@then("3D 마스크 캔버스의 배경 색이 검은 색으로 변경됨을 확인")
def step_impl():
  Regions.TomoAnalysis7.Check(Aliases.TomoAnalysis.mainwindow.TomoAnalysis)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@when("Background Option의 White 버튼을 클릭한다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TomoAnalysis.Click(775, 27)
  tomoAnalysis.Menu.QtMenu.Check("Background Options|White", True)

@then("3D 마스크 캔버스의 배경 색이 흰 색으로 변경됨을 확인")
def step_impl():
  Regions.TomoAnalysis8.Check(Aliases.TomoAnalysis.mainwindow.TomoAnalysis)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@when("마우스를 커서 모양이 바뀌는 3D 마스크 캔버스 위로 가져가 클릭한 채로 마우스를 움직인다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TomoAnalysis15.Click(41, 413)
  Aliases.TomoAnalysis.mainwindow.TomoAnalysis15.Drag(29, 443, 714, -23)

@then("생성된 3D 마스크가 회전하는지 확인")
def step_impl():
  Regions.TomoAnalysis4.Check(Aliases.TomoAnalysis.mainwindow.TomoAnalysis15, False, False, 40000, 25)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)




@when("View 버튼을 클릭한다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis2 = tomoAnalysis.mainwindow.TomoAnalysis
  tomoAnalysis2.Click(773, 19)
  tomoAnalysis2.Click(779, 31)
  menu = tomoAnalysis.Menu2
  menu.QtMenu.Click("View")
  menu.Click(429, 3)

@when("View의 Reset View 버튼을 클릭한다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TomoAnalysis.Click(772, 21)
  tomoAnalysis.Menu.QtMenu.Click("View|Reset View")

@then("처음 마스크가 생성되었을 때의 초기 시점으로 마스크가 보이는지 확인")
def step_impl():
  Regions.TomoAnalysis9.Check(Aliases.TomoAnalysis.mainwindow.TomoAnalysis)

@when("Background Option의 Gradient 버튼을 클릭한다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis2 = tomoAnalysis.mainwindow.TomoAnalysis
  tomoAnalysis2.Click(777, 23)
  tomoAnalysis.Menu3.Click(290, -8)
  tomoAnalysis2.Click(779, 23)
  tomoAnalysis.Menu2.QtMenu.Check("Background Options|Gradient", False)

@then("3D 마스크 캔버스의 배경 색이 gradient로 변경됨을 확인")
def step_impl():
  Regions.TomoAnalysis11.Check(Aliases.TomoAnalysis.mainwindow.TomoAnalysis)

@when("Mask Viewing panel 상단 Mask 툴박스의 Nucleus 체크박스에 체크한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.controlSocket.RadioButton2.ClickButton()

@then("3D 마스크 캔버스에서 Cell의 Nucleus에 마스크가 입혀졌는지 확인")
def step_impl():
  Regions.TomoAnalysis14.Check(Aliases.TomoAnalysis.mainwindow.TomoAnalysis15)

@then("HT 사진의 Cell의 Nucleus 영역에 마스크가 입혀졌는지 확인")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TomoAnalysis4.HoverMouse(221, 376)
  Aliases.TomoAnalysis.mainwindow.TomoAnalysis4.MouseWheel(1)  
  Aliases.TomoAnalysis.mainwindow.TomoAnalysis4.MouseWheel(1)  
  Aliases.TomoAnalysis.mainwindow.TomoAnalysis4.MouseWheel(1)
  Aliases.TomoAnalysis.mainwindow.TomoAnalysis4.MouseWheel(1) 
  Aliases.TomoAnalysis.mainwindow.TomoAnalysis4.MouseWheel(1)  
  Aliases.TomoAnalysis.mainwindow.TomoAnalysis4.MouseWheel(1)
  Aliases.TomoAnalysis.mainwindow.TomoAnalysis4.MouseWheel(1)  
  Aliases.TomoAnalysis.mainwindow.TomoAnalysis4.MouseWheel(1)  
  Aliases.TomoAnalysis.mainwindow.TomoAnalysis4.MouseWheel(1)
  Aliases.TomoAnalysis.mainwindow.TomoAnalysis4.MouseWheel(1) 
  Aliases.TomoAnalysis.mainwindow.TomoAnalysis4.MouseWheel(1)  
  Aliases.TomoAnalysis.mainwindow.TomoAnalysis4.MouseWheel(1)
  Regions.TomoAnalysis44.Check(Aliases.TomoAnalysis.mainwindow.TomoAnalysis4)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)



@when("Mask Viewing panel 상단 Mask 툴박스의 Nucleolus 체크박스에 체크한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.controlSocket.RadioButton3.ClickButton()

@then("3D 마스크 캔버스에서 Cell의 Nucleolus에 마스크가 입혀졌는지 확인")
def step_impl():
  Regions.TomoAnalysis17.Check(Aliases.TomoAnalysis.mainwindow.TomoAnalysis15)

@then("HT 사진의 Cell의 Nucleolus 영역에 마스크가 입혀졌는지 확인")
def step_impl():
  Regions.TomoAnalysis18.Check(Aliases.TomoAnalysis.mainwindow.TomoAnalysis4)
  tomoAnalysis = Aliases.TomoAnalysis.mainwindow.TomoAnalysis4
  tomoAnalysis.HoverMouse(739, 21)
  tomoAnalysis.MouseWheel(1)
  tomoAnalysis.MouseWheel(1)
  tomoAnalysis.MouseWheel(1)
  tomoAnalysis.MouseWheel(1)
  tomoAnalysis.MouseWheel(1)
  tomoAnalysis.MouseWheel(1)
  tomoAnalysis.MouseWheel(1)
  tomoAnalysis.MouseWheel(1)
  tomoAnalysis.MouseWheel(1)
  tomoAnalysis.MouseWheel(1)
  tomoAnalysis.MouseWheel(1)
  tomoAnalysis.MouseWheel(1)
  Regions.TomoAnalysis19.Check(Aliases.TomoAnalysis.mainwindow.TomoAnalysis4)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)


@then("3D 마스크 캔버스에서 Cell의 Lipid droplet에 마스크가 입혀졌는지 확인")
def step_impl():
  Regions.TomoAnalysis20.Check(Aliases.TomoAnalysis.mainwindow.TomoAnalysis15)

@then("HT 사진의 Cell의 Lipid droplet 영역에 마스크가 입혀졌는지 확인")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis.mainwindow.TomoAnalysis4
  tomoAnalysis.HoverMouse(739, 21)
  tomoAnalysis.MouseWheel(1)
  tomoAnalysis.MouseWheel(1)
  tomoAnalysis.MouseWheel(1)
  tomoAnalysis.MouseWheel(1)
  tomoAnalysis.MouseWheel(1)
  Regions.TomoAnalysis21.Check(Aliases.TomoAnalysis.mainwindow.TomoAnalysis4)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
  
@when("Mask Viewing panel 상단 Mask 툴박스의 Cell Instance 체크박스에 체크한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.bt_switch.ClickButton()

@then("3D 마스크 캔버스에서 Cell Instance 영역에 마스크가 입혀졌는지 확인")
def step_impl():
  Regions.TomoAnalysis22.Check(Aliases.TomoAnalysis.mainwindow.TomoAnalysis15)

@then("HT 사진의 Cell의 Cell Instance 영역에 마스크가 입혀졌는지 확인")
def step_impl():
  Regions.TomoAnalysis23.Check(Aliases.TomoAnalysis.mainwindow.TomoAnalysis4)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
  
@given("Basic Analyzer 탭의 Mask Viewing panel에 3D Whole Cell 마스크가 생성되어 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir = r'E:\Regression_Test\Prefix_File\BA_Single_RUN\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir3 = r'E:\Regression_Test\Prefix_File\BA_Single_RUN\playground\test\Hypercube\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)
  dir1 = r'E:\Regression_Test\Prefix_File\BA_Single_RUN'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BA_Single_RUN'
  copy_tree(dir2,dir1) 
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(92, 19)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.WorkerW.ReBarWindow32.AddressBandRoot.progress.BreadcrumbParent.toolbar.ClickItemXY("‎Prefix_File", 22, 13, False)
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BA_Single_RUN\\BA_Single_RUN.tcpro")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_gray.Drag(111, 16, -7, 1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel = tomoAnalysis.widget_popup
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget.panel_contents_image.widget.panel_contents_image.Widget.Click(136, 89)
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.bt_square_primary.Click(52, 23)
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterControl.buttonExecute.ClickButton()
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.controlSocket.RadioButton.ClickButton()


@given("Mask Viewing panel 상단 Mask 툴박스의 Cell-wise filter 스위치가 켜져 있다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.bt_switch.ClickButton()

@when("Mask Viewing panel 상단 Mask 툴박스의 Cell-wise filter 스위치를 끈다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.bt_switch.ClickButton()

@then("TCF에서 모든 cell의 마스크가 같은 색으로 표시되는지 확인")
def step_impl():
  Regions.TomoAnalysis24.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel)

@then("Mask 툴박스의 Cell-wise filter 스위치 버튼이 하이라이트 해제됐는지 확인")
def step_impl():
  Regions.bt_switch.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.bt_switch)

@then("Mask 툴박스의 Cell-wise filter 스위치 버튼이 왼쪽으로 움직였는지 확인")
def step_impl():
  Regions.bt_switch1.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.bt_switch)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@given("Basic Analyzer 탭의 Mask Viewing panel에 마스크가 생성되어 있다.")
def step_impl():

  dir = r'E:\Regression_Test\Prefix_File\BA_Single_RUN\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir3 = r'E:\Regression_Test\Prefix_File\BA_Single_RUN\playground\test\Hypercube\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)
  dir4 = r'E:\Regression_Test\Prefix_File\BA_Single_RUN\playground\test\Hypercube\*.rep'
  for f in glob.glob(dir4):
      os.remove(f)
  dir1 = r'E:\Regression_Test\Prefix_File\BA_Single_RUN'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BA_Single_RUN'
  copy_tree(dir2,dir1) 
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(92, 19)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.WorkerW.ReBarWindow32.AddressBandRoot.progress.BreadcrumbParent.toolbar.ClickItemXY("‎Prefix_File", 22, 13, False)
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BA_Single_RUN\\BA_Single_RUN.tcpro")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_gray.Drag(111, 16, -7, 1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel = tomoAnalysis.widget_popup
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget.panel_contents_image.widget.panel_contents_image.Widget.Click(136, 89)
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.bt_square_primary.Click(52, 23)
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.buttonExecute.ClickButton()


@given("Mask 툴박스의 Whole Cell 체크박스가 체크 되어있다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.controlSocket.RadioButton.ClickButton()

@when("Mask Viewing panel의 HT 이미지 캔버스 위에 마우스 포인터를 올리고 마우스 휠을 위로 5칸 올린다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel.Splitter.Widget2.TomoAnalysis_BasicAnalysis_Plugins_BaRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis
  tomoAnalysis.Click(472, 361)
  tomoAnalysis.MouseWheel(1)
  tomoAnalysis.MouseWheel(1)
  tomoAnalysis.MouseWheel(1)
  tomoAnalysis.MouseWheel(1)
  tomoAnalysis.MouseWheel(1)

@then("Basic Analyzer 탭의 Mask Viewing panel HT 단층 사진의 Z축이 올라가는 지 확인")
def step_impl():
  Regions.TomoAnalysis25.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel, False, False, 1000, 25)

@when("Mask Viewing panel의 HT 이미지 캔버스 위에 마우스 포인터를 올리고 마우스 휠을 아래로 5칸 내린다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel.Splitter.Widget2.TomoAnalysis_BasicAnalysis_Plugins_BaRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis
  tomoAnalysis.Click(472, 361)
  tomoAnalysis.MouseWheel(-1)
  tomoAnalysis.MouseWheel(-1)
  tomoAnalysis.MouseWheel(-1)
  tomoAnalysis.MouseWheel(-1)
  tomoAnalysis.MouseWheel(-1)

@then("Basic Analyzer 탭의 Mask Viewing panel HT 단층 사진의 Z축이 내려가는 지 확인")
def step_impl():
  Regions.TomoAnalysis26.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel, False, False, 1000, 25)

  
@when("HT 이미지 캔버스의 우상단의 톱니바퀴 아이콘을 클릭한다.")
def step_impl():
  raise NotImplementedError

@when("Color Map 버튼을 클릭한다.")
def step_impl():
  raise NotImplementedError

@when("Color map의 Inverse grayscale 버튼을 클릭한다.")
def step_impl():
  raise NotImplementedError

@then("3D 마스크 캔버스의 이미지 색이 Inverse grayscale로 변경됨을 확인")
def step_impl():
  raise NotImplementedError

@when("Color map의 Hot iron 버튼을 클릭한다.")
def step_impl():
  raise NotImplementedError

@then("3D 마스크 캔버스의 이미지 색이 Hot iron으로 변경됨을 확인")
def step_impl():
  raise NotImplementedError

@when("Color map의 JET 버튼을 클릭한다.")
def step_impl():
  raise NotImplementedError

@then("3D 마스크 캔버스의 이미지 색이 JET로 변경됨을 확인")
def step_impl():
  raise NotImplementedError

@when("Color map의 Rainbow 버튼을 클릭한다.")
def step_impl():
  raise NotImplementedError

@then("3D 마스크 캔버스의 이미지 색이 Rainbow로 변경됨을 확인")
def step_impl():
  raise NotImplementedError

@when("Color map의 Grayscale 버튼을 클릭한다.")
def step_impl():
  raise NotImplementedError

@then("3D 마스크 캔버스의 이미지 색이 Grayscale로 변경됨을 확인")
def step_impl():
  raise NotImplementedError

@when("Level Window 버튼을 클릭한다.")
def step_impl():
  raise NotImplementedError

@when("Data Range 사이즈 바의 왼쪽 버튼을 마우스로 오른쪽 끝까지 움직인다.")
def step_impl():
  raise NotImplementedError

@then("HT 이미지가 어둡게 변하는지 확인")
def step_impl():
  raise NotImplementedError

@then("사이즈 바 밑의 왼쪽 수치가 오른쪽 수치와 14320으로 같아졌는지 확인")
def step_impl():
  raise NotImplementedError

@when("Data Range 사이즈 바의 오른쪽 버튼을 마우스로 왼쪽 끝까지 움직인다.")
def step_impl():
  raise NotImplementedError

@then("HT 이미지가 밝게 변하다가 마지막 프레임에서 어두워지는지 확인")
def step_impl():
  raise NotImplementedError

@then("사이즈 바 밑의 오른쪽 수치가 왼쪽 수치와 13306으로 같아졌는지 확인")
def step_impl():
  raise NotImplementedError

@when("Processing mode 패널  Whole cell column의 Preset 드롭다운 버튼에서 Protein을 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.Preset_Setter_0.ClickItem("Protein")

@when("Processing mode 패널  Nucleus column의 Preset 드롭다운 버튼에서 Protein을 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.Preset_Setter_1.ClickItem("Protein")

@when("Processing mode 패널  Nucleolus column의 Preset 드롭다운 버튼에서 Protein을 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.Preset_Setter_2.ClickItem("Protein")

@when("Processing mode 패널 Lipid droplet column의 Preset 드롭다운 버튼에서 Protein을 선택한다.")
def step_impl():
  tableWidget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget
  tableWidget.qt_scrollarea_hcontainer.ScrollBar.wPosition = 2
  tableWidget.qt_scrollarea_viewport.Preset_Setter_3.ClickItem("Protein")

@when("Processing mode 패널 Whole cell column의 Preset 드롭다운 버튼에서 Hemoglobin을 선택한다.")
def step_impl():
  tableWidget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget
  tableWidget.qt_scrollarea_hcontainer.ScrollBar.wPosition = 0
  tableWidget.qt_scrollarea_viewport.Preset_Setter_0.ClickItem("Hemoglobin")

@when("Processing mode 패널 Nucleus column의 Preset 드롭다운 버튼에서 Hemoglobin을 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.Preset_Setter_1.ClickItem("Hemoglobin")

@when("Processing mode 패널 Nucleolus column의 Preset 드롭다운 버튼에서 Hemoglobin을 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.Preset_Setter_2.ClickItem("Hemoglobin")

@when("Processing mode 패널 Lipid droplet column의 Preset 드롭다운 버튼에서 Hemoglobin을 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.Preset_Setter_3.ClickItem("Hemoglobin")

@when("Processing mode 패널 Whole cell column의 Preset 드롭다운 버튼에서 Lipid를 선택한다.")
def step_impl():
  widget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport
  widget.Preset_Setter_0.ClickItem("Lipid")
  comboBox = widget.Preset_Setter_1
  comboBox.DropDown()
  comboBox.CloseUp()

@when("Processing mode 패널 Nucleus column의 Preset 드롭다운 버튼에서 Lipid를 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.Preset_Setter_1.ClickItem("Lipid")

@when("Processing mode 패널 Nucleolus column의 Preset 드롭다운 버튼에서 Lipid를 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.Preset_Setter_2.ClickItem("Lipid")

@when("Processing mode 패널 Lipid droplet column의 Preset 드롭다운 버튼에서 Lipid를 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.Preset_Setter_3.ClickItem("Lipid")

@when("Processing mode 패널 Basic Measurement의 table에서 모든 column의 Preset 드롭다운 버튼을 Customize로 선택한다.")
def step_impl():
  tableWidget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget
  tableWidget.qt_scrollarea_hcontainer.ScrollBar.wPosition = 0
  tableWidget.qt_scrollarea_viewport.Preset_Setter_0.ClickItem("Customize")
  widget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport
  widget.Preset_Setter_1.ClickItem("Customize")
  widget.Preset_Setter_2.ClickItem("Customize")
  widget.Preset_Setter_3.ClickItem("Customize")

@when("Processing mode 패널에서 Baseline RI row의 cell을 클릭하고 키보드로 ‘{arg}’를 입력하고 키보드의 엔터 키를 누른다.")
def step_impl(param1):
  tableWidget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget
  tableWidget.ClickCell("Baseline RI", "Whole cell")
  widget = tableWidget.qt_scrollarea_viewport
  lineEdit = widget.LineEdit
  widget.DblClick(50, 64)
  lineEdit.SetText("123456")
  widget.DblClick(90, 64)
  lineEdit.SetText("123456")
  widget.DblClick(150, 64)
  lineEdit.SetText("123456")
  widget.DblClick(150, 64)
  lineEdit.SetText("123456")



@when("Processing mode 패널에서 RII row의 cell을 클릭하고 키보드로 ‘{arg}’를 입력하고 키보드의 엔터 키를 누른다.")
def step_impl(param1):
  tableWidget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget
  tableWidget.qt_scrollarea_hcontainer.ScrollBar.wPosition = 0
  tableWidget.ClickCell("RII", "Whole cell")
  tableWidget.Keys("1")
  lineEdit = tableWidget.qt_scrollarea_viewport.LineEdit
  lineEdit.SetText("123456")
  tableWidget.ClickCell("RII", "Nucleus")
  tableWidget.Keys("1")
  lineEdit.SetText("123456")
  tableWidget.ClickCell("RII", "Nucleolus")
  tableWidget.Keys("1")
  lineEdit.SetText("123456")
  tableWidget.ClickCell("Baseline RI", "Lipid drolet")
  tableWidget.ClickCell("RII", "Lipid drolet")
  tableWidget.Keys("12")
  lineEdit.SetText("123456")


@then("네 column에서 모두 Baseline RI 값과 RII값이 ‘{arg}’로 변경되는지 확인")
def step_impl(param1):
  Tables.TableWidget12.Check()
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
  
@when("Basic Measurement의 table의 parameter를 설정하고 Basic Measurement 탭의 Execute 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.Click(189, 450)
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParamTable.buttonExecute.ClickButton()

@then("Measurement 팝업이 생기는지 확인한다.")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.BasicAnalysisResultPanel, "QtText", cmpEqual, "Measurement")

@then("Basic Measurement 탭의 Execute 버튼의 하이라이트가 해제됐는지 확인")
def step_impl():
  Regions.BA14.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable)

@then("Apply Parameter 버튼이 하이라이트 되었는지 확인")
def step_impl():
  Regions.parameterPanel.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel)
  tomoAnalysis_BasicAnalysis_Plugins_ResultPanel = Aliases.TomoAnalysis.BasicAnalysisResultPanel
  tomoAnalysis_BasicAnalysis_Plugins_ResultPanel.Close()
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)


@then("Measuerment 팝업의 각 row가 하이라이트 된 색이 cell mask 색과 동일한지 확인")
def step_impl():
  Regions.qt_scrollarea_viewport47.Check(Aliases.TomoAnalysis.BasicAnalysisResultPanel.tableWidget.qt_scrollarea_viewport)
  tomoAnalysis_BasicAnalysis_Plugins_ResultPanel = Aliases.TomoAnalysis.BasicAnalysisResultPanel
  tomoAnalysis_BasicAnalysis_Plugins_ResultPanel.Close()
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
  
@given("Basic Analyzer 탭에서 Measurement 팝업이 열려 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir = r'E:\Regression_Test\Prefix_File\BA_Single_RUN\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir3 = r'E:\Regression_Test\Prefix_File\BA_Single_RUN\playground\test\Hypercube\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)
  dir4 = r'E:\Regression_Test\Prefix_File\BA_Single_RUN\playground\test\Hypercube\*.rep'
  for f in glob.glob(dir4):
      os.remove(f)
  dir1 = r'E:\Regression_Test\Prefix_File\BA_Single_RUN'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BA_Single_RUN'
  copy_tree(dir2,dir1) 
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(92, 19)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.WorkerW.ReBarWindow32.AddressBandRoot.progress.BreadcrumbParent.toolbar.ClickItemXY("‎Prefix_File", 22, 13, False)
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BA_Single_RUN\\BA_Single_RUN.tcpro")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_gray.Drag(111, 16, -7, 1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel = tomoAnalysis.widget_popup
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget.panel_contents_image.widget.panel_contents_image.Widget.Click(136, 89)
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.bt_square_primary.Click(52, 23)
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterControl.buttonExecute.ClickButton()
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.buttonExecute.ClickButton()
  dat = r'E:\Regression_Test\*.csv'
  for f in glob.glob(dat):
    os.remove(f)
@when("Hide Result 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.resultBtn.ClickButton()

@then("Measurement 팝업이 사라졌는지 확인")
def step_impl():
  Regions.mainwindow2.Check(Regions.CreateRegionInfo(Aliases.TomoAnalysis.mainwindow, 433, 285, 1271, 632, False))

@then("Hide Result 버튼이 Show Result 버튼으로 바뀌었는지 확인")
def step_impl():
  Regions.parameterPanel1.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@given("주어진 TCF에 대해 위, 아래 Execute 버튼을 모두 눌러 작업을 수행한 Basic Analyzer 탭이 열려 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir = r'E:\Regression_Test\Prefix_File\BA_Single_RUN\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir3 = r'E:\Regression_Test\Prefix_File\BA_Single_RUN\playground\test\Hypercube\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)
  dir4 = r'E:\Regression_Test\Prefix_File\BA_Single_RUN\playground\test\Hypercube\*.rep'
  for f in glob.glob(dir4):
      os.remove(f)
  dir1 = r'E:\Regression_Test\Prefix_File\BA_Single_RUN'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BA_Single_RUN'
  copy_tree(dir2,dir1) 
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(92, 19)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.WorkerW.ReBarWindow32.AddressBandRoot.progress.BreadcrumbParent.toolbar.ClickItemXY("‎Prefix_File", 22, 13, False)
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BA_Single_RUN\\BA_Single_RUN.tcpro")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_gray.Drag(111, 16, -7, 1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel = tomoAnalysis.widget_popup
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget.panel_contents_image.widget.panel_contents_image.Widget.Click(136, 89)
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.bt_square_primary.Click(52, 23)
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterControl.buttonExecute.ClickButton()
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.buttonExecute.ClickButton()


@given("Measurement 팝업이 닫혀 있다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.resultBtn.ClickButton()

@when("Show Result 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.resultBtn.ClickButton()

@then("Measurement 팝업이 생기는지 확인")
def step_impl():
  Regions.singleRunTab1.Check(Regions.CreateRegionInfo(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab, 313, 15, 1564, 870, False))

@then("Show Result 버튼이 Hide Result 버튼으로 바뀌었는지 확인")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.Click(154, 766)
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.resultBtn, "QtText", cmpEqual, "Hide Result")
  tomoAnalysis_BasicAnalysis_Plugins_ResultPanel = Aliases.TomoAnalysis.BasicAnalysisResultPanel
  tomoAnalysis_BasicAnalysis_Plugins_ResultPanel.Close()
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@when("테이블의 셀 하나에 마우스 포인터를 가져가서 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.BasicAnalysisResultPanel.tableWidget.ClickCell("4", "Cell#")

@then("클릭한 테이블 셀이 포함된 row 전체가 파란색으로 하이라이트 됨을 확인")
def step_impl():
  Regions.qt_scrollarea_viewport51.Check(Aliases.TomoAnalysis.BasicAnalysisResultPanel.tableWidget.qt_scrollarea_viewport)
  tomoAnalysis_BasicAnalysis_Plugins_ResultPanel = Aliases.TomoAnalysis.BasicAnalysisResultPanel
  tomoAnalysis_BasicAnalysis_Plugins_ResultPanel.Close()
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
  
@when("Measurement 팝업의 Export 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.BasicAnalysisResultPanel.csvBtn.ClickButton()







@when("저장 경로와 파일 이름을 지정하고 저장 버튼을 누른다.")
def step_impl():
  dlgSaveFile = Aliases.TomoAnalysis.dlgSaveFile
  HWNDView = dlgSaveFile.DUIViewWndClassName.Item
  tv_ = HWNDView.CtrlNotifySink.NamespaceTreeControl.tv_
  tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  tv_.DblClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  tv_.ExpandItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  HWNDView.CtrlNotifySink2.ShellView.Item.Regression_Test.Item.DblClick(36, 10)
  comboBox = HWNDView.FloatNotifySink.ComboBox
  comboBox.Edit.Click(247, 5)
  comboBox.SetText("test.csv")
  dlgSaveFile.btn_S.ClickButton()

@then("지정한 경로에 지정한 이름의 .csv 파일이 생성되었는지 확인한다.")#폴더 없는 상태에서 안하면애러남\
def step_impl():
  explorer = Aliases.explorer
  explorer.wndShell_TrayWnd.ReBarWindow32.MSTaskSwWClass.MSTaskListWClass.Click(32, 12)
  HWNDView = explorer.wndGenerated_File.Generated_File.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink2.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC")
  UIItemsView = HWNDView.CtrlNotifySink.ShellView.Item
  UIItem = UIItemsView.Item.DATADISK1_E_
  UIProperty = UIItem.Item
  UIProperty.Click(58, 0)
  UIProperty.Click(58, 4)
  UIProperty.Click(140, 13)
  UIProperty.Click(140, 13)
  UIItem.Keys("[Enter]")
  UIItem = UIItemsView.Result
  UIItem.Item.Click(22, 16)
  UIItem.Keys("[Enter]")
  Regions.result.Check(Aliases.explorer.wndGenerated_File.Generated_File.DUIViewWndClassName.Item.CtrlNotifySink.ShellView.Item)

@then(".csv 파일의 데이터가 Measurement 탭의 데이터와 일치하는지 확인한다.")
def step_impl():
  explorer = Aliases.explorer
  explorer.wndShell_TrayWnd.ReBarWindow32.MSTaskSwWClass.MSTaskListWClass.Click(31, 17)
  wndCabinetWClass = explorer.wndGenerated_File
  TestedApps.EXCEL2.Run(1, True)
  Regions.EXCEL72.Check(Regions.CreateRegionInfo(Aliases.EXCEL.wndXLMAIN2.XLDESK.EXCEL7, 22, 17, 773, 206, False))
  Aliases.EXCEL.wndXLMAIN2.Close()
  wndCabinetWClass.Close()
  Aliases.TomoAnalysis.BasicAnalysisResultPanel.Close() 
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)


@when("파일 탐색기에서 취소 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.dlgSaveFile.btn_.ClickButton()

@then("‘Failed to save CSV’ 라는 내용의 에러 메세지 팝업이 뜨는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.MessageBox5, "QtText", cmpEqual, "Failed to save CSV")
  Aliases.TomoAnalysis.MessageBox5.qt_msgbox_buttonbox.buttonOk.ClickButton()
  tomoAnalysis_BasicAnalysis_Plugins_ResultPanel = Aliases.TomoAnalysis.BasicAnalysisResultPanel

  
@given("Basic Analyzer 탭의 Apply Parameter 버튼이 하이라이트 되어 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir = r'E:\Regression_Test\Prefix_File\BA_Single_RUN\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir3 = r'E:\Regression_Test\Prefix_File\BA_Single_RUN\playground\test\Hypercube\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)
  dir4 = r'E:\Regression_Test\Prefix_File\BA_Single_RUN\playground\test\Hypercube\*.rep'
  for f in glob.glob(dir4):
      os.remove(f)
  dir1 = r'E:\Regression_Test\Prefix_File\BA_Single_RUN'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BA_Single_RUN'
  copy_tree(dir2,dir1) 
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(92, 19)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.WorkerW.ReBarWindow32.AddressBandRoot.progress.BreadcrumbParent.toolbar.ClickItemXY("‎Prefix_File", 22, 13, False)
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BA_Single_RUN\\BA_Single_RUN.tcpro")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_gray.Drag(111, 16, -7, 1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel = tomoAnalysis.widget_popup
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget.panel_contents_image.widget.panel_contents_image.Widget.Click(136, 89)
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.bt_square_primary.Click(52, 23)
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterControl.buttonExecute.ClickButton()
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.buttonExecute.ClickButton()

@when("BA 탭의 Apply Parameter 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.paramBtn.ClickButton()

@then("Basic Analyzer 탭의 Processing mode 패널과 Project Manager 탭의 BA Application Parameter 패널의 두 Basic Measurement 값이 일치하는지 확인")
def step_impl():
  Tables.TableWidget14.Check()
  Tables.TableWidget13.Check()

@then("화면에서 Basic Analyzer 탭이 Project Manager 탭으로 넘어갔는지 확인")
def step_impl():
  Regions.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow)

@then("Application tab 바에서 Basic Analyzer 탭 이름의 하이라이트가 사라지고 Project Manager 탭 이름에 하이라이트가 생겼는지 확인")
def step_impl():
  Regions.SR2.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.TC_TabBar)
  tomoAnalysis_BasicAnalysis_Plugins_ResultPanel = Aliases.TomoAnalysis.BasicAnalysisResultPanel
  tomoAnalysis_BasicAnalysis_Plugins_ResultPanel.Close()
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@given("Basic Analyzer 탭이 열려 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir = r'E:\Regression_Test\Prefix_File\BA_Single_RUN\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir3 = r'E:\Regression_Test\Prefix_File\BA_Single_RUN\playground\test\Hypercube\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)
  dir4 = r'E:\Regression_Test\Prefix_File\BA_Single_RUN\playground\test\Hypercube\*.rep'
  for f in glob.glob(dir4):
      os.remove(f)
  dir1 = r'E:\Regression_Test\Prefix_File\BA_Single_RUN'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BA_Single_RUN'
  copy_tree(dir2,dir1) 
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(92, 19)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.WorkerW.ReBarWindow32.AddressBandRoot.progress.BreadcrumbParent.toolbar.ClickItemXY("‎Prefix_File", 22, 13, False)
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BA_Single_RUN\\BA_Single_RUN.tcpro")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_gray.Drag(111, 16, -7, 1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel = tomoAnalysis.widget_popup
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget.panel_contents_image.widget.panel_contents_image.Widget.Click(136, 89)
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.bt_square_primary.Click(52, 23)



@when("Application tab 바의 Basic Analyzer 탭 이름 옆의 x 버튼을 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.TC_TabBar.CloseButton.Click(9, 13)

@then("Basic Analyzer 탭이 종료되는지 확인")
def step_impl():
  Regions.SR3.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget)

  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@when("팝업에서 App이 연결된 hypercube 안의 TCF 중 이미 Single run 해 본 적이 있는 TCF를 선택한다.")
def step_impl():
  raise NotImplementedError

@then("Basic Analyzer에서 Mask와 Measurement 팝업이 바로 열리는지 확인")
def step_impl():
  raise NotImplementedError

@when("Mask Viewing panel 상단 Mask 툴박스의 Whole Cell 체크박스에 체크한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.controlSocket.RadioButton.ClickButton()

@then("3D 마스크 캔버스에서 Cell 전체에 마스크가 입혀졌는지 확인")
def step_impl():
  Regions.TomoAnalysis12.Check(Aliases.TomoAnalysis.mainwindow.TomoAnalysis15)

@then("HT 사진의 Cell 영역 전체에 마스크가 입혀졌는지 확인")
def step_impl():
  Regions.TomoAnalysis13.Check(Aliases.TomoAnalysis.mainwindow.TomoAnalysis4)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@when("Mask Viewing panel 상단 Mask 툴박스의 Cell-wise filter 스위치를 켠다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.bt_switch.ClickButton()

@then("TCF에서 각각의 cell의 마스크가 다른 색으로 표시되는지 확인")
def step_impl():
  Regions.TomoAnalysis32.Check(Aliases.TomoAnalysis.mainwindow.TomoAnalysis15)

@then("Mask 툴박스의 Cell-wise filter 스위치 버튼이 하이라이트 됐는지 확인")
def step_impl():
  Regions.VizControlPanel2.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel)

@then("Mask 툴박스의 Cell-wise filter 스위치 버튼이 오른쪽으로 움직였는지 확인")
def step_impl():
  Regions.bt_switch2.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.bt_switch)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
  
@when("3D 마스크 캔버스의 우상단의 톱니바퀴 아이콘을 클릭한다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TomoAnalysis.Click(780, 28)
  tomoAnalysis.Menu2.Click(619, -104)

@when("Mask Viewing panel 상단 Mask 툴박스의 Lipid droplet 체크박스에 체크한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.controlSocket.RadioButton4.ClickButton()

@given("Basic Measurement버튼 맨 오른쪽의 화살표가 윗 방향\\(△)이다.")
def step_impl():
  buttonBasicMeasurement = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.panel_contents.bt_collapse3
  buttonBasicMeasurement.ClickButton()
  buttonBasicMeasurement.ClickButton()

@then("Basic Measurement 버튼 맨 오른쪽의 화살표가 아래 방향\\(▽)으로 바뀌는지 확인")
def step_impl():
  Regions.bt_collapse30.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.panel_contents.bt_collapse)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@then("Basic Measurement 버튼 맨 오른쪽의 화살표가 윗 방향\\(△)으로 바뀌는지 확인")
def step_impl():
  Regions.bt_collapse31.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.panel_contents.bt_collapse)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@then("‘Assign lower threshold’ 사이즈 바가 ‘Assign upper threshold’ 값을 넘지 않는 1.4384까지만 올라가는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "1.4384")
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@then("‘Assign upper threshold’ 사이즈 바가 ‘Assign lower threshold’ 값을 넘지 않는 1.3584까지만 내려가는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl.ScalarUi2.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "1.3584")

  
@when("AI Segmentation의 Basic Measurement 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.panel_contents.bt_collapse3.ClickButton()

@when("RI Thresholding의 Basic Measurement 버튼을 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 203
  scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.panel_contents.bt_collapse.ClickButton()

@given("AI Segmentation의 Basic Measurement 버튼 맨 오른쪽의 화살표가 아래 방향\\(▽)이다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.panel_contents.bt_collapse3.ClickButton()

@when("Labeling 버튼을 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 102
  scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.panel_contents.bt_collapse.ClickButton()

@then("RI Thresholding의 Basic Measurement 버튼 맨 오른쪽의 화살표가 윗 방향\\(△)으로 바뀌는지 확인")
def step_impl():
  Regions.CollapseWidget33.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@when("Single Run 탭의 AI Segmentation을 선택한다.")
def step_impl():
  groupBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox
  groupBox.line.Click(91, 1)
  groupBox.procCombo.ClickItem("AI Segmentation")

@then("네 column에서 모두 Baseline RI 값이 1.3337로, RII값이 0.135로 입력되는지 확인한다.")
def step_impl():
  Tables.TableWidget59.Check()
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
  
@then("네 column에서 모두 Baseline RI 값이 1.3337로, RII값이 0.19로 입력되는지 확인한다.")
def step_impl():
  Tables.TableWidget57.Check()
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
  
@then("네 column에서 모두 Baseline RI 값이 1.3337로, RII값이 0.15로 입력되는지 확인한다.")
def step_impl():
  Tables.TableWidget58.Check()
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@then("RI Thresholding의 Basic Measurement 밑의 UI가 접히는지 확인한다.")
def step_impl():
  Regions.panel_base15.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base)

@then("버튼 맨 오른쪽의 화살표가 아래 방향\\(▽)으로 바뀌는지 확인한다.")
def step_impl():
  Regions.bt_collapse58.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.panel_contents.bt_collapse2)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
  
@then("RI Thresholding의 Basic Measurement 버튼 맨 오른쪽의 화살표가 아래 방향\\(▽)으로 바뀌는지 확인한다.")
def step_impl():
  Regions.bt_collapse59.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.panel_contents.bt_collapse2)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@when("Application Parameter 패널의 RI Thresholding을 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.procCombo.ClickItem("RI Thresholding")


@then("Application Parameter의 UI가 AI Segmentation의 것으로 달라지는지 확인")
def step_impl():
  Regions.processorParamFrame11.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame)

@then("Application Parameter Whole cell column에서 Baseline RI 값이 1.3337로, RII값이 0.15로 입력되는지 확인")
def step_impl():
  Tables.TableWidget60.Check()

@then("Application Parameter Nucleus column에서 Baseline RI 값이 1.3337로, RII값이 0.135로 입력되는지 확인")
def step_impl():
  Tables.TableWidget61.Check()

@when("Application Parameter 패널 Nucleolus column의 Preset 드롭다운 버튼에서 Customize를 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.QtObject("Preset_2").ClickItem("Customize")

@when("Application Parameter 패널 Nucleolus column에서 Baseline RI row의 cell을 클릭하고 키보드로 ‘{arg}’를 입력하고 키보드의 엔터 키를 누른다.")
def step_impl(param1):
  tableWidget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget
  tableWidget.ClickCell("Baseline RI", "Nucleolus")
  tableWidget.Keys("1")
  lineEdit = tableWidget.qt_scrollarea_viewport.LineEdit
  lineEdit.SetText("12345")
  lineEdit.Keys("[Enter]")

@when("RII row의 cell을 클릭하고 키보드로 ‘{arg}’를 입력하고 키보드의 엔터 키를 누른다.")
def step_impl(param1):
  tableWidget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget
  tableWidget.ClickCell("RII", "Nucleolus")
  tableWidget.Keys("1")
  tableWidget.qt_scrollarea_viewport.LineEdit.SetText("12345")

@then("Nucleolus column에서 Baseline RI 값과 RII값이 ‘{arg}’로 변경되는지 확인")
def step_impl(param1):
  TC_CollapseWidget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget
  TC_CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget.TC_AdaptiveHeader2.qt_scrollarea_viewport.Click(94, 10)

  Tables.TableWidget63.Check()

@then("Application Parameter 패널 Lipid droplet column의 Preset 드롭다운 버튼에서 Protein을 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.QtObject("Preset_3").ClickItem("Protein")

@then("Application Parameter Lipid droplet column에서 Baseline RI 값이 1.3337로, RII값이 0.19로 입력되는지 확인")
def step_impl():
  Tables.TableWidget62.Check()

@then("‘Assign lower threshold’ 사이즈 바가 ‘Assign upper threshold’ 값을 넘지 않는 값 까지만 올라가는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "1.4384")

@then("‘Assign upper threshold’ 사이즈 바 옆의 칸에 {arg}를 입력한다.")
def step_impl(param1):
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  scrollBar.wPosition = 0
  doubleSpinBox = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl.ScalarUi2.valueSpin
  doubleSpinBox.qt_spinbox_lineedit.Drag(44, 15, -90, 0)
  doubleSpinBox.Keys("2")
  doubleSpinBox.wValue = 2
  doubleSpinBox.Keys("[Enter]")
  doubleSpinBox.wValue = 2

@when("Select Algorithm 드롭다운 버튼에서 Entropy를 클릭한다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 30
  scrollArea2 = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea
  scrollArea2.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea2.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.StrComboBox.strCombo.ClickItem("Entropy")

@when("Auto lower Threshold 체크박스를 한 번 더 클릭한다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea2 = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea
  scrollArea2.qt_scrollarea_vcontainer.ScrollBar.wPosition = 60
  scrollArea2.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParamControl.TC_StrCheckBox.Click(2, 7)

@then("‘Select Option’ 문구와 옵션 드롭다운 버튼과 ‘Assign Size of Neglectable Particle’ 문구와 사이즈 바, 수치 입력 칸이 나타나는지 확인")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 203
  scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 51
  scrollBar.wPosition = 203
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.Label, "QtText", cmpEqual, "Select Labeling Option")
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.StrComboBox.strCombo, "wText", cmpEqual, "Multi Labels")

@when("BA Application Parameter 패널의 Select Option 드롭다운 버튼을 누르고 Largest Label 버튼을 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 203
  scrollArea2 = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea
  scrollArea2.qt_scrollarea_vcontainer.ScrollBar.wPosition = 51
  scrollArea2.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.StrComboBox.strCombo.ClickItem("Largest Label")

@then("‘Select Option’ 문구와 옵션 드롭다운 버튼과 ‘Assign Size of Neglectable Particle’ 문구와 사이즈 바, 수치 입력 칸이 사라지는지 확인")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 203
  Regions.panel_base18.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base)

@when("Single Run 탭의 RI Thresholding을 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.procCombo.ClickItem("RI Thresholding")

@then("에러창이 뜬 TA 삭제")
def step_impl():
  tomoAnalysis_BasicAnalysis_Plugins_ResultPanel = Aliases.TomoAnalysis.BasicAnalysisResultPanel
  tomoAnalysis_BasicAnalysis_Plugins_ResultPanel.Close()
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@when("File changes 팝업 Flush 탭의 Accept 버튼을 누른다.")
def step_impl():
    Aliases.TomoAnalysis.fileChangeForm.bt_square_primary.ClickButton()
    while tomoAnalysis.ProgressDialog.Exists :
      Delay(1000, "Waiting for window to close...");
      
@then("베치런 TA종료")
def step_impl():
  Aliases.TomoAnalysis.ProgressDialog.buttonCancel.ClickButton()
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@when("Report 탭 Select Prameter 패널에서 Mean RI 체크박스에 체크한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.selectParameterWidget.selectParameterWidget.parametersGroupBox.RadioButton7.ClickButton()

@then("Inter-Cube mean comparison panel의 그래프 y축이 Meean RI로 되어 있는지 확인")
def step_impl():
  Regions.Widget53.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.dataOverviewTabWidget.qt_tabwidget_stackedwidget.graphTab.overviewGraphView.Widget)

@when("Report 탭 Select organelle 패널에서 Whole cell 체크박스에 체크한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.selectParameterWidget.selectOrganelleWidget.organellesGroupBox.RadioButton4.ClickButton()

@when("Graph of individual cell 패널의 Max 값을 입력하는 창에 ‘{arg}’를 입력하고 키보드의 엔터 키를 누른다.")
def step_impl(param1):
  doubleSpinBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.widget.yMaxInputBox
  doubleSpinBox.qt_spinbox_lineedit.Click(40, 9)
  doubleSpinBox.Keys("[BS][BS][BS]")
  doubleSpinBox.wValue = 1.3
  doubleSpinBox.Keys("[BS]")
  doubleSpinBox.wValue = 1
  doubleSpinBox.Keys("[NumLock]4")
  doubleSpinBox.wValue = 1.3999999999999999
  doubleSpinBox.Keys("[Enter]")
  doubleSpinBox.wValue = 1.3999999999999999

@then("Graph of individual cell 패널 그래프의 y축 최댓값이 1.4로 바뀌었는지 확인")
def step_impl():
  Regions.Widget54.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.widget.Widget.TCChartView.Widget)

@when("파일 경로와 파일 이름을 지정하고 저장 버튼을 누른다.")
def step_impl():
  dlgSaveCubeReport = Aliases.TomoAnalysis.dlgSaveCubeReport
  HWNDView = dlgSaveCubeReport.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  UIItem = HWNDView.CtrlNotifySink2.ShellView.Item.Regression_Test
  UIItem.Item.Click(59, 17)
  UIItem.Keys("[Enter]")
  dlgSaveCubeReport.btn_S.ClickButton()

@when("Basic Analyzer 탭의 Tool box에서 Multi-layered mask 버튼을 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.bt_switch.ClickButton()

@when("Basic Analyzer 탭의 Tool box에서 Whole Cell, Nucleus, Nucleolus 체크박스를 클릭한다.")
def step_impl():
  widget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.controlSocket
  widget.CheckBox.ClickButton(cbUnchecked)
  widget.CheckBox2.ClickButton(cbUnchecked)
  widget.CheckBox3.ClickButton(cbUnchecked)

@then("Basic Analyzer 탭에 Lipid droplet 마스크만 남아있는지 확인")
def step_impl():
  Regions.TomoAnalysis202.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel, False, False, 5000, 8)

@when("Basic Analyzer 탭의 Tool box에서 Nucleolus, Lipid droplet 체크박스를 클릭한다.")
def step_impl():
  widget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.controlSocket
  widget.CheckBox3.ClickButton(cbChecked)
  widget.CheckBox4.ClickButton(cbUnchecked)

@then("Basic Analyzer 탭에 Nucleolus 마스크만 남아있는지 확인")
def step_impl():
  Regions.TomoAnalysis203.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel, False, False, 5000, 8)

@then("Application Parameter Whole cell column에서 RII값이 0.15로 입력되는지 확인")
def step_impl():
  Tables.TableWidget73.Check()

@then("Application Parameter Nucleus column에서 RII값이 0.135로 입력되는지 확인")
def step_impl():
  Tables.TableWidget74.Check()

@then("Application Parameter Lipid droplet column에서 RII값이 0.19로 입력되는지 확인")
def step_impl():
  Tables.TableWidget75.Check()

@when("Basic Analyzer 탭의 Tool box에서 All 체크박스를 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.controlSocket.CheckBox5.ClickButton(cbChecked)

@then("Mask Viewing panel 3D 화면에 네가지 mask가 모두 생성되었는지 확인")
def step_impl():
  Regions.viewerPanel2.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel, False, False, 1000, 25)

@when("Basic Analyzer 탭의 Tool box에서 Hide Mask 체크박스를 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.vizToggle.ClickButton(cbChecked)

@then("Mask Viewing panel의 2D화면과 3D 화면에 마스크가 없는지 확인")
def step_impl():
  Regions.viewerPanel3.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel)

@when("Hide Mask 체크박스를 선택 해제한 뒤, Whole cell, Nucleolus, Lipid droplet 체크박스를 선택 해제한다.")
def step_impl():
  tomoAnalysis_BasicAnalysis_Plugins_VizControlPanel = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel
  tomoAnalysis_BasicAnalysis_Plugins_VizControlPanel.vizToggle.ClickButton(cbUnchecked)
  widget = tomoAnalysis_BasicAnalysis_Plugins_VizControlPanel.controlSocket
  widget.CheckBox6.ClickButton(cbUnchecked)
  widget.CheckBox7.ClickButton(cbUnchecked)
  widget.CheckBox8.Click(34, 11)

@then("Mask Viewing panel 3D 화면에 Nucleolus 마스크만 남아있는지 확인")
def step_impl():
  Regions.viewerPanel4.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel, False, False, 1000, 25)


@when("Application Parameter 패널 Nucleolus column에서 row의 cell을 클릭하고 키보드로 ‘{arg}’를 입력하고 키보드의 엔터 키를 누른다.")
def step_impl(param1):
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  scrollBar.wPosition = 0
  scrollBar.wPosition = 0
  tableWidget = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParamTable.TableWidget
  tableWidget.ClickCell("RII", "Nucleolus")
  lineEdit = tableWidget.qt_scrollarea_viewport.LineEdit
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParamTable.TableWidget.qt_scrollarea_viewport.DblClick(151, 45)
  lineEdit.Keys("12345")
  lineEdit.Keys("[Enter]")


@then("Nucleolus column에서 RII값이 ‘{arg}’로 변경되는지 확인")
def step_impl(param1):
  Tables.TableWidget76.Check()
