﻿
  
def getSum():
  #이 코드가 실행되는 위치에 테스트 로그를 가져옴
  ProLogs = Project.Logs
  #가져온 테스트 로그의 갯수
  LogItemNum = ProLogs.LogItemsCount
  #가저온 테스트 로그에 접근
  LogItem = ProLogs.LogItem[LogItemNum-1]
  LogData = LogItem.Data[0]
  LogItemChildNum = LogItem.ChildCount
  #for i in range(0, LogItemChildNum) : 
  LogItemChild = LogItem.Child[LogItemChildNum-1]
  if (LogItemChild.Status == 2 ) :
    LogError = LogItemChild.Child[0]
    LogErrorData = LogError.Data[0]
    LogErrorRow = LogErrorData.Rows[0]
    LogErrorRowCount = LogErrorRow.ChildRowCount
    LogErrorName = LogErrorRow.ChildRow[LogErrorRowCount-1]
    LogErrorDesCount = LogErrorName.ChildRowCount

    Name = LogErrorName.ValueByName["Message"]
      
    #Log.Warning(Name)
    #Log.Warning(listdes)
    return Name
      
def getDes():
  #이 코드가 실행되는 위치에 테스트 로그를 가져옴
  ProLogs = Project.Logs
  #가져온 테스트 로그의 갯수
  LogItemNum = ProLogs.LogItemsCount
  #가저온 테스트 로그에 접근
  LogItem = ProLogs.LogItem[LogItemNum-1]
  LogData = LogItem.Data[0]
  LogItemChildNum = LogItem.ChildCount
    #로그 아이템서치
  LogItemChild = LogItem.Child[LogItemChildNum-1]
  if (LogItemChild.Status == 2 ) :
   #에러가 있는 로그 아이템 찾고 
    LogError = LogItemChild.Child[0]
    LogErrorData = LogError.Data[0]
    LogErrorRow = LogErrorData.Rows[0]
    LogErrorRowCount = LogErrorRow.ChildRowCount
    LogErrorName = LogErrorRow.ChildRow[LogErrorRowCount-1]
    LogErrorDesCount = LogErrorName.ChildRowCount

      #LogErrorDes = LogErrorName.ChildRow[0]
    Name = LogErrorName.ValueByName["Message"]
      
    #Log.Warning(Name)
    #Log.Warning(LogErrorDesCount)
    list = []
    for j in range(0, LogErrorDesCount) :
      LogErrorDes = LogErrorName.ChildRow[j]
      Des = LogErrorDes.ValueByName["Message"]
      #Log.Warning(Des)
      list.insert(j,Des)
      
      listdes = "\n".join(list)
      #Log.Warning(listdes)
      return listdes
      

import shutil
import os.path
from distutils.dir_util import copy_tree
import time
import glob
import re
global Count
Count = 0
global mhtpass
mhtpass = 'C:\\Work\\Log{count}.mht'
global testname
testname = Project.TestItems.Current.Name
global Des
global Sum

def GeneralEvents_OnLogError(Sender, LogParams):
  global testname
  global testbody
  #testname=Project.TestItems.Current.Name
  global Count
  global Des
  global Sum
  Log.SaveToDisk()
  Des = getDes()
  Sum = getSum()

  
def GeneralEvents_OnStopTestCase(Sender, StopTestCaseParams):
  if StopTestCaseParams.StatusAsText == "Error":
    global Count
    global testname
    Log.SaveResultsAs(mhtpass.format(count=Count), lsMHT,True,2)
    Log.SaveToDisk()
    global Des
    global Sum
    Des = getDes()
    Sum = getSum()
    testname=Project.TestItems.Current.Name
    scenario = Features.Item[0].Description
    TestItems = Project.TestItems
    summary = '[TCRT]{name} ' + Sum
    IssueToJira(summary.format(name=testname), Des)
  
def IssueToJira(summary,description):
  # Log in to Jira
  global Count
  Jira.Login("https://tomocube.atlassian.net/", "seji@tomocube.com", "HxauRk8w78iNlYxCscMC536E")
  priorityJSON = '{"SUNGEUN JI":"Low"}'
  # Create an object that defines task properties
  jiraData = Jira.CreateNewIssueData("TCRT", "버그").\
                setField("summary", summary).\
                setField("description", description).\
                setField("customfield_10667", ProjectSuite.Variables.version);
  # Post the issue to Jira 
  key = Jira.PostIssue(jiraData)
  # Attach the desired file to the created issue
  Jira.PostAttachment(key, mhtpass.format(count=Count))
  mhtremove = r'C:\Work\*.mht'
  upPriorityJSON = '{"SUNGEUN JI":"High"}'
  # Create an object that defines updating task properties
  upJiraData = Jira.CreateUpdateIssueData()
  # Update the issue
  Count = Count + 1
  Jira.UpdateIssue(key, upJiraData)
    
@given("빈 TA가 열려있다.")
def step_impl():
  test = Project.Variables.test
  cils = Project.Variables.cils
  if test :
    if cils :    
      TestedApps.TomoAnalysis.Run(1, True) 
      #대충 실즈 매뉴 선택
    else :
      TestedApps.TomoAnalysis.Run(1, True) 
  else :
    #테스트 버전의 TA test
    TestedApps.TomoAnalysis1.Run(1, True) 
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.bt_square_primary.ClickButton()
    
     
@when("open 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(162, 20)


@when("기존 프로젝트 파일을 선택한다.")
def step_impl():
  HWNDView = Aliases.TomoAnalysis.dlgOpenProject.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  tomoAnalysis = Aliases.TomoAnalysis
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\Select Project\\Select Project.tcpro")
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.explorerFrame.explorerSplitter.explorerPanel.panel_base.panel_contents.qt_scrollarea_viewport.Click(216, 172)

@then("기존의 프로젝트가 열렸는지 확인")
def step_impl():
  Regions.qt_scrollarea_viewport1.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.explorerFrame.explorerSplitter.explorerPanel.panel_base.panel_contents.qt_scrollarea_viewport)

  
@then("Operation sequence 화면이 Select Project 탭에서 다음 진행 단계의 탭으로 자동으로 넘어갔는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.frame_tab.bt_round_tab6, "checked", cmpEqual, True)

@given("TCF파일이 들어있는 프로젝트가 열려있다.")
def step_impl():

  dir = r'E:\Regression_Test\Prefix_File\Import Data - Set TCF\playground'
  if os.path.isdir(dir):
    shutil.rmtree(dir)
  dir1 = r'E:\Regression_Test\Prefix_File\Import Data - Set TCF'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\Import Data - Set TCF'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(136, 21)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\Import Data - Set TCF\\Import Data - Set TCF.tcpro")
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.AddTcfPanel2.bt_round_operation.Click(70, 34)
  dlgOpenTcfFolder = tomoAnalysis.dlgOpenTcfFolder
  UIItemsView = dlgOpenTcfFolder.DUIViewWndClassName.Item.CtrlNotifySink2.ShellView.Item
  UIItem = UIItemsView.Regression_Test
  UIItem.Item.Click(64, 9)
  UIItem.Keys("[Enter]")
  UIItemsView.TomoAnalysis_test_data.Item.Click(93, 10)
  dlgOpenTcfFolder.btn_2.ClickButton()

@given("TCF의 썸네일이 보이는 Preview 패널이 열려 있다.")
def step_impl():

  dir = r'E:\Regression_Test\Prefix_File\Import Data - Set TCF\playground'
  if os.path.isdir(dir):
    shutil.rmtree(dir)
  dir1 = r'E:\Regression_Test\Prefix_File\Import Data - Set TCF'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\Import Data - Set TCF'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(136, 21)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\Import Data - Set TCF\\Import Data - Set TCF.tcpro")
    
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.frame_tab.bt_round_tab6.ClickButton()

  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.AddTcfPanel2.bt_round_operation.Click(146, 18)
  dlgOpenTcfFolder = tomoAnalysis.dlgOpenTcfFolder
  HWNDView = dlgOpenTcfFolder.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  UIItemsView = HWNDView.CtrlNotifySink2.ShellView.Item
  UIItem = UIItemsView.Regression_Test
  UIItem.Item.Click(91, 16)
  UIItem.Keys("[Enter]")
  UIItemsView.TomoAnalysis_test_data.Item.Click(95, 5)
  dlgOpenTcfFolder.btn_2.ClickButton()

@when("원하는 TCF 파일을 누른다.")
def step_impl():
  tableWidget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.detailsTab.DetailPanel.widget.contentsTableWidget
  tableWidget.ClickCell("1", "Name")
  tableWidget.ClickCell("2", "Name")

@then("1개의 TCF 파일이 선택됐는지 확인")
def step_impl():
  Regions.qt_scrollarea_viewport10.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.detailsTab.DetailPanel.widget.contentsTableWidget.qt_scrollarea_viewport)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
  
@when("Preview 패널에서 Select all 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.bt_round_gray500.ClickButton()
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.Click(430, 1)

@then("불러진 모든 TCF가 선택되었는지 확인")
def step_impl():
  Regions.ThumbnailPanel15.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel)


@when("Preview 패널에서 Deselect all 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.bt_round_gray5002.ClickButton()

@when("List 패널에서 Deselect all 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.detailsTab.DetailPanel.widget.bt_round_gray5002.ClickButton()

  
@when("New Playground 버튼을 누른다.")
def step_impl():
  frame = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base
  frame.frame_tab.bt_round_tab.ClickButton()
  frame.widget_panel.AddPGPanel.widget_panel2.bt_round_operation.Click(123, 12)

@when("Playground의 이름을 지정한다.")
def step_impl():
  widget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.AddPGPanel.widget_panel
  lineEdit = widget.input_high
  lineEdit.Click(113, 16)
  lineEdit.SetText("test")


@then("Project Explorer 패널에 지정한 이름의 playground가 생성됐는지 확인")
def step_impl():
  Regions.qt_scrollarea_viewport17.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.explorerFrame.explorerSplitter.pglistPanel.panel_base.panel_contents.qt_scrollarea_viewport)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@when("OK 버튼을 누른다.")
def step_impl():
  tomoAnalysis_ProjectManager_Plugins_SelectAppPanel = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectAppPanel
  tomoAnalysis_ProjectManager_Plugins_SelectAppPanel.bt_round_operation.ClickButton()

@then("Playground 이름을 지정하는 칸에 ‘현재 시간-pc ID’ 로 default name이 설정되었는지 확인1")
def step_impl():
  global time1 
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.AddPGPanel.widget_panel.input_high, "placeholderText", cmpEqual, "{}-DESKTOP-PANKC9P".format(time1))
  del time1
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
  
@then("Playground 이름을 지정하는 칸에 ‘현재 시간-pc ID’ 로 default name이 설정되었는지 확인2")
def step_impl():
  global time2 
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.AddPGPanel.widget_panel.input_high, "placeholderText", cmpEqual, "{}-DESKTOP-PANKC9P".format(time2))
  del time2
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)  

@given("Set playground의 Playground 이름을 지정하는 탭이 열려있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir = r'E:\Regression_Test\Prefix_File\Set Playground - no PG\playground'
  if os.path.isdir(dir):
    shutil.rmtree(dir)
  dir1 = r'E:\Regression_Test\Prefix_File\Set Playground - no PG'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\Set Playground - no PG'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(47, 17)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\Set Playground - no PG\\Set Playground - no PG.tcpro")
  del dir
  del dir1
  del dir2
  
@when("Default name 외의 Playground의 이름을 직접 지정한다.")
def step_impl():
  lineEdit = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.AddPGPanel.widget_panel.input_high
  lineEdit.Click(94, 9)
  lineEdit.SetText("test")


@when("Playground를 클릭 하고 키보드의 엔터 키를 누른다.")
def step_impl():
  widget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.AddPGPanel.widget_panel
  lineEdit = widget.input_high
  lineEdit.Click(151, 31)
  widget.Click(303, 116)
  lineEdit.Keys("[Enter]")

@then("Playground setup 패널에서 playground의 이름이 변경되었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.h4, "QtText", cmpEqual, "test")

@then("Playground setup 패널에서 playground가 삭제되었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.h4, "QtText", cmpEqual, "Playground1")

@given("‘Playground’라는 이름의 playground가 생성되어 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(65, 16)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  tv_ = dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_
  tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\Set Playground - Playground\\Set Playground - Playground.tcpro")

@when("Set playground 탭으로 이동한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.frame_tab.bt_round_tab.ClickButton()

@given("프로젝트에 Playground가 두 개 이상 생성되어 있다.")
def step_impl():

  dir = r'E:\Regression_Test\Prefix_File\Set Playground - Two Playground\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir1 = r'E:\Regression_Test\Prefix_File\Set Playground - Two Playground'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\Set Playground - Two Playground'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(129, 17)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\Set Playground - Two Playground\\Set Playground - Two Playground.tcpro")

@when("Playground Setup 패널에서 원하는 Playground를 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.explorerFrame.explorerSplitter.pglistPanel.panel_base.panel_contents.ClickItem("Playground1")

@then("Playground Setup 패널에서 선택된 Playground가 하이라이트 됨을 확인")
def step_impl():
  Regions.qt_scrollarea_viewport22.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.explorerFrame.explorerSplitter.pglistPanel.panel_base.panel_contents.qt_scrollarea_viewport)

@then("Playground canvas 패널 상단에 선택된 Playground의 이름이 나타나는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.h4, "QtText", cmpEqual, "Playground1")

  
@when("Create Hypercube 탭을 연다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.frame_tab.bt_round_tab2.ClickButton()

@then("Hypercube의 이름이 ‘Hypercube’로 default name이 설정되었는지 확인")
def step_impl():
  Regions.HyperCubePanel.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.HyperCubePanel)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)  

@given("Create Hypercube 탭이 열려있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir = r'E:\Regression_Test\Prefix_File\Set Playground - PG\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir1 = r'E:\Regression_Test\Prefix_File\Create Hypercube'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\Create Hypercube'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(32, 32)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  HWNDView = dlgOpenProject.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  UIItemsView = HWNDView.CtrlNotifySink2.ShellView.Item
  UIItem = UIItemsView.Regression_Test
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\Create Hypercube\\Create Hypercube.tcpro")
  del dir
  del dir1
  del dir2
  
@when("Hypercube의 이름을 지정한다.")
def step_impl():
  lineEdit = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.HyperCubePanel.input_high
  lineEdit.Click(104, 26)
  lineEdit.SetText("HC")

@when("Create Hypercube 탭의 OK 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.HyperCubePanel.bt_round_operation.ClickButton()



@then("Project Explorer 패널에 생성된 Hypercube가 표시됨을 확인")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.explorerFrame.explorerSplitter.explorerPanel.panel_base.panel_contents.ExpandItem("Create Hypercube|test")
  Regions.qt_scrollarea_viewport23.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.explorerFrame.explorerSplitter.explorerPanel.panel_base.panel_contents.qt_scrollarea_viewport)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)  
  
@when("default name 외의 Hypercube의 이름을 직접 지정한다.")
def step_impl():
  lineEdit = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.HyperCubePanel.input_high
  lineEdit.Click(120, 35)
  lineEdit.SetText("HC")

@when("Hypercube 탭에서 키보드의 엔터 키를 누른다.")
def step_impl():
  lineEdit = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.HyperCubePanel.input_high
  lineEdit.Click(164, 15)
  lineEdit.Keys("[Enter]")


@then("Playground canvas 탭에 Normal Hypercube 아이콘과 Hypercube 이름이 나타남을 확인")
def step_impl():
  Regions.HC1.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.playgroundPanel.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents.TomoAnalysis_ProjectManager_Plugins_DragIconWindow)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)  

@then("Operation sequence의 화면이 Create Hypercube 탭에서 Create Cube 탭으로 자동으로 넘어감을 확인")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.CubePanel.Click(681, 175)
  Regions.panel_base2.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
  

@given("한 개 이상의 hypercube가 생성되어 있다.")
def step_impl():

  dir = r'E:\Regression_Test\Prefix_File\Create Hypercube - HC\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir1 = r'E:\Regression_Test\Prefix_File\Create Hypercube - HC'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\Create Hypercube - HC'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(70, 14)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  tv_ = dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_
  tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\Create Hypercube - HC\\Create Hypercube - HC.tcpro")

@when("Playground canvas 위의 Normal hypercube 아이콘을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.playgroundPanel.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents.TomoAnalysis_ProjectManager_Plugins_DragIconWindow2.TomoAnalysis_ProjectManager_Plugins_DraggableIcon.Click(55, 48)

@then("Playground canvas 위의 Normal hypercube 아이콘이 Active hypercube 아이콘으로 바뀜을 확인")
def step_impl():
  Regions.HC7.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.playgroundPanel.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents.TomoAnalysis_ProjectManager_Plugins_DragIconWindow2)

@then("Preview 패널의 Title이 선택한 hypercube 이름으로 바뀌며 표시되는 TCF도 Hypercube 내의 데이터로 바뀜을 확인")
def step_impl():
  Regions.widget_panel.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel)

@given("Hypercube가 선택되어 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir = r'E:\Regression_Test\Prefix_File\Create Hypercube - HC\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir1 = r'E:\Regression_Test\Prefix_File\Create Hypercube - HC'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\Create Hypercube - HC'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  splitter = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter
  splitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.DblClick(112, 36)
  tomoAnalysis.dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\Create Hypercube - HC\\Create Hypercube - HC.tcpro")
  splitter.playgroundFrame.playgroundSplitter.playgroundPanel.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents.TomoAnalysis_ProjectManager_Plugins_DragIconWindow2.TomoAnalysis_ProjectManager_Plugins_DraggableIcon.Click(35, 32)
  del dir
  del dir1
  del dir2
  
@when("Hypercube 아이콘 바깥의Playground canvas 아무 영역이나 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.playgroundPanel.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents.TomoAnalysis_ProjectManager_Plugins_DragIconWindow2.Click(75, 182)

@then("Playground canvas 위의 Active hypercube 아이콘이 Normal hypercube 아이콘으로 바뀜을 확인")
def step_impl():
  Regions.HC3.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.playgroundPanel.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents.TomoAnalysis_ProjectManager_Plugins_DragIconWindow2)


@when("Hypercube 아이콘 바깥의Playground canvas 빈 공간 아무 곳이나 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.playgroundPanel.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents.TomoAnalysis_ProjectManager_Plugins_DragIconWindow2.Click(63, 244)

@then("Preview 패널의 Title이 Playground 이름으로 바뀜을 확인")
def step_impl():
  Regions.h4.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.h4)


@given("Hypercube가 생성되어 있다.")
def step_impl():

  dir = r'E:\Regression_Test\Prefix_File\Create Hypercube - Hypercube\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir1 = r'E:\Regression_Test\Prefix_File\Create Hypercube - Hypercube'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\Create Hypercube - Hypercube'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(11, 4)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.WorkerW.ReBarWindow32.AddressBandRoot.progress.BreadcrumbParent.toolbar.ClickItemXY("‎Prefix_File", 40, 10, False)
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\Create Hypercube - Hypercube\\Create Hypercube - Hypercube.tcpro")
  del dir
  del dir1
  del dir2
  
@when("Playground canvas의 Hypercube 아이콘을 우클릭한다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.playgroundPanel.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents.TomoAnalysis_ProjectManager_Plugins_DragIconWindow2.TomoAnalysis_ProjectManager_Plugins_DraggableIcon.ClickR(32, 49)

@when("Rename Hypercube 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.Menu.QtMenu.Click("Rename Hypercube")

@when("팝업에서 Hypercube의 이름을 지정하고 OK 버튼을 누른다.")
def step_impl():
  tomoAnalysis_ProjectManager_Plugins_RenameCube = Aliases.TomoAnalysis.RenameCube
  lineEdit = tomoAnalysis_ProjectManager_Plugins_RenameCube.newName
  lineEdit.Click(47, 14)
  lineEdit.SetText("HC1")
  tomoAnalysis_ProjectManager_Plugins_RenameCube.okBtn.ClickButton()


@then("Playground canvas에서 Hypercube 이름이 변경됨을 확인")
def step_impl():
  Regions.HC4.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.playgroundPanel.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents.TomoAnalysis_ProjectManager_Plugins_DragIconWindow)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)



@when("팝업에서 Delete 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.DeleteCube.deleteBtn.ClickButton()

@then("Project Explorer 패널에서 Hypercube가 삭제됨을 확인")
def step_impl():
  Regions.qt_scrollarea_viewport25.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.explorerFrame.explorerSplitter.explorerPanel.panel_base.panel_contents.qt_scrollarea_viewport)

@then("Playground canvas에서 Hypercube 아이콘이 삭제됨을 확인")
def step_impl():
  Regions.HC5.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.playgroundPanel.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents.TomoAnalysis_ProjectManager_Plugins_DragIconWindow)


@when("Delete Hypercube 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.Menu.QtMenu.Click("Delete Hypercube")

@when("팝업에서 키보드의 엔터 키를 누른다.")
def step_impl():
  tomoAnalysis_ProjectManager_Plugins_DeleteCube = Aliases.TomoAnalysis.DeleteCube
  tomoAnalysis_ProjectManager_Plugins_DeleteCube.Click(208, 28)
  tomoAnalysis_ProjectManager_Plugins_DeleteCube.cubeName.Keys("[Enter]")

@when("Copy Hypercube 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.Menu.QtMenu.Click("Copy Hypercube")


@then("Project Explorer 패널에서 ‘\\(원본 Hypercube 이름)_copy’의 Hypercube가 생성됨을 확인")
def step_impl():
  Regions.HC6.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.playgroundPanel.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents.TomoAnalysis_ProjectManager_Plugins_DragIconWindow)

@then("Playground canvas에서 ‘\\(원본 Hypercube 이름)_copy’의 Hypercube가 생성됨을 확인")
def step_impl():
  Regions.ST39.Check(Regions.CreateRegionInfo(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.playgroundPanel.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents.TomoAnalysis_ProjectManager_Plugins_DragIconWindow4, 115, 254, 118, 90, False))


@given("‘Hypercube’라는 이름의 Hypercube가 생성되어 있다.")
def step_impl():

  dir = r'E:\Regression_Test\Prefix_File\Create Hypercube - New Hypercube\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir1 = r'E:\Regression_Test\Prefix_File\Create Hypercube - New Hypercube'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\Create Hypercube - New Hypercube'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(164, 34)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\Create Hypercube - New Hypercube\\Create Hypercube - New Hypercube.tcpro")

@when("같은 Playground에서 Creat Hypercube 탭을 연다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.frame_tab.bt_round_tab2.ClickButton()

@when("새로 생성할 hypercube의 이름을 ‘Hypercube’로 지정한다.")
def step_impl():
  tomoAnalysis_ProjectManager_Plugins_HyperCubePanel = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.HyperCubePanel
  lineEdit = tomoAnalysis_ProjectManager_Plugins_HyperCubePanel.input_high
  lineEdit.Click(128, 21)
  lineEdit.SetText("Hypercube")


@when("Creat Hypercube 탭의 OK 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.HyperCubePanel.bt_round_operation.ClickButton()

@then("에러 팝업이 나타나며 hypercube가 생성되지 않음을 확인")
def step_impl():
  Regions.MessageBox.Check(Aliases.TomoAnalysis.MessageBox)
  Aliases.TomoAnalysis.MessageBox.qt_msgbox_buttonbox.buttonOk.ClickButton()


@given("Hypercube가 하나 이상 생성되어 있다.")
def step_impl():

  dir = r'E:\Regression_Test\Prefix_File\Create Hypercube - Hypercube\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir1 = r'E:\Regression_Test\Prefix_File\Create Hypercube - Hypercube'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\Create Hypercube - Hypercube'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(89, 8)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\Create Hypercube - Hypercube\\Create Hypercube - Hypercube.tcpro")

@when("Create Cube 탭을 연다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.frame_tab.bt_round_tab3.ClickButton()

@then("Cube의 이름이 ‘CubeName’으로 default name이 설정되었는지 확인")
def step_impl():
  Regions.input_high.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.CubePanel.input_high)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@given("Create Cube 탭이 열려있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir = r'E:\Regression_Test\Prefix_File\Create Cube\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir1 = r'E:\Regression_Test\Prefix_File\Create Cube'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\Create Cube'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(73, 21)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\Create Cube\\Create Cube.tcpro")

@when("Cube의 이름과 경로를 지정하고 Create Cube 탭의 OK 버튼을 누른다.")
def step_impl():
  tomoAnalysis_ProjectManager_Plugins_CubePanel = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.CubePanel
  lineEdit = tomoAnalysis_ProjectManager_Plugins_CubePanel.input_high
  lineEdit.Click(102, 24)
  lineEdit.SetText("Cube")
  tomoAnalysis_ProjectManager_Plugins_CubePanel.dropdown_high.ClickItem("Hypercube")
  tomoAnalysis_ProjectManager_Plugins_CubePanel.bt_round_operation.ClickButton()

@then("Project Explorer 패널에 생성된 Cube가 표시됨을 확인")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.explorerFrame.explorerSplitter.explorerPanel.panel_base.panel_contents.ExpandItem("Create Cube|test|Hypercube")
  Regions.qt_scrollarea_viewport28.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.explorerFrame.explorerSplitter.explorerPanel.panel_base.panel_contents.qt_scrollarea_viewport)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)


@when("이름을 입력하는 빈 칸을  클릭하여 Cube의 이름을 직접 지정한다.")
def step_impl():
  lineEdit = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.CubePanel.input_high
  lineEdit.Click(45, 34)
  lineEdit.SetText("Cube")

@when("Cube의 경로를 지정하고키보드의 엔터 키를 누른다.")
def step_impl():
  tomoAnalysis_ProjectManager_Plugins_CubePanel = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.CubePanel
  comboBox = tomoAnalysis_ProjectManager_Plugins_CubePanel.dropdown_high
  comboBox.DropDown()
  comboBox.Keys("[Enter]")
  tomoAnalysis_ProjectManager_Plugins_CubePanel.Click(255, 151)
  comboBox.Keys("[Enter]")

@when("Cube의 이름을 지정한다.")
def step_impl():
  lineEdit = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.CubePanel.input_high
  lineEdit.Click(97, 23)
  lineEdit.SetText("Cube")

@when("Cube의 경로를 지정하는 드롭다운 버튼을 클릭하여 경로를 지정하고 키보드의 엔터 키를 누른다.")
def step_impl():
  comboBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.CubePanel.dropdown_high
  comboBox.DropDown()
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.CubePanel.dropdown_high.ClickItem("Hypercube")
  tomoAnalysis_ProjectManager_Plugins_CubePanel = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.CubePanel
  tomoAnalysis_ProjectManager_Plugins_CubePanel.Click(665, 147)
  tomoAnalysis_ProjectManager_Plugins_CubePanel.Keys("[Enter]")




@given("Hypercube가 비어있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir = r'E:\Regression_Test\Prefix_File\Create Cube\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir1 = r'E:\Regression_Test\Prefix_File\Create Cube'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\Create Cube'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(73, 21)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\Create Cube\\Create Cube.tcpro")

@when("Cube 이름을 지정하고 빈 Hypercube를 경로로 지정한다.")
def step_impl():
  tomoAnalysis_ProjectManager_Plugins_CubePanel = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.CubePanel
  lineEdit = tomoAnalysis_ProjectManager_Plugins_CubePanel.input_high
  lineEdit.Click(101, 20)
  lineEdit.SetText("Cube")


@when("Create Cube 탭의 OK 버튼을 누른다.")
def step_impl():
  tomoAnalysis_ProjectManager_Plugins_CubePanel = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.CubePanel
  tomoAnalysis_ProjectManager_Plugins_CubePanel.bt_round_operation.ClickButton()

@then("Playground canvas에서 큐브가 생성된 Hypercube의 아이콘이 빈 Hypercube 아이콘에서 Hypercube containing Cubes 아이콘으로 변경됨을 확인")
def step_impl():
  Regions.CC1.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.playgroundPanel.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents.TomoAnalysis_ProjectManager_Plugins_DragIconWindow)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

  
@given("‘CubeName’ 이라는 이름의 Cube가 생성되어 있다.")
def step_impl():

  dir = r'E:\Regression_Test\Prefix_File\Create Cube - CubeName\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir1 = r'E:\Regression_Test\Prefix_File\Create Cube - CubeName'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\Create Cube - CubeName'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(85, 30)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\Create Cube - CubeName\\Create Cube - CubeName.tcpro")

@when("같은 Playground에서 Creat Cube 탭을 연다.")
def step_impl():
  splitter = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter
  splitter.explorerFrame.explorerSplitter.pglistPanel.panel_base.panel_contents.ClickItem("test")
  splitter.operationFrame.operationSplitter.operationPanel.panel_base.frame_tab.bt_round_tab3.ClickButton()

@when("새로 생성할 cube의 이름을 ‘CubeName’ 으로 지정하고 경로를 지정한다.")
def step_impl():
  tomoAnalysis_ProjectManager_Plugins_CubePanel = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.CubePanel
  lineEdit = tomoAnalysis_ProjectManager_Plugins_CubePanel.input_high
  lineEdit.Click(117, 17)
  lineEdit.SetText("CubeName")
  tomoAnalysis_ProjectManager_Plugins_CubePanel.dropdown_high.ClickItem("HC")


@given("한 개 이상의 cube가 생성되어 있다.")
def step_impl():

  dir = r'E:\Regression_Test\Prefix_File\Create Cube - CubeName\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir1 = r'E:\Regression_Test\Prefix_File\Create Cube - CubeName'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\Create Cube - CubeName'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(85, 30)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\Create Cube - CubeName\\Create Cube - CubeName.tcpro")

@when("Playground canvas 위의 Normal cube 아이콘을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.playgroundPanel.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents.TomoAnalysis_ProjectManager_Plugins_DragIconWindow2.TomoAnalysis_ProjectManager_Plugins_DraggableIcon.Click(42, 20)



@then("Preview 패널의 Title이 선택한 cube 이름으로 바뀌며 표시되는 TCF도 cube 내의 데이터로 바뀜을 확인")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.playgroundPanel.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents.TomoAnalysis_ProjectManager_Plugins_DragIconWindow2.TomoAnalysis_ProjectManager_Plugins_DraggableIcon.Click(24, 45)
  Regions.widget_panel1.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
  
@then("Playground canvas 위의 Normal cube 아이콘이 Active cube 아이콘으로 바뀜을 확인")
def step_impl():
  Regions.CC4.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.playgroundPanel.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents.TomoAnalysis_ProjectManager_Plugins_DragIconWindow2)


@given("Cube가 선택되어 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  tomoAnalysis = Aliases.TomoAnalysis
  splitter = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter
  splitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(75, 45)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\Create Cube - CubeName\\Create Cube - CubeName.tcpro")
  splitter.playgroundFrame.playgroundSplitter.playgroundPanel.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents.TomoAnalysis_ProjectManager_Plugins_DragIconWindow2.TomoAnalysis_ProjectManager_Plugins_DraggableIcon.Click(35, 53)

@when("Cube 아이콘 바깥의Playground canvas 아무 영역이나 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.playgroundPanel.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents.TomoAnalysis_ProjectManager_Plugins_DragIconWindow2.Click(46, 179)

@then("Playground canvas 위의 Active cube 아이콘이 Normal cube 아이콘으로 바뀜을 확인")
def step_impl():
  Regions.CC5.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.playgroundPanel.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents.TomoAnalysis_ProjectManager_Plugins_DragIconWindow2)

@then("Preview 패널에 표시되는 TCF가 Playground 내의 전체 TCF로 바뀜을 확인한다")
def step_impl():
  Regions.ThumbnailPanel9.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel)

  

@then("Hypercube만 존재하는 Preview 패널에 표시되는 TCF가 Playground 내의 전체 TCF로 바뀜을 확인")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.playgroundPanel.panel_base.scrollarea_bottom_round.Keys("^s")
  Regions.ThumbnailPanel10.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@then("Playground canvas 탭에 Normal Cube 아이콘과 Cube 이름이 나타남을 확인")
def step_impl():
  Regions.CC7.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.playgroundPanel.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents.TomoAnalysis_ProjectManager_Plugins_DragIconWindow)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@then("에러 팝업이 나타나며 cube가 생성되지 않음을 확인")
def step_impl():
  Regions.MessageBox2.Check(Aliases.TomoAnalysis.MessageBox2)
  Aliases.TomoAnalysis.MessageBox2.qt_msgbox_buttonbox.buttonOk.ClickButton()


@then("Project Explorer 패널에서 Hypercube 이름이 변경됨을 확인")
def step_impl():
  Regions.qt_scrollarea_viewport30.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.explorerFrame.explorerSplitter.explorerPanel.panel_base.panel_contents.qt_scrollarea_viewport)

@given("Cube가 생성되어 있다.")
def step_impl():

  dir = r'E:\Regression_Test\Prefix_File\Create Cube - CubeName\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir1 = r'E:\Regression_Test\Prefix_File\Create Cube - CubeName'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\Create Cube - CubeName'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(85, 30)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\Create Cube - CubeName\\Create Cube - CubeName.tcpro")



@when("Rename Cube 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.Menu.QtMenu.Click("Rename Cube")

@when("팝업에서 Cube의 이름을 지정하고 OK 버튼을 누른다.")
def step_impl():
  tomoAnalysis_ProjectManager_Plugins_RenameCube = Aliases.TomoAnalysis.RenameCube
  lineEdit = tomoAnalysis_ProjectManager_Plugins_RenameCube.newName
  lineEdit.Click(59, 12)
  lineEdit.SetText("Cube")
  tomoAnalysis_ProjectManager_Plugins_RenameCube.okBtn.ClickButton()

@then("Project Explorer 패널에서 Cube 이름이 변경됨을 확인")
def step_impl():
  Regions.qt_scrollarea_viewport31.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.explorerFrame.explorerSplitter.explorerPanel.panel_base.panel_contents.qt_scrollarea_viewport)

@then("Playground canvas에서 Cube 이름이 변경됨을 확인")
def step_impl():
  Regions.CC8.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.playgroundPanel.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents.TomoAnalysis_ProjectManager_Plugins_DragIconWindow)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@when("팝업에서 Cube의 이름을 지정하고 키보드의 엔터 키를 누른다.")
def step_impl():
  lineEdit = Aliases.TomoAnalysis.RenameCube.newName
  lineEdit.Click(52, 16)
  lineEdit.SetText("Cube")
  lineEdit.Keys("[Enter]")


@then("Project Explorer 패널에서 Cube가 삭제됨을 확인")
def step_impl():
  Regions.qt_scrollarea_viewport32.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.explorerFrame.explorerSplitter.explorerPanel.panel_base.panel_contents.qt_scrollarea_viewport)

@then("Playground canvas에서 Cube 아이콘이 삭제됨을 확인")
def step_impl():
  Regions.CC9.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.playgroundPanel.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents.TomoAnalysis_ProjectManager_Plugins_DragIconWindow)

  
@when("Delete Cube 팝업에서 키보드의 엔터 키를 누른다.")
def step_impl():
  tomoAnalysis_ProjectManager_Plugins_DeleteCube = Aliases.TomoAnalysis.DeleteCube
  tomoAnalysis_ProjectManager_Plugins_DeleteCube.Click(139, 122)
  tomoAnalysis_ProjectManager_Plugins_DeleteCube.cubeName.Keys("[Enter]")

@when("Playground canvas의 Cube 아이콘을 우클릭한다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.playgroundPanel.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents.TomoAnalysis_ProjectManager_Plugins_DragIconWindow2.TomoAnalysis_ProjectManager_Plugins_DraggableIcon.ClickR(44, 51)


@when("Delete Cube 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.Menu.QtMenu.Click("Delete Cube")

@when("Copy Cube 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.Menu.QtMenu.Click("Copy Cube")

@then("Project Explorer 패널에서 '\\(Cube가 속해 있는 hypercube의 원본 Hypercube 이름)_copy’의 Hypercube가 생성됨을 확인")
def step_impl():
  Regions.qt_scrollarea_viewport33.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.explorerFrame.explorerSplitter.explorerPanel.panel_base.panel_contents.qt_scrollarea_viewport)

@then("Playground canvas에서 ‘\\(원본 Hypercube 이름)_copy’의 Hypercube가 생성됨을 확인.")
def step_impl():
  Regions.CC10.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.playgroundPanel.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents.TomoAnalysis_ProjectManager_Plugins_DragIconWindow.TomoAnalysis_ProjectManager_Plugins_DraggableIcon)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@given("Playground에 생성된 cube가 모두 비어있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(110, 28)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.WorkerW.ReBarWindow32.AddressBandRoot.progress.BreadcrumbParent.toolbar.ClickItemXY("‎Prefix_File", 16, 7, False)
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\Link Data to Cube\\Link Data to Cube.tcpro")

@when("Link Data to Cube 탭을 연다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.frame_tab.bt_round_tab5.ClickButton()

@then("Toolbox의 Link to Cube, Unlink from Cube 두 버튼이 민트색으로 하이라이트 됨을 확인")
def step_impl():
  Regions.panel_base7.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)


@given("Cube와 TCF가 생성되어 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir = r'E:\Regression_Test\Prefix_File\Link Data to Cube\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir1 = r'E:\Regression_Test\Prefix_File\Link Data to Cube'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\Link Data to Cube'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(110, 28)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.WorkerW.ReBarWindow32.AddressBandRoot.progress.BreadcrumbParent.toolbar.ClickItemXY("‎Prefix_File", 16, 7, False)
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\Link Data to Cube\\Link Data to Cube.tcpro")

@when("연결하고 싶은 TCF를 한 개 이상 선택한다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(143, 45)

@when("Toolbox의 Link to Data 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.bt_round_tool2.Click(63, 13)

@when("팝업에서 경로를 지정하고 Link 버튼을 누른다.")
def step_impl():
  tomoAnalysis_ProjectManager_Plugins_LinkDialog = Aliases.TomoAnalysis.LinkDialog
  tomoAnalysis_ProjectManager_Plugins_LinkDialog.h2.Click(192, 13)
  tomoAnalysis_ProjectManager_Plugins_LinkDialog.dropdown_high.ClickItem("CubeName")
  tomoAnalysis_ProjectManager_Plugins_LinkDialog.bt_square_primary.ClickButton()

@then("Project Explorer 패널에서 cube에 TCF가 연결되었는지 확인")
def step_impl():
  Regions.qt_scrollarea_viewport71.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.detailsTab.DetailPanel.widget.includedTableWidget.qt_scrollarea_viewport)

@then("Operation sequence 패널의 Link to Cube 탭에서 TCF가 연결되었는지 확인")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.frame_tab.bt_round_tab5.ClickButton()
  Regions.panel_base6.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base)

@then("Preview\\/List 패널에서 데이터가 연결되었음이 표시되었는지 확인")
def step_impl():
  Regions.ThumbnailPanel11.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
  
@when("팝업에서 경로를 지정하고 키보드의 엔터 키를 누른다.")
def step_impl():
   tomoAnalysis_ProjectManager_Plugins_LinkDialog = Aliases.TomoAnalysis.LinkDialog
   tomoAnalysis_ProjectManager_Plugins_LinkDialog.Click(96, 165)
   tomoAnalysis_ProjectManager_Plugins_LinkDialog.dropdown_high.Keys("[Enter]") 

@given("TCF가 cube에 연결되어 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir = r'E:\Regression_Test\Prefix_File\Link Data to Cube - Linked\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir1 = r'E:\Regression_Test\Prefix_File\Link Data to Cube - Linked'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\Link Data to Cube - Linked'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(129, 26)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.WorkerW.ReBarWindow32.AddressBandRoot.progress.BreadcrumbParent.toolbar.ClickItemXY("‎Prefix_File", 39, 11, False)
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\Link Data to Cube - Linked\\Link Data to Cube - Linked.tcpro")

@when("cube에 연결되어 있는 TCF를 선택한다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(151, 90)

@when("Toolbox의 Unlink from Cube 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.bt_round_tool3.Click(61, 20)

@when("팝업에서 Unlink 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.UnlinkDialog.bt_square_primary.ClickButton()

@then("Project Explorer 패널에서 cube에서 TCF가 연결 해제되었는지 확인")
def step_impl():
  Regions.qt_scrollarea_viewport72.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.detailsTab.DetailPanel.widget.includedTableWidget.qt_scrollarea_viewport)

@then("Operation sequence 패널의 Link to Cube 탭에서 TCF가 연결 해제되었는지 확인")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.frame_tab.bt_round_tab5.ClickButton()
  Regions.TcfLinkPanel.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.TcfLinkPanel)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@when("Unlink from Cube 팝업에서 키보드의 엔터 키를 누른다.")
def step_impl():
  tomoAnalysis_ProjectManager_Plugins_UnlinkDialog = Aliases.TomoAnalysis.UnlinkDialog
  tomoAnalysis_ProjectManager_Plugins_UnlinkDialog.Click(141, 136)
  tomoAnalysis_ProjectManager_Plugins_UnlinkDialog.input_high.Keys("[Enter]")
  
@given("Cube가 존재하는 List 패널이 열려 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir = r'E:\Regression_Test\Prefix_File\Link Data to Cube - List\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir1 = r'E:\Regression_Test\Prefix_File\Link Data to Cube - List'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\Link Data to Cube - List'
  copy_tree(dir2,dir1) 
  tomoAnalysis = Aliases.TomoAnalysis
  splitter = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter
  splitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(57, 35)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  toolbar = dlgOpenProject.WorkerW.ReBarWindow32.AddressBandRoot.progress.BreadcrumbParent.toolbar
  toolbar.ClickItemXY("‎Prefix_File", 25, 13, False)
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\Link Data to Cube - List\\Link Data to Cube - List.tcpro")
  splitter.previewPanel.panel_base.tabWidget.ClickTab("List")

@when("List 패널의 왼쪽 table에서 cube에 연결하고 싶은 TCF를 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.detailsTab.DetailPanel.widget.contentsTableWidget.ClickCell("1", "BF")

@when("List 패널의 > 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.detailsTab.DetailPanel.widget.bt_arrow_right.ClickButton()

@then("Project Explorer 패널에서 cube에 TCF가 연결되었는지 확인 한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.detailsTab.DetailPanel.widget.Click(613, 136)
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.explorerFrame.explorerSplitter.explorerPanel.panel_base.panel_contents.ExpandItem("Link Data to Cube - List|test|HC|CubeName")
  Regions.qt_scrollarea_viewport36.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.explorerFrame.explorerSplitter.explorerPanel.panel_base.panel_contents.qt_scrollarea_viewport)

 
@then("List 패널의 오른쪽 table에서 cube에 연결된 TCF row가 생성되었는지 확인")
def step_impl():
  Regions.DetailPanel.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.detailsTab.DetailPanel)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)


@when("List 패널의 오른쪽 table에서 cube에서 연결 해제하고 싶은 TCF를 선택한다.")
def step_impl():
  tabWidget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget
  tabWidget.ClickTab("List")
  tabWidget.qt_tabwidget_stackedwidget.detailsTab.DetailPanel.widget.includedTableWidget.ClickCell("1", "Name")

@when("List 패널의 < 버튼을 누른다.")
def step_impl():
  widget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.detailsTab.DetailPanel.widget
  widget.bt_arrow_right.ClickButton()
  widget.bt_arrow_left.ClickButton()

@then("List 패널의 오른쪽 table에서 cube에 연결 해제된 TCF row가 삭제되었는지 확인")
def step_impl():
  Regions.qt_scrollarea_viewport72.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.detailsTab.DetailPanel.widget.includedTableWidget.qt_scrollarea_viewport)

@given("cube가 비어있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir = r'E:\Regression_Test\Prefix_File\Link Data to Cube - Emty Cube\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir1 = r'E:\Regression_Test\Prefix_File\Link Data to Cube - Emty Cube'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\Link Data to Cube - Emty Cube'
  copy_tree(dir2,dir1) 
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(101, 36)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.WorkerW.ReBarWindow32.AddressBandRoot.progress.BreadcrumbParent.toolbar.ClickItemXY("‎Prefix_File", 21, 14, False)
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\Link Data to Cube - Emty Cube\\Link Data to Cube - Emty Cube.tcpro")

@when("빈 cube에 TCF를 연결한다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  frame = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base
  scrollArea = frame.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(167, 56)
  frame.bt_round_tool2.Click(53, 16)
  tomoAnalysis.LinkDialog.bt_square_primary.ClickButton()

@then("Playground canvas에서 Empty Cube 아이콘이 Cube containing TCF 아이콘으로 바뀌는지 확인")
def step_impl():
  Regions.LDtC2.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.playgroundPanel.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents.TomoAnalysis_ProjectManager_Plugins_DragIconWindow)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)


@when("cube에 연결된 TCF를 모두 연결 해제한다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  frame = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base
  tomoAnalysis_ProjectManager_Plugins_ThumbnailPanel = frame.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel
  tomoAnalysis_ProjectManager_Plugins_ThumbnailPanel.bt_round_gray5002.ClickButton()
  tomoAnalysis_ProjectManager_Plugins_ThumbnailPanel.bt_round_gray500.ClickButton()
  frame.bt_round_tool3.Click(48, 10)
  tomoAnalysis.UnlinkDialog.bt_square_primary.ClickButton()

@then("Playground canvas에서 TCF containing Cube 아이콘이 Empty Cube 아이콘으로 바뀌는지 확인")
def step_impl():
  Regions.LDtC.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.playgroundPanel.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents.TomoAnalysis_ProjectManager_Plugins_DragIconWindow)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@given("Preview 패널이 열려 있다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.ClickTab("Preview")

@when("TCF를 cube에 연결한다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  frame = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base
  scrollArea = frame.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(71, 98)
  frame.bt_round_tool2.Click(35, 16)
  tomoAnalysis.LinkDialog.bt_square_primary.ClickButton()

@then("TCF 썸네일에 연결된 cube의 이름이 표시됨을 확인")
def step_impl():
  Regions.ThumbnailPanel12.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@given("Hypercube에 Application이 연결되어있지 않다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir = r'E:\Regression_Test\Prefix_File\Link Data to Cube - Emty Cube\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir1 = r'E:\Regression_Test\Prefix_File\Link Data to Cube - Emty Cube'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\Link Data to Cube - Emty Cube'
  copy_tree(dir2,dir1) 
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(101, 36)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.WorkerW.ReBarWindow32.AddressBandRoot.progress.BreadcrumbParent.toolbar.ClickItemXY("‎Prefix_File", 21, 14, False)
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\Link Data to Cube - Emty Cube\\Link Data to Cube - Emty Cube.tcpro")

@when("cube에 TCF를 연결한다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  frame = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base
  scrollArea = frame.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(135, 66)
  frame.bt_round_tool2.Click(86, 11)
  tomoAnalysis.LinkDialog.bt_square_primary.ClickButton()

@then("Operation sequance 패널이 Select Application 탭으로 자동으로 이동하는지 확인")
def step_impl():
  Regions.LDtC3.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.playgroundPanel.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents.TomoAnalysis_ProjectManager_Plugins_DragIconWindow)
  Regions.panel_base8.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base)



@given("cube에 TCF가 연결되어 있다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  frame = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base
  scrollArea = frame.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  frame2 = scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame
  frame2.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(153, 86)
  scrollBar.wPosition = 0
  frame2.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget2.TC_TCF2DWidget.panel_contents_image.Widget.Click(73, 85)
  frame.bt_round_tool2.Click(67, 21)
  tomoAnalysis.LinkDialog.bt_square_primary.ClickButton()

@when("1개 이상의 TCF가 cube에 남아 있도록 TCF를 cube에서 연결 해제한다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  frame = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base
  scrollArea = frame.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget2.TC_TCF2DWidget.panel_contents_image.Widget.Click(150, 123)
  frame.bt_round_tool3.Click(99, 13)
  tomoAnalysis.UnlinkDialog.bt_square_primary.ClickButton()

@given("Operation sequence 패널의 Select Application 탭 화면이 열려 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir = r'E:\Regression_Test\Prefix_File\Link Data to Cube - Linked\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir1 = r'E:\Regression_Test\Prefix_File\Link Data to Cube - Linked'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\Link Data to Cube - Linked'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(129, 26)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.WorkerW.ReBarWindow32.AddressBandRoot.progress.BreadcrumbParent.toolbar.ClickItemXY("‎Prefix_File", 39, 11, False)
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\Link Data to Cube - Linked\\Link Data to Cube - Linked.tcpro")

@when("Cube에 연결된 TCF를 모두 연결 해제한다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  frame = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base
  frame.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  NameMapping.Sys.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(77, 83)
  frame.bt_round_tool3.Click(51, 8)
  tomoAnalysis.UnlinkDialog.bt_square_primary.ClickButton()

@then("Operation sequence 패널의 Select Application 탭 화면이 Link Data to Cube 탭으로 자동으로 돌아감을 확인")
def step_impl():
  Regions.panel_base9.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@given("Operation sequence에 Select Application 탭이 열려 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir = r'E:\Regression_Test\Prefix_File\Select Application\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir1 = r'E:\Regression_Test\Prefix_File\Select Application'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\Select Application'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(39, 39)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  HWNDView = dlgOpenProject.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  UIItemsView = HWNDView.CtrlNotifySink2.ShellView.Item
  UIItemsView.Regression_Test.Item.DblClick(52, 5)
  UIItemsView.Prefix_File.Item.DblClick(65, 15)
  UIItemsView.Create_Cube.Item.MouseWheel(-1)
  UIItemsView.Create_Hypercube_HC.Item.MouseWheel(-1)
  UIItemsView.Import_Data_Set_TCF.Item.MouseWheel(-1)
  UIItemsView.Link_Data_to_Cube_Emty_Cube.Item.MouseWheel(-1)
  UIItemsView.Link_Data_to_Cube_List.Item.MouseWheel(-1)
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\Select Application\\Select Application.tcpro")

@when("적용할 App과 적용 시킬 hypercube를 선택한다.")
def step_impl():
  tomoAnalysis_ProjectManager_Plugins_SelectAppPanel = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectAppPanel
  tomoAnalysis_ProjectManager_Plugins_SelectAppPanel.dropdown_high.ClickItem("Basic Analyzer")
  tomoAnalysis_ProjectManager_Plugins_SelectAppPanel.dropdown_high2.ClickItem("HC")

@then("Playground canvas에 Active 상태의 Application 아이콘의 생성을 확인")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectAppPanel.Click(263, 166)
  Regions.panel_base10.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)


@given("hypercube에 어떤 app도 연결되어 있지 않다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir = r'E:\Regression_Test\Prefix_File\Select Application\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir1 = r'E:\Regression_Test\Prefix_File\Select Application'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\Select Application'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(42, 31)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.WorkerW.ReBarWindow32.AddressBandRoot.progress.BreadcrumbParent.toolbar.ClickItemXY("‎Prefix_File", 43, 18, False)
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink2.ShellView.Item.Select_Application.Item.Click(88, 4)
  dlgOpenProject.btn_O.ClickButton()
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\Select Application\\Select Application.tcpro")

@then("Playground canvas에서 연결된 hypercube의 아이콘이 Hypercube containing Cubes 아이콘에서 Hypercube to which the application is connected 아이콘으로 변경됨을 확인")
def step_impl():
  Regions.SA1.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.playgroundPanel.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents.TomoAnalysis_ProjectManager_Plugins_DragIconWindow)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@when("Select Application패널에 OK 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectAppPanel.bt_round_operation.ClickButton()



@given("Hypercube에 BA app이 연결되어 있지 않다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir = r'E:\Regression_Test\Prefix_File\Select Application\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir1 = r'E:\Regression_Test\Prefix_File\Select Application'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\Select Application'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(42, 31)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.WorkerW.ReBarWindow32.AddressBandRoot.progress.BreadcrumbParent.toolbar.ClickItemXY("‎Prefix_File", 43, 18, False)
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink2.ShellView.Item.Select_Application.Item.Click(88, 4)
  dlgOpenProject.btn_O.ClickButton()
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\Select Application\\Select Application.tcpro")

@when("BA app과 적용 시킬 hypercube를 선택한다.")
def step_impl():
  tomoAnalysis_ProjectManager_Plugins_SelectAppPanel = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectAppPanel
  comboBox = tomoAnalysis_ProjectManager_Plugins_SelectAppPanel.dropdown_high2
  comboBox.DropDown()
  comboBox.CloseUp()
  tomoAnalysis_ProjectManager_Plugins_SelectAppPanel.dropdown_high.ClickItem("Basic Analyzer")

@then("Application Parameter 패널의 UI가 BA 버전으로 바뀌는지 확인")
def step_impl():
  Regions.SA2.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.playgroundPanel.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents.TomoAnalysis_ProjectManager_Plugins_DragIconWindow)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
@given("BA 아이콘이 Active 상태가 아니다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir = r'E:\Regression_Test\Prefix_File\Select Application\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir1 = r'E:\Regression_Test\Prefix_File\Select Application'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\Select Application'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(42, 31)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.WorkerW.ReBarWindow32.AddressBandRoot.progress.BreadcrumbParent.toolbar.ClickItemXY("‎Prefix_File", 43, 18, False)
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink2.ShellView.Item.Select_Application.Item.Click(88, 4)
  dlgOpenProject.btn_O.ClickButton()
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\Select Application\\Select Application.tcpro")

@when("hypercube에 연결된 BA 아이콘을 선택한다.")
def step_impl():
  splitter = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter
  splitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectAppPanel.bt_round_operation.ClickButton()
  splitter.playgroundFrame.playgroundSplitter.playgroundPanel.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents.TomoAnalysis_ProjectManager_Plugins_DragIconWindow.TomoAnalysis_ProjectManager_Plugins_DraggableIcon2.Click(52, 30)

@then("선택된 Normal Application 아이콘이 Active Application 아이콘으로 변경되는지 확인")
def step_impl():
  Regions.SA3.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.playgroundPanel.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents.TomoAnalysis_ProjectManager_Plugins_DragIconWindow)

@then("Application Parameter의 UI가 BA 버전으로 바뀌는지 확인")
def step_impl():
  Regions.panel_base11.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base)


@given("Hypercube에 2DIG app이 연결되어 있지 않다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir = r'E:\Regression_Test\Prefix_File\Select Application\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir1 = r'E:\Regression_Test\Prefix_File\Select Application'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\Select Application'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(42, 31)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.WorkerW.ReBarWindow32.AddressBandRoot.progress.BreadcrumbParent.toolbar.ClickItemXY("‎Prefix_File", 43, 18, False)
  dlgOpenProject.btn_O.ClickButton()
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\Select Application\\Select Application.tcpro")
 
@when("2DIG App과 적용 시킬 hypercube를 선택하고 ok버튼을 누른다.")
def step_impl():
  comboBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectAppPanel.dropdown_high
  comboBox.ClickItem("2D Image Gallery")
  tomoAnalysis_ProjectManager_Plugins_SelectAppPanel = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectAppPanel
  tomoAnalysis_ProjectManager_Plugins_SelectAppPanel.dropdown_high2.ClickItem("HC")
  tomoAnalysis_ProjectManager_Plugins_SelectAppPanel.bt_round_operation.ClickButton()

@then("Application Parameter 패널의 UI가 2DIG 버전으로 바뀌는지 확인")
def step_impl():
  Regions.panel_base12.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.playgroundPanel.panel_base)

@given("2DIG 아이콘이 Active 상태가 아니다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir = r'E:\Regression_Test\Prefix_File\Select Application\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir1 = r'E:\Regression_Test\Prefix_File\Select Application'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\Select Application'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(42, 31)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.WorkerW.ReBarWindow32.AddressBandRoot.progress.BreadcrumbParent.toolbar.ClickItemXY("‎Prefix_File", 43, 18, False)
  dlgOpenProject.btn_O.ClickButton()
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\Select Application\\Select Application.tcpro")
  splitter = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter
  comboBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectAppPanel.dropdown_high
  comboBox.ClickItem("2D Image Gallery")
  tomoAnalysis_ProjectManager_Plugins_SelectAppPanel = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectAppPanel
  tomoAnalysis_ProjectManager_Plugins_SelectAppPanel.dropdown_high2.ClickItem("HC")
  tomoAnalysis_ProjectManager_Plugins_SelectAppPanel.bt_round_operation.ClickButton()
  splitter.playgroundFrame.playgroundSplitter.playgroundPanel.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents.TomoAnalysis_ProjectManager_Plugins_DragIconWindow.Click(43, 174)

@when("hypercube에 연결된 2DIG 아이콘을 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.playgroundPanel.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents.TomoAnalysis_ProjectManager_Plugins_DragIconWindow.TomoAnalysis_ProjectManager_Plugins_DraggableIcon2.Click(31, 53)

@then("Application Parameter의 UI가 2DIG 버전으로 바뀌는지 확인")
def step_impl():
  Regions.SA4.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.playgroundPanel.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents.TomoAnalysis_ProjectManager_Plugins_DragIconWindow)
  tomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)


@given("Project Explorer Panel에 한 개 이상의 항목이 생성되어 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir = r'E:\Regression_Test\Prefix_File\PEP UI features\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir1 = r'E:\Regression_Test\Prefix_File\PEP UI features'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\PEP UI features'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(99, 35)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.WorkerW.ReBarWindow32.AddressBandRoot.progress.BreadcrumbParent.toolbar.ClickItemXY("‎Prefix_File", 17, 14, False)
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\PEP UI features\\PEP UI features.tcpro")

@given("항목 왼쪽 끝에 위치한 화살표가 오른쪽 방향\\(▷)이다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.explorerFrame.explorerSplitter.explorerPanel.panel_base.panel_contents.qt_scrollarea_viewport.Click(77, 212)

@when("펼치고 싶은 항목의 왼쪽 끝에 위치한 화살표를 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.explorerFrame.explorerSplitter.explorerPanel.panel_base.panel_contents.ExpandItem("PEP UI features|TCF|TomoAnalysis test data")

@then("화살표 방향이 오른쪽 방향\\(▷)에서 아래 방향\\(▽)으로 바뀌었는지 확인")
def step_impl():
  Regions.qt_scrollarea_viewport37.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.explorerFrame.explorerSplitter.explorerPanel.panel_base.panel_contents.qt_scrollarea_viewport)

@then("해당 항목 바로 아래 수준의 항목들이 펼쳐졌는지 확인")
def step_impl():
  Regions.qt_scrollarea_viewport38.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.explorerFrame.explorerSplitter.explorerPanel.panel_base.panel_contents.qt_scrollarea_viewport)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@given("항목 왼쪽 끝에 위치한 화살표가 아래 방향\\(▽)이다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.explorerFrame.explorerSplitter.explorerPanel.panel_base.panel_contents.qt_scrollarea_viewport.Click(56, 309)


@when("접고 싶은 항목의 왼쪽 끝에 위치한 화살표를 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.explorerFrame.explorerSplitter.explorerPanel.panel_base.panel_contents.CollapseItem("PEP UI features|TCF")

@then("화살표 방향이 아래 방향\\(▽)에서 오른쪽 방향\\(▷)으로 바뀌었는지 확인")
def step_impl():
  Regions.qt_scrollarea_viewport39.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.explorerFrame.explorerSplitter.explorerPanel.panel_base.panel_contents.qt_scrollarea_viewport)

@then("해당 항목 이하 수준의 모든 항목들이 숨겨졌는지 확인")
def step_impl():
  Regions.qt_scrollarea_viewport40.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.explorerFrame.explorerSplitter.explorerPanel.panel_base.panel_contents.qt_scrollarea_viewport)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@when("Project Explorer Panel 최하단부에 위치한 아래 방향 겹쳐진 화살표 버튼\\(︾)을 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.explorerFrame.explorerSplitter.explorerPanel.panel_base.bt_expand_all.ClickButton()
  Regions.qt_scrollarea_viewport41.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.explorerFrame.explorerSplitter.explorerPanel.panel_base.panel_contents.qt_scrollarea_viewport)

@then("Project Explorer Panel의 모든 항목이 펼쳐졌는지 확인")
def step_impl():
  Regions.qt_scrollarea_viewport42.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.explorerFrame.explorerSplitter.explorerPanel.panel_base.panel_contents.qt_scrollarea_viewport)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@when("Project Explorer Panel 최하단부에 위치한 위 방향 겹쳐진 화살표 버튼\\(︽)을 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.explorerFrame.explorerSplitter.explorerPanel.panel_base.bt_collapse_all.ClickButton()

@then("Project Explorer Panel에서 cube 단위까지의 항목들이 펼쳐졌는지 확인")
def step_impl():
  Regions.qt_scrollarea_viewport43.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.explorerFrame.explorerSplitter.explorerPanel.panel_base.panel_contents.qt_scrollarea_viewport)

@then("Project Explorer Panel에서 cube 단위 이하의 개별 TCF 파일들이 숨겨졌는지 확인")
def step_impl():
  Regions.qt_scrollarea_viewport74.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.explorerFrame.explorerSplitter.explorerPanel.panel_base.panel_contents.qt_scrollarea_viewport)


@when("Project Explorer Panel에서 프로젝트 이름을 우클릭한다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.explorerFrame.explorerSplitter.explorerPanel.panel_base.panel_contents.qt_scrollarea_viewport.ClickR(57, 11)

@when("Rename Project 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.Menu.QtMenu.Click("Rename Project")

@when("팝업에서 변경할 이름을 입력하고 OK 버튼을 누른다.")
def step_impl():
  tomoAnalysis_ProjectManager_Plugins_RenameProjectDialog = Aliases.TomoAnalysis.RenameProjectDialog
  lineEdit = tomoAnalysis_ProjectManager_Plugins_RenameProjectDialog.newName
  lineEdit.Click(69, 16)
  lineEdit.SetText("Import Data2")
  tomoAnalysis_ProjectManager_Plugins_RenameProjectDialog.okBtn.ClickButton()

@then("Project Explorer Panel에서 프로젝트 이름의 변경을 확인")
def step_impl():
  Regions.explorerFrame.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.explorerFrame)

@when("팝업에서 변경할 이름을 입력하고 키보드의 엔터 키를 누른다.")
def step_impl():
  tomoAnalysis_ProjectManager_Plugins_RenameProjectDialog = Aliases.TomoAnalysis.RenameProjectDialog
  lineEdit = tomoAnalysis_ProjectManager_Plugins_RenameProjectDialog.newName
  lineEdit.Click(104, 9)
  lineEdit.SetText("Import Data2")
  tomoAnalysis_ProjectManager_Plugins_RenameProjectDialog.Click(256, 123)
  lineEdit.Keys("[Enter]")

@given("프로젝트에 TCF가 불러져 있다.")
def step_impl():

  dir = r'E:\Regression_Test\Prefix_File\PEP UI features\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir1 = r'E:\Regression_Test\Prefix_File\PEP UI features'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\PEP UI features'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(99, 35)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.WorkerW.ReBarWindow32.AddressBandRoot.progress.BreadcrumbParent.toolbar.ClickItemXY("‎Prefix_File", 17, 14, False)
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\PEP UI features\\PEP UI features.tcpro")

@when("Project Explorer Panel의 삭제하고 싶은 TCF 폴더에 마우스 포인터를 대고 우클릭한후 Remove TCF 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.Menu.QtMenu.Click("Remove TCF")



@when("팝업에서 Yes 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.MessageBox4.qt_msgbox_buttonbox.buttonYes.ClickButton()
  
@then("Project Explorer Panel에서 삭제한 TCF 폴더가 사라졌는지 확인")
def step_impl():
  Regions.qt_scrollarea_viewport45.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.explorerFrame.explorerSplitter.explorerPanel.panel_base.panel_contents.qt_scrollarea_viewport)

@then("Preview\\/List 패널에서 해당 폴더의 TCF가 삭제됐는지 확인")
def step_impl():
  Regions.widget_panel2.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel)


@when("Project Explorer Panel의 삭제하고 싶은 TCF 폴더에 마우스 포인터를 대고 우클릭후 Remove TCF 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  widget = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.explorerFrame.explorerSplitter.explorerPanel.panel_base.panel_contents.qt_scrollarea_viewport
  widget.ClickR(67, 91)
  widget.ClickR(63, 89)
  widget.ClickR(74, 106)
  tomoAnalysis.Menu.QtMenu.Click("Remove TCF")

@when("Remove TCF Panel의 키보드의 엔터 키를 누른다.")
def step_impl():
  messageBox = Aliases.TomoAnalysis.MessageBox4
  messageBox.Click(342, 76)
  messageBox.qt_msgbox_buttonbox.buttonYes.Keys("[Enter]")

@given("Operation sequence 패널에서 현재 단계의 해야 하는 작업을 모두 완료한 상태로 해당 탭이 열려 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir = r'E:\Regression_Test\Prefix_File\PEP UI features\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir1 = r'E:\Regression_Test\Prefix_File\PEP UI features'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\PEP UI features'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(99, 35)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.WorkerW.ReBarWindow32.AddressBandRoot.progress.BreadcrumbParent.toolbar.ClickItemXY("‎Prefix_File", 17, 14, False)
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\PEP UI features\\PEP UI features.tcpro")
  frame = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base
  frame.bt_arrow_left.ClickButton()

@when("Operation sequence 패널의 > 버튼을 누른다.")
def step_impl():
  frame = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base
  frame.bt_arrow_right.ClickButton()

@then("다음 단계로 탭이 넘어가는지 확인")
def step_impl():
  Regions.panel_base13.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)
  
@given("Operation sequence 패널에서 현재 단계의 해야 하는 작업을 모두 완료하지 않은 상태로 해당 탭이 열려 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir = r'E:\Regression_Test\Prefix_File\PEP UI features\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir1 = r'E:\Regression_Test\Prefix_File\PEP UI features'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\PEP UI features'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(99, 35)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.WorkerW.ReBarWindow32.AddressBandRoot.progress.BreadcrumbParent.toolbar.ClickItemXY("‎Prefix_File", 17, 14, False)
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\PEP UI features\\PEP UI features.tcpro")

@then("현재 화면에서 변화가 없음을 확인")
def step_impl():
  Regions.UI1.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget)

@given("List 패널이 열려 있다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.ClickTab("List")

@given("Exclusive 체크박스가 비어있다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.detailsTab.DetailPanel.widget.exclusiveCheckBox.Drag(8, 14, 7, 49)

@when("Exclusive 체크박스를 체크한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.detailsTab.DetailPanel.widget.exclusiveCheckBox.ClickButton(cbChecked)

@then("List 패널의 오른쪽 table에 존재하는 데이터가 왼쪽 table에서 제거됨을 확인")
def step_impl():
  Regions.qt_scrollarea_viewport46.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.detailsTab.DetailPanel.widget.contentsTableWidget.qt_scrollarea_viewport)


@when("Set Playground패널의 OK 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.AddPGPanel.widget_panel.bt_round_operation.ClickButton()

@when("원하는 TCF 파일들 중 가장 윗 줄의 가장 왼쪽 파일을 선택한다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(102, 77)

@when("원하는 TCF 파일들 중 가장 아랫 줄의 가장 오른쪽 파일을 키보드의 Shift 키와 함께 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  tomoAnalysis_ProjectManager_Plugins_ThumbnailPanel = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel
  for i in range(1,60) :
   tomoAnalysis_ProjectManager_Plugins_ThumbnailPanel.MouseWheel(-1)

  scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget6.TC_TCF2DWidget.panel_contents_image.Widget.Click(148, 113, skShift)





@then("선택한 두 개의 파일 사이에 위치한 모든 TCF 파일들이 선택되었는지 확인")
def step_impl():
  Regions.ThumbnailPanel15.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel)

@when("팝업에서 Hypercube의 이름을 지정하고 키보드의 엔터 키를 누른다.")
def step_impl():
  tomoAnalysis_ProjectManager_Plugins_RenameCube = Aliases.TomoAnalysis.RenameCube
  lineEdit = tomoAnalysis_ProjectManager_Plugins_RenameCube.newName
  lineEdit.Click(73, 22)
  lineEdit.SetText("HC1")
  tomoAnalysis_ProjectManager_Plugins_RenameCube.Drag(266, 139, 0, 18)
  lineEdit.Keys("[Enter]")

@then("선택된 BA 아이콘이 Active Application 아이콘으로 변경되는지 확인")
def step_impl():
  Regions.SA10.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.playgroundPanel.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents.TomoAnalysis_ProjectManager_Plugins_DragIconWindow)

@then("선택된 2DIG 아이콘이 Active Application 아이콘으로 변경되는지 확인")
def step_impl():
  Regions.SA11.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.playgroundPanel.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents.TomoAnalysis_ProjectManager_Plugins_DragIconWindow)

@when("원하는 TCF 파일의 썸네일을 두 개 순서대로 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  NameMapping.Sys.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(92, 33)
  scrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget3.TC_TCF2DWidget.panel_contents_image.Widget.Click(56, 47)

  
@then("두 개의 TCF 썸네일이 동시에 선택되는지 확인")
def step_impl():
  Regions.Widget48.Check(NameMapping.Sys.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget)
  Regions.Widget49.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget3.TC_TCF2DWidget.panel_contents_image.Widget)

@when("선택된 두 TCF의 썸네일을 다시 한 번 순서대로 누른다.")
def step_impl():
  raise NotImplementedError

@then("두 개의 TCF 썸네일이 선택 해제됐는지 확인")
def step_impl():
  Regions.Widget50.Check(NameMapping.Sys.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget)
  Regions.Widget51.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget3.TC_TCF2DWidget.panel_contents_image.Widget)

@then("List 패널의 왼쪽 table에 불러진 TCF의  Cube\\/Hypercube column은 비어있는지 확인")
def step_impl():
  Tables.contentsTableWidget1.Check()

@when("List 패널에서 원하는 TCF 파일을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.detailsTab.DetailPanel.widget.contentsTableWidget.ClickCell("1", "BF")

@then("List 패널에서 1개의 TCF 파일이 선택됐는지 확인")
def step_impl():
  Regions.qt_scrollarea_viewport69.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.detailsTab.DetailPanel.widget.contentsTableWidget.qt_scrollarea_viewport)

@when("컨트롤 키를 누른 상태로 원하는 TCF 파일을 두 개 선택한다.")
def step_impl():
  widget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.detailsTab.DetailPanel.widget.contentsTableWidget.qt_scrollarea_viewport
  widget.Click(341, 50, skCtrl)
  widget.Click(347, 67, skCtrl)

@then("세 개의 TCF 파일이 선택되었는지 확인")
def step_impl():
  Regions.qt_scrollarea_viewport70.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.detailsTab.DetailPanel.widget.contentsTableWidget.qt_scrollarea_viewport)

@when("List 패널 HT column의 table header를 한 번 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.detailsTab.DetailPanel.widget.contentsTableWidget.ClickColumnHeader("HT")

@then("HT 값에 대해 오름차순으로 List가 정렬되었는지 확인")
def step_impl():
  raise NotImplementedError

@when("List 패널 HT column의 table header를 한 번 더 누른다.")
def step_impl():
  raise NotImplementedError

@then("HT 값에 대해 내림차순으로 List가 정렬되었는지 확인")
def step_impl():
  raise NotImplementedError

@given("Playground가 두 개 생성되어 있다.")
def step_impl():
  Aliases.slack.wndChrome_WidgetWin_1.Activate()

@when("Playground Setup 패널에서 선택되어 있지 않은 Playground를 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.explorerFrame.explorerSplitter.pglistPanel.panel_base.panel_contents.ClickItem("Playground1")

@when("playground setup 패널에서 이름을 변경하고 싶은 playground를 선택하고 Rename 버튼을 누른다.")
def step_impl():
  frame = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.explorerFrame.explorerSplitter.pglistPanel.panel_base
  frame.panel_contents.ClickItem("Playground2")
  frame.bt_square_gray.Click(126, 25)

@when("Playground의 이름을 변경하는 팝업에서 변경할 이름을 입력하고 OK 버튼을 누른다.")
def step_impl():
  tomoAnalysis_ProjectManager_Plugins_RenameDialog = Aliases.TomoAnalysis.RenameDialog
  lineEdit = tomoAnalysis_ProjectManager_Plugins_RenameDialog.input_high
  lineEdit.Click(26, 22)
  lineEdit.SetText("test")
  tomoAnalysis_ProjectManager_Plugins_RenameDialog.bt_square_primary.ClickButton()

@when("playground setup 패널에서 삭제할 playground를 선택하고 Delete 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.explorerFrame.explorerSplitter.pglistPanel.panel_base.bt_square_line.Click(75, 14)

@when("Playground의 이름을 삭제하는 팝업의 Delete 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.DeleteDialog.bt_square_primary.ClickButton()

@when("Hypercube 아이콘 바깥의 Playground canvas 아무 영역이나 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.playgroundPanel.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents.TomoAnalysis_ProjectManager_Plugins_DragIconWindow3.Click(39, 132)

@when("Playground canvas의 Hypercube 아이콘을 우클릭하고 Rename Hypercube 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.playgroundPanel.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents.TomoAnalysis_ProjectManager_Plugins_DragIconWindow2.TomoAnalysis_ProjectManager_Plugins_DraggableIcon.ClickR(19, 43)
  tomoAnalysis.Menu.QtMenu.Click("Rename Hypercube")

@when("Playground canvas의 Hypercube 아이콘을 우클릭 하고 Copy Hypercube 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.playgroundPanel.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents.TomoAnalysis_ProjectManager_Plugins_DragIconWindow.TomoAnalysis_ProjectManager_Plugins_DraggableIcon3.ClickR(33, 57)
  tomoAnalysis.Menu.QtMenu.Click("Copy Hypercube")

@when("Playground canvas의 Copy된 Hypercube 아이콘을 우클릭 하고 Delete Hypercube 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  splitter = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter
  splitter.explorerFrame.explorerSplitter.explorerPanel.panel_base.panel_contents.qt_scrollarea_viewport.Click(224, 436)
  scrollArea = splitter.playgroundFrame.playgroundSplitter.playgroundPanel.panel_base.scrollarea_bottom_round
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_hcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.panel_contents.TomoAnalysis_ProjectManager_Plugins_DragIconWindow4.TomoAnalysis_ProjectManager_Plugins_DraggableIcon.ClickR(33, 39)
  tomoAnalysis.Menu.QtMenu.Click("Delete Hypercube")

@when("팝업의 Delete 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.DeleteCube.deleteBtn.ClickButton()

@then("Preview 패널의 Title이 선택한 cube 이름으로 바뀜을 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.h4, "QtText", cmpEqual, "CubeName")

@then("표시되는 TCF도 cube 내의 데이터로 바뀜을 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.TomoAnalysis_ProjectManager_Plugins_LinkInfoWidget.bt_annotation_link, "QtText", cmpEqual, "CubeName")
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget3.TC_TCF2DWidget.panel_contents_image.TomoAnalysis_ProjectManager_Plugins_LinkInfoWidget.bt_annotation_link, "QtText", cmpEqual, "CubeName")

@then("Preview 패널의 Title이 Playground 이름으로 바뀜을 확인한다.")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.h4, "QtText", cmpEqual, "test")

@when("Playground canvas의 Cube 아이콘을 우클릭 하고 Rename Cube 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.playgroundPanel.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents.TomoAnalysis_ProjectManager_Plugins_DragIconWindow2.TomoAnalysis_ProjectManager_Plugins_DraggableIcon.ClickR(29, 48)
  tomoAnalysis.Menu.QtMenu.Click("Rename Cube")

@when("Playground canvas의 Cube 아이콘을 우클릭 하고 Copy Cube 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.playgroundPanel.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents.TomoAnalysis_ProjectManager_Plugins_DragIconWindow.TomoAnalysis_ProjectManager_Plugins_DraggableIcon3.ClickR(34, 51)
  tomoAnalysis.Menu.QtMenu.Click("Copy Cube")

@when("Playground canvas의 cube 아이콘을 우클릭 하고 Delete Cube 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  scrollArea = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.playgroundPanel.panel_base.scrollarea_bottom_round
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_hcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.panel_contents.TomoAnalysis_ProjectManager_Plugins_DragIconWindow4.TomoAnalysis_ProjectManager_Plugins_DraggableIcon3.ClickR(14, 35)
  tomoAnalysis.Menu.QtMenu.Click("Delete Cube")

@given("Operation sequence 패널의 Link Data to Cube 탭 화면이 열려 있다.")
def step_impl():

  dir = r'E:\Regression_Test\Prefix_File\Link Data to Cube\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir1 = r'E:\Regression_Test\Prefix_File\Link Data to Cube'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\Link Data to Cube'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(110, 28)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.WorkerW.ReBarWindow32.AddressBandRoot.progress.BreadcrumbParent.toolbar.ClickItemXY("‎Prefix_File", 16, 7, False)
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\Link Data to Cube\\Link Data to Cube.tcpro")

@given("cube에 TCF가 2개 연결되어 있다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  frame = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base
  scrollArea = frame.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  NameMapping.Sys.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(171, 87)
  scrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget3.TC_TCF2DWidget.panel_contents_image.Widget.Click(61, 92)
  frame.bt_round_tool2.Click(53, 18)
  tomoAnalysis.LinkDialog.bt_square_primary.ClickButton()

@when("cube에 연결되어 있는 TCF를 한 개만 선택하고 Toolbox의 Unlink from Cube 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  frame = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base
  scrollArea = frame.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget3.TC_TCF2DWidget.panel_contents_image.Widget.Click(109, 140)
  frame.bt_round_tool3.Click(80, 13)
  tomoAnalysis.UnlinkDialog.bt_square_primary.ClickButton()

@then("Project Explorer 패널에서 TCF가 연결 해제되었는지 확인")
def step_impl():
  Regions.Widget52.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget3.TC_TCF2DWidget.panel_contents_image.Widget)

@then("Operation sequence 패널의 Select Application 탭 화면이 Link Data to Cube 탭으로 다시 돌아감을 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.frame_tab.bt_round_tab5, "QtText", cmpEqual, "Link Data to Cube")

@when("List 패널의 왼쪽 table에서 cube에 연결하고 싶은 TCF를 선택하고 List 패널의 > 버튼을 누른다.")
def step_impl():
  widget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.detailsTab.DetailPanel.widget
  widget.contentsTableWidget.ClickCell("1", "HT")
  widget.bt_arrow_right.ClickButton()

@then("List 패널의 오른쪽 table에 cube에 연결한 TCF row의 Cube, Hypercube column에 연결된 cube 이름과 hypercube 이름이 채워짐을 확인")
def step_impl():
  raise NotImplementedError

@when("List 패널의 Exclusive 체크박스를 체크한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.detailsTab.DetailPanel.widget.exclusiveCheckBox.ClickButton(cbChecked)

@when("List 패널의 오른쪽 table에서 cube에서 연결 해제하고 싶은 TCF를 선택하고 List 패널의 {arg} 버튼을 누른다.")
def step_impl(param1):
  widget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.detailsTab.DetailPanel.widget
  widget.includedTableWidget.ClickCell("1", "HT")
  widget.bt_arrow_left.Drag(15, 31, 2, 10)

@given("TCF와 Cube 불러진 List 패널이 열려 있다.")
def step_impl():

  dir = r'E:\Regression_Test\Prefix_File\Link Data to Cube\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir1 = r'E:\Regression_Test\Prefix_File\Link Data to Cube'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\Link Data to Cube'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(110, 28)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.WorkerW.ReBarWindow32.AddressBandRoot.progress.BreadcrumbParent.toolbar.ClickItemXY("‎Prefix_File", 16, 7, False)
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\Link Data to Cube\\Link Data to Cube.tcpro")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.ClickTab("List")

@given("Hypercube에 연결된 BA 아이콘이 Active 상태가 아니다.")
def step_impl():

  dir = r'E:\Regression_Test\Prefix_File\PEP UI features\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir1 = r'E:\Regression_Test\Prefix_File\PEP UI features'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\PEP UI features'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(99, 35)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.WorkerW.ReBarWindow32.AddressBandRoot.progress.BreadcrumbParent.toolbar.ClickItemXY("‎Prefix_File", 17, 14, False)
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\PEP UI features\\PEP UI features.tcpro")
  tomoAnalysis = Aliases.TomoAnalysis
  splitter = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter
  splitter2 = splitter.operationFrame.operationSplitter
  frame = splitter2.previewPanel.panel_base
  frame.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  NameMapping.Sys.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(67, 91)
  frame.bt_round_tool2.Click(65, 15)
  tomoAnalysis.LinkDialog.bt_square_primary.ClickButton()
  splitter2.operationPanel.panel_base.widget_panel.SelectAppPanel.bt_round_operation.ClickButton()
  splitter.playgroundFrame.playgroundSplitter.playgroundPanel.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents.TomoAnalysis_ProjectManager_Plugins_DragIconWindow4.Click(63, 210)

@when("Playground canvas에서 Hypercube에 연결된 BA 아이콘을 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.playgroundPanel.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents.TomoAnalysis_ProjectManager_Plugins_DragIconWindow4.TomoAnalysis_ProjectManager_Plugins_DraggableIcon3.Click(45, 50)

@when("Select Application 탭에서 BAT App과 적용 시킬 hypercube를 선택하고 Select Application 패널의 OK 버튼을 누른다.")
def step_impl():
  tomoAnalysis_ProjectManager_Plugins_SelectAppPanel = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectAppPanel
  tomoAnalysis_ProjectManager_Plugins_SelectAppPanel.dropdown_high.ClickItem("Basic Analyzer[T]")
  tomoAnalysis_ProjectManager_Plugins_SelectAppPanel.bt_round_operation.ClickButton()

@then("Application Parameter 패널의 UI가 BAT 버전으로 바뀌는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.playgroundPanel.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents.TomoAnalysis_ProjectManager_Plugins_DragIconWindow5, "QtText", cmpEqual, "")

@when("Select Application 탭에서 2DIG App과 적용 시킬 hypercube를 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectAppPanel.dropdown_high.ClickItem("2D Image Gallery")

@when("Select Application 패널의 OK 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectAppPanel.bt_round_operation.ClickButton()

@given("Hypercube에 BA 아이콘이 연결되어 있다.")
def step_impl():

  tomoAnalysis = Aliases.TomoAnalysis
  dir = r'E:\Regression_Test\Prefix_File\BA Application Parameter\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir3 = r'E:\Regression_Test\Prefix_File\BA Application Parameter\playground\test\Hypercube\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)
  dir4 = r'E:\Regression_Test\Prefix_File\BA Application Parameter\playground\test\Hypercube\*.rep'
  for f in glob.glob(dir4):
      os.remove(f)

  dir1 = r'E:\Regression_Test\Prefix_File\BA Application Parameter'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BA Application Parameter'
  copy_tree(dir2,dir1) 
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(137, 27)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BA Application Parameter\\BA Application Parameter.tcpro")

@given("Project Explorer Panel의 cube 이하의 항목이 모두 숨겨져 있다.")
def step_impl():
  treeWidget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.explorerFrame.explorerSplitter.explorerPanel.panel_base.panel_contents
  treeWidget.ExpandItem("BA Application Parameter|test|HC|CubeName")
  treeWidget.CollapseItem("BA Application Parameter|test|HC|CubeName")

@when("Project Explorer Panel에서 Cube 왼쪽 끝에 위치한 화살표를 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.explorerFrame.explorerSplitter.explorerPanel.panel_base.panel_contents.ExpandItem("BA Application Parameter|test|HC|CubeName")

@then("Project Explorer Panel에서 화살표를 클릭한 Cube 외의 모든 항목이 펼쳐졌는지 확인")
def step_impl():
  Regions.qt_scrollarea_viewport73.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.explorerFrame.explorerSplitter.explorerPanel.panel_base.panel_contents.qt_scrollarea_viewport)

@given("Operation sequence 패널에 Create Hypercube 탭이 열려 있다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.frame_tab.bt_round_tab2.ClickButton()

@then("Operation sequence 패널에 Create Cube 탭이 열렸는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.frame_tab.bt_round_tab3, "checked", cmpEqual, True)

@when("Operation sequence 패널의 < 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.bt_arrow_left.ClickButton()

@then("Operation sequence 패널에 Create Hypercube 탭이 열렸는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.frame_tab.bt_round_tab2, "checked", cmpEqual, True)

@when("Operation sequence 패널 상단의 Import Data 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.frame_tab.bt_round_tab6.ClickButton()

@then("Operation sequence 패널에 Import Data 탭이 열렸는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.frame_tab.bt_round_tab6, "checked", cmpEqual, True)

@when("Operation sequence 패널의 {arg} 버튼을 누른다.")
def step_impl(param1):
  raise NotImplementedError

@given("Hypercube가 생성되어 있지 않다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir = r'E:\Regression_Test\Prefix_File\Create Hypercube - Hypercube\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir1 = r'E:\Regression_Test\Prefix_File\Create Hypercube - Hypercube'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\Create Hypercube - Hypercube'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(89, 8)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\Create Hypercube - Hypercube\\Create Hypercube - Hypercube.tcpro")


@when("Operation sequence 패널 상단의 Create Cube 버튼을 누른다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir = r'E:\Regression_Test\Prefix_File\PEP UI features\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir1 = r'E:\Regression_Test\Prefix_File\PEP UI features'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\PEP UI features'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(99, 35)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.WorkerW.ReBarWindow32.AddressBandRoot.progress.BreadcrumbParent.toolbar.ClickItemXY("‎Prefix_File", 17, 14, False)
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\PEP UI features\\PEP UI features.tcpro")

@given("프로젝트에 TCF가 있다.")
def step_impl():
  pass


  dir = r'E:\Regression_Test\Prefix_File\PEP UI features\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir1 = r'E:\Regression_Test\Prefix_File\PEP UI features'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\PEP UI features'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(99, 35)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.WorkerW.ReBarWindow32.AddressBandRoot.progress.BreadcrumbParent.toolbar.ClickItemXY("‎Prefix_File", 17, 14, False)
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\PEP UI features\\PEP UI features.tcpro")
  


  
  
@when("Preview 패널에서 TCF를 우클릭 한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  NameMapping.Sys.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.ClickR(127, 102)

@when("Create Simple Playground 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  NameMapping.Sys.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.ClickR(90, 115)
  tomoAnalysis.Menu.QtMenu.Click("Create Simple Playground")

@then("Playground canvas에 ‘여섯 자리 숫자_여섯 자리 숫자’ 의 동일한 이름으로 Hypercube와 cube 아이콘이 생성됨을 확인")
def step_impl():
  myObject1 = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.h4
  text1 = myObject1.QtText 
  
  myObject2 = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectAppPanel.dropdown_high2
  text2 = myObject2.wText
  
  myObject3 = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.TomoAnalysis_ProjectManager_Plugins_LinkInfoWidget.bt_annotation_link
  text3 = myObject3.QtText
  
  if (aqObject.CompareProperty(text1,0,text2)) :
   Log.Message("Playground와 Hypercube가 같음")
  else :
   Log.Error("Playground와 Hypercube가 다름")
   
  if (aqObject.CompareProperty(text1,0,text3)) :
   Log.Message("Playground와 cube가 같음")
  else :
   Log.Error("Playground와 cube가 다름")
   
   
@then("Playground setup 패널에 ‘여섯 자리 숫자_여섯 자리 숫자’ 의 이름으로 playground가 새로 생성되었는지 확인")
def step_impl():
  import re
  myObject1 = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.h4
  text1 = myObject1.QtText
  p = re.compile('\d\d\d\d\d\d_\d\d\d\d\d\d')
  m = p.match(text1)
  if (aqObject.CompareProperty(text1,0,m.group())) :
   Log.Message("Playground와 Hypercube가 같음")
  else :
   Log.Error("Playground와 Hypercube가 다름")

@then("Operation Sequence가 Select Application으로 넘어감을 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.frame_tab.bt_round_tab7, "checked", cmpEqual, True)

@then("Preview 패널에 표시되는 TCF가 Playground 내의 전체 TCF로 바뀜을 확인")
def step_impl():
  Regions.ThumbnailPanel16.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel)

@then("선택되었던 모든 TCF가 선택 해제되었는지 확인")
def step_impl():
  Regions.ThumbnailPanel17.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel)

@given("TA의 Select Project 탭 화면이 나타나있다.")
def step_impl():
  pass

@given("Project Manager 탭에 Project 파일이 열려 있다.")
def step_impl():
  dir = r'E:\Regression_Test\Prefix_File\Import_Data_No_Data\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir1 = r'E:\Regression_Test\Prefix_File\Import_Data_No_Data'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\Import_Data_No_Data'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(51, 38)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\Import_Data_No_Data\\Import_Data_No_Data.tcpro")

@when("Import Data 탭에서 import 버튼을 누르고 팝업창에서 HEK293FT-002P001 파일을 더블클릭한다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.AddTcfPanel2.bt_round_operation.Click(44, 3)
  dlgOpenTcfFolder = tomoAnalysis.dlgOpenTcfFolder
  HWNDView = dlgOpenTcfFolder.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  UIItemsView = HWNDView.CtrlNotifySink2.ShellView.Item
  UIItem = UIItemsView.Regression_Test
  UIItem.Item.Click(94, 16)
  UIItem.Keys("[Enter]")
  UIItem = UIItemsView.TomoAnalysis_test_data
  UIItem.Item.Click(137, 5)
  UIItem.Keys("[Enter]")
  UIItem = UIItemsView.HT_BF_test_sample_for_SW_team
  UIItem.Item.Click(85, 9)
  UIItem.Keys("[Enter]")
  UIItem = UIItemsView.HEK293FT_002P001
  UIItem.Item.Click(231, 16)
  UIItem.Keys("[Enter]")
  dlgOpenTcfFolder.btn_2.ClickButton()

@then("Preview 패널에 HEK293FT-002P001 파일의 썸네일이 보이고 썸네일 상단에 “AI” 글자와 초록색 원이 표기 돼 있는지 확인")
def step_impl():
  Regions.FT_1.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.TomoAnalysis_ProjectManager_Plugins_FileInfoWidget)

@when("Import Data 탭에서 import 버튼을 누르고 팝업창에서 HT1080FFF-002Stitching 파일을 더블클릭한다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.AddTcfPanel2.bt_round_operation.Click(89, 26)
  dlgOpenTcfFolder = tomoAnalysis.dlgOpenTcfFolder
  HWNDView = dlgOpenTcfFolder.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  UIItemsView = HWNDView.CtrlNotifySink2.ShellView.Item
  UIItem = UIItemsView.Regression_Test
  UIItem.Item.Click(97, 7)
  UIItem.Keys("[Enter]")
  UIItem = UIItemsView.F_Test
  UIItem.Item.Click(79, 16)
  UIItem.Keys("[Enter]")
  UIItem = UIItemsView.F2_no_ai
  UIItem.Item.Click(83, 8)
  UIItem.Keys("[Enter]")
  UIItemsView.HT1080FFF_002Stitching.Item.Click(83, 8)
  dlgOpenTcfFolder.btn_2.ClickButton()

@then("Preview 패널에 HT1080FFF-002Stitching 파일의 썸네일이 보이고 해당 썸네일 상단에 빨간색 원이 표기 돼 있는지 확인")
def step_impl():
  Regions.FT_2.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox2.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.TomoAnalysis_ProjectManager_Plugins_FileInfoWidget)

@when("Import Data 탭에서 import 버튼을 누르고 팝업창에서 5x5-005Stitching 파일을 더블클릭한다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.AddTcfPanel2.bt_round_operation.Click(145, 35)
  dlgOpenTcfFolder = tomoAnalysis.dlgOpenTcfFolder
  HWNDView = dlgOpenTcfFolder.DUIViewWndClassName.Item
  tv_ = HWNDView.CtrlNotifySink.NamespaceTreeControl.tv_
  tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  UIItemsView = HWNDView.CtrlNotifySink2.ShellView.Item
  UIItem = UIItemsView.Regression_Test
  UIItem.Item.Click(110, 10)
  tv_.SelectItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  tv_.Keys("[Enter]")
  UIItem.Keys("[Enter]")
  UIItem = UIItemsView.TomoAnalysis_test_data
  UIItem.Item.Click(129, 8)
  UIItem.Keys("[Enter]")
  dlgOpenTcfFolder.WorkerW.ReBarWindow32.AddressBandRoot.progress.BreadcrumbParent.toolbar.ClickItemXY("‎Regression_Test", 78, 7, False)
  UIItem = UIItemsView.F_Test
  UIItem.Item.Click(110, 10)
  UIItem.Keys("[Enter]")
  UIItem = UIItemsView.F2_no_ai
  UIItem.Item.Click(99, 19)
  UIItem.Keys("[Enter]")
  UIItemsView.x5_005Stitching.Item.Click(99, 11)
  dlgOpenTcfFolder.btn_2.ClickButton()

@then("Preview 패널에 5x5-005Stitching 파일의 썸네일이 보이고 썸네일 상단에 아무런 글자 없이 초록색 원이 표기 돼 있는지 확인")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 258
  Regions.FT_3.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox3.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.TomoAnalysis_ProjectManager_Plugins_FileInfoWidget)
