﻿import shutil
import os.path
from distutils.dir_util import copy_tree
import time
import glob

@given("BAT Application Parameter에서 프로세서로 AI Segmentation이 선택되어 있다.")
def step_impl():

  dir = r'E:\Regression_Test\Prefix_File\BAT\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir3 = r'E:\Regression_Test\Prefix_File\BAT\playground\test\Hypercube\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)
  dir4 =r'E:\Regression_Test\Prefix_File\BAT\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\BAT'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BAT'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(155, 29)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BAT\\BAT.tcpro")

@when("BAT Application Parameter의 Single RUN 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_gray.Click(122, 12)

@when("팝업에서 App이 연결된 hypercube 안의 Time-lapse TCF 중 Single RUN 하고 싶은 TCF를 더블 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.widget_popup.scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget.panel_contents_image.widget.panel_contents_image.Widget.DblClick(76, 93)

@then("Basic Analyzer[T] 탭이 열리는지 확인")
def step_impl():
  Regions.F14_1.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.TC_TabBar)

@then("Application tab 바에서 Project Manager 탭 이름의 하이라이트가 사라지고 Basic Analyzer[T] 탭 이름에 하이라이트가 생겼는지 확인")
def step_impl():
  Regions.F14_2.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.TC_TabBar)

@then("Basic Analyzer[T] 탭의 Single Run 탭 이름에 하이라이트가 생겼는지 확인")
def step_impl():
  Regions.F14_3.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.TC_TabBar)

@then("Basic Analyzer[T] 탭에서 Apply time points 버튼이 하이라이트 되었는지 확인")
def step_impl():
  Regions.timeGroup.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup)



@when("팝업에서 App이 연결된 hypercube 안의 TCF 중 Single RUN 하고 싶은 Time-lapse TCF를 선택하고 팝업 안의 Single RUN 버튼을 누른다.")
def step_impl():
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel = Aliases.TomoAnalysis.widget_popup
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget.panel_contents_image.widget.panel_contents_image.Widget.Click(114, 75)
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.bt_square_primary.Click(56, 21)


@given("Basic Analyzer[T] 탭의 Single run 탭이 열려 있다.")
def step_impl():

  dir = r'E:\Regression_Test\Prefix_File\BAT\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir3 = r'E:\Regression_Test\Prefix_File\BAT\playground\test\Hypercube\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)
  dir4 =r'E:\Regression_Test\Prefix_File\BAT\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\BAT'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BAT'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(155, 29)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BAT\\BAT.tcpro")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_gray.Click(122, 12)
  Aliases.TomoAnalysis.widget_popup.scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget.panel_contents_image.widget.panel_contents_image.Widget.DblClick(76, 93)

@when("Basic Analyzer[T] 탭의 Movie player의 타임 바에 마우스 포인터를 대고 클릭한 상태로 움직인다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.playerPanel.progressSlider.wPosition = 3

@then("Basic Analyzer[T] 탭의 Mask Viewing panel에 나타나는 이미지가 바뀌었는지 확인")
def step_impl():
  Regions.F14_4.Check(Aliases.TomoAnalysis.mainwindow.TomoAnalysis6)

@then("Basic Analyzer[T] 탭의 Movie player 타임 바 오른쪽의 숫자가 바뀌었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.playerPanel.progressSpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "3")


@when("Basic Analyzer[T] 탭의 Movie player 타임 바 오른쪽의 숫자 입력 칸에 {arg}을 입력한다.")
def step_impl(param1):
  spinBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.playerPanel.progressSpinBox
  lineEdit = spinBox.qt_spinbox_lineedit
  lineEdit.Click(25, 20)
  lineEdit.Drag(23, 16, -21, -2)
  spinBox.Keys("4")
  spinBox.wValue = 4

@then("Basic Analyzer[T] 탭의 Mask Viewing panel에 나타나는 이미지가 time point 4의 이미지로 바뀌었는지 확인")
def step_impl():
  Regions.TomoAnalysis35.Check(Aliases.TomoAnalysis.mainwindow.TomoAnalysis6)

@then("Basic Analyzer[T] 탭의 Movie player 타임 바 커서가 오른쪽으로 조금 움직였는지 확인")
def step_impl():
  Regions.F14_5.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.playerPanel.progressSlider)


@when("Basic Analyzer[T] 탭의 Movie player 타임 바 오른쪽의 숫자 칸 옆의 위 화살표 버튼을 한 번 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.playerPanel.progressSpinBox.Click(86, 10)

@then("Basic Analyzer[T] 탭의 Mask Viewing panel 화면에 나타나는 이미지가 time point 2의 이미지로 바뀌었는지 확인")
def step_impl():
  Regions.TomoAnalysis36.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel)

@then("Basic Analyzer[T] 탭의 Movie player 타임 바 오른쪽의 숫자가 2로 올라갔는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.playerPanel.progressSpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "2")




@given("Basic Analyzer[T] 탭의 Mask Viewing panel에 time point 3의 이미지가 나타나있다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.playerPanel.progressSpinBox.wValue = 3

@when("Basic Analyzer[T] 탭의 Movie player 타임 바 오른쪽의 숫자 칸 옆의 아래 화살표 버튼을 한 번 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.playerPanel.progressSpinBox.wValue = 2

@then("Basic Analyzer[T] 탭의 Mask Viewing panel에 나타나는 이미지가 time point 2의 이미지로 바뀌었는지 확인")
def step_impl():
  Regions.TomoAnalysis40.Check(Aliases.TomoAnalysis.mainwindow.TomoAnalysis12)

@then("Basic Analyzer[T] 탭의 Movie player 타임 바 오른쪽의 숫자가 2로 내려갔는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.playerPanel.progressSpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "2")


  
@when("Basic Analyzer[T] 탭의 Movie player 패널에 마우스 포인터를 대고 마우스 휠을 위로 {arg} 칸 올린다.")
def step_impl(param1):
  widget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab
  spinBox = widget.playerPanel.progressSpinBox
  spinBox.qt_spinbox_lineedit.Click(41, 14)
  spinBox.MouseWheel(20)

@then("Basic Analyzer[T] 탭의 Mask Viewing panel에 나타나는 이미지가 time point 17의 이미지로 바뀌었는지 확인")
def step_impl():
  Regions.TomoAnalysis37.Check(Aliases.TomoAnalysis.mainwindow.TomoAnalysis12)

@then("Basic Analyzer[T] 탭의 Movie player 타임 바 커서의 위치가 오른쪽 끝으로 움직였는지 확인")
def step_impl():
  Regions.progressSlider.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.playerPanel.progressSlider)

@then("Basic Analyzer[T] 탭의 Movie player 타임 바 오른쪽의 숫자가 17로 올라갔는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.playerPanel.progressSpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "17")


  
@given("Basic Analyzer[T] 탭의 Mask Viewing panel에 time point 17의 이미지가 나타나있다.")
def step_impl():
  spinBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.playerPanel.progressSpinBox
  spinBox.qt_spinbox_lineedit.Drag(13, 17, -20, 0)
  spinBox.Keys("17")
  spinBox.wValue = 17

@when("Basic Analyzer[T] 탭의 Movie player 패널에 마우스 포인터를 대고 마우스 휠을 아래로 {arg} 칸 내린다.")
def step_impl(param1):
  widget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab
  spinBox = widget.playerPanel.progressSpinBox
  spinBox.qt_spinbox_lineedit.Click(41, 14)
  spinBox.MouseWheel(-20)

@then("Basic Analyzer[T] 탭의 Mask Viewing panel에 나타나는 이미지가 time point 1의 이미지로 바뀌었는지 확인")
def step_impl():
  Regions.TomoAnalysis39.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel.Splitter.Widget2.TomoAnalysis_BasicAnalysisTime_Plugins_BaRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis)

@then("Basic Analyzer[T] 탭의 Movie player 타임 바 커서의 위치가 왼쪽 끝으로 움직였는지 확인")
def step_impl():
  Regions.progressSlider1.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.playerPanel.progressSlider)

@then("Basic Analyzer[T] 탭의 Movie player 타임 바 오른쪽의 숫자가 1로 내려갔는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.playerPanel.progressSpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "1")


  
  
@when("Basic Analyzer[T] 탭의 Time points setting 패널에서 start 칸에 {arg}를 입력한다.")
def step_impl(param1):
  spinBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup.startTime
  spinBox.qt_spinbox_lineedit.Drag(35, 7, -47, 1)
  spinBox.Keys("2")
  spinBox.wValue = 2

@then("Basic Analyzer[T] 탭 Time points setting 패널의 Selected time points에 나타나는 시작 숫자가 {arg}로 바뀌었는지 확인")
def step_impl(param1):
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup.timePoints, "wText", cmpEqual, "2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17")


  
@when("Basic Analyzer[T] 탭의 Time points setting 패널에서 start 칸 옆의 위 화살표 버튼을 한 번 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup.startTime.wValue = 2

@then("Basic Analyzer[T] 탭 start 칸의 숫자가 1만큼 올라갔는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup.timePoints, "wText", cmpEqual, "2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17")

@then("Basic Analyzer[T] 탭 Time points setting 패널의 Selected time points에 나타나는 시작 숫자가 1만큼 올라갔는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup.timePoints, "wText", cmpEqual, "2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17")


  
@given("Basic Analyzer[T] 탭의 Time points setting 패널 start 칸에 '{arg}’이 입력되어 있다.")
def step_impl(param1):
  spinBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup.startTime
  spinBox.qt_spinbox_lineedit.Drag(15, 16, -18, -3)
  spinBox.Keys("3")
  spinBox.wValue = 3

@when("Basic Analyzer[T] 탭의 Time points setting 패널에서 start 칸 옆의 아래 화살표 버튼을 한 번 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.batchRunTab.batchParameterPanel.timeGroup.startTime.Click(70,15)

@then("Basic Analyzer[T] 탭 start 칸의 숫자가 1만큼 내려갔는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup.startTime.qt_spinbox_lineedit, "wText", cmpEqual, "2")

@then("Basic Analyzer[T] 탭 Time points setting 패널의 Selected time points에 나타나는 시작 숫자가 1만큼 내려갔는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup.timePoints, "wText", cmpEqual, "2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17")


  
@when("Basic Analyzer[T] 탭의 Time points setting 패널에서 start 칸에 마우스 포인터를 대고 마우스 휠을 한 칸 위로 올린다.")
def step_impl():
  groupBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup
  spinBox = groupBox.startTime
  spinBox.qt_spinbox_lineedit.MouseWheel(1)


@when("Basic Analyzer[T] 탭의 Time points setting 패널에서 start 칸에 마우스 포인터를 대고 마우스 휠을 한 칸 아래로 내린다.")
def step_impl():
  groupBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup
  groupBox.label_4.MouseWheel(-1)
  spinBox = groupBox.startTime
  spinBox.qt_spinbox_lineedit.Click(42, 9)
  spinBox.wValue = 2

@when("Basic Analyzer[T] 탭의 Time points setting 패널에서 end 칸에 수치를 입력한다.")
def step_impl():
  spinBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup.endTime
  spinBox.Click(53, 26)
  spinBox.qt_spinbox_lineedit.Drag(40, 13, -40, -1)
  spinBox.Keys("2")
  spinBox.wValue = 2

@then("Basic Analyzer[T] 탭 end 칸의 숫자가 입력한 수치로 변경되는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup.endTime.qt_spinbox_lineedit, "wText", cmpEqual, "2")

@then("Basic Analyzer[T] 탭 Time points setting 패널의 Selected time points에 나타나는 숫자가 모두 end 값보다 작은지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup.timePoints, "wText", cmpEqual, "1,2")


  
@given("Basic Analyzer[T] 탭의 Time points setting 패널 end 칸에 '{arg}’이 입력되어 있다.")
def step_impl(param1):
  spinBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup.endTime
  spinBox.qt_spinbox_lineedit.Drag(27, 15, -28, 0)
  spinBox.Keys("3")
  spinBox.wValue = 3

@when("Basic Analyzer[T] 탭의 Time points setting 패널에서 end 칸 옆의 위 화살표 버튼을 한 번 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup.endTime.wValue = 4

@then("Basic Analyzer[T] 탭 end 칸의 숫자가 1만큼 올라갔는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup.endTime.qt_spinbox_lineedit, "wText", cmpEqual, "4")

@then("Basic Analyzer[T] 탭 Time points setting 패널의 Selected time points에 나타나는 끝 숫자가 1만큼 올라갔는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup.timePoints, "wText", cmpEqual, "1,2,3,4")


  
@when("Basic Analyzer[T] 탭의 Time points setting 패널에서 end 칸 옆의 아래 화살표 버튼을 한 번 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup.endTime.wValue = 16

@then("Basic Analyzer[T] 탭 end 칸의 숫자가 1만큼 내려갔는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup.timePoints, "wText", cmpEqual, "2,3,4,5,6,7,8,9,10,11,12,13,14,15,16")

@then("Basic Analyzer[T] 탭 Time points setting 패널의 Selected time points에 나타나는 끝 숫자가 1만큼 내려갔는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup.timePoints, "wText", cmpEqual, "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16")


  
@when("Basic Analyzer[T] 탭의 Time points setting 패널에서 end 칸에 마우스 포인터를 대고 마우스 휠을 위로 한 칸 올린다.")
def step_impl():
  groupBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup
  spinBox = groupBox.endTime
  spinBox.qt_spinbox_lineedit.Click(37, 15)
  spinBox.qt_spinbox_lineedit.MouseWheel(1)


@when("Basic Analyzer[T] 탭의 Time points setting 패널에서 end 칸에 마우스 포인터를 대고 마우스 휠을 아래로 한 칸 내린다.")
def step_impl():
  groupBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup
  spinBox = groupBox.endTime
  spinBox.qt_spinbox_lineedit.Click(37, 15)
  spinBox.qt_spinbox_lineedit.MouseWheel(-1)

@then("Basic Analyzer[T] 탭 end 칸의 숫자가 {arg} 내려갔는지 확인")
def step_impl(param1):
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup.endTime, "wValue", cmpEqual, 16)

@then("Time points setting 패널의 Selected time points에 나타나는 끝 숫자가 {arg} 내려갔는지 확인")
def step_impl(param1):
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup.timePoints, "wText", cmpEqual, "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16")


  
@when("Basic Analyzer[T] 탭의 Time points setting 패널에서 interval 칸에 {arg}를 입력한다.")
def step_impl(param1):
  spinBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup.timeInterval
  spinBox.qt_spinbox_lineedit.Click(28, 10)
  spinBox.Keys("[BS]2")
  spinBox.wValue = 2

@then("Basic Analyzer[T] 탭 interval 칸의 숫자가 2로 변경되는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup.timeInterval.qt_spinbox_lineedit, "wText", cmpEqual, "2")

@then("Basic Analyzer[T] 탭 Time points setting 패널의 Selected time points에 나타나는 수열의 공차가 2로 바뀌었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup.timePoints, "wText", cmpEqual, "1,3,5,7,9,11,13,15,17")


  
@when("Basic Analyzer[T] 탭의 Time points setting 패널에서 interval 칸 옆의 위 화살표 버튼을 한 번 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup.timeInterval.wValue = 2

@then("Basic Analyzer[T] 탭 interval 칸의 숫자가 1만큼 올라갔는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup.timeInterval.qt_spinbox_lineedit, "wText", cmpEqual, "2")

@then("Basic Analyzer[T] 탭 Time points setting 패널의 Selected time points에 나타나는 수열의 공차가 1만큼 올라갔는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup.timePoints, "wText", cmpEqual, "2,4,6,8,10,12,14,16")


  
@given("Basic Analyzer[T] 탭의 Time points setting 패널 interval 칸에 '{arg}’이 입력되어 있다.")
def step_impl(param1):
  spinBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup.timeInterval
  spinBox.qt_spinbox_lineedit.Drag(34, 10, -41, 4)
  spinBox.Keys("3")
  spinBox.wValue = 3
  spinBox.Keys("![ReleaseLast]")

@when("Basic Analyzer[T] 탭의 Time points setting 패널에서 interval 칸 옆의 아래 화살표 버튼을 한 번 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup.timeInterval.wValue = 2

@then("Basic Analyzer[T] 탭 interval 칸의 숫자가 1만큼 내려갔는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup.timeInterval.qt_spinbox_lineedit, "wText", cmpEqual, "2")

@then("Basic Analyzer[T] 탭 Time points setting 패널의 Selected time points에 나타나는 수열의 공차가 1만큼 내려갔는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup.timePoints, "wText", cmpEqual, "1,3,5,7,9,11,13,15,17")


  
@when("Basic Analyzer[T] 탭의 Time points setting 패널에서 interval 칸에 마우스 포인터를 대고 마우스 휠을 위로 한 칸 올린다.")
def step_impl():
  groupBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup
  spinBox = groupBox.timeInterval
  spinBox.qt_spinbox_lineedit.MouseWheel(1)



@when("Basic Analyzer[T] 탭의 Time points setting 패널에서 interval 칸에 마우스 포인터를 대고 마우스 휠을 아래로 한 칸 내린다.")
def step_impl():
  groupBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup
  spinBox = groupBox.timeInterval
  spinBox.qt_spinbox_lineedit.MouseWheel(-1)

  groupBox.MouseWheel(-1)
  spinBox.wValue = 2

@given("Basic Analyzer[T] 탭 Time points setting 패널의 custom 체크박스가 비어있다.")
def step_impl():
  raise NotImplementedError

@when("Basic Analyzer[T] 탭의 Time points setting 패널에서 custom 체크박스에 체크한다.")
def step_impl():
  groupBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup
  groupBox.Click(9, 146)
  groupBox.customTime.ClickButton(cbChecked)

@then("Basic Analyzer[T] 탭 custom 체크박스와 글자가 하이라이트 되는지 확인")
def step_impl():
  Regions.timeGroup1.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup)

@then("Basic Analyzer[T] 탭 체크 박스에 체크 표시가 생기는지 확인")
def step_impl():
  Regions.customTime.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup.customTime)

@then("Basic Analyzer[T] 탭 Time points setting 패널의 Selected time points에 직접 숫자를 입력할 수 있게 되는지 확인")
def step_impl():
  lineEdit = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup.timePoints
  lineEdit.Click(318, 13)
  lineEdit.SetText("1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,171")
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup.timePoints, "wText", cmpEqual, "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,171")

@then("Basic Analyzer[T] 탭 Time points setting 패널의 start, end, interval 값을 변경할 수 없게 되었는지 확인")
def step_impl():
  Regions.timeGroup3.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup)


  
@given("Basic Analyzer[T] 탭에서 Single Run 탭의 Time points setting 패널의 custom 체크박스에 체크 되어있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir = r'E:\Regression_Test\Prefix_File\BAT\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir3 = r'E:\Regression_Test\Prefix_File\BAT\playground\test\Hypercube\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)
  dir4 =r'E:\Regression_Test\Prefix_File\BAT\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\BAT'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BAT'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(155, 29)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BAT\\BAT.tcpro")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_gray.Click(122, 12)
  Aliases.TomoAnalysis.widget_popup.scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget.panel_contents_image.widget.panel_contents_image.Widget.DblClick(76, 93)
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup.customTime.ClickButton(cbChecked)

@when("Basic Analyzer[T] 탭 Time points setting 패널의 Selected time points 칸에 time points를 직접 입력한다.")
def step_impl():
  lineEdit = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup.timePoints
  lineEdit.Click(295, 17)
  lineEdit.SetText("1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16")


@then("Basic Analyzer[T] 탭 Time points setting 패널의 Selected time points 칸의 숫자가 입력한 대로 바뀌는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup.timePoints, "wText", cmpEqual, "1,2,3")


@when("Time points setting 패널에서 time points를 지정하고 Apply time points 버튼을 누른다.")
def step_impl():
  groupBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup
  groupBox.Click(332, 140)
  lineEdit = groupBox.timePoints
  lineEdit.Click(323, 18)
  lineEdit.Click(251, 15)
  lineEdit.Keys("[BS]")
  groupBox.customTime.ClickButton(cbChecked)
  lineEdit.Click(257, 15)
  lineEdit.SetText("1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16")
  groupBox.applyBtn.ClickButton()

@then("Apply time points 버튼의 하이라이트가 해제됐는지 확인")
def step_impl():
  Regions.timeGroup4.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup)

@then("Processing mode 패널의 AI Inference 탭의 Execute 버튼이 하이라이트 되었는지 확인")
def step_impl():
  Regions.groupBox9.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox)


  
@when("BAT Application Parameter 의 Single RUN 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_gray.Click(83, 22)

@when("팝업에서 App이 연결된 hypercube 안의 Time-lapse TCF 중 Single RUN 하고 싶은 TCF를 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.widget_popup.scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget.panel_contents_image.widget.panel_contents_image.Widget.Click(123, 82)
  Aliases.TomoAnalysis.widget_popup.bt_square_primary.Click(49, 19)

@then("Basic Analyzer[T] 탭 Processing mode 패널의 맨 위 드롭다운 버튼이 AI Segmentation으로 설정됐는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.procCombo, "wText", cmpEqual, "AI Segmentation")


  
@when("Basic Analyzer[T] 탭 Processing mode 패널 상단의 드롭다운 버튼에서 AI Segmentation을 선택한다.")
def step_impl():
  comboBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.procCombo
  comboBox.ClickItem("AI Segmentation")

@then("Basic Analyzer[T] 탭 Single run 탭의 Processing mode 패널에 AI Segmentation 모드의 UI가 나타남을 확인")
def step_impl():
  Regions.parameterPanel3.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel)

@then("Basic Analyzer[T] 탭에 Time points setting 패널이 있는지 확인")
def step_impl():
  Regions.timeGroup5.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup)


  
@then("Project Manager 탭의 BAT Application Parameter 패널과 Basic Analyzer[T] 탭의 Processing mode 패널의 두 Basic Measurement 값이 서로 일치하는지 확인")
def step_impl():
  Tables.TableWidget41.Check()
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.TC_TabBar.Click(120, 40)
  Tables.TableWidget42.Check()


  
@given("Basic Analyzer[T] Single Run 탭에서 AI Segmentation이 프로세서로 선택되어 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir = r'E:\Regression_Test\Prefix_File\BAT\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir3 = r'E:\Regression_Test\Prefix_File\BAT\playground\test\Hypercube\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)
  dir4 =r'E:\Regression_Test\Prefix_File\BAT\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\BAT'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BAT'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(155, 29)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BAT\\BAT.tcpro")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_gray.Click(122, 12)
  Aliases.TomoAnalysis.widget_popup.scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget.panel_contents_image.widget.panel_contents_image.Widget.DblClick(76, 93)
  
@given("Basic Analyzer[T] 탭 AI Inference 버튼 맨 오른쪽의 화살표가 윗 방향\\(△)이다.")
def step_impl():
  tomoAnalysis_mainwindow = Aliases.TomoAnalysis.mainwindow
  tomoAnalysis_mainwindow.TomoAnalysis3.MouseWheel(-2)
  buttonAiInference = tomoAnalysis_mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.panel_contents.bt_collapse
  buttonAiInference.ClickButton()
  buttonAiInference.ClickButton()

@when("Basic Analyzer[T] 탭의 AI Inference 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.panel_contents.bt_collapse.ClickButton()

@then("Basic Analyzer[T] 탭의 AI Inference 버튼 밑의 UI가 접히는지 확인")
def step_impl():
  Regions.qt_scrollarea_viewport50.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport)
  Regions.bt_collapse48.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.panel_contents.bt_collapse)

@then("Basic Analyzer[T] 탭의 AI Inference 버튼 맨 오른쪽의 화살표가 아래 방향\\(▽)으로 바뀌는지 확인")
def step_impl():
  Regions.bt_collapse49.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.panel_contents.bt_collapse2)


  
@given("Basic Analyzer[T] 탭의 AI Inference 버튼 맨 오른쪽의 화살표가 아래 방향\\(▽)이다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.panel_contents.bt_collapse.ClickButton()

@then("Basic Analyzer[T] 탭의 AI Inference 버튼 밑의 UI가 펼쳐지는지 확인")
def step_impl():
  Regions.CollapseWidget36.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2)

@then("Basic Analyzer[T] 탭의 AI Inference 버튼 맨 오른쪽의 화살표가 윗 방향\\(△)으로 바뀌는지 확인")
def step_impl():
  Regions.bt_collapse54.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.panel_contents.bt_collapse2)


  
@given("Basic Analyzer[T] Single Run 탭에서 AI Segmentation이 선택되어 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir = r'E:\Regression_Test\Prefix_File\BAT\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir3 = r'E:\Regression_Test\Prefix_File\BAT\playground\test\Hypercube\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)
  dir4 =r'E:\Regression_Test\Prefix_File\BAT\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\BAT'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BAT'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(155, 29)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BAT\\BAT.tcpro")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_gray.Click(122, 12)
  Aliases.TomoAnalysis.widget_popup.scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget.panel_contents_image.widget.panel_contents_image.Widget.DblClick(76, 93)
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup.applyBtn.ClickButton()

@when("Basic Analyzer[T] 탭의 AI Inference 탭의 Execute 버튼을 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterControl.buttonExecute.ClickButton()

@when("프로세싱 바 팝업이 생기면 진행이 완료되고 팝업이 사라질 때 까지 기다린다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  while tomoAnalysis.ProgressDialog.Exists :
   Delay(1000, "Waiting for window to close...");
  
@then("setting한 time points에서 Basic Analyzer[T] 탭의 Mask Viewing panel에 마스크가 생성되었는지 확인")
def step_impl():
  Regions.TomoAnalysis45.Check(Aliases.TomoAnalysis.mainwindow.TomoAnalysis12)

@then("Basic Analyzer[T] 탭 Mask Viewing panel 상단에 Cell Instance, Whole Cell, Nucleus, Nucleolus, Lipid droplet 체크박스와 Cell-wise filter 스위치, Opacity 조절 버튼이 있는 툴박스가 생성되었는지 확인")
def step_impl():
  Regions.VizControlPanel4.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel)

@then("Basic Analyzer[T] 탭 Processing mode 패널 AI Inference 탭의 Execute 버튼이 하이라이트 해제되었는지 확인")
def step_impl():
  Regions.CollapseWidget37.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2)

@then("Basic Analyzer[T] 탭 Processing mode 패널 Basic Measurement 탭의 Execute 버튼이 하이라이트 되었는지 확인")
def step_impl():
  Regions.CollapseWidget38.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget)


  
@given("Basic Analyzer[T] Single Run 탭의 데이터 {arg}, {arg}, {arg} time point에 마스크가 생성되어 있다.")
def step_impl(param1, param2, param3):
  TestedApps.TomoAnalysis.Run()
  dir = r'E:\Regression_Test\Prefix_File\BAT\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir3 = r'E:\Regression_Test\Prefix_File\BAT\playground\test\Hypercube\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)
  dir4 =r'E:\Regression_Test\Prefix_File\BAT\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\BAT'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BAT'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(155, 29)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BAT\\BAT.tcpro")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_gray.Click(122, 12)
  Aliases.TomoAnalysis.widget_popup.scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget.panel_contents_image.widget.panel_contents_image.Widget.DblClick(76, 93)

  groupBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup
  groupBox.customTime.ClickButton(cbChecked)
  lineEdit = groupBox.timePoints
  lineEdit.Drag(273, 8, -208, 14)
  lineEdit.SetText("1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,1")
  lineEdit.Drag(246, 12, -195, 10)
  lineEdit.SetText("1,2,3")
  tomoAnalysis_BasicAnalysisTime_Plugins_ParameterPanel = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel
  groupBox = tomoAnalysis_BasicAnalysisTime_Plugins_ParameterPanel.timeGroup
  groupBox.Click(378, 207)
  groupBox.applyBtn.ClickButton()
  tomoAnalysis_BasicAnalysisTime_Plugins_ParameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterControl.buttonExecute.ClickButton()
  tomoAnalysis = Aliases.TomoAnalysis
  while tomoAnalysis.ProgressDialog.Exists :
   Delay(1000, "Waiting for window to close...");
  
@when("Basic Analyzer[T] 탭의 Movie player에서 time point를 4에서 17까지 움직인다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.playerPanel.progressSlider.wPosition = 17

@then("Basic Analyzer[T] 탭의 Mask Viewing panel에서 마스크가 생성되지 않았음을 확인")
def step_impl():
  Regions.TomoAnalysis46.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel.Splitter.Widget2.TomoAnalysis_BasicAnalysisTime_Plugins_BaRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis)


  
@given("Basic Analyzer[T] Single Run 탭의 Mask Viewing panel에 3D 마스크가 생성되어 있다.")
def step_impl():

  dir = r'E:\Regression_Test\Prefix_File\BAT\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir3 = r'E:\Regression_Test\Prefix_File\BAT\playground\test\Hypercube\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)
  dir4 =r'E:\Regression_Test\Prefix_File\BAT\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir5 =r'E:\Regression_Test\*.tcpg'
  if(os.path.isdir(dir5)):
    shutil.rmtree(dir5)
    os.makedirs(dir5)
  dir1 = r'E:\Regression_Test\Prefix_File\BAT'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BAT'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(155, 29)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BAT\\BAT.tcpro")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_gray.Click(122, 12)
  Aliases.TomoAnalysis.widget_popup.scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget.panel_contents_image.widget.panel_contents_image.Widget.DblClick(76, 93)

  groupBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup
  groupBox.customTime.ClickButton(cbChecked)
  lineEdit = groupBox.timePoints
  lineEdit.Drag(273, 8, -208, 14)
  lineEdit.SetText("1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,1")
  lineEdit.Drag(246, 12, -195, 10)
  lineEdit.SetText("1,2,3")
  tomoAnalysis_BasicAnalysisTime_Plugins_ParameterPanel = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel
  tomoAnalysis_BasicAnalysisTime_Plugins_ParameterPanel.timeGroup.applyBtn.ClickButton()
  scrollArea = tomoAnalysis_BasicAnalysisTime_Plugins_ParameterPanel.groupBox.panel_contents.ScrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.buttonExecute.ClickButton()
  while tomoAnalysis.ProgressDialog.Exists :
   Delay(1000, "Waiting for window to close...");
   
@when("마우스를 커서 모양이 바뀌는 Basic Analyzer[T] 탭의 Mask Viewing panel 3D 마스크 캔버스 위로 가져가 클릭한 채로 마우스를 움직인다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel.Splitter.Widget.TomoAnalysis_BasicAnalysisTime_Plugins_BaRenderWindow3D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis.Drag(101, 428, 362, -22)

@then("Basic Analyzer[T] 탭 Mask Viewing panel에 생성된 3D 마스크와 좌표축이 회전하는지 확인")
def step_impl():
  Regions.TomoAnalysis149.Check(Aliases.TomoAnalysis.mainwindow.TomoAnalysis12, False, False, 35811, 17)



  
@when("마우스를 커서 모양이 바뀌는 Basic Analyzer[T] 탭의 Mask Viewing panel 3D 마스크 캔버스 위로 가져가 마우스 휠을 위로 두 칸 올린다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel.Splitter.Widget.TomoAnalysis_BasicAnalysisTime_Plugins_BaRenderWindow3D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis
  tomoAnalysis.Click(426, 237)
  tomoAnalysis.MouseWheel(2)

@then("Basic Analyzer[T] 탭 Mask Viewing panel에 생성된 3D 마스크가 두 단위 확대됨을 확인")
def step_impl():
  Regions.TomoAnalysis48.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel.Splitter.Widget.TomoAnalysis_BasicAnalysisTime_Plugins_BaRenderWindow3D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis, False, False, 35811, 17)


  
@when("마우스를 커서 모양이 바뀌는 Basic Analyzer[T] 탭 Mask Viewing panel 3D 마스크 캔버스 위로 가져가 마우스 휠을 아래로 두 칸 내린다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel.Splitter.Widget.TomoAnalysis_BasicAnalysisTime_Plugins_BaRenderWindow3D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis
  tomoAnalysis.Click(426, 237)
  tomoAnalysis.MouseWheel(-2)

@then("Basic Analyzer[T] 탭 Mask Viewing panel에 생성된 3D 마스크가 두 단위 축소됨을 확인")
def step_impl():
  Regions.TomoAnalysis49.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel.Splitter.Widget.TomoAnalysis_BasicAnalysisTime_Plugins_BaRenderWindow3D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis, False, False, 35811, 17)


  
@given("Basic Analyzer[T] Single Run 탭이 열려 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir = r'E:\Regression_Test\Prefix_File\BAT\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir3 = r'E:\Regression_Test\Prefix_File\BAT\playground\test\Hypercube\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)
  dir4 =r'E:\Regression_Test\Prefix_File\BAT\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\BAT'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BAT'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(155, 29)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BAT\\BAT.tcpro")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_gray.Click(122, 12)
  Aliases.TomoAnalysis.widget_popup.scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget.panel_contents_image.widget.panel_contents_image.Widget.DblClick(76, 93)

@when("Basic Analyzer[T] 탭 Mask Viewing panel의 3D 마스크 캔버스의 우상단의 톱니바퀴 아이콘을 클릭하고 Background Option 버튼을 클릭한 뒤 Black 버튼을 클릭한다.")
def step_impl():
  raise NotImplementedError

@then("Basic Analyzer[T] 탭 Mask Viewing panel의 3D 마스크 캔버스의 배경 색이 검은 색으로 변경됨을 확인")
def step_impl():
  raise NotImplementedError

@when("Basic Analyzer[T] 탭 Mask Viewing panel의 3D 마스크 캔버스의 우상단의 톱니바퀴 아이콘을 클릭하고 Background Option 버튼을 클릭한 뒤 White 버튼을 클릭한다.")
def step_impl():
  raise NotImplementedError

@then("Basic Analyzer[T] 탭 Mask Viewing panel의 3D 마스크 캔버스의 배경 색이 흰 색으로 변경됨을 확인")
def step_impl():
  raise NotImplementedError

@when("Basic Analyzer[T] 탭 Mask Viewing panel의 3D 마스크 캔버스의 우상단의 톱니바퀴 아이콘을 클릭하고 Background Option 버튼을 클릭한 뒤  Gradient 버튼을 클릭한다.")
def step_impl():
  raise NotImplementedError

@then("Basic Analyzer[T] 탭 Mask Viewing panel의 3D 마스크 캔버스의 배경 색이 Gradient로 변경됨을 확인")
def step_impl():
  raise NotImplementedError

@when("Basic Analyzer[T] 탭 Mask Viewing panel의 3D 마스크 캔버스 우상단의 톱니바퀴 아이콘을 클릭하고 View 버튼을 클릭한 뒤 Reset View 버튼을 클릭한다.")
def step_impl():
  raise NotImplementedError

@then("Basic Analyzer[T] 탭 Mask Viewing panel에서 처음 마스크가 생성되었을 때의 초기 시점으로 마스크가 보이는지 확인")
def step_impl():
  raise NotImplementedError

@when("Basic Analyzer[T] 탭의 Mask Viewing panel 상단 Mask 툴박스의 Whole Cell 체크박스에 체크한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.controlSocket.RadioButton4.ClickButton()

@then("Basic Analyzer[T] 탭의 Mask Viewing panel의 Cell의 전체 영역에 마스크가 입혀졌는지 확인")
def step_impl():
  Regions.TomoAnalysis50.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel, False, False, 1000, 25)



@when("Basic Analyzer[T] 탭의 Mask Viewing panel 상단 Mask 툴박스의 Nucleus 체크박스에 체크한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.controlSocket.RadioButton.ClickButton()

@then("Basic Analyzer[T] 탭의 Mask Viewing panel의 Cell의 Nucleus 영역에 마스크가 입혀졌는지 확인")
def step_impl():
  Regions.TomoAnalysis51.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel, False, False, 1000, 25)



@when("BAT Application Parameter 패널의 Basic Measurement 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.panel_contents.bt_collapse3.ClickButton()

@when("BAT Application Parameter 패널 Basic Measurement 버튼을 클릭한다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea  
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 203
  scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.panel_contents.bt_collapse.ClickButton()

@then("BAT Application Parameter 패널 Basic Measurement 밑 UI가 펼쳐지는지 확인")
def step_impl():
  Regions.CollapseWidget30.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2)

@then("BAT Application Parameter 패널의 Basic Measurement 버튼의 맨 오른쪽 화살표가 윗 방향\\(△)으로 바뀌었는지 확인")
def step_impl():
  Regions.bt_collapse42.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.panel_contents.bt_collapse2)



@then("BAT Application Parameter 패널 Basic Measurement 밑 UI가 접히는지 확인")
def step_impl():
  Regions.Widget35.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget)


@then("BAT Application Parameter 패널의 Basic Measurement 버튼의 맨 오른쪽 화살표가 아래 방향\\(▽)으로 바뀌었는지 확인")
def step_impl():
  Regions.bt_collapse41.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.panel_contents.bt_collapse2)



@given("BAT Application Parameter 패널의 Basic Measurement 버튼의 맨 오른쪽의 화살표가 아래 방향\\(▽)이다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.panel_contents.bt_collapse.CheckButton(cbUnchecked)

@given("BAT Application Parameter 패널의 Basic Measurement버튼의 맨 오른쪽의 화살표가 윗 방향\\(△)이다.")
def step_impl():
  buttonBasicMeasurement = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame.scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.panel_contents.bt_collapse3
  buttonBasicMeasurement.ClickButton()
  buttonBasicMeasurement.ClickButton()


@when("Basic Analyzer[T] 탭의 Mask Viewing panel 상단 Mask 툴박스의 Nucleolus 체크박스에 체크한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.controlSocket.RadioButton2.ClickButton()

@then("Basic Analyzer[T] 탭의 Mask Viewing panel의 Cell의 Nucleolus 영역에 마스크가 입혀졌는지 확인")
def step_impl():
  Regions.TomoAnalysis52.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel, False, False, 1000, 25)
  
@when("Basic Analyzer[T] 탭의 Mask Viewing panel 상단 Mask 툴박스의 Lipid droplet 체크박스에 체크한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.controlSocket.RadioButton3.ClickButton()

@then("Basic Analyzer[T] 탭의 Mask Viewing panel의 Cell의 Lipid droplet 영역에 마스크가 입혀졌는지 확인")
def step_impl():
  Regions.TomoAnalysis53.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel, False, False, 1000, 25)



@given("Basic Analyzer[T] 탭의 Mask Viewing panel 상단 Mask 툴박스에 Whole Cell이 체크되어 있다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.controlSocket.RadioButton.ClickButton()

@when("Basic Analyzer[T] 탭의 Mask Viewing panel 상단 Mask 툴박스의 Cell Instance 체크박스에 체크한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.bt_switch.ClickButton()

@then("Basic Analyzer[T] 탭의 Mask Viewing panel의 Cell Instance 영역에 마스크가 입혀졌는지 확인")
def step_impl():
  Regions.TomoAnalysis54.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel.Splitter.Widget2.TomoAnalysis_BasicAnalysisTime_Plugins_BaRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis)



@given("Basic Analyzer[T] 탭 Mask Viewing panel 상단 Mask 툴박스의 Cell-wise filter 스위치가 꺼져 있다.")
def step_impl():
  pushButton = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.bt_switch
  pushButton.ClickButton()
  pushButton.ClickButton()

@when("Basic Analyzer[T] 탭 Mask Viewing panel 상단 Mask 툴박스의 Cell-wise filter 스위치를 켠다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.bt_switch.ClickButton()

@then("Basic Analyzer[T] 탭 Mask Viewing panel에서 각각의 cell의 마스크가 다른 색으로 표시되는지 확인")
def step_impl():
  Regions.TomoAnalysis55.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel.Splitter.Widget2.TomoAnalysis_BasicAnalysisTime_Plugins_BaRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis)

@then("Basic Analyzer[T] 탭 Mask 툴박스의 Cell-wise filter 스위치 버튼이 하이라이트 됐는지 확인")
def step_impl():
  Regions.bt_switch7.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.bt_switch)

@then("Basic Analyzer[T] 탭 Mask 툴박스의 Cell-wise filter 스위치 버튼이 오른쪽으로 움직였는지 확인")
def step_impl():
  Regions.bt_switch8.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.bt_switch)



@given("Basic Analyzer[T] 탭 Mask Viewing panel 상단 Mask 툴박스의 Cell-wise filter 스위치가 켜져 있다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.bt_switch.ClickButton()

@when("Basic Analyzer[T] 탭 Mask Viewing panel 상단 Mask 툴박스의 Cell-wise filter 스위치를 끈다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.bt_switch.ClickButton()

@then("Basic Analyzer[T] 탭 Mask Viewing panel에서 모든 cell의 마스크가 같은 색으로 표시되는지 확인")
def step_impl():
  Regions.TomoAnalysis56.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel.Splitter.Widget2.TomoAnalysis_BasicAnalysisTime_Plugins_BaRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis)

@then("Basic Analyzer[T] 탭 Mask 툴박스의 Cell-wise filter 스위치 버튼이 하이라이트 해제됐는지 확인")
def step_impl():
  Regions.bt_switch9.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.bt_switch)

@then("Basic Analyzer[T] 탭 Mask 툴박스의 Cell-wise filter 스위치 버튼이 왼쪽으로 움직였는지 확인")
def step_impl():
  Regions.bt_switch10.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.bt_switch)



@when("Basic Analyzer[T] 탭 Mask Viewing panel 상단 Mask 툴박스의 Opacity 칸에 ‘{arg}’를 입력한다.")
def step_impl(param1):
  doubleSpinBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.maskOpacity.qt_spinbox_lineedit
  doubleSpinBox.QLineEdit_clear()
  doubleSpinBox.SetText("0.5")
  
@then("Basic Analyzer[T] 탭 Mask Viewing panel의 마스크들이 입력 값에 맞게 투명해짐을 확인")
def step_impl():
  Regions.TomoAnalysis57.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel, False, False, 1000, 17)


@given("Basic Analyzer[T] 탭 Mask Viewing panel 상단 Mask 툴박스의 Opacity 값이 ‘{arg}’ 이다.")
def step_impl(param1):
  doubleSpinBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.maskOpacity.qt_spinbox_lineedit
  doubleSpinBox.QLineEdit_clear()
  doubleSpinBox.SetText("0.5")

@when("Basic Analyzer[T] 탭 Mask Viewing panel 상단 Mask 툴박스의 Opacity 칸 수치 입력창 옆의 윗 화살표 버튼을 10번 누른다.")
def step_impl():
  doubleSpinBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.maskOpacity.qt_spinbox_lineedit

  for i in range(1, 11): 
    Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.maskOpacity.Click(80, 10)

@then("Basic Analyzer[T] 탭 Mask Viewing panel 상단 Mask 툴박스의Opacity 수치 값이 0.60으로 올라갔는지 확인한다")
def step_impl():
  aqUtils.Delay(1000)
  Regions.maskOpacity.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.maskOpacity)

@then("Basic Analyzer[T] Mask Viewing panel의 마스크들이 진해짐을 확인")
def step_impl():
  Regions.TomoAnalysis58.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel.Splitter.Widget2.TomoAnalysis_BasicAnalysisTime_Plugins_BaRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis)


  
@when("Basic Analyzer[T] 탭 Mask Viewing panel 상단 Mask 툴박스의 Opacity 칸 수치 입력창 옆의 아래 화살표 버튼을 10번 누른다.")
def step_impl():
  doubleSpinBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.maskOpacity.qt_spinbox_lineedit

  for i in range(1, 11): 
    Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.maskOpacity.Click(80, 20)

@then("Basic Analyzer[T] 탭 Mask Viewing panel 상단 Mask 툴박스의 Opacity 수치 값이 0.40으로 내려감을 확인")
def step_impl():
  Regions.maskOpacity1.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.maskOpacity)

@then("Basic Analyzer[T] Mask Viewing panel의 마스크들이 투명해짐을 확인")
def step_impl():
  widget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab


  
@when("Basic Analyzer[T] 탭 Mask Viewing panel 상단 Mask 툴박스의 Opacity 칸 수치 입력창 옆의 마우스 휠을 위로 10칸 올린다.")
def step_impl():
  widget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab
  doubleSpinBox = widget.VizControlPanel.maskOpacity
  doubleSpinBox.qt_spinbox_lineedit.Click(29, 10)
  for i in range(1, 11):     
    doubleSpinBox.MouseWheel(1)


@then("Basic Analyzer[T] 탭 Mask Viewing panel 상단 Mask 툴박스의 Opacity 수치 값이 0.60으로 올라감을 확인")
def step_impl():
  raise NotImplementedError

@given("Basic Analyzer[T] 탭 Mask Viewing panel 상단 Mask 툴박스의 Opacity 칸 수치 입력창 옆의 마우스 휠을 아래로 10칸 내린다.")
def step_impl():
  widget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab
  doubleSpinBox = widget.VizControlPanel.maskOpacity
  doubleSpinBox.qt_spinbox_lineedit.Click(29, 10)
  for i in range(1, 11):     
    doubleSpinBox.MouseWheel(-1)

@given("Basic Analyzer[T] Single Run 탭의 Mask Viewing panel에 마스크가 생성되어 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir = r'E:\Regression_Test\Prefix_File\BAT\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir3 = r'E:\Regression_Test\Prefix_File\BAT\playground\test\Hypercube\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)
  dir4 =r'E:\Regression_Test\Prefix_File\BAT\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\BAT'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BAT'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(155, 29)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BAT\\BAT.tcpro")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_gray.Click(122, 12)
  Aliases.TomoAnalysis.widget_popup.scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget.panel_contents_image.widget.panel_contents_image.Widget.DblClick(76, 93)


  groupBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup
  groupBox.customTime.ClickButton(cbChecked)
  lineEdit = groupBox.timePoints
  lineEdit.Drag(273, 8, -208, 14)
  lineEdit.SetText("1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,1")
  lineEdit.Drag(246, 12, -195, 10)
  lineEdit.SetText("1")
  tomoAnalysis_BasicAnalysisTime_Plugins_ParameterPanel = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel
  groupBox = tomoAnalysis_BasicAnalysisTime_Plugins_ParameterPanel.timeGroup
  groupBox.Click(378, 207)
  groupBox.applyBtn.ClickButton()
  tomoAnalysis_BasicAnalysisTime_Plugins_ParameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterControl.buttonExecute.ClickButton()
  tomoAnalysis = Aliases.TomoAnalysis
  while tomoAnalysis.ProgressDialog.Exists :
   Delay(1000, "Waiting for window to close...");

    


  
@given("Basic Analyzer[T] 탭 Mask 툴박스의 Whole Cell 체크박스가 체크 되어있다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.controlSocket.RadioButton.ClickButton()

@when("Basic Analyzer[T] 탭 Mask Viewing panel의 HT 이미지 캔버스 위에 마우스 포인터를 올리고 마우스 휠을 위로 5칸 올린다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel.Splitter.Widget.TomoAnalysis_BasicAnalysisTime_Plugins_BaRenderWindow3D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis
  tomoAnalysis.Click(452, 382)
  tomoAnalysis.MouseWheel(5)

@then("Basic Analyzer[T] 탭의 Mask Viewing panel HT 단층 사진의 Z축이 올라가는지 확인")
def step_impl():
  Regions.TomoAnalysis60.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel.Splitter.Widget.TomoAnalysis_BasicAnalysisTime_Plugins_BaRenderWindow3D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis, False, False, 35811, 17)
@when("Basic Analyzer[T] 탭 Mask Viewing panel의 HT 이미지 캔버스 위에 마우스 포인터를 올리고 마우스 휠을 아래로 5칸 내린다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel.Splitter.Widget.TomoAnalysis_BasicAnalysisTime_Plugins_BaRenderWindow3D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis
  tomoAnalysis.Click(452, 382)
  tomoAnalysis.MouseWheel(-5)

@then("Basic Analyzer[T] Single Run 탭의 Mask Viewing panel HT 단층 사진의 Z축이 내려가는지 확인")
def step_impl():
  Regions.TomoAnalysis61.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel.Splitter.Widget.TomoAnalysis_BasicAnalysisTime_Plugins_BaRenderWindow3D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis, False, False, 35811, 17)


  
@when("Basic Analyzer[T] 탭 HT 이미지 캔버스의 우상단의 톱니바퀴 아이콘을 클릭한 뒤 Level Window 버튼을 클릭하고 Data Range 사이즈 바의 왼쪽 버튼을 마우스로 오른쪽 끝까지 움직인다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TomoAnalysis4.Click(651, 27)
  tomoAnalysis.Menu2.QtMenu.Click("Level Window")
  tomoAnalysis.Item.Item.Item.Drag(7, 7, 550, 3)
  tomoAnalysis.Menu3.Click(7, -110)

@then("Basic Analyzer[T] 탭 Mask Viewing panel에서 HT 이미지가 어둡게 변하는지 확인")
def step_impl():
  Regions.TomoAnalysis62.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel.Splitter.Widget2.TomoAnalysis_BasicAnalysisTime_Plugins_BaRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis)

@then("Basic Analyzer[T] 탭 Level Window 사이즈 바 밑의 왼쪽 수치가 오른쪽 수치와 14441로 같아졌는지 확인")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TomoAnalysis4.Click(653, 27)
  tomoAnalysis.Menu.QtMenu.Click("Level Window")
  tomoAnalysis.Menu2.Click(83, -108)

@when("Basic Analyzer[T] 탭 HT 이미지 캔버스의 우상단의 톱니바퀴 아이콘을 클릭한 뒤 Level Window 버튼을 클릭하고 Data Range 사이즈 바의 오른쪽 버튼을 마우스로 왼쪽 끝까지 움직인다.")
def step_impl():
  raise NotImplementedError

@then("Basic Analyzer[T] 탭 Mask Viewing panel에서 HT 이미지가 밝게 변하다가 마지막 프레임에서 어두워지는지 확인")
def step_impl():
  raise NotImplementedError

@then("Basic Analyzer[T] 탭 Level Window 사이즈 바 밑의 오른쪽 수치가 왼쪽 수치와 13298로 같아졌는지 확인")
def step_impl():
  raise NotImplementedError

@given("Basic Analyzer[T] Single Run 탭의 Processing mode 패널에서 프로세서가 AI Segmentation으로 선택되어 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir = r'E:\Regression_Test\Prefix_File\BAT\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir3 = r'E:\Regression_Test\Prefix_File\BAT\playground\test\Hypercube\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)
  dir4 =r'E:\Regression_Test\Prefix_File\BAT\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\BAT'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BAT'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(155, 29)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BAT\\BAT.tcpro")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_gray.Click(122, 12)
  Aliases.TomoAnalysis.widget_popup.scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget.panel_contents_image.widget.panel_contents_image.Widget.DblClick(76, 93)


  groupBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup
  groupBox.customTime.ClickButton(cbChecked)
  lineEdit = groupBox.timePoints
  lineEdit.Drag(273, 8, -208, 14)
  lineEdit.SetText("1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,1")
  lineEdit.Drag(246, 12, -195, 10)
  lineEdit.SetText("1")
  tomoAnalysis_BasicAnalysisTime_Plugins_ParameterPanel = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel
  groupBox = tomoAnalysis_BasicAnalysisTime_Plugins_ParameterPanel.timeGroup
  groupBox.Click(378, 207)
  groupBox.applyBtn.ClickButton()
  tomoAnalysis_BasicAnalysisTime_Plugins_ParameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterControl.buttonExecute.ClickButton()
  tomoAnalysis = Aliases.TomoAnalysis
  while tomoAnalysis.ProgressDialog.Exists :
   Delay(1000, "Waiting for window to close...");
  
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.procCombo.ClickItem("AI Segmentation")
@when("Basic Analyzer[T] 탭 Processing mode 패널 Whole cell column의 Preset 드롭다운 버튼에서 Protein을 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.Preset_Setter_0.ClickItem("Protein")

@when("Basic Analyzer[T] 탭 Processing mode 패널 Nucleus column의 Preset 드롭다운 버튼에서 Protein을 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.Preset_Setter_1.ClickItem("Protein")

@when("Basic Analyzer[T] 탭 Processing mode 패널 Nucleolus column의 Preset 드롭다운 버튼에서 Protein을 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.Preset_Setter_2.ClickItem("Protein")

@when("Basic Analyzer[T] 탭 Processing mode 패널 Lipid droplet column의 Preset 드롭다운 버튼에서 Protein을 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.Preset_Setter_3.ClickItem("Protein")

@then("Basic Analyzer[T] 탭 Processing mode 패널 table의 네 column에서 모두 Baseline RI 값이 1.3337로, RII값이 0.19로 입력되는지 확인")
def step_impl():
  Regions.qt_scrollarea_viewport52.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport)


  
@when("Basic Analyzer[T] 탭 Processing mode 패널 Whole cell column의 Preset 드롭다운 버튼에서 Hemoglobin을 선택한다.")
def step_impl():
  widget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport
  widget.Preset_Setter_0.ClickItem("Hemoglobin")
  comboBox = widget.Preset_Setter_1
  comboBox.DropDown()
  comboBox.CloseUp()

@when("Basic Analyzer[T] 탭 Processing mode 패널 Nucleus column의 Preset 드롭다운 버튼에서 Hemoglobin을 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.Preset_Setter_1.ClickItem("Hemoglobin")

@when("Basic Analyzer[T] 탭 Processing mode 패널 Nucleolus column의 Preset 드롭다운 버튼에서 Hemoglobin을 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.Preset_Setter_2.ClickItem("Hemoglobin")

@when("Basic Analyzer[T] 탭 Processing mode 패널 Lipid droplet column의 Preset 드롭다운 버튼에서 Hemoglobin을 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.Preset_Setter_3.ClickItem("Hemoglobin")

@then("Basic Analyzer[T] 탭 Processing mode 패널 table의 네 column에서 모두 Baseline RI 값이 1.3337로, RII값이 0.15로 입력되는지 확인")
def step_impl():
  Tables.TableWidget50.Check()


  
@when("Basic Analyzer[T] 탭 Processing mode 패널 Whole cell column의 Preset 드롭다운 버튼에서 Lipid를 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.Preset_Setter_0.ClickItem("Lipid")

@when("Basic Analyzer[T] 탭 Processing mode 패널 Nucleus column의 Preset 드롭다운 버튼에서 Lipid를 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.Preset_Setter_1.ClickItem("Lipid")

@when("Basic Analyzer[T] 탭 Processing mode 패널 Nucleolus column의 Preset 드롭다운 버튼에서 Lipid를 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.Preset_Setter_2.ClickItem("Lipid")

@when("Basic Analyzer[T] 탭 Processing mode 패널 Lipid droplet column의 Preset 드롭다운 버튼에서 Lipid를 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport.Preset_Setter_3.ClickItem("Lipid")

@then("Basic Analyzer[T] 탭 Processing mode 패널 table의 네 column에서 모두 Baseline RI 값이 1.3337로, RII값이 0.135로 입력되는지 확인")
def step_impl():
  Tables.TableWidget51.Check()


  
@when("Basic Analyzer[T] 탭 Processing mode 패널 Basic Measurement의 table에서 모든 column의 Preset 드롭다운 버튼을 Customize로 선택한다.")
def step_impl():
  widget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget.qt_scrollarea_viewport
  widget.Preset_Setter_0.ClickItem("Customize")
  widget.Preset_Setter_1.ClickItem("Customize")
  widget.Preset_Setter_2.ClickItem("Customize")
  widget.Preset_Setter_3.ClickItem("Customize")

@when("Basic Analyzer[T] 탭 Processing mode 패널에서 Baseline RI row의 cell을 클릭하고 키보드로 ‘{arg}’를 입력하고 키보드의 엔터 키를 누른다.")
def step_impl(param1):
  tableWidget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget
  tableWidget.ClickCell("Baseline RI", "Whole cell")
  lineEdit = tableWidget.qt_scrollarea_viewport.LineEdit
  tableWidget.Keys("1.3337")
  lineEdit.SetText("12345")
  tableWidget.ClickCell("Baseline RI", "Nucleus")
  tableWidget.Keys("1.3337")
  lineEdit.SetText("12345")
  tableWidget.ClickCell("Baseline RI", "Nucleolus")
  tableWidget.Keys("1.3337")
  lineEdit.SetText("12345")
  tableWidget.ClickCell("Baseline RI", "Lipid drolet")
  tableWidget.Keys("1.3337")
  lineEdit.SetText("12345")

@when("Basic Analyzer[T] 탭 Processing mode 패널에서 RII row의 cell을 클릭하고 키보드로 ‘{arg}’를 입력하고 키보드의 엔터 키를 누른다.")
def step_impl(param1):
  tableWidget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.TableWidget
  tableWidget.ClickCell("RII", "Whole cell")
  lineEdit = tableWidget.qt_scrollarea_viewport.LineEdit
  tableWidget.Keys("0.19")
  lineEdit.SetText("12345")
  tableWidget.ClickCell("RII", "Nucleus")
  tableWidget.Keys("0.19")
  lineEdit.SetText("12345")
  tableWidget.ClickCell("RII", "Nucleolus")
  tableWidget.Keys("0.19")
  lineEdit.SetText("12345")
  tableWidget.ClickCell("RII", "Lipid drolet")
  tableWidget.Keys("0.135")
  lineEdit.SetText("12345")
  tableWidget.qt_scrollarea_viewport.LineEdit.Keys("[Enter]")


@then("Basic Analyzer[T] 탭 Processing mode 패널 table의 네 column에서 모두 Baseline RI 값과 RII값이 ‘{arg}’로 변경되는지 확인")
def step_impl(param1):
  Tables.TableWidget52.Check()



@when("Basic Analyzer[T] 탭 Basic Measurement의 table의 parameter를 설정하고 Basic Measurement 탭의 Execute 버튼을 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 302
  scrollArea.qt_scrollarea_viewport.Widget.CollapseWidget5.contentsFrame.TC_ParameterTable.buttonExecute.ClickButton()



@when("프로세싱 바 팝업이 전부 진행될 때 까지 기다린다.")
def step_impl():
  
  tomoAnalysis = Aliases.TomoAnalysis
  while tomoAnalysis.ProgressDialog.Exists :
   Delay(1000, "Waiting for window to close...");

@then("Basic Analyzer[T] 탭 위에서 프로세싱 바 팝업이 사라지고 Measurement 팝업이 생기는지 확인한다.")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.BaTimeResultPanel, "QtText", cmpEqual, "Measurement")

@then("Basic Analyzer[T] 탭 Basic Measurement 탭의 Execute 버튼의 하이라이트가 해제됐는지 확인")
def step_impl():
  Regions.buttonExecute1.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterControl.buttonExecute)

@then("Basic Analyzer[T] 탭의 Apply Parameter 버튼이 하이라이트 되었는지 확인")
def step_impl():
  Regions.paramBtn.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.paramBtn)


  
@given("Basic Analyzer[T] Single Run 탭에서 Measurement 팝업이 열려 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir = r'E:\Regression_Test\Prefix_File\BAT\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir3 = r'E:\Regression_Test\Prefix_File\BAT\playground\test\Hypercube\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)
  dir4 =r'E:\Regression_Test\Prefix_File\BAT\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\BAT'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BAT'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(155, 29)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BAT\\BAT.tcpro")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_gray.Click(122, 12)
  Aliases.TomoAnalysis.widget_popup.scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget.panel_contents_image.widget.panel_contents_image.Widget.DblClick(76, 93)


  groupBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup
  groupBox.customTime.ClickButton(cbChecked)
  lineEdit = groupBox.timePoints
  lineEdit.Drag(273, 8, -208, 14)
  lineEdit.SetText("1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,1")
  lineEdit.Drag(246, 12, -195, 10)
  lineEdit.SetText("1,2")
  tomoAnalysis_BasicAnalysisTime_Plugins_ParameterPanel = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel
  groupBox = tomoAnalysis_BasicAnalysisTime_Plugins_ParameterPanel.timeGroup
  groupBox.Click(378, 207)
  groupBox.applyBtn.ClickButton()
  tomoAnalysis_BasicAnalysisTime_Plugins_ParameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterControl.buttonExecute.ClickButton()
  tomoAnalysis = Aliases.TomoAnalysis
  while tomoAnalysis.ProgressDialog.Exists :
   Delay(1000, "Waiting for window to close...");

  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.buttonExecute.ClickButton()

  tomoAnalysis = Aliases.TomoAnalysis
  while tomoAnalysis.ProgressDialog.Exists :
   Delay(1000, "Waiting for window to close...");
   
@when("Basic Analyzer[T] 탭의 Hide Result 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.resultBtn.ClickButton()

@then("Basic Analyzer[T] 탭의 Hide Result 버튼이 Show Result 버튼으로 바뀌었는지 확인")
def step_impl():
  Regions.resultBtn1.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.resultBtn)


  
@given("주어진 TCF에 대해 위, 아래 Execute 버튼을 모두 눌러 작업을 수행한 BAT Single Run 탭이 열려 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir = r'E:\Regression_Test\Prefix_File\BAT\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir3 = r'E:\Regression_Test\Prefix_File\BAT\playground\test\Hypercube\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)
  dir4 =r'E:\Regression_Test\Prefix_File\BAT\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\BAT'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BAT'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(155, 29)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BAT\\BAT.tcpro")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_gray.Click(122, 12)
  Aliases.TomoAnalysis.widget_popup.scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget.panel_contents_image.widget.panel_contents_image.Widget.DblClick(76, 93)


  groupBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup
  groupBox.customTime.ClickButton(cbChecked)
  lineEdit = groupBox.timePoints
  lineEdit.Drag(273, 8, -208, 14)
  lineEdit.SetText("1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,1")
  lineEdit.Drag(246, 12, -195, 10)
  lineEdit.SetText("1,2")
  tomoAnalysis_BasicAnalysisTime_Plugins_ParameterPanel = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel
  groupBox = tomoAnalysis_BasicAnalysisTime_Plugins_ParameterPanel.timeGroup
  groupBox.Click(378, 207)
  groupBox.applyBtn.ClickButton()
  tomoAnalysis_BasicAnalysisTime_Plugins_ParameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterControl.buttonExecute.ClickButton()
  tomoAnalysis = Aliases.TomoAnalysis
  while tomoAnalysis.ProgressDialog.Exists :
   Delay(1000, "Waiting for window to close...");

  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.buttonExecute.ClickButton()

  tomoAnalysis = Aliases.TomoAnalysis
  while tomoAnalysis.ProgressDialog.Exists :
   Delay(1000, "Waiting for window to close...");

@given("Basic Analyzer[T] 탭 위에 Measurement 팝업이 열려 있지 않다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.resultBtn.ClickButton()

@when("Basic Analyzer[T] 탭의 Show Result 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.resultBtn.ClickButton()

@then("Basic Analyzer[T] 탭 위에 Measurement 팝업이 생기는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.BaTimeResultPanel, "QtText", cmpEqual, "Measurement")

@then("Basic Analyzer[T] 탭의 Show Result 버튼이 Hide Result 버튼으로 바뀌었는지 확인")
def step_impl():
  Regions.buttonExecute2.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterTable.buttonExecute)
  Aliases.TomoAnalysis.BaTimeResultPanel.Close()


  
@then("Basic Analyzer[T] 탭 위의 Measuerment 팝업의 각 row가 하이라이트 된 색이 cell mask 색과 동일한지 확인")
def step_impl():
  Regions.qt_scrollarea_viewport53.Check(Aliases.TomoAnalysis.BaTimeResultPanel.tableWidget.QtObject("qt_scrollarea_viewport"))
  Regions.TomoAnalysis63.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel.Splitter.Widget2.TomoAnalysis_BasicAnalysisTime_Plugins_BaRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis)
  Aliases.TomoAnalysis.BaTimeResultPanel.Close()


  
@when("Basic Analyzer[T] 탭 위의 Measurement 팝업 테이블의 셀 하나에 마우스 포인터를 가져가서 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.BaTimeResultPanel
  tableWidget = Aliases.TomoAnalysis.Item.Item
  tableWidget.ClickCell("1", "Time Point")
  tableWidget.ClickCell("2", "Time Point")
  tableWidget.ClickCell("3", "Time Point")
  tableWidget.ClickCell("4", "Time Point")

@then("클릭한 Basic Analyzer[T] 탭 위 Measurement 팝업 테이블 셀이 포함된 row 전체가 파란색으로 하이라이트 됨을 확인")
def step_impl():
  Regions.qt_scrollarea_viewport54.Check(Aliases.TomoAnalysis.BaTimeResultPanel.tableWidget.qt_scrollarea_viewport)

  
@when("Basic Analyzer[T] 탭 위 Measurement 팝업의 Export 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.BaTimeResultPanel.csvBtn.ClickButton()

@when("저장 경로와 이름을 지정하고 저장 버튼을 누른다.")
def step_impl():
  dlgSaveFile = Aliases.TomoAnalysis.dlgSaveFile
  HWNDView = dlgSaveFile.DUIViewWndClassName.Item
  tv_ = HWNDView.CtrlNotifySink.NamespaceTreeControl.tv_
  tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  tv_.DblClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  tv_.ExpandItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  HWNDView.CtrlNotifySink2.ShellView.Item.Regression_Test.Item.DblClick(36, 10)
  comboBox = HWNDView.FloatNotifySink.ComboBox
  comboBox.Edit.Click(247, 5)
  comboBox.SetText("test.csv")
  dlgSaveFile.btn_S.ClickButton()

@then("지정한 경로에 .csv 파일이 생성됐는지 확인한다.")
def step_impl():
  explorer = Aliases.explorer
  explorer.wndShell_TrayWnd.ReBarWindow32.MSTaskSwWClass.MSTaskListWClass.Click(35, 15)
  HWNDView = explorer.wndGenerated_File.Generated_File.DUIViewWndClassName.Item
  tv_ = HWNDView.CtrlNotifySink2.NamespaceTreeControl.tv_
  tv_.DblClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  tv_.ExpandItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  HWNDView.CtrlNotifySink.ShellView.Item.Regression_Test.Item.DblClick(64, 17)
  aqObject.CheckProperty(Aliases.explorer.wndGenerated_File.Generated_File.DUIViewWndClassName.Item.CtrlNotifySink.ShellView.Item.test_csv.Item, "Value", cmpEqual, "test.csv")


@then("생성된 .csv 파일의 데이터가 Measurement 탭의 데이터와 내용이 일치하는지 확인한다.")
def step_impl():
  raise NotImplementedError

@given("Basic Analyzer[T] Single Run 탭의 Apply Parameter 버튼이 하이라이트 되어 있다.")
def step_impl():
  raise NotImplementedError

@when("Basic Analyzer[T] Single Run 탭의 Apply Parameter 버튼을 누른다.")
def step_impl():
  raise NotImplementedError

@then("Basic Analyzer[T] Single Run 탭의 Processing mode 패널과 Project Manager 탭의 BAT Application Parameter 패널의 두 Basic Measurement 값이 일치하는지 확인")
def step_impl():
  raise NotImplementedError

@then("화면에서 Basic Analyzer[T] Single Run 탭이 Project Manager 탭으로 넘어갔는지 확인")
def step_impl():
  raise NotImplementedError

@then("Application tab 바에서 Basic Analyzer[T] Single Run 탭 이름의 하이라이트가 사라지고 Project Manager 탭 이름에 하이라이트가 생겼는지 확인")
def step_impl():
  raise NotImplementedError

@when("Application tab 바의 Basic Analyzer[T] 탭 이름 옆의 x 버튼을 클릭한다.")
def step_impl():
  raise NotImplementedError

@then("Basic Analyzer[T] Single Run 탭이 종료되는지 확인")
def step_impl():
  raise NotImplementedError

@when("팝업에서 App이 연결된 hypercube 안의 Time-lapse TCF 중 이미 Single run 해 본 적이 있는 TCF를 선택한다.")
def step_impl():
  raise NotImplementedError

@then("Basic Analyzer[T]에서 Mask와 Measurement 팝업이 열리는지 확인")
def step_impl():
  raise NotImplementedError

@then("Basic Analyzer[T] 탭 Mask Viewing panel 상단 Mask 툴박스의Opacity 수치 값이 0.60으로 올라갔는지 확인")
def step_impl():
  Regions.qt_spinbox_lineedit2.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.maskOpacity.qt_spinbox_lineedit)

@then("Basic Analyzer[T] 탭 Mask Viewing panel 상단 Mask 툴박스의Opacity 수치 값이 0.60으로 올라갔는지 확인한다.")
def step_impl():
  Regions.qt_spinbox_lineedit3.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.maskOpacity.qt_spinbox_lineedit)

@then("Basic Analyzer[T] Mask Viewing panel의 마스크들이 진해짐을 확인한다.")
def step_impl():
  raise NotImplementedError

@then("Basic Analyzer[T] Mask Viewing panel의 마스크들이 투명해짐을 확인한다.")
def step_impl():
  Regions.TomoAnalysis59.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel.Splitter.Widget2.TomoAnalysis_BasicAnalysisTime_Plugins_BaRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis)



@when("Basic Analyzer[T] 탭의 Movie player 패널에서 setting한 time points 중 하나로 넘어간다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.playerPanel.progressSlider.wPosition = 1

@then("새로 생긴 Basic Analyzer[T] 탭 Measurement 팝업의 table의 Time Point column 값이 원래 열려 있던 Measurement 팝업의 table의 Time Point column 값과 다른지 확인")
def step_impl():
  Aliases.explorer.wndShell_TrayWnd.ReBarWindow32.MSTaskSwWClass.MSTaskListWClass.Click(134, 15)
  Aliases.TomoAnalysis.BaTimeResultPanel.Activate()
  Tables.tableWidget83.Check()


@when("Batch Run setting 탭의 Time points setting 패널에서 start 칸 옆의 아래 화살표 버튼을 한 번 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.timeGroup.startTime.wValue = 2

@then("Mask Viewing panel의 3D 화면에 네가지 mask가 모두 보이는지 확인")
def step_impl():
  Regions.viewerPanel5.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel, False, False, 10000)

@when("Basic Analyzer[T] 탭의 Tool box에서 Hide Mask 체크박스를 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.vizToggle.ClickButton(cbChecked)

@then("Mask Viewing panel 2D화면과 3D 화면에 마스크가 보이지 않는지 확인")
def step_impl():
  Regions.viewerPanel6.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel)

@when("Hide Mask 체크박스를 선택 해제한 뒤, Nucleus, Nucleolus, Lipid droplet 체크박스를 선택 해제한다.")
def step_impl():
  tomoAnalysis_BasicAnalysisTime_Plugins_VizControlPanel = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel
  tomoAnalysis_BasicAnalysisTime_Plugins_VizControlPanel.vizToggle.Click(-1)
  widget = tomoAnalysis_BasicAnalysisTime_Plugins_VizControlPanel.controlSocket
  widget.CheckBox9.ClickButton(cbUnchecked)
  widget.CheckBox7.ClickButton(cbUnchecked)
  widget.CheckBox8.Click(14, 7)

@then("Basic Analyzer[T] 탭의 Mask Viewing panel 3D 화면에 Whole Cell 마스크만 남아있는지 확인")
def step_impl():
  Regions.viewerPanel7.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel, False, False, 1000, 25)
