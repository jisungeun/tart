﻿import shutil
import os.path
from distutils.dir_util import copy_tree
import time
import glob
  
@given("3개의 cube에 대해 2D Image Gallery 탭이 열려 있다.")
def step_impl():

  dir = r'E:\Regression_Test\Prefix_File\BAT\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir3 = r'E:\Regression_Test\Prefix_File\BAT\playground\test\Hypercube\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)
  dir4 =r'E:\Regression_Test\Prefix_File\BAT\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\BAT'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BAT'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(50, 14)
  dlgOpenProject = Aliases.TomoAnalysis.dlgOpenProject
  toolbar = dlgOpenProject.WorkerW.ReBarWindow32.AddressBandRoot.progress.BreadcrumbParent.toolbar
  toolbar.ClickItem("‎Prefix_File", False)
  toolbar.ClickItemXY("‎Prefix_File", 33, 8, False)
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\2D_Image_Gallery\\2D_Image_Gallery.tcpro")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_primary.Click(136, 23)

@when("2D Image Gallery 탭 오른쪽의 스크롤 바를 마우스로 클릭한 상태로 내린다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget2.panel_base.scrollarea_bottom_round.qt_scrollarea_vcontainer.ScrollBar.wPosition = 434

@then("2D Image Gallery 탭 화면이 내려갔는지 확인")
def step_impl():
  Regions.h7.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget2.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents.CubeTCFWidget.panel_contents.h7)

@when("2D Image Gallery 탭 오른쪽의 스크롤 바를 마우스로 클릭한 상태로 올린다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget2.panel_base.scrollarea_bottom_round.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0

@then("2D Image Gallery 탭 화면이 올라갔는지 확인")
def step_impl():
  Regions.h71.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget2.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents.CubeTCFWidget2.panel_contents.h7)

@when("2D Image Gallery 탭 화면에서 마우스 휠을 아래로 한 칸 내린다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget2.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents.MouseWheel(-1)

@when("2D Image Gallery 탭 화면에서 마우스 휠을 위로 한 칸 올린다.")
def step_impl():
  widget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget2.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents
  widget.MouseWheel(1)

@given("2D Image Gallery 탭이 열려 있다.")
def step_impl():

  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(50, 14)
  dlgOpenProject = Aliases.TomoAnalysis.dlgOpenProject
  toolbar = dlgOpenProject.WorkerW.ReBarWindow32.AddressBandRoot.progress.BreadcrumbParent.toolbar
  toolbar.ClickItem("‎Prefix_File", False)
  toolbar.ClickItemXY("‎Prefix_File", 33, 8, False)
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\2D_Image_Gallery\\2D_Image_Gallery.tcpro")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_primary.Click(136, 23)

@when("2D Image Gallery 탭의 Rep 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.panel_base.toolWidget.bt_round_tool.ClickButton()

@then("2D Image Gallery 탭의 오른쪽 하단에 ‘There is no selected item.’ 이라는 안내문이 나타났는지 확인")
def step_impl():
  if Aliases.TomoAnalysis.TC_ToastMessageBox.Exists :
    Log.Message("안내문 등장")
  else :
    Log.Error("안내문 안보임")

@then("TA종료")
def step_impl():
  aqUtils.Delay(3000)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)

@given("Basic Analyzer[T] 탭에서 RI Thresholding이 프로세서로 선택되어 있다.")
def step_impl():

  dir = r'E:\Regression_Test\Prefix_File\BAT\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir3 = r'E:\Regression_Test\Prefix_File\BAT\playground\test\Hypercube\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)
  dir4 =r'E:\Regression_Test\Prefix_File\BAT\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\BAT'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BAT'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(37, 30)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.WorkerW.ReBarWindow32.AddressBandRoot.progress.BreadcrumbParent.toolbar.ClickItemXY("‎Prefix_File", 15, 13, False)
  tomoAnalysis = Aliases.TomoAnalysis
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  UIItem = dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink2.ShellView.Item.BAT
  UIItem.Item.Click(84, 8)
  UIItem.Keys("![ReleaseLast][Enter]")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BAT\\BAT.tcpro")
  tomoAnalysis_ProjectManager_AppUI_MainWindow = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  tomoAnalysis_ProjectManager_AppUI_MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(156, 116)
  frame = tomoAnalysis_ProjectManager_AppUI_MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base
  frame.panel_contents.processorParamFrame.procCombo.ClickItem("RI Thresholding")
  frame.bt_square_gray.Click(61, 6)
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel = tomoAnalysis.widget_popup
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget.panel_contents_image.widget.panel_contents_image.Widget.Click(99, 76)
  widget = Aliases.TomoAnalysis.widget_popup.scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget.panel_contents_image
  widget.DblClick(155, 110)

@when("Basic Analyzer[T] 탭에서 UL Threshold 탭의 Execute 버튼을 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.buttonExecute.ClickButton()

@then("Basic Analyzer[T] Single Run 탭의 Mask Viewing panel 3D 화면과 2D 화면에 RI 마스크가 생성되었는지 확인")
def step_impl():
  Regions.viewerPanel1.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel)

@then("Basic Analyzer[T] 탭 Mask Viewing panel에 생성된 3D 마스크와 정육면체가 회전하는지 확인")
def step_impl():
  Regions.TomoAnalysis145.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel.Splitter.Widget.TomoAnalysis_BasicAnalysisTime_Plugins_BaRenderWindow3D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis, False, False, 35811, 17)

@when("3D 마스크 캔버스 좌하단 정육면체의 위쪽 변을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel.Splitter.Widget.TomoAnalysis_BasicAnalysisTime_Plugins_BaRenderWindow3D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis
  tomoAnalysis.Click(76, 629)
  tomoAnalysis.Click(108, 663)

@then("해당 변이 정면으로 오도록 정육면체와 마스크의 방향이 움직였는지 확인")
def step_impl():
  Regions.TomoAnalysis146.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel.Splitter.Widget.TomoAnalysis_BasicAnalysisTime_Plugins_BaRenderWindow3D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis, False, False, 35811, 17)

@when("3D 마스크 캔버스 좌하단 정육면체의 Left면을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel.Splitter.Widget.TomoAnalysis_BasicAnalysisTime_Plugins_BaRenderWindow3D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis.Click(35, 669)
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel.Splitter.Widget.TomoAnalysis_BasicAnalysisTime_Plugins_BaRenderWindow3D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis.Click(115, 675)

@then("Left 면이 정면으로 오도록 정육면체와 마스크의 방향이 움직였는지 확인")
def step_impl():
  Regions.TomoAnalysis148.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel.Splitter.Widget.TomoAnalysis_BasicAnalysisTime_Plugins_BaRenderWindow3D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis, False, False, 35811, 17)


@then("해당 점이 정면으로 오도록 정육면체와 마스크의 방향이 움직였는지 확인")
def step_impl():
  Regions.TomoAnalysis147.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel.Splitter.Widget.TomoAnalysis_BasicAnalysisTime_Plugins_BaRenderWindow3D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis, False, False, 35811, 17)

@then("Basic Analyzer[T] 탭 위의 Measurement 팝업이 사라졌는지 확인")
def step_impl():
  raise NotImplementedError

@when("파일 탐색기 팝업에서 취소 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.dlgSaveFile.btn_.ClickButton()

@given("Basic Analyzer[T] 탭 Auto threshold 탭의 Auto lower threshold 체크박스가 체크되어 있지 않다.")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.TC_StrCheckBox, "checked", cmpEqual, False)

@when("Basic Analyzer[T] 탭 Auto threshold 탭의 Auto lower Threshold 체크박스를 클릭한다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.TC_StrCheckBox.Click(7, 10)

@then("Basic Analyzer[T] 탭 Auto Thresholde 탭에 ‘Select Algorithm’ 문구와 알고리즘 드롭다운 버튼, Execute 버튼이 나타나는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.Label, "QtText", cmpEqual, "Select Automatic Threshold Algorithm")
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.StrComboBox.strCombo, "wText", cmpEqual, "Otsu")
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl.buttonExecute, "QtText", cmpEqual, "Execute")

@when("Basic Analyzer[T] 탭 Auto Threshold 탭의 Select Algorithm 드롭다운 버튼에서 Entropy 알고리즘을 선택하고 Auto Threshold 탭의 Execute 버튼을 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  TC_ParameterControl = scrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl
  TC_ParameterControl.StrComboBox.strCombo.ClickItem("Entropy")
  scrollBar.wPosition = 0
  TC_ParameterControl.buttonExecute.ClickButton()

@then("Basic Analyzer[T] 탭의 UL Threshold 탭에서 값과 사이즈 바가 Entropy 알고리즘에 의해 자동으로 바뀌었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "1.3417")
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.ScalarUi2.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "2.0000")

@when("Basic Analyzer[T] 탭 Auto Threshold 탭의 Select Algorithm 드롭다운 버튼에서 Moment Preserving 알고리즘을 선택하고 Auto Threshold 탭의 Execute 버튼을 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  TC_ParameterControl = scrollArea.qt_scrollarea_viewport.Widget.CollapseWidget3.contentsFrame.TC_ParameterControl
  TC_ParameterControl.StrComboBox.strCombo.ClickItem("Moment Preserving")
  scrollBar.wPosition = 0
  TC_ParameterControl.buttonExecute.ClickButton()

@then("Basic Analyzer[T] 탭의 UL Threshold 탭에서 값과 사이즈 바가 Moment Preserving 알고리즘에 의해 자동으로 바뀌었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "1.3517")
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.ScalarUi.valueSlider, "wPosition", cmpEqual, 351)

@given("Basic Analyzer[T] 탭에서 Use Labeling 체크박스가 체크되어 있다.")
def step_impl():

  dir = r'E:\Regression_Test\Prefix_File\BAT\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir3 = r'E:\Regression_Test\Prefix_File\BAT\playground\test\Hypercube\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)
  dir4 =r'E:\Regression_Test\Prefix_File\BAT\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\BAT'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BAT'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(37, 30)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.WorkerW.ReBarWindow32.AddressBandRoot.progress.BreadcrumbParent.toolbar.ClickItemXY("‎Prefix_File", 15, 13, False)
  tomoAnalysis = Aliases.TomoAnalysis
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  UIItem = dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink2.ShellView.Item.BAT
  UIItem.Item.Click(84, 8)
  UIItem.Keys("![ReleaseLast][Enter]")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BAT\\BAT.tcpro")
  tomoAnalysis_ProjectManager_AppUI_MainWindow = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  tomoAnalysis_ProjectManager_AppUI_MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(156, 116)
  frame = tomoAnalysis_ProjectManager_AppUI_MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base
  frame.panel_contents.processorParamFrame.procCombo.ClickItem("RI Thresholding")
  frame.bt_square_gray.Click(61, 6)
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel = tomoAnalysis.widget_popup
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget.panel_contents_image.widget.panel_contents_image.Widget.Click(99, 76)
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.bt_square_primary.Click(82, 15)
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 97
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterTable.TC_StrCheckBox.Click(4, 11)


@given("Basic Analyzer[T] 탭의 Mask Viewing panel에 마스크가 생성되어 있다.")
def step_impl():
  spinBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.playerPanel.progressSpinBox
  spinBox.qt_spinbox_lineedit.Drag(41, 13, -213, 41)
  spinBox.Keys("[BS]3[Enter]")
  spinBox.wValue = 3
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.buttonExecute.ClickButton()


@when("Basic Analyzer[T] 탭 Labeling 탭의 Select Option 드롭다운 버튼에서 Largest Label을 선택하고 Labeling 탭의 Execute 버튼을 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  TC_ParameterControl = scrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterControl2
  TC_ParameterControl.StrComboBox.strCombo.ClickItem("Largest Label")
  scrollBar.wPosition = 366
  TC_ParameterControl.buttonExecute.ClickButton()

@then("Basic Analyzer[T] 탭의 Mask Viewing 패널에서 가장 큰 크기의 마스크만 남는 방식으로 마스크가 수정되었는지 확인")
def step_impl():
  Regions.TomoAnalysis150.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel.Splitter.Widget.TomoAnalysis_BasicAnalysisTime_Plugins_BaRenderWindow3D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis)

@given("Basic Analyzer[T] 탭의 RI Thresholding 모드에서 Mask Viewing panel에 마스크가 생성되어 있다.")
def step_impl():

  dir = r'E:\Regression_Test\Prefix_File\BAT\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir3 = r'E:\Regression_Test\Prefix_File\BAT\playground\test\Hypercube\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)
  dir4 =r'E:\Regression_Test\Prefix_File\BAT\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\BAT'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BAT'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(37, 30)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.WorkerW.ReBarWindow32.AddressBandRoot.progress.BreadcrumbParent.toolbar.ClickItemXY("‎Prefix_File", 15, 13, False)
  tomoAnalysis = Aliases.TomoAnalysis
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  UIItem = dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink2.ShellView.Item.BAT
  UIItem.Item.Click(84, 8)
  UIItem.Keys("![ReleaseLast][Enter]")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BAT\\BAT.tcpro")
  tomoAnalysis_ProjectManager_AppUI_MainWindow = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  tomoAnalysis_ProjectManager_AppUI_MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(156, 116)
  frame = tomoAnalysis_ProjectManager_AppUI_MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base
  frame.panel_contents.processorParamFrame.procCombo.ClickItem("RI Thresholding")
  frame.bt_square_gray.Click(61, 6)
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel = tomoAnalysis.widget_popup
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget.panel_contents_image.widget.panel_contents_image.Widget.Click(99, 76)
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.bt_square_primary.Click(82, 15)
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.buttonExecute.ClickButton()


@when("Basic Analyzer[T] 탭의 Movie Player 패널에서 time point를 한 포인트 위로 이동한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.playerPanel.progressSpinBox.wValue = 2

@then("Basic Analyzer[T] 탭의 Mask Viewing panel에서 마스크가 사라지는지 확인")
def step_impl():
  Regions.TomoAnalysis151.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel.Splitter.Widget.TomoAnalysis_BasicAnalysisTime_Plugins_BaRenderWindow3D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis)

@when("Basic Analyzer[T] 탭의 Movie Player 패널에서 time point를 다시 한 포인트 아래로 이동한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.playerPanel.progressSpinBox.wValue = 1

@then("Basic Analyzer[T] 탭의 Mask Viewing panel에서 마스크가 나타나지 않는지 확인")
def step_impl():
  Regions.TomoAnalysis152.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel.Splitter.Widget.TomoAnalysis_BasicAnalysisTime_Plugins_BaRenderWindow3D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis)

@when("3D 마스크 캔버스 좌하단 정육면체의 Left 면의 오른쪽 위 점을 클릭.")
def step_impl():
  raise NotImplementedError

@when("3D 마스크 캔버스 정육면체의 Left 면의 오른쪽 위 점을 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel.Splitter.Widget.TomoAnalysis_BasicAnalysisTime_Plugins_BaRenderWindow3D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis.Click(116, 629)

@given("Basic Analyzer[T] Single Run 탭에 RAW LPS-24h-002P011 데이터의 RI 마스크가 열려 있다.")
def step_impl():

  dir = r'E:\Regression_Test\Prefix_File\BA_borderkill\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir3 = r'E:\Regression_Test\Prefix_File\BA_borderkill\playground\test\Hypercube\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)
  dir4 =r'E:\Regression_Test\Prefix_File\BA_borderkill\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\BA_borderkill'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BA_borderkill'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis_ProjectManager_AppUI_MainWindow = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  tomoAnalysis_ProjectManager_AppUI_MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(61, 18)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  HWNDView = dlgOpenProject.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  UIItemsView = HWNDView.CtrlNotifySink2.ShellView.Item
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BA_borderkill\\BA_borderkill.tcpro")
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis_ProjectManager_AppUI_MainWindow = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  frame = tomoAnalysis_ProjectManager_AppUI_MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base
  frame.frame_tab.bt_round_tab3.ClickButton()
  tomoAnalysis_ProjectManager_Plugins_CubePanel = frame.widget_panel.CubePanel
  lineEdit = tomoAnalysis_ProjectManager_Plugins_CubePanel.input_high
  lineEdit.Click(124, 23)
  lineEdit.SetText("CB")
  tomoAnalysis_ProjectManager_Plugins_CubePanel.bt_round_operation.ClickButton()
  widget = tomoAnalysis_ProjectManager_AppUI_MainWindow.centralwidget.mainSplitter
  widget2 = widget.operationFrame.operationSplitter.previewPanel.panel_base
  widget2.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget3.TC_TCF2DWidget.panel_contents_image.Widget.Click(153, 85)
  widget2.bt_round_tool2.Click(45, 14)
  tomoAnalysis_ProjectManager_Plugins_LinkDialog = tomoAnalysis.LinkDialog
  tomoAnalysis_ProjectManager_Plugins_LinkDialog.dropdown_high.ClickItem("CB")
  tomoAnalysis_ProjectManager_Plugins_LinkDialog.bt_square_primary.ClickButton()
  frame = widget.playgroundFrame.playgroundSplitter.parameterPanel.panel_base
  frame.panel_contents.processorParamFrame.procCombo.ClickItem("RI Thresholding")
  frame.Click(61, 414)
  frame.bt_square_gray.Click(54, 7)
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel = tomoAnalysis.widget_popup
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget.panel_contents_image.widget.panel_contents_image.Widget.Click(106, 64)
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.bt_square_primary.Click(111, 28)
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.buttonExecute.ClickButton()


@when("Basic Analyzer[T] 탭 Labeling 탭에서 Use Labeling을 체크하고 Option을 Multi Labels로 선택한 뒤 Labeling 탭의 Execute 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  scrollArea = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  TC_ParameterControl = scrollArea.qt_scrollarea_viewport.Widget.CollapseWidget2.contentsFrame.TC_ParameterControl
  TC_ParameterControl.TC_StrCheckBox2.Click(10, 10)
  scrollBar.wPosition = 0
  TC_ParameterControl.buttonExecute.ClickButton()

@when("Basic Analyzer[T] 탭 Border Kill 탭에서 Use Border Kill을 체크하고 Border Kill 탭의 Execute를 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 325
  TC_ParameterControl = scrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl
  TC_ParameterControl.TC_StrCheckBox.Click(57, 16)
  scrollBar.wPosition = 325
  TC_ParameterControl.buttonExecute.ClickButton()

@then("Basic Analyzer[T] 탭 Mask Viewing panel에서 가장자리 세포의 마스크가 사라졌는지 확인")
def step_impl():
  Regions.TomoAnalysis208.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel)

@given("BAT Application Parameter패널이 열려 있다.")
def step_impl():

  dir = r'E:\Regression_Test\Prefix_File\BAT\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir3 = r'E:\Regression_Test\Prefix_File\BAT\playground\test\Hypercube\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)
  dir4 =r'E:\Regression_Test\Prefix_File\BAT\playground'
  if(os.path.isdir(dir4)):
    shutil.rmtree(dir4)
    os.makedirs(dir4)
  dir1 = r'E:\Regression_Test\Prefix_File\BAT'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BAT'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(37, 30)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.WorkerW.ReBarWindow32.AddressBandRoot.progress.BreadcrumbParent.toolbar.ClickItemXY("‎Prefix_File", 15, 13, False)
  tomoAnalysis = Aliases.TomoAnalysis
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  UIItem = dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink2.ShellView.Item.BAT
  UIItem.Item.Click(84, 8)
  UIItem.Keys("![ReleaseLast][Enter]")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BAT\\BAT.tcpro")


@when("BAT Application Parameter 패널에서 프로세서로 RI Thresholding을 선택하고 Assign lower threshold에 1을 입력한다.")
def step_impl():
  frame = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.panel_contents.processorParamFrame
  frame.procCombo.ClickItem("RI Thresholding")
  scrollArea = frame.scrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  scrollBar.wPosition = 0
  doubleSpinBox = scrollArea.qt_scrollarea_viewport.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget4.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin
  doubleSpinBox.qt_spinbox_lineedit.Drag(45, 13, -73, 1)
  doubleSpinBox.Keys("1")
  doubleSpinBox.wValue = 1
  doubleSpinBox.Keys("[Enter]")
  doubleSpinBox.wValue = 1

@when("BAT Application Parameter 패널의 Single RUN 버튼을 누르고 팝업에서 데이터를 선택한다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_gray.Click(101, 25)
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel = tomoAnalysis.widget_popup
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget.panel_contents_image.widget.panel_contents_image.Widget.Click(134, 82)
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.bt_square_primary.Click(104, 28)

@then("Basic Analyzer[T] 탭의 Processing mode 패널에서 Assign lower threshold 값이 1.0000인지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.ScalarUi.valueSpin.qt_spinbox_lineedit, "wText", cmpEqual, "1.0000")

@when("Basic Analyzer[T] 탭 Processing mode 패널의 제일 위 드롭다운 버튼에서 AI Segmentation을 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.procCombo.ClickItem("AI Segmentation")

@when("Basic Analyzer[T] 탭 Processing mode 패널의 제일 위 드롭다운 버튼에서 RI Threholding을 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.procCombo.ClickItem("RI Thresholding")

@then("Basic Analyzer[T] 탭 Single run 탭의 Processing mode 패널에 RI Thresholding 모드의 UI가 나타남을 확인")
def step_impl():
  Regions.groupBox12.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox)

@then("Basic Analyzer[T]에서 Time points setting 패널이 없고 Notice 패널이 나타나는지 확인")
def step_impl():
  Regions.noticeGroup.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.noticeGroup)
