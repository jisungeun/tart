﻿import shutil
import os.path
from distutils.dir_util import copy_tree
import time
import glob
import re
import os

@given("“F23RH“프로젝트가 열려있다.")
def step_impl():


  dir1 = r'E:\Regression_Test\Prefix_File\F23RH'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\F23RH'
  copy_tree(dir2,dir1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(47, 44)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.WorkerW.ReBarWindow32.AddressBandRoot.progress.BreadcrumbParent.toolbar.ClickItemXY("‎Prefix_File", 28, 8, False)

  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\F23RH\\F23RH.tcpro")

@when("Toolbox의 Report history 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.bt_round_tool5.Click(97, 19)

@then("Result History 팝업이 열렸는지 확인")
def step_impl():
  raise NotImplementedError

@when("Result History 팝업 왼쪽의 리스트에서 선택되어있지 않은 다른 리포트를 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.widget_popup.scrollArea.qt_scrollarea_viewport.Widget.resultTable.ClickCell("2", "Processing mode")

@then("Result History 팝업의 Playground canvas와 Application Parameter가 변경되었는지 확인")
def step_impl():
  Regions.F23_1.Check(Aliases.TomoAnalysis.widget_popup.playground.panel_base.scrollarea_bottom_round.qt_scrollarea_viewport.panel_contents.TomoAnalysis_ProjectManager_Plugins_DragIconWindow.TomoAnalysis_ProjectManager_Plugins_DraggableIcon)

@when("Result History 팝업의 Playground canvas에 마우스를 가져가서 마우스 휠을 아래로 내린다.")
def step_impl():
  raise NotImplementedError

@then("Result History 팝업의 Playground canvas 스크롤이 아래로 내려가는지 확인")
def step_impl():
  raise NotImplementedError

@when("Result History 팝업의 Playground canvas 스크롤 바를 마우스로 클릭한 채로 위로 드래그 한다.")
def step_impl():
  raise NotImplementedError

@then("Result History 팝업의 Playground canvas 스크롤이 위로 올라가는지 확인")
def step_impl():
  raise NotImplementedError

@when("Result History 팝업의 Load Playground 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.widget_popup.bt_square_gray.ClickButton()

@then("Result History 팝업 왼쪽의 리스트에서 선택한 Playground가 Project Manager 탭의 Playground canvas 패널에 불러졌는지 확인")
def step_impl():
  Aliases.TomoAnalysis.widget_popup.Minimize()

@when("Result History 팝업 왼쪽의 리스트에서 Report 탭을 불러오고 싶은 리포트를 선택한다.")
def step_impl():
  raise NotImplementedError

@when("Result History 팝업의 Open Report App 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.widget_popup.bt_square_gray2.ClickButton()
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.Click(614, 24)

@then("TA에 선택한 Report 탭이 나타났는지 확인")
def step_impl():
  Regions.report.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget)

@when("Result History 팝업 왼쪽의 리스트에서 Report[T] 탭을 불러오고 싶은 리포트를 선택한다.")
def step_impl():
  raise NotImplementedError

@then("TA에 선택한 Report[T] 탭이 나타났는지 확인")
def step_impl():
  raise NotImplementedError

@when("Project Explorer Panel의 삭제하고 싶은 TCF 폴더에 마우스 포인터를 대고 우클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.explorerFrame.explorerSplitter.explorerPanel.panel_base.panel_contents.qt_scrollarea_viewport.ClickR(137, 102)

@when("Remove TCF 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.explorerFrame.explorerSplitter.explorerPanel.panel_base.panel_contents.qt_scrollarea_viewport.ClickR(132, 102)
  tomoAnalysis.Menu.QtMenu.Click("Remove TCF")

@when("Toolbox의 Report history 버튼을 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.bt_round_tool5.Click(58, 21)

@when("Result History 팝업 왼쪽의 리스트에서 선택되어있지 않은 다른 리포트를 선택하고 Delete History 버튼을 클릭한다.")
def step_impl():
  tomoAnalysis_ProjectManager_Plugins_ResultListWidget = Aliases.TomoAnalysis.widget_popup
  tomoAnalysis_ProjectManager_Plugins_ResultListWidget.scrollArea.qt_scrollarea_viewport.Widget.resultTable.ClickCell("9", "Date")
  tomoAnalysis_ProjectManager_Plugins_ResultListWidget.bt_square_gray3.ClickButton()

@then("Result History 팝업의 리스트에서 이전에 선택한 Result history 파일이 삭제되었는지 확인")
def step_impl():
  Tables.resultTable3.Check()

@when("Project Manager 탭에서 Batch Run을 클릭한다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.widget_popup.Close()
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_primary.Click(43, 15)

@when("Report 탭의 오른쪽 그래프에서 막대 아무거나 하나를 클릭하고 Edit Mask 버튼을 누른다.")
def step_impl():
  raise NotImplementedError

@when("Mask Editor 탭의 General 패널에서 Apply to Result 버튼을 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.leftLayout.GeneralPanel.panel_base.panel_contents.SaveLinkBtn.ClickButton()

@when("Delete History 팝업 창에서 Delete 버튼을 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.DeleteDialog.bt_square_primary.ClickButton()

@when("Project Manager 탭의 Tool box에서 Report History를 클릭한다.")
def step_impl():
  TC_AdaptiveTabWidget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget
  TC_AdaptiveTabWidget.TC_TabBar.Click(115, 29)
  TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.bt_round_tool5.Click(38, 20)

@then("Report History 팝업창의 리스트에 History 파일이 있는지 확인")
def step_impl():
  Tables.resultTable6.Check()

@when("Delete History 팝업에 Delete 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.DeleteDialog.bt_square_primary.ClickButton()

@when("Project Manager 탭의  Playground canvas에서 Hypercube1에 연결된 BA 아이콘을 선택하고 Batch Run을 클릭한다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.widget_popup.Close()
  splitter = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.playgroundFrame.playgroundSplitter
  scrollArea = splitter.playgroundPanel.panel_base.scrollarea_bottom_round
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_hcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.panel_contents.TomoAnalysis_ProjectManager_Plugins_DragIconWindow2.TomoAnalysis_ProjectManager_Plugins_DraggableIcon2.Click(45, 53)
  splitter.parameterPanel.panel_base.bt_square_primary.Click(87, 23)
  Aliases.TomoAnalysis.fileChangeForm.bt_square_primary.ClickButton()
  tomoAnalysis = Aliases.TomoAnalysis
  while tomoAnalysis.ProgressDialog.Exists :
   Delay(1000, "Waiting for window to close...");

@when("Report 탭의 오른쪽 그래프에서 막대 아무거나 하나를 클릭하고 Edit Mask 버튼을 누른다..")
def step_impl():
  widget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.widget
  widget.Widget2.TCChartView.Widget.Click(455, 86)
  widget.correctBtn.ClickButton()
