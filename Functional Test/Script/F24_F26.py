﻿import shutil
import os.path
from distutils.dir_util import copy_tree
import time
import glob
@when("Toolbox의 Mask editor 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.bt_round_tool6.Click(35, 11)

@when("General 패널의 Apply to Result 버튼이 비활성화 되었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea.qt_scrollarea_viewport.leftLayout.GeneralPanel.panel_base.panel_contents.SaveLinkBtn, "checkable", cmpEqual, False)
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea.qt_scrollarea_viewport.leftLayout.GeneralPanel.panel_base.panel_contents.SaveLinkBtn, "checked", cmpEqual, False)

@when("Mask selector 패널이 비활성화 되었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.MaskSelectionPanel.panel_base.panel_contents.instBtn, "checkable", cmpEqual, False)
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.MaskSelectionPanel.panel_base.panel_contents.instBtn, "checked", cmpEqual, False)

@when("General 패널의 Switch window 버튼을 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.leftLayout.GeneralPanel.panel_base.panel_contents.WinBtn.ClickButton()

@then("XY Slice 화면과 YZ Slice 화면의 위치가 바뀌었는지 확인")
def step_impl():
  Regions.TomoAnalysis153.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow)

@then("YZ Slice 화면과 XZ Slice 화면의 위치가 바뀌었는지 확인")
def step_impl():
  Regions.RenderWindow10.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow)

@then("가운데로 3D Mask 화면이, 오른쪽은 위부터 XY, YZ, XZ Slice 화면이 오도록 바뀌었는지 확인")
def step_impl():
  Regions.RenderWindow.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow)

@then("3D Mask 화면과 XY Slice 화면의 위치가 바뀌었는지 확인")
def step_impl():
  Regions.RenderWindow1.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow)

@when("Label tools 패널의 Label picker 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  widget = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.LabelControlPanel.panel_base.panel_contents
  pushButton = widget.PickBtn
  pushButton.ClickButton()


@then("“Failed to activate Picking Tool” 경고창이 뜨는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.MessageBox11, "QtText", cmpEqual, "Failed to activate Picking tool")
  aqObject.CheckProperty(Aliases.TomoAnalysis.MessageBox11, "WndCaption", cmpEqual, "Error")
  Aliases.TomoAnalysis.MessageBox11.qt_msgbox_buttonbox.buttonOk.ClickButton()


@when("Drawing tool 패널의 Add 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.DrawingToolPanel.panel_base.panel_contents.AddBtn.ClickButton()

@then("“Failed to activate add tool” 경고창이 뜨는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.MessageBox12.qt_msgbox_label, "QtText", cmpEqual, "Failed to activate add tool")
  Aliases.TomoAnalysis.MessageBox12.qt_msgbox_buttonbox.buttonOk.ClickButton()
  
@when("Clean tool 패널의 Cleaning-2D plane 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.CleanerPanel.panel_base.panel_contents.CleanBtn.ClickButton()

@then("“Failed Clean current slice” 경고창이 뜨는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.MessageBox13, "QtText", cmpEqual, "Failed Clean current slice")
  aqObject.CheckProperty(Aliases.TomoAnalysis.MessageBox13, "WndCaption", cmpEqual, "Error")
  Aliases.TomoAnalysis.MessageBox13.qt_msgbox_buttonbox.buttonOk.ClickButton()

@when("Semi-auto tool 패널의 Divide 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.SemiAutoPanel.panel_base.panel_contents.DividerBtn.ClickButton()

@then("“Failed to activate divide tool” 경고창이 뜨는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.MessageBox14, "QtText", cmpEqual, "Failed to activate divide tool")
  Aliases.TomoAnalysis.MessageBox14.qt_msgbox_buttonbox.buttonOk.ClickButton()
  
@given("Preview, List 패널에서 어떤 TCF도 선택되어 있지 않다.")
def step_impl():
  pass

@then("경고창이 생기며 Mask Editor 탭이 열리지 않는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.MessageBox15, "QtText", cmpEqual, "Select tcf files to perform segmentation")
  Aliases.TomoAnalysis.MessageBox15.qt_msgbox_buttonbox.buttonOk.ClickButton()

@given("Mask Editor 탭이 열려 있다.")
def step_impl():

  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(136, 21)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\Import Data - Set TCF\\Import Data - Set TCF.tcpro")

  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(138, 64)

  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.bt_round_tool6.Click(35, 11)

@when("ID manager 패널의 New 버튼을 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.leftLayout.IDManagerPanel.panel_base.panel_contents.bt_round_gray500.ClickButton()

@when("ID 입력 칸에 아무것도 입력하지 않고 OK 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.NewIdDialog.bt_square_primary.ClickButton()

@then("“Empty or invalid playground name'이라는 경고창이 나타나는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.MessageBox16, "QtText", cmpEqual, "Empty or invalid playground name")
  Aliases.TomoAnalysis.MessageBox16.qt_msgbox_buttonbox.buttonOk.ClickButton()
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.NewIdDialog.bt_square_line.ClickButton()
  tomoAnalysis.MessageBox18.qt_msgbox_buttonbox.buttonOk.ClickButton()

@when("ID 입력 칸에 ‘test’를 입력하고 OK 버튼을 누른다.")
def step_impl():
  tomoAnalysis_InterSeg_Plugins_NewIdDialog = Aliases.TomoAnalysis.NewIdDialog
  lineEdit = tomoAnalysis_InterSeg_Plugins_NewIdDialog.input_high
  lineEdit.Click(128, 18)
  lineEdit.SetText("test")
  tomoAnalysis_InterSeg_Plugins_NewIdDialog.bt_square_primary.ClickButton()

@then("ID manager 패널 드롭다운 버튼에서 test 라는 ID가 생겼는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea.qt_scrollarea_viewport.leftLayout.IDManagerPanel.panel_base.panel_contents.IDCombo, "wText", cmpEqual, "test")

@when("ID manager 패널의 New 버튼을 누르고 팝업에서 ID 입력 칸에 ‘test’를 입력하고 OK 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  scrollArea = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.leftLayout.IDManagerPanel.panel_base.panel_contents.bt_round_gray500.ClickButton()
  tomoAnalysis_InterSeg_Plugins_NewIdDialog = tomoAnalysis.NewIdDialog
  lineEdit = tomoAnalysis_InterSeg_Plugins_NewIdDialog.input_high
  lineEdit.Click(100, 22)
  lineEdit.SetText("test")
  tomoAnalysis_InterSeg_Plugins_NewIdDialog.bt_square_primary.ClickButton()

@then("“ID already exist!” 라는 경고창이 나타나는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.MessageBox17, "QtText", cmpEqual, "ID already exist!")
  Aliases.TomoAnalysis.MessageBox17.qt_msgbox_buttonbox.buttonOk.ClickButton()

@then("ID manager 패널의 드롭다운 버튼에서 ‘test’ ID가 한 개만 존재하는지 확인")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.leftLayout.IDManagerPanel.panel_base.panel_contents.IDCombo.DropDown()
  Regions.ComboBoxListView.Check(Aliases.TomoAnalysis.ComboBoxPrivateContainer.ComboBoxListView)
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.leftLayout.IDManagerPanel.panel_base.h6.Click(351, 14)

@when("ID manager 패널의 New 버튼을 누르고 팝업에서 Cancel 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  scrollArea = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.leftLayout.IDManagerPanel.panel_base.panel_contents.bt_round_gray500.ClickButton()
  tomoAnalysis.NewIdDialog.bt_square_line.ClickButton()

@then("“ID is invalid!” 라는 경고창이 나타나는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.MessageBox18, "QtText", cmpEqual, "ID is invalid!")
  Aliases.TomoAnalysis.MessageBox18.qt_msgbox_buttonbox.buttonOk.ClickButton()

@given("Mask Editor 탭에서 ‘test’ ID가 선택되어 있다.")
def step_impl():

  dir3 = r'E:\Regression_Test\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)
  dir1 = r'E:\Regression_Test\Prefix_File\Import Data - Set TCF'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\Import Data - Set TCF'
  copy_tree(dir2,dir1)
    
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(136, 21)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\Import Data - Set TCF\\Import Data - Set TCF.tcpro")
  
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.frame_tab.bt_round_tab6.ClickButton()

  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.AddTcfPanel2.bt_round_operation.Click(124, 30)
  dlgOpenTcfFolder = tomoAnalysis.dlgOpenTcfFolder
  HWNDView = dlgOpenTcfFolder.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  UIItemsView = HWNDView.CtrlNotifySink2.ShellView.Item
  UIItem = UIItemsView.Regression_Test
  UIItem.Item.Click(129, 5)
  UIItem.Keys("[Enter]")
  UIItem = UIItemsView.TomoAnalysis_test_data
  UIItem.Item.Click(98, 2)
  UIItem.Keys("[Enter]")
  UIItemsView.HT_BF_time_lapse_test_sample_for_SW_team.Item.Click(145, 16)
  dlgOpenTcfFolder.btn_2.ClickButton()
  
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea
  scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(138, 64)

  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.bt_round_tool6.Click(35, 11)
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.leftLayout.IDManagerPanel.panel_base.panel_contents.bt_round_gray500.ClickButton()

  tomoAnalysis_InterSeg_Plugins_NewIdDialog = Aliases.TomoAnalysis.NewIdDialog
  lineEdit = tomoAnalysis_InterSeg_Plugins_NewIdDialog.input_high
  lineEdit.Click(128, 18)
  lineEdit.SetText("test")
  tomoAnalysis_InterSeg_Plugins_NewIdDialog.bt_square_primary.ClickButton()




@when("Mask control 패널 Cell Instance 마스크의 오른쪽 회색 버튼을 클릭한다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.leftLayout.MaskControlPanel.panel_base.panel_contents.instStatus.ClickButton()

@then("Mask control 패널 Cell Instance 오른쪽 버튼이 파란색으로 바뀌었는지 확인")
def step_impl():
  Regions.instStatus.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea.qt_scrollarea_viewport.leftLayout.MaskControlPanel.panel_base.panel_contents.instStatus)

@then("Mask Selector 패널에서 Cell Instance 아이콘이 활성화 되었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.MaskSelectionPanel.panel_base.panel_contents.instBtn, "Enabled", cmpEqual, True)

@when("Mask selector 패널의 Cell Instance 버튼을 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.MaskSelectionPanel.panel_base.panel_contents.instBtn.ClickButton()


@then("Mask selector 패널의 Cell Instance 버튼이 하이라이트 되었는지 확인")
def step_impl():
  raise NotImplementedError

@when("Drawing tool 패널의 Fill 버튼을 누르고 Mask Editor 탭 Cell instance의 XY Slice 화면 영역을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.DrawingToolPanel.panel_base.panel_contents.FillBtn.ClickButton()
  Aliases.TomoAnalysis.mainwindow.TomoAnalysis3.Click(448, 362)

@then("Mask Editor 탭 XY Slice 화면에 Cell instance 마스크가 넓게 채워지는지 확인")
def step_impl():
  Regions.TomoAnalysis154.Check(Aliases.TomoAnalysis.mainwindow.TomoAnalysis3)

@when("Drawing tool 패널의 Erase 버튼을 클릭하고 Mask Editor 탭 중앙 XY Slice 화면의 Cell instance 마스크를 클릭한다.")
def step_impl():
  TC_AdaptiveTabWidget = Aliases.TomoAnalysis.mainwindow
  TC_AdaptiveTabWidget.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.DrawingToolPanel.panel_base.panel_contents.EraseBtn.ClickButton()
  TC_AdaptiveTabWidget.TomoAnalysis3.Click(459, 339)

@then("Mask Editor 탭에서 클릭한 Cell instance 마스크가 사라졌는지 확인")
def step_impl():
  Regions.TomoAnalysis155.Check(Aliases.TomoAnalysis.mainwindow.TomoAnalysis3)

@when("Mask control 패널 Whole cell 마스크의 오른쪽 회색 버튼을 클릭한다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_hcontainer.ScrollBar.wPosition = 53
  scrollArea.qt_scrollarea_viewport.leftLayout.MaskControlPanel.panel_base.panel_contents.memStatus.ClickButton()

@when("Mask selector 패널의 Whole cell 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.MaskSelectionPanel.panel_base.panel_contents.memBtn.ClickButton()

@when("Drawing tool 패널의 Fill 버튼을 누르고 Mask Editor 탭 Whole cell의 XY Slice 화면 영역을 누른다.")
def step_impl():
  tomoAnalysis_mainwindow = Aliases.TomoAnalysis.mainwindow
  tomoAnalysis_mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.DrawingToolPanel.panel_base.panel_contents.FillBtn.ClickButton()
  tomoAnalysis_mainwindow.TomoAnalysis3.Click(427, 399)

@then("Mask Editor 탭 XY Slice 화면에 Whole cell 마스크가 넓게 채워지는지 확인")
def step_impl():
  raise NotImplementedError

@when("Drawing tool 패널의 Erase 버튼을 클릭하고 Mask Editor 탭 중앙 XY Slice 화면의 Whole cell 마스크를 클릭한다.")
def step_impl():
  raise NotImplementedError

@then("Mask Editor 탭에서 클릭한 Whole cell 마스크가 사라졌는지 확인")
def step_impl():
  raise NotImplementedError

@when("Mask control 패널 Nucleus 마스크의 오른쪽 회색 버튼을 클릭한다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_hcontainer.ScrollBar.wPosition = 53
  scrollArea.qt_scrollarea_viewport.leftLayout.MaskControlPanel.panel_base.panel_contents.nucleStatus.ClickButton()

@when("Mask selector 패널의 Nucleus 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.MaskSelectionPanel.panel_base.panel_contents.nucleBtn.ClickButton()

@when("Drawing tool 패널의 Fill 버튼을 누르고 Mask Editor 탭 Nucleus의 XY Slice 화면 영역을 누른다.")
def step_impl():
  raise NotImplementedError

@then("Mask Editor 탭 XY Slice 화면에 Nucleus 마스크가 넓게 채워지는지 확인")
def step_impl():
  raise NotImplementedError

@when("Drawing tool 패널의 Erase 버튼을 클릭하고 Mask Editor 탭 중앙 XY Slice 화면의 Nucleus 마스크를 클릭한다.")
def step_impl():
  raise NotImplementedError

@then("Mask Editor 탭에서 클릭한 Nucleus 마스크가 사라졌는지 확인")
def step_impl():
  raise NotImplementedError

@when("Mask control 패널 Nucleolus 마스크의 오른쪽 회색 버튼을 클릭한다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_hcontainer.ScrollBar.wPosition = 53
  scrollArea.qt_scrollarea_viewport.leftLayout.MaskControlPanel.panel_base.panel_contents.nucliStatus.ClickButton()

@when("Mask selector 패널의 Nucleolus 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.MaskSelectionPanel.panel_base.panel_contents.nucliBtn.ClickButton()

@when("Drawing tool 패널의 Fill 버튼을 누르고 Mask Editor 탭 Nucleolus의 XY Slice 화면 영역을 누른다.")
def step_impl():
  raise NotImplementedError

@then("Mask Editor 탭 XY Slice 화면에 Nucleolus 마스크가 넓게 채워지는지 확인")
def step_impl():
  raise NotImplementedError

@when("Drawing tool 패널의 Erase 버튼을 클릭하고 Mask Editor 탭 중앙 XY Slice 화면의 Nucleolus 마스크를 클릭한다.")
def step_impl():
  raise NotImplementedError

@then("Mask Editor 탭에서 클릭한 Nucleolus 마스크가 사라졌는지 확인")
def step_impl():
  raise NotImplementedError

@when("Mask control 패널 Lipid droplet 마스크의 오른쪽 회색 버튼을 클릭한다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_hcontainer.ScrollBar.wPosition = 53
  scrollArea.qt_scrollarea_viewport.leftLayout.MaskControlPanel.panel_base.panel_contents.lipStatus.ClickButton()

@when("Mask selector 패널의 Lipid droplet 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.MaskSelectionPanel.panel_base.panel_contents.lipBtn.ClickButton()

@when("Drawing tool 패널의 Fill 버튼을 누르고 Mask Editor 탭 Lipid droplet의 XY Slice 화면 영역을 누른다.")
def step_impl():
  raise NotImplementedError

@then("Mask Editor 탭 XY Slice 화면에 Lipid droplet 마스크가 넓게 채워지는지 확인")
def step_impl():
  raise NotImplementedError

@when("Drawing tool 패널의 Erase 버튼을 클릭하고 Mask Editor 탭 중앙 XY Slice 화면의 Lipid droplet 마스크를 클릭한다.")
def step_impl():
  raise NotImplementedError

@then("Mask Editor 탭에서 클릭한 Lipid droplet 마스크가 사라졌는지 확인")
def step_impl():
  raise NotImplementedError

@when("Mask control 패널 Cell Instance와 Whole cell 마스크의 오른쪽 회색 버튼을 클릭한다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  widget = scrollArea.qt_scrollarea_viewport.leftLayout.MaskControlPanel.panel_base.panel_contents
  widget.instStatus.ClickButton()
  scrollBar.wPosition = 0
  widget.memStatus.ClickButton()

@when("Mask selector 패널 Cell instance 아이콘을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.MaskSelectionPanel.panel_base.panel_contents.instBtn.ClickButton()

@when("Drawing tool 패널의 Paint 버튼을 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.DrawingToolPanel.panel_base.panel_contents.PaintBtn.ClickButton()

@when("Drawing tool 패널 오른쪽의 Size 입력 칸에 마우스 포인터를 올리고 마우스 휠을 위로 다섯 칸 올린다.")
def step_impl():
  spinBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.DrawingToolPanel.panel_base.panel_contents.stackedWidget.BrushPage.BrushSizeSpin
  lineEdit = spinBox.qt_spinbox_lineedit
  lineEdit.MouseWheel(1)
  spinBox.wValue = 6
  lineEdit.MouseWheel(1)
  spinBox.wValue = 7
  lineEdit.MouseWheel(1)
  spinBox.wValue = 8
  lineEdit.MouseWheel(1)
  spinBox.wValue = 9
  lineEdit.MouseWheel(1)
  spinBox.wValue = 10

@then("Drawing tool 패널 오른쪽의 Size 칸 값이 10이 되었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.DrawingToolPanel.panel_base.panel_contents.stackedWidget.BrushPage.BrushSizeSpin.qt_spinbox_lineedit, "wText", cmpEqual, "10")

@when("Mask Editor 탭 중앙의 XY Slice 화면 중 마스크를 그리고 싶은 영역에 마우스를 클릭한다.")
def step_impl():
  raise NotImplementedError

@then("Mask Editor 탭에서 클릭한 자리에 점 Cell instance 마스크가 생겼는지 확인")
def step_impl():
  raise NotImplementedError

@when("Mask selector 패널의 Cell Instance 버튼을 클릭한다.")
def step_impl():
  raise NotImplementedError

@then("Mask Editor 탭 중앙의 XY Slice 화면에 붉은 색 Cell Instance 마스크가 사라졌는지 확인")
def step_impl():
  raise NotImplementedError

@then("Mask selector 패널의 Cell Instance 버튼의 하이라이트가 해제됐는지 확인")
def step_impl():
  raise NotImplementedError

@when("Mask selector 패널의 Cell Instance 버튼을 한번 더 클릭한다.")
def step_impl():
  raise NotImplementedError

@then("Mask Editor 탭 중앙의 XY Slice 화면에 원래의 붉은 색 Cell Instance 마스크가 생겼는지 확인")
def step_impl():
  raise NotImplementedError

@when("Mask selector 패널 Whole cell 아이콘을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.MaskSelectionPanel.panel_base.panel_contents.memBtn.ClickButton()

@when("Drawing tool 패널의 Paint 버튼을 누른다.")
def step_impl():
  raise NotImplementedError

@when("Mask Editor 탭 중앙의 XY Slice 화면 중 마스크를 그리고 싶은 영역에 마우스 포인터를 대고 클릭한다.")
def step_impl():
  raise NotImplementedError

@then("Mask Editor 탭에서 마우스 포인터로 클릭한 자리에 Whole cell 마스크가 생겼는지 확인")
def step_impl():
  raise NotImplementedError

@when("Mask selector 패널에서 Cell instance 아이콘을 누른다.")
def step_impl():
  raise NotImplementedError

@then("Mask selector 패널의 Whole cell 버튼의 하이라이트가 해제되고 Cell instance 버튼이 하이라이트 되며 화면에 Cell instance 마스크가 나타났는지 확인")
def step_impl():
  raise NotImplementedError


@then("Mask selector 패널의 Cell instance 버튼의 하이라이트가 해제되고 Whole cell 버튼이 하이라이트 되며 화면에 Whole cell 마스크가 나타났는지 확인")
def step_impl():
  raise NotImplementedError

@when("General 패널의 Mask file download 버튼을 누르고 Organ Mask와 Whole cell에 체크한 뒤 Save 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  scrollArea = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.leftLayout.GeneralPanel.panel_base.panel_contents.SaveBtn.ClickButton()
  tomoAnalysis_InterSeg_Plugins_MaskSelectionDialog = tomoAnalysis.MaskSelectionDialog
  tomoAnalysis_InterSeg_Plugins_MaskSelectionDialog.OrganRadio.ClickButton()
  tomoAnalysis_InterSeg_Plugins_MaskSelectionDialog.stackedWidget.OrganPage.organGroup.scrollArea.qt_scrollarea_viewport.orgChkGroup.CheckBox.ClickButton(cbChecked)
  tomoAnalysis_InterSeg_Plugins_MaskSelectionDialog.bt_square_primary.ClickButton()

@when("파일의 경로와 이름을 지정한다.")
def step_impl():
  dlgSaveMask = Aliases.TomoAnalysis.dlgSaveMask
  HWNDView = dlgSaveMask.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  HWNDView.CtrlNotifySink2.ShellView.Item.Regression_Test.Item.DblClick(63, 15)
  dlgSaveMask.btn_S.ClickButton()

@then("지정한 파일의 경로에 .msk 형식의 Whole cell 마스크 파일이 생성됐는지 확인")
def step_impl():
  raise NotImplementedError

@when("Mask selector 패널에서 Cell instance 버튼을 누른 뒤 General 패널의 Mask file download 버튼을 누르고 Cell Instance Mask에 체크한 후 Save 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis_InterSeg_AppUI_MainWindow = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  tomoAnalysis_InterSeg_AppUI_MainWindow.MaskSelectionPanel.panel_base.panel_contents.instBtn.ClickButton()
  scrollArea = tomoAnalysis_InterSeg_AppUI_MainWindow.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.leftLayout.GeneralPanel.panel_base.panel_contents.SaveBtn.ClickButton()
  tomoAnalysis_InterSeg_Plugins_MaskSelectionDialog = tomoAnalysis.MaskSelectionDialog
  tomoAnalysis_InterSeg_Plugins_MaskSelectionDialog.InstRadio.ClickButton()
  tomoAnalysis_InterSeg_Plugins_MaskSelectionDialog.bt_square_primary.ClickButton()

@when("파일의 이름과 경로를 지정한다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis_InterSeg_Plugins_MaskSelectionDialog = tomoAnalysis.MaskSelectionDialog
  dlgSaveMask = tomoAnalysis.dlgSaveMask
  HWNDView = dlgSaveMask.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  UIItem = HWNDView.CtrlNotifySink2.ShellView.Item.Regression_Test
  UIItem.Item.Click(43, 9)
  UIItem.Keys("[Enter]")
  dlgSaveMask.btn_S.ClickButton()


@then("지정한 파일의 경로에 .msk 형식의 Cell instance 마스크 파일이 생성됐는지 확인")
def step_impl():
  raise NotImplementedError

@when("General 패널의 msk file upload 버튼을 누른다.")
def step_impl():
  dir1 = r'E:\Regression_Test\20201022.095301.707.HEK293FT-003P001_MultiLabel_test.msk'
  dir2 = r'E:\Regression_Test\20201022.095301.707.HEK293FT-003P001_MultiLayer_test.msk'
  dir3 = r'E:\Regression_Test\Prefix_File\backup\20201022.095301.707.HEK293FT-003P001_MultiLabel_test.msk'
  dir4 = r'E:\Regression_Test\Prefix_File\backup\20201022.095301.707.HEK293FT-003P001_MultiLayer_test.msk'
  shutil.copy2(dir3,dir1)
  shutil.copy2(dir4,dir2)
  dir5 = r'E:\Regression_Test'
  dir6 = r'E:\Regression_Test\Prefix_File\backup\20201022.095301.707.HEK293FT-003P001_MultiLabel_test'
  dir7 = r'E:\Regression_Test\Prefix_File\backup\20201022.095301.707.HEK293FT-003P001_MultiLayer_test'
  dir8 = r'E:\Regression_Test20201022.095301.707.HEK293FT-003P001_MultiLabel_test'
  dir9 = r'E:\Regression_Test20201022.095301.707.HEK293FT-003P001_MultiLayer_test'
  if os.path.isdir(dir8) :
    shutil.copytree(dir6,dir5)
  if os.path.isdir(dir9) :
    shutil.copytree(dir7,dir5)
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.leftLayout.GeneralPanel.panel_base.panel_contents.LoadBtn.ClickButton()

@when("TC-F24-0040에서 저장한 Whole cell 마스크 파일을 선택한다.")
def step_impl():
  raise NotImplementedError

@then("Mask control 패널의 Whole cell 색깔 버튼이 파란색으로 바뀌었는지 확인")
def step_impl():
  raise NotImplementedError

@then("Mask selector 패널의 Whole cell 버튼이 활성화 되었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.MaskSelectionPanel.panel_base.panel_contents.memBtn, "Enabled", cmpEqual, True)

@when("Mask selector 패널 Whole cell의 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.MaskSelectionPanel.panel_base.panel_contents.memBtn.ClickButton()

@then("Mask Editor 탭 3D 화면과 2D Slice 화면에 Whole cell 마스크가 나타났는지 확인")
def step_impl():
  Regions.TomoAnalysis169.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow)
  Regions.TomoAnalysis170.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow)

@when("General 패널에서 msk file upload 버튼을 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.leftLayout.GeneralPanel.panel_base.panel_contents.LoadBtn.ClickButton()

@when("TC-F24-0040에서 저장한 Cell instance 마스크 파일을 선택한다.")
def step_impl():
  raise NotImplementedError

@then("Mask control 패널의 Cell instance 색깔 버튼이 파란색으로 바뀌었는지 확인")
def step_impl():
  raise NotImplementedError

@then("Mask selector 패널의 Cell instance 버튼이 활성화 되었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.MaskSelectionPanel.panel_base.panel_contents.instBtn, "Enabled", cmpEqual, True)

@when("Mask selector 패널 Cell instance 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.MaskSelectionPanel.panel_base.panel_contents.instBtn.ClickButton()

@then("Mask Editor 탭 3D 화면과 2D Slice 화면에 Cell instance 마스크가 나타났는지 확인")
def step_impl():
  Regions.TomoAnalysis171.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow)
  Regions.TomoAnalysis172.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow)

@when("Drawing tool 패널의 Wipe 버튼을 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.DrawingToolPanel.panel_base.panel_contents.WipeBtn.ClickButton()

@when("Mask Editor 탭 중앙의 XY Slice 화면 중 마스크가 존재하는 영역에 마우스를 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis.Click(549, 366)

@then("Mask Editor 탭에서 클릭한 자리에서 점으로 Cell instance 마스크가 지워졌는지 확인")
def step_impl():
  Regions.TomoAnalysis173.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis)

@when("Mask selector 패널 Whole cell 아이콘을 클릭하고 Drawing tool 패널의 Wipe 버튼을 클릭한다.")
def step_impl():
  raise NotImplementedError

@when("Mask Editor 탭 중앙의 XY Slice 화면 중 마스크가 존재하는 영역에 마우스 포인터를 대고 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis.Click(677, 358)

@then("Mask Editor 탭에서 마우스 포인터로 클릭한 자리에서 점으로 Whole cell 마스크가 지워졌는지 확인")
def step_impl():
  raise NotImplementedError

@when("History 패널의 Undo 버튼을 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.leftLayout.HistoryPanel.panel_base.panel_contents.UndoBtn.ClickButton()

@then("Mask Editor 탭 XY Slice 화면에서 Wipe로 지운 점이 다시 생겼는지 확인")
def step_impl():
  Regions.TomoAnalysis175.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis)

@when("History 패널의 Redo 버튼을 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.leftLayout.HistoryPanel.panel_base.panel_contents.RedoBtn.ClickButton()

@then("Mask Editor 탭 XY Slice 화면에서 Undo 버튼으로 생긴 부분이 다시 사라졌는지 확인")
def step_impl():
  Regions.TomoAnalysis176.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis)

@when("History 패널의 Refresh 버튼을 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.leftLayout.HistoryPanel.panel_base.panel_contents.RefreshBtn.ClickButton()

@when("History 패널의 Undo 버튼과 Redo 버튼을 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  widget = scrollArea.qt_scrollarea_viewport.leftLayout.HistoryPanel.panel_base.panel_contents
  widget.UndoBtn.ClickButton()
  scrollBar.wPosition = 0
  widget.RedoBtn.ClickButton()

@then("History 패널의 Undo 버튼과 Redo 버튼을 눌러도 Mask Editor 탭 XY Slice 화면에서 아무런 변화가 없는지 확인")
def step_impl():
  Regions.TomoAnalysis177.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis)

@given("Mask Editor 탭에 Time-lapse TCF가 열려 있다.")
def step_impl():
  dir3 = r'E:\Regression_Test\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)

    
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(136, 21)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\Import Data - Set TCF\\Import Data - Set TCF.tcpro")

  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea
  scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(138, 64)

  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.bt_round_tool6.Click(35, 11)
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.leftLayout.IDManagerPanel.panel_base.panel_contents.bt_round_gray500.ClickButton()

  tomoAnalysis_InterSeg_Plugins_NewIdDialog = Aliases.TomoAnalysis.NewIdDialog
  lineEdit = tomoAnalysis_InterSeg_Plugins_NewIdDialog.input_high
  lineEdit.Click(128, 18)
  lineEdit.SetText("test")
  tomoAnalysis_InterSeg_Plugins_NewIdDialog.bt_square_primary.ClickButton()


@when("Mask Editor 탭 하단 타임 바 오른쪽의 위 화살표 버튼을 한 번 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.PlayerPanel.progressSpinBox.wValue = 2

@then("Mask Editor 탭에 나타나는 TCF 이미지가 time step 2의 이미지로 바뀌었는지 확인")
def step_impl():
  Regions.TomoAnalysis178.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow)

@then("Mask Editor 탭의 타임 바 오른쪽의 숫자가 2로 올라갔는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.PlayerPanel.progressSpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "2")

@given("Blue FL을 가진 TCF로 Mask Editor 탭이 열려 있다.")
def step_impl():

  dir3 = r'E:\Regression_Test\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)

    
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(136, 21)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\3D_Visualizer\\3D_Visualizer.tcpro")

  widget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea
  widget.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  widget.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox2.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget3.TC_TCF2DWidget.panel_contents_image.Widget.Click(103, 40)

  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.bt_round_tool6.Click(35, 11)
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.leftLayout.IDManagerPanel.panel_base.panel_contents.bt_round_gray500.ClickButton()

  tomoAnalysis_InterSeg_Plugins_NewIdDialog = Aliases.TomoAnalysis.NewIdDialog
  lineEdit = tomoAnalysis_InterSeg_Plugins_NewIdDialog.input_high
  lineEdit.Click(128, 18)
  lineEdit.SetText("test")
  tomoAnalysis_InterSeg_Plugins_NewIdDialog.bt_square_primary.ClickButton()



@when("FL tool 패널의 Ch0 버튼을 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.leftLayout.FLControlPanel.panel_base.panel_contents.ch0Rad.ClickButton()

@then("Mask Editor의 2D Slice 세 화면에 Blue FL이 나타났는지 확인")
def step_impl():
  Regions.RenderWindow2.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow)

@when("FL tool 패널에서 Ch1 버튼을 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.leftLayout.FLControlPanel.panel_base.panel_contents.ch1Rad.ClickButton()

@then("Mask Editor의 2D Slice 세 화면에 Blue FL이 사라지고 Green FL이 나타났는지 확인")
def step_impl():
  Regions.RenderWindow3.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow)

@then("FL tool 패널의 Channel 박스에서 Ch0의 체크가 사라지고 Ch1의 체크가 나타났는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea.qt_scrollarea_viewport.leftLayout.FLControlPanel.panel_base.panel_contents.ch0Rad, "wChecked", cmpEqual, False)
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea.qt_scrollarea_viewport.leftLayout.FLControlPanel.panel_base.panel_contents.ch1Rad, "wChecked", cmpEqual, True)

@when("FL tool 패널의 None 버튼을 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.leftLayout.FLControlPanel.panel_base.panel_contents.noneRad.ClickButton()

@then("Mask Editor의 2D Slice 세 화면에 FL이 사라졌는지 확인")
def step_impl():
  Regions.RenderWindow4.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow)

@when("FL tool 패널에서 Ch0 버튼을 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.leftLayout.FLControlPanel.panel_base.panel_contents.ch0Rad.ClickButton()

@when("FL tool 패널의 Channel 박스 아래 사이즈 바를 마우스로 클릭한 채로 오른쪽 끝으로 드래그한다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.leftLayout.FLControlPanel.panel_base.panel_contents.opaSlider.wPosition = 100
  scrollBar.wPosition = 0

@then("FL tool 패널 사이즈 바 옆 값이 1.00으로 바뀌었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea.qt_scrollarea_viewport.leftLayout.FLControlPanel.panel_base.panel_contents.opaSpin.qt_spinbox_lineedit, "wText", cmpEqual, "1.00")

@then("Mask Editor 탭 2D Slice 화면의 FL 렌더링이 선명해졌는지 확인")
def step_impl():
  Regions.TomoAnalysis180.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow)

@when("FL tool 패널의 Channel 박스 아래의 Color 버튼을 누르고 팝업에서 흰색을 선택한 후 OK 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  scrollArea = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.leftLayout.FLControlPanel.panel_base.panel_contents.bt_round_gray500.ClickButton()
  colorDialog = tomoAnalysis.ColorDialog
  colorDialog.WellArray.Click(207, 132)
  colorDialog.DialogButtonBox.buttonOk.ClickButton()

@then("Mask Editor 탭 2D Slice 화면의 Green FL이 흰색으로 바뀌었는지 확인")
def step_impl():
  Regions.TomoAnalysis181.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow)

@given("Red FL을 가진 TCF로 Mask Editor 탭이 열려 있다.")
def step_impl():

  dir3 = r'E:\Regression_Test\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)

    
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(136, 21)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\3D_Visualizer\\3D_Visualizer.tcpro")

  widget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea
  widget.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  widget.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox2.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(149, 71)

  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.bt_round_tool6.Click(35, 11)
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.leftLayout.IDManagerPanel.panel_base.panel_contents.bt_round_gray500.ClickButton()

  tomoAnalysis_InterSeg_Plugins_NewIdDialog = Aliases.TomoAnalysis.NewIdDialog
  lineEdit = tomoAnalysis_InterSeg_Plugins_NewIdDialog.input_high
  lineEdit.Click(128, 18)
  lineEdit.SetText("test")
  tomoAnalysis_InterSeg_Plugins_NewIdDialog.bt_square_primary.ClickButton()


@when("FL tool 패널의 Ch2 버튼을 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.leftLayout.FLControlPanel.panel_base.panel_contents.ch2Rad.ClickButton()

@then("Mask Editor의 2D Slice 세 화면에 Red FL이 나타났는지 확인")
def step_impl():
  Regions.RenderWindow5.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow)

@when("FL tool 패널의 Channel 박스 아래의 Color 버튼을 누르고 팝업에서 흰색을 선택한 후 키보드의 엔터 키를 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  scrollArea = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.leftLayout.FLControlPanel.panel_base.panel_contents.bt_round_gray500.ClickButton()
  colorDialog = tomoAnalysis.ColorDialog
  colorDialog.WellArray.Click(203, 131)
  colorDialog.DialogButtonBox.buttonOk.ClickButton()

@when("Drawing tool 패널의 Paint 버튼을 클릭하고 사이즈를 100으로 설정한 뒤 마스크를 생성한다.")
def step_impl():
  raise NotImplementedError

@when("Label tools 패널의 Add 버튼을 누른다.")
def step_impl():
  raise NotImplementedError

@then("Label tools 패널의 Label 값이 2로 바뀌었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.LabelControlPanel.panel_base.panel_contents.LabelSpin.qt_spinbox_lineedit, "wText", cmpEqual, "2")

@when("Drawing tool 패널의 Paint 버튼을 누르고 겹치지 않게 마스크를 그린다.")
def step_impl():
  raise NotImplementedError

@then("Mask Editor 탭에 녹색 마스크가 생성됐는지 확인")
def step_impl():
  Regions.TomoAnalysis182.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow)

@when("Mask selector에서 Whole cell 아이콘을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.MaskSelectionPanel.panel_base.panel_contents.memBtn.ClickButton()

@when("Label tools 패널에서 Add 버튼을 누른다.")
def step_impl():
  aqUtils.Delay(10000)
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.LabelControlPanel.panel_base.panel_contents.bt_round_gray500.ClickButton()

@then("“Failed to add label” 이라는 경고창이 나타났는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.MessageBox20, "QtText", cmpEqual, "Failed to add label")
  aqObject.CheckProperty(Aliases.TomoAnalysis.MessageBox20, "WndCaption", cmpEqual, "Error")

@when("경고창의 OK 버튼을 누르고 Mask Selector 패널에서 Cell instance 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.MessageBox20.qt_msgbox_buttonbox.buttonOk.ClickButton()
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.MaskSelectionPanel.panel_base.panel_contents.instBtn.ClickButton()

@when("Mask Editor 탭에 생성된 녹색 마스크를 한 번 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis.Click(500, 234)

@when("Label tools 패널에서 활성화된 Label picker 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.LabelControlPanel.panel_base.panel_contents.PickBtn.ClickButton()

@then("Label tools 패널의 Label picker 버튼이 비활성화 되었는지 확인")
def step_impl():
  raise NotImplementedError

@when("Drawing tool 패널에서 Erase 버튼을 누르고 XY Slice 화면의 빨간색 마스크를 클릭한다.")
def step_impl():
  tomoAnalysis_InterSeg_AppUI_MainWindow = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  tomoAnalysis_InterSeg_AppUI_MainWindow.DrawingToolPanel.panel_base.panel_contents.EraseBtn.ClickButton()
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis.Click(507, 374)

@then("화면의 빨간색 마스크에 아무런 변화가 없는지 확인")
def step_impl():
  raise NotImplementedError

@when("Drawing tool 패널에서 Erase 버튼이 선택된 상태로 XY Slice 화면의 초록색 마스크를 클릭한다.")
def step_impl():
  raise NotImplementedError

@then("XY Slice 화면에서 초록색 마스크가 지워졌는지 확인")
def step_impl():
  Regions.TomoAnalysis185.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow)

@when("Label tools 패널의 Merge label 버튼을 누른다.")
def step_impl():
  raise NotImplementedError

@when("Mask Editor 탭의 빨간색, 녹색 마스크를 하나씩 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis
  tomoAnalysis.Click(509, 368)
  tomoAnalysis.Click(503, 246)

@when("Label tools 패널에서 활성화된 Merge label 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.LabelControlPanel.panel_base.panel_contents.MergeLabelBtn.ClickButton()

@then("Mask Editor에서 선택한 두 마스크가 모두 빨간색 마스크로 바뀌었는지 확인")
def step_impl():
  Regions.TomoAnalysis184.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow)

@when("Basic Analyzer Single Run 탭의 Mask Correction 버튼을 누른다.")
def step_impl():
  aqUtils.Delay(40000)
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.correctionBtn.ClickButton()
  
@then("There is no temporal mask 경고창이 나타났는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.MessageBox21, "QtText", cmpEqual, "There is no temporal mask")
  aqObject.CheckProperty(Aliases.TomoAnalysis.MessageBox21, "WndCaption", cmpEqual, "ERROR")
  Aliases.TomoAnalysis.MessageBox21.qt_msgbox_buttonbox.buttonOk.ClickButton()

@then("Apply to Result 버튼과 Cell Instance, Organelle 마스크가 모두 활성화 되어있는 Mask Editor 탭이 열렸는지 확인")
def step_impl():
  Regions.F25_1.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget)

@when("Mask selector 패널 Cell instance 버튼을 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.MaskSelectionPanel.panel_base.panel_contents.instBtn.ClickButton()

@then("Mask Editor 탭에 Cell Instance 마스크가 나타나는지 확인")
def step_impl():
  Regions.TomoAnalysis186.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow, False, False, 472, 59)


@when("Mask selector 패널 Whole cell 버튼을 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.MaskSelectionPanel.panel_base.panel_contents.memBtn.ClickButton()

@then("Mask Editor 탭에 Whole cell 마스크가 나타나는지 확인")
def step_impl():
  Regions.TomoAnalysis187.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow, False, False, 472, 59)

@when("Mask selector 패널 Nucleus 버튼을 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.MaskSelectionPanel.panel_base.panel_contents.nucleBtn.ClickButton()

@then("Mask Editor 탭에 Nucleus 마스크가 나타나는지 확인")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis
  tomoAnalysis.HoverMouse(-1)
  for i in range(7):
    tomoAnalysis.MouseWheel(1)
  Regions.TomoAnalysis190.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis, False, False, 472, 59)



@when("Mask selector 패널 Nucleolus 버튼을 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.MaskSelectionPanel.panel_base.panel_contents.nucliBtn.ClickButton()

@then("Mask Editor 탭에 Nucleolus 마스크가 나타나는지 확인")
def step_impl():
  Regions.TomoAnalysis188.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis, False, False, 472, 59)

@when("Mask selector 패널 Lipid droplet 버튼을 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.MaskSelectionPanel.panel_base.panel_contents.lipBtn.ClickButton()

@then("Mask Editor 탭에 Lipid droplet 마스크가 나타나는지 확인")
def step_impl():
  Regions.TomoAnalysis189.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis, False, False, 472, 59)

@when("ID manager 패널의 Copy 버튼을 누르고 ‘test’를 입력한 후 OK 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  scrollArea = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.leftLayout.IDManagerPanel.panel_base.panel_contents.bt_round_gray5002.ClickButton()
  tomoAnalysis_InterSeg_Plugins_NewIdDialog = tomoAnalysis.NewIdDialog
  tomoAnalysis_InterSeg_Plugins_NewIdDialog.input_high.SetText("test")
  tomoAnalysis_InterSeg_Plugins_NewIdDialog.bt_square_primary.ClickButton()

@when("Mask selector 패널의 Cell Instance 아이콘을 누른다.")
def step_impl():
  raise NotImplementedError

@when("Drawing tool 패널의 Paint 버튼을 누르고 사이즈를 100으로 키운 뒤 Mask Editor 탭 XY Slice 화면에서 겹치지 않게 점을 찍어 마스크를 추가한다.")
def step_impl():
  tomoAnalysis_InterSeg_AppUI_MainWindow = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  widget = tomoAnalysis_InterSeg_AppUI_MainWindow.DrawingToolPanel.panel_base.panel_contents
  widget.PaintBtn.ClickButton()
  spinBox = widget.stackedWidget.BrushPage.BrushSizeSpin
  spinBox.qt_spinbox_lineedit.Drag(18, 12, -39, 1)
  spinBox.Keys("100")
  spinBox.wValue = 100
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis.Click(592, 106)

@when("Mask selector 패널의 Cell instance 아이콘을 한 번 더 누른다.")
def step_impl():
  raise NotImplementedError

@when("General 패널의 Apply to Result 버튼을 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_hcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.leftLayout.GeneralPanel.panel_base.panel_contents.SaveLinkBtn.ClickButton()

@then("Basic Analyzer 탭 Mask Viewing panel에서 Cell instance 마스크가 수정한 대로 바뀌어있는지 확인")
def step_impl():
  Regions.TomoAnalysis209.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.viewerPanel, False, False, 472, 59)

@given("Mask Editor 탭에 2개의 Cell이 있는 TCF의 AI Segmentation cell instance 마스크가 ID ‘test’로 Copy되어 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir = r'E:\Regression_Test\Prefix_File\BA_Single_RUN\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir3 = r'E:\Regression_Test\Prefix_File\BA_Single_RUN\playground\test\Hypercube\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)
  dir4 = r'E:\Regression_Test\Prefix_File\BA_Single_RUN\playground\test\Hypercube\*.rep'
  for f in glob.glob(dir4):
      os.remove(f)
  dir1 = r'E:\Regression_Test\Prefix_File\BA_Single_RUN'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BA_Single_RUN'
  copy_tree(dir2,dir1) 
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(92, 19)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.WorkerW.ReBarWindow32.AddressBandRoot.progress.BreadcrumbParent.toolbar.ClickItemXY("‎Prefix_File", 22, 13, False)
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BA_Single_RUN\\BA_Single_RUN.tcpro")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_gray.Drag(111, 16, -7, 1)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel = tomoAnalysis.widget_popup
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget.panel_contents_image.widget.panel_contents_image.Widget.Click(136, 89)
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.bt_square_primary.Click(52, 23)
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.buttonExecute.ClickButton()

  aqUtils.Delay(15000)
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.correctionBtn.ClickButton()

  
  tomoAnalysis = Aliases.TomoAnalysis
  scrollArea = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.leftLayout.IDManagerPanel.panel_base.panel_contents.bt_round_gray5002.ClickButton()
  tomoAnalysis_InterSeg_Plugins_NewIdDialog = tomoAnalysis.NewIdDialog
  tomoAnalysis_InterSeg_Plugins_NewIdDialog.input_high.SetText("test")
  tomoAnalysis_InterSeg_Plugins_NewIdDialog.bt_square_primary.ClickButton()

@when("Mask control 패널의 체크박스 5개를 모두 클릭한다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  widget = scrollArea.qt_scrollarea_viewport.leftLayout.MaskControlPanel.panel_base.panel_contents
  widget.instChk.ClickButton(cbChecked)
  scrollBar.wPosition = 0
  widget.memChk.ClickButton(cbChecked)
  scrollBar.wPosition = 0
  checkBox = widget.nucleChk
  checkBox.Click(5, 7)
  scrollBar.wPosition = 0
  widget.nucliChk.Click(5, 7)
  scrollBar.wPosition = 0
  checkBox.ClickButton(cbChecked)
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.leftLayout.MaskControlPanel.panel_base.panel_contents.lipChk.ClickButton(cbChecked)

@then("Mask Editor 탭의 2D, 3D 화면에서 모든 마스크가 multi-layered mask로 표현됐는지 확인")
def step_impl():
  Regions.RenderWindow7.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow,False,False,1000)

@when("Mask selector에서 Cell instance 아이콘을 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.MaskSelectionPanel.panel_base.panel_contents.instBtn.ClickButton()

@then("Mask control 패널의 체크박스가 모두 체크 해제됐는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea.qt_scrollarea_viewport.leftLayout.MaskControlPanel.panel_base.panel_contents.instChk, "checked", cmpEqual, False)
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea.qt_scrollarea_viewport.leftLayout.MaskControlPanel.panel_base.panel_contents.memChk, "checked", cmpEqual, False)
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea.qt_scrollarea_viewport.leftLayout.MaskControlPanel.panel_base.panel_contents.nucleChk, "checked", cmpEqual, False)
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea.qt_scrollarea_viewport.leftLayout.MaskControlPanel.panel_base.panel_contents.nucliChk, "checked", cmpEqual, False)
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea.qt_scrollarea_viewport.leftLayout.MaskControlPanel.panel_base.panel_contents.lipChk, "checked", cmpEqual, False)

@then("Label tools 패널의 Label picker 버튼을 누르고 Mask Editor의 초록색 마스크를 클릭한다.")
def step_impl():
  tomoAnalysis_InterSeg_AppUI_MainWindow = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  tomoAnalysis_InterSeg_AppUI_MainWindow.LabelControlPanel.panel_base.panel_contents.PickBtn.ClickButton()
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis.Click(585, 459)

@then("Lable tools 패널 Label 칸의 값이 2로 바뀌었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.LabelControlPanel.panel_base.panel_contents.LabelSpin, "wValue", cmpEqual, 2)

@when("Label tools 패널의 아래 화살표 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.LabelControlPanel.panel_base.panel_contents.LabelSpin.wValue = 1

@then("Lable tools 패널 Label 칸의 값이 1로 바뀌었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.LabelControlPanel.panel_base.panel_contents.LabelSpin.qt_spinbox_lineedit, "wText", cmpEqual, "1")

@when("Label tools 패널의 위 화살표 버튼을 누른다.")
def step_impl():
  raise NotImplementedError

@when("Lable tools 패널 Label 칸에 {arg}을 키보드로 입력한 뒤 Label tools 패널의 Label picker 버튼을 누른다.")
def step_impl(param1):
  raise NotImplementedError

@then("Lable picker 버튼이 비활성화 되었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.LabelControlPanel.panel_base.panel_contents.PickBtn, "checked", cmpEqual, False)

@then("Drawing tool 패널에서 Paint 버튼을 클릭하고 Size를 100으로 키운다.")
def step_impl():
  tomoAnalysis_InterSeg_AppUI_MainWindow = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  tomoAnalysis_InterSeg_AppUI_MainWindow.ClickR(1097, 6)
  widget = tomoAnalysis_InterSeg_AppUI_MainWindow.DrawingToolPanel.panel_base.panel_contents
  widget.PaintBtn.ClickButton()
  spinBox = widget.stackedWidget.BrushPage.BrushSizeSpin
  spinBox.qt_spinbox_lineedit.Drag(15, 7, -23, 1)
  spinBox.Keys("10")
  spinBox.wValue = 10
  spinBox.Keys("0")
  spinBox.wValue = 100
  spinBox.Keys("[Enter]")
  spinBox.wValue = 100

@then("Label tools 패널의 Add 버튼을 누르고 Mask Editor 탭의 XY Slice 화면에서 기존의 마스크와 겹치지 않게 마우스를 클릭한다.")
def step_impl():
  raise NotImplementedError

@then("Mask Editor 탭의 XY Slice 화면에 노란색 마스크가 그려졌는지 확인")
def step_impl():
  Regions.TomoAnalysis191.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis,False,False,1000)

@when("Label tools 패널에서 Merge label 버튼을 누른다.")
def step_impl():
  raise NotImplementedError

@when("Mask Editor 탭에서 노란색과 초록색 마스크를 순서대로 누른다.")
def step_impl():
  raise NotImplementedError

@when("Label tools 패널에서 Merge label 버튼을 다시 누른다.")
def step_impl():
  raise NotImplementedError

@then("Mask Editor에서 선택한 두 마스크가 모두 초록색 마스크로 병합되었는지 확인")
def step_impl():
  raise NotImplementedError

@when("Clean tool의 Clean selected label 버튼을 누르고 Mask Editor XY Slice 화면의 초록색 마스크를 클릭한다.")
def step_impl():
  TC_AdaptiveTabWidget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget
  TC_AdaptiveTabWidget.ClickR(1550, 43)
  tomoAnalysis_InterSeg_AppUI_MainWindow = TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  tomoAnalysis_InterSeg_AppUI_MainWindow.CleanerPanel.panel_base.panel_contents.CleanSelBtn.ClickButton()
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis.Click(595, 506)

@then("Mask Editor XY Slice 화면에서 초록색 마스크가 완전히 삭제되었는지 확인")
def step_impl():
  Regions.TomoAnalysis193.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis)

@then("Mask Editor 3D 화면에 초록색 마스크가 존재하는지 확인")
def step_impl():
  Regions.TomoAnalysis194.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget2.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow3D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis,False,False,1000)

@when("History 패널에서 Undo 버튼을 누른다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.leftLayout.HistoryPanel.panel_base.panel_contents.UndoBtn.ClickButton()

@when("Clean tool의 Flush selected label 버튼을 누르고 Mask Editor XY Slice 화면의 초록색 마스크를 클릭한다.")
def step_impl():
  tomoAnalysis_InterSeg_AppUI_MainWindow = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  tomoAnalysis_InterSeg_AppUI_MainWindow.CleanerPanel.panel_base.panel_contents.FlushSelBtn.ClickButton()
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis.Click(587, 509)

@then("Mask Editor의 XY Slice 화면과 3D 화면에서 초록색 마스크가 완전히 삭제되었는지 확인")
def step_impl():
  Regions.RenderWindow8.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow,False,False,1000)

@when("History 패널 Undo 버튼을 누른다.")
def step_impl():
  raise NotImplementedError

@when("Clean tool의 Clean Slice 버튼을 누르고 Mask Editor XY Slice 화면의 빨간색 마스크를 클릭한다.")
def step_impl():
  tomoAnalysis_InterSeg_AppUI_MainWindow = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  tomoAnalysis_InterSeg_AppUI_MainWindow.CleanerPanel.panel_base.panel_contents.CleanBtn.ClickButton()
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis.Click(586, 289)

@then("Mask Editor XY Slice 화면에서 모든 마스크가 완전히 삭제되었는지 확인")
def step_impl():
  Regions.TomoAnalysis195.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis)

@then("Mask Editor 3D 화면에 마스크가 존재하는지 확인")
def step_impl():
  Regions.TomoAnalysis196.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget2.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow3D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis,False,False,1000)

@when("History 패널 Undo 버튼을 클릭한다.")
def step_impl():
  raise NotImplementedError

@when("Clean tool의 Flush Slice 버튼을 누르고 Mask Editor XY Slice 화면의 빨간색 마스크를 클릭한다.")
def step_impl():
  tomoAnalysis_InterSeg_AppUI_MainWindow = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  tomoAnalysis_InterSeg_AppUI_MainWindow.CleanerPanel.panel_base.panel_contents.FlushBtn.ClickButton()
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis.Click(628, 305)

@then("Mask Editor의 XY Slice 화면과 3D 화면에서 모든 마스크가 완전히 삭제되었는지 확인")
def step_impl():
  Regions.RenderWindow9.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow)

@given("Mask Editor 탭에 1개의 Cell이 있는 TCF의 RI Thresholding cell instance 마스크가 ID ‘test’로 Copy되어 있다.")
def step_impl():

  dir = r'E:\Regression_Test\Prefix_File\BA_Single_RUN\playground\*.tcpg'
  for f in glob.glob(dir):
    os.remove(f)
  dir3 = r'E:\Regression_Test\Prefix_File\BA_Single_RUN\playground\test\Hypercube\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)
  dir4 = r'E:\Regression_Test\Prefix_File\BA_Single_RUN\playground\test\Hypercube\*.rep'
  for f in glob.glob(dir4):
      os.remove(f)
  dir1 = r'E:\Regression_Test\Prefix_File\BA_Single_RUN'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\BA_Single_RUN'
  copy_tree(dir2,dir1) 
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(92, 19)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.WorkerW.ReBarWindow32.AddressBandRoot.progress.BreadcrumbParent.toolbar.ClickItemXY("‎Prefix_File", 22, 13, False)
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\BA_Single_RUN\\BA_Single_RUN.tcpro")
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.playgroundFrame.playgroundSplitter.parameterPanel.panel_base.bt_square_gray.Drag(111, 16, -7, 1)
  tomoAnalysis = Aliases.TomoAnalysis
  
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget5.TC_TCF2DWidget.panel_contents_image.Widget.Click(104, 65)
  
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel = tomoAnalysis.widget_popup
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.scrollArea.qt_scrollarea_viewport.Widget.NamedImageWidgetGroupBox.frame.NamedImageWidget3.panel_contents_image.widget.panel_contents_image.Widget.Click(107, 66)
  tomoAnalysis_ProjectManager_Plugins_SingleRunPanel.bt_square_primary.Click(20, 14)
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.groupBox.panel_contents.ScrollArea.qt_scrollarea_viewport.Widget.CollapseWidget.contentsFrame.TC_ParameterControl.buttonExecute.ClickButton()

  aqUtils.Delay(15000)
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.parameterPanel.correctionBtn.ClickButton()

  
  tomoAnalysis = Aliases.TomoAnalysis
  scrollArea = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.leftLayout.IDManagerPanel.panel_base.panel_contents.bt_round_gray5002.ClickButton()
  tomoAnalysis_InterSeg_Plugins_NewIdDialog = tomoAnalysis.NewIdDialog
  tomoAnalysis_InterSeg_Plugins_NewIdDialog.input_high.SetText("test")
  tomoAnalysis_InterSeg_Plugins_NewIdDialog.bt_square_primary.ClickButton()


@when("Mask selector에서 Cell instance 아이콘을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.MaskSelectionPanel.panel_base.panel_contents.instBtn.ClickButton()

@when("Semi-auto tool 패널의 Erosion 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.SemiAutoPanel.panel_base.panel_contents.ErodeBtn.ClickButton()

@then("Mask Editor의 XY Slice 화면에서 마스크가 쪼그라들었는지 확인")
def step_impl():
  Regions.RenderWindow11.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow)

@when("Semi-auto tool 패널의 Dilation 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.SemiAutoPanel.panel_base.panel_contents.DilateBtn.ClickButton()

@then("Mask Editor의 XY Slice 화면에서 마스크가 팽창했는지 확인")
def step_impl():
  Regions.RenderWindow12.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow)

@when("Semi-auto tool 패널의 Watershed 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.SemiAutoPanel.panel_base.panel_contents.WaterShedBtn.ClickButton()

@then("Mask Editor의 XY, YZ, XZ Slice 화면과 3D 화면에서 모든 마스크가 Watershed 알고리즘에 따라 수정되었는지 확인")
def step_impl():
  Regions.MainWindow5.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow,False,False,1000)

@when("Semi-auto tool 패널의 Divider 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.SemiAutoPanel.panel_base.panel_contents.DividerBtn.ClickButton()

@when("Mask Editor 탭의 3D 마스크 화면에서 마우스를 클릭한 채로 마스크를 가로지르도록 직선으로 드래그한다.")
def step_impl():
  tomoAnalysis_InterSeg_AppUI_MainWindow = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  scrollArea = tomoAnalysis_InterSeg_AppUI_MainWindow.scrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  pushButton = scrollArea.qt_scrollarea_viewport.leftLayout.GeneralPanel.panel_base.panel_contents.WinBtn
  pushButton.ClickButton()
  scrollBar.wPosition = 0
  pushButton.ClickButton()
  scrollBar.wPosition = 0
  pushButton.ClickButton()
  tomoAnalysis_InterSeg_AppUI_MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow3D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis.Drag(247, 400, 597, 8)

@when("Semi-auto tool 패널에서 활성화된 Divider 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.SemiAutoPanel.panel_base.panel_contents.DividerBtn.ClickButton()

@then("Mask Editor 탭의 마스크가 빨간색, 녹색 마스크 두 개로 나뉘었는지 확인")
def step_impl():
  Regions.MainWindow6.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow,False,False, 5000)

@given("TomoAnalysis에 Report[T] 탭이 열려 있다.")
def step_impl():
  TestedApps.TomoAnalysis.Run()
  dir3 = r'E:\Regression_Test\Prefix_File\F23RH\playground\history\2\220310.162840\CubeName\*.msk'
  for f in glob.glob(dir3):
      os.remove(f)
  
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(47, 44)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.WorkerW.ReBarWindow32.AddressBandRoot.progress.BreadcrumbParent.toolbar.ClickItemXY("‎Prefix_File", 28, 8, False)
  UIItem = dlgOpenProject.DUIViewWndClassName.Item.CtrlNotifySink2.ShellView.Item.F23RH
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\F23RH\\F23RH.tcpro")
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.centralwidget.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.bt_round_tool5.Click(81, 17)
  tomoAnalysis_ProjectManager_Plugins_ResultListWidget = tomoAnalysis.widget_popup
  tomoAnalysis_ProjectManager_Plugins_ResultListWidget.scrollArea.qt_scrollarea_viewport.Widget.resultTable.ClickCell("3", "Date")
  
  tomoAnalysis_ProjectManager_Plugins_ResultListWidget.bt_square_gray2.ClickButton()
  tomoAnalysis_ProjectManager_Plugins_ResultListWidget.Minimize()

@given("Report[T] 탭에 Projected area-Lipid droplet 그래프가 그려져 있다.")
def step_impl():
  tomoAnalysis_Report_Plugins_ParameterSelectionPanel = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.selectParameterWidget
  tomoAnalysis_Report_Plugins_ParameterSelectionPanel.selectParameterWidget.parametersGroupBox.RadioButton3.ClickButton()
  tomoAnalysis_Report_Plugins_ParameterSelectionPanel.selectOrganelleWidget.organellesGroupBox.RadioButton3.ClickButton()

@when("Report[T] 탭 Graph 패널의 점 한 개를 클릭하고 Report[T] 탭의 Edit mask 버튼을 누른다.")
def step_impl():
  widget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.graphWidget
  widget2 = widget.chartView.Widget
  widget2.Click(940, 106)
  widget2.Click(618, 133)
  widget.editBtn.ClickButton()

@then("Report[T] 탭에서 선택한 파일이 Mask Editor 탭으로 열렸는지 확인")
def step_impl():
  Regions.TomoAnalysis197.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis)

@when("Mask selector 패널에서 Cell instance 버튼을 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.MaskSelectionPanel.panel_base.panel_contents.instBtn.ClickButton()

@then("Mask Editor 탭에 Cell Instance 마스크가 생겼는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis, "WndCaption", cmpEqual, "TomoAnalysis")

@when("Mask selector 패널에서 Whole cell 버튼을 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.MaskSelectionPanel.panel_base.panel_contents.memBtn.ClickButton()

@then("Mask Editor 탭에 Whole cell 마스크가 생겼는지 확인")
def step_impl():
  Regions.TomoAnalysis198.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis)

@when("Mask selector 패널에서 Nucleus 버튼을 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.MaskSelectionPanel.panel_base.panel_contents.nucleBtn.ClickButton()

@then("Mask Editor 탭에 Nucleus 마스크가 생겼는지 확인")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis
  tomoAnalysis.Click(674, 339)
  tomoAnalysis.HoverMouse(-1)
  for i in range(5):
    tomoAnalysis.MouseWheel(1)
  Regions.TomoAnalysis199.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis)

@when("Mask selector 패널에서 Nucleolus 버튼을 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.MaskSelectionPanel.panel_base.panel_contents.nucliBtn.ClickButton()

@then("Mask Editor 탭에 Nucleolus 마스크가 생겼는지 확인")
def step_impl():
  Regions.TomoAnalysis200.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis)

@when("Mask selector 패널에서 Lipid droplet 버튼을 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.MaskSelectionPanel.panel_base.panel_contents.lipBtn.ClickButton()

@then("Mask Editor 탭에 Lipid droplet 마스크가 생겼는지 확인")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis
  tomoAnalysis.HoverMouse(-1)
  for i in range(1):
    tomoAnalysis.MouseWheel(1)
  Regions.TomoAnalysis201.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis)

@when("ID manager 패널의 Copy 버튼을 누르고 ‘test’를 입력한 후 OK 버튼을 클릭한다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  scrollArea = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.leftLayout.IDManagerPanel.panel_base.panel_contents.bt_round_gray5002.ClickButton()
  tomoAnalysis_InterSeg_Plugins_NewIdDialog = tomoAnalysis.NewIdDialog
  lineEdit = tomoAnalysis_InterSeg_Plugins_NewIdDialog.input_high
  lineEdit.Click(144, 23)
  lineEdit.SetText("test")
  tomoAnalysis_InterSeg_Plugins_NewIdDialog.bt_square_primary.ClickButton()

@when("Mask selector 패널에서 Lipid droplet 버튼을 누르고 Drawing tool 패널의 Paint 버튼을 클릭한 뒤 사이즈를 100으로 키우고 Mask Editor 탭 XY Slice 화면에서 마스크를 추가한다.")
def step_impl():
  tomoAnalysis_InterSeg_AppUI_MainWindow = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  widget = tomoAnalysis_InterSeg_AppUI_MainWindow.DrawingToolPanel.panel_base.panel_contents
  widget.PaintBtn.ClickButton()
  spinBox = widget.stackedWidget.BrushPage.BrushSizeSpin
  spinBox.qt_spinbox_lineedit.Drag(13, 10, -27, -1)
  spinBox.Keys("10")
  spinBox.wValue = 10
  spinBox.Keys("0")
  spinBox.wValue = 100
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis.Click(532, 277)

@when("Report[T] 탭 Graph 패널에서 점이 겹쳐진 부분을 클릭한다.")
def step_impl():
  widget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.graphWidget
  widget2 = widget.chartView.Widget
  widget2.Click(438, 126)
  widget2.Click(440, 129)
  doubleSpinBox = widget.yAxisMinSpinBox
  doubleSpinBox.qt_spinbox_lineedit.Drag(7, 9, 47, -4)
  doubleSpinBox.Keys("1.3")
  doubleSpinBox.wValue = 1.3
  doubleSpinBox.Keys("[Enter]")
  doubleSpinBox.wValue = 1.3
  widget2.Click(616, 64)

@when("Report[T] 탭 Graph 패널의 Current file 드롭다운 버튼을 클릭한다.")
def step_impl():
  widget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.graphWidget
  comboBox = widget.editMaskCombo
  comboBox.DropDown()
  widget.Click(662, 116)
  comboBox.Keys("[NumLock][Win][NumLock]S")
  NameMapping.Sys.Keys("[Hold][Win]!S[Release]")
  comboBox.Keys("![NumLock]![NumLock]")
  wndChrome_WidgetWin_1 = Aliases.slack.wndChrome_WidgetWin_18
  wndChrome_WidgetWin_1.Activate()
  chrome_RenderWidgetHostHWND = wndChrome_WidgetWin_1.Chrome_RenderWidgetHostHWND


@then("Report[T] 탭 Graph 패널의 Current file 드롭다운 버튼에 여러 개의 파일 리스트가 나타났는지 확인")
def step_impl():
  raise NotImplementedError

@given("TomoAnalysis에 1개 cube로 Report 탭이 열려 있다.")
def step_impl():
  raise NotImplementedError

@when("Report 탭 Data Table 패널의 테이블에서 가장 위에 있는 데이터를 클릭한다.")
def step_impl():
  raise NotImplementedError

@when("Report 탭의 Edit Mask 버튼을 클릭한다.")
def step_impl():
  raise NotImplementedError

@then("Mask Editor 탭이 열렸는지 확인")
def step_impl():
  raise NotImplementedError

@when("Mask Editor 탭 XY Slice 화면의 왼쪽 위 부분을 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis.Click(340, 202)

@then("Mask Editor 탭 XY Slice 화면 클릭한 위치에 Cell instance 마스크가 찍혔는지 확인")
def step_impl():
  Regions.TomoAnalysis156.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis)

@when("Drawing tool 패널의 Paint 버튼을 누르고 Mask Editor 탭 XY Slice 화면의 왼쪽 아래 부분을 클릭한다.")
def step_impl():
  aqUtils.Delay(3000)
  tomoAnalysis_InterSeg_AppUI_MainWindow = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.DrawingToolPanel.panel_base.panel_contents.PaintBtn.ClickButton()
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis.Click(293, 649)

@then("Mask Editor 탭 XY Slice 화면 클릭한 위치에 Whole cell 마스크가 찍혔는지 확인")
def step_impl():
  Regions.TomoAnalysis157.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis)

@when("Drawing tool 패널의 Paint 버튼을 누르고 Mask Editor 탭 XY Slice 화면의 오른쪽 위 부분을 클릭한다.")
def step_impl():
  aqUtils.Delay(3000)
  tomoAnalysis_InterSeg_AppUI_MainWindow = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  tomoAnalysis_InterSeg_AppUI_MainWindow.DrawingToolPanel.panel_base.panel_contents.PaintBtn.ClickButton()
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis.Click(788, 128)

@then("Mask Editor 탭 XY Slice 화면 클릭한 위치에 Nucleus 마스크가 찍혔는지 확인")
def step_impl():
  Regions.TomoAnalysis158.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis)

@when("Drawing tool 패널의 Paint 버튼을 누르고 Mask Editor 탭 XY Slice 화면의 오른쪽 아래 부분을 클릭한다.")
def step_impl():
  aqUtils.Delay(3000)
  tomoAnalysis_InterSeg_AppUI_MainWindow = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  tomoAnalysis_InterSeg_AppUI_MainWindow.DrawingToolPanel.panel_base.panel_contents.PaintBtn.ClickButton()
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis.Click(829, 546)

@then("Mask Editor 탭 XY Slice 화면 클릭한 위치에 Nucleolus 마스크가 찍혔는지 확인")
def step_impl():
  Regions.TomoAnalysis159.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis)

@when("Drawing tool 패널의 Paint 버튼을 누르고 Mask Editor 탭 XY Slice 화면의 중앙 부분을 클릭한다.")
def step_impl():
  aqUtils.Delay(3000)
  tomoAnalysis_InterSeg_AppUI_MainWindow = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  tomoAnalysis_InterSeg_AppUI_MainWindow.DrawingToolPanel.panel_base.panel_contents.PaintBtn.ClickButton()
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis.Click(546, 403)

@then("Mask Editor 탭 XY Slice 화면 클릭한 위치에 Lipid droplet 마스크가 찍혔는지 확인")
def step_impl():
  Regions.TomoAnalysis160.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis)

@when("Mask Selector Cell instance 아이콘을 클릭한다.")
def step_impl():
  raise NotImplementedError

@then("Mask Editor 탭 XY Slice 화면의 왼쪽 위 부분에 점으로 찍힌 마스크가 나타났는지 확인")
def step_impl():
  Regions.TomoAnalysis161.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis)


@when("Mask Selector Whole cell 아이콘을 클릭한다.")
def step_impl():
  raise NotImplementedError

@then("Mask Editor 탭 XY Slice 화면의 왼쪽 아래 부분에 점으로 찍힌 마스크가 나타났는지 확인")
def step_impl():
  Regions.TomoAnalysis162.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis)

@when("Mask Selector Nucleus 아이콘을 클릭한다.")
def step_impl():
  raise NotImplementedError

@then("Mask Editor 탭 XY Slice 화면의 오른쪽 위 부분에 점으로 찍힌 마스크가 나타났는지 확인")
def step_impl():
  Regions.TomoAnalysis163.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis)

@when("Mask Selector Nucleolus 아이콘을 클릭한다.")
def step_impl():
  raise NotImplementedError

@then("Mask Editor 탭 XY Slice 화면의 오른쪽 아래 부분에 점으로 찍힌 마스크가 나타났는지 확인")
def step_impl():
  Regions.TomoAnalysis164.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis)

@when("Mask Selector Lipid droplet 아이콘을 클릭한다.")
def step_impl():
  raise NotImplementedError

@then("Mask Editor 탭 XY Slice 화면의 중앙 부분에 점으로 찍힌 마스크가 나타났는지 확인")
def step_impl():
  Regions.TomoAnalysis165.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis)

@when("Drawing tool 패널의 Fill 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.DrawingToolPanel.panel_base.panel_contents.FillBtn.ClickButton()

@when("Mask Editor 탭 중앙의 XY Slice 화면을 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis.Click(519, 358)

@then("Mask Editor 탭에서 클릭한 slice에 Cell instance 마스크가 채워졌는지 확인")
def step_impl():
  Regions.TomoAnalysis166.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis)

@when("Mask Editor 탭 중앙의 XY Slice 화면에 마우스 포인터를 대고 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis.Click(487, 380)

@then("Mask Editor 탭에서 마우스 포인터로 클릭한 slice에 Whole cell 마스크가 생겼는지 확인")
def step_impl():
  Regions.TomoAnalysis167.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis)

@then("Mask selector 화면에 Whole cell 마스크가 사라졌는지 확인")
def step_impl():
  Regions.TomoAnalysis168.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis)

@then("지정한 파일의 경로에 .msk 형식의 Multi-Layer 마스크 파일이 생성됐는지 확인")
def step_impl():
  explorer = Aliases.explorer
  explorer.wndShell_TrayWnd.ReBarWindow32.MSTaskSwWClass.MSTaskListWClass.Click(30, 15)
  HWNDView = explorer.wndGenerated_File.Generated_File.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink2.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  UIItem = HWNDView.CtrlNotifySink.ShellView.Item.Regression_Test
  UIItem.Item.Click(117, 10)
  UIItem.Keys("[Enter]")
  aqObject.CheckProperty(Aliases.explorer.wndGenerated_File.Generated_File.DUIViewWndClassName.Item.CtrlNotifySink.ShellView.Item.HEK293FT_003P001_MultiLayer_test_msk.Item, "Enabled", cmpEqual, True)
  aqObject.CheckProperty(Aliases.explorer.wndGenerated_File.Generated_File.DUIViewWndClassName.Item.CtrlNotifySink.ShellView.Item.HEK293FT_003P001_MultiLayer_test_msk.Item, "Value", cmpEqual, "20201022.095301.707.HEK293FT-003P001_MultiLayer_test.msk")
  Aliases.explorer.wndGenerated_File.Close()
  
@then("지정한 파일의 경로에 .msk 형식의 Multi-Label 마스크 파일이 생성됐는지 확인")
def step_impl():
  explorer = Aliases.explorer
  explorer.wndShell_TrayWnd.ReBarWindow32.MSTaskSwWClass.MSTaskListWClass.Click(33, 10)
  HWNDView = explorer.wndGenerated_File.Generated_File.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink2.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  HWNDView.CtrlNotifySink.ShellView.Item.Regression_Test.Item.DblClick(84, 3)
  aqObject.CheckProperty(Aliases.explorer.wndGenerated_File.Generated_File.DUIViewWndClassName.Item.CtrlNotifySink.ShellView.Item.HEK293FT_003P001_MultiLayer_test_msk.Item, "Value", cmpEqual, "20201022.095301.707.HEK293FT-003P001_MultiLayer_test.msk")
  Aliases.explorer.wndGenerated_File.Close()

@when("{arg}{arg}.Hep G2{arg}_MultiLayer_test 파일을 선택한다.")
def step_impl(param1, param2, param3):
  dlgOpenMask = Aliases.TomoAnalysis.dlgOpenMask
  HWNDView = dlgOpenMask.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink2.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  UIItemsView = HWNDView.CtrlNotifySink.ShellView.Item
  UIItem = UIItemsView.Regression_Test
  UIItem.Item.Click(108, 12)
  UIItem.Keys("[Enter]")
  dlgOpenMask.OpenFile("E:\\Regression_Test\\20201022.095301.707.HEK293FT-003P001_MultiLayer_test.msk")

@when("{arg}{arg}.Hep G2{arg}_MultiLabel_test 파일을 선택한다.")
def step_impl(param1, param2, param3):
  dlgOpenMask = Aliases.TomoAnalysis.dlgOpenMask
  dlgOpenMask.OpenFile("E:\\Regression_Test\\20201022.095301.707.HEK293FT-003P001_MultiLabel_test.msk")

@when("Mask selector 패널 Whole cell 아이콘을 클릭하고 Drawing tool 패널의 Erase 버튼을 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.DrawingToolPanel.panel_base.panel_contents.EraseBtn.ClickButton()

@then("Mask Editor 탭에서 마우스 포인터로 클릭한 slice에서 Whole cell 마스크가 전부 지워졌는지 확인")
def step_impl():
  Regions.TomoAnalysis174.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis)

@then("Mask Editor 탭 2D Slice 화면의 Red FL이 흰색으로 바뀌었는지 확인")
def step_impl():
  Regions.RenderWindow6.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow)

@when("Drawing tool 패널의 Paint 버튼을 클릭하고 사이즈를 100으로 설정한 뒤 XY slice 화면을 클릭한다.")
def step_impl():
  tomoAnalysis_InterSeg_AppUI_MainWindow = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  widget = tomoAnalysis_InterSeg_AppUI_MainWindow.DrawingToolPanel.panel_base.panel_contents
  widget.PaintBtn.ClickButton()
  spinBox = widget.stackedWidget.BrushPage.BrushSizeSpin
  spinBox.qt_spinbox_lineedit.Drag(18, 14, -32, 2)
  spinBox.Keys("10")
  spinBox.wValue = 10
  spinBox.Keys("0")
  spinBox.wValue = 100
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis.Click(549, 367)

@when("Label tools 패널의 Add 글씨 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.LabelControlPanel.panel_base.panel_contents.bt_round_gray500.ClickButton()

@when("Mask Editor 탭 XY Slice 화면에서 빨간 마스크와 겹치지 않게 화면을 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis.Click(533, 222)

@then("XY Slice 화면의 빨간색 마스크가 남아 있는지 확인\\(스크린 샷)")
def step_impl():
  Regions.TomoAnalysis183.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis)

@when("Mask Editor 탭 XY Slice 화면의 초록색 마스크를 클릭한다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis
  tomoAnalysis.Click(502, 248)
  tomoAnalysis.Click(492, 239)
  aqUtils.Delay(3000)
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis.Click(533, 222)

@when("History 패널의 Undo 버튼을 누르고 Label tools 패널의 Merge label 버튼을 누른다.")
def step_impl():
  tomoAnalysis_InterSeg_AppUI_MainWindow = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  scrollArea = tomoAnalysis_InterSeg_AppUI_MainWindow.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.leftLayout.HistoryPanel.panel_base.panel_contents.UndoBtn.ClickButton()
  tomoAnalysis_InterSeg_AppUI_MainWindow.LabelControlPanel.panel_base.panel_contents.MergeLabelBtn.ClickButton()

@when("Lable tools 패널 Label 칸에 '{arg}’를 키보드로 입력한 뒤 Label tools 패널의 Label picker 버튼을 누른다.")
def step_impl(param1):
  widget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.LabelControlPanel.panel_base.panel_contents
  spinBox = widget.LabelSpin
  spinBox.qt_spinbox_lineedit.Click(10, 16)
  spinBox.Keys("[BS]2")
  spinBox.wValue = 2
  widget.PickBtn.ClickButton()

@then("Label tools 패널의 Add 글씨 버튼을 누르고 Mask Editor 탭의 XY Slice 화면에서 기존의 마스크와 겹치지 않게 마우스를 클릭한다.")
def step_impl():
  tomoAnalysis_InterSeg_AppUI_MainWindow = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  tomoAnalysis_InterSeg_AppUI_MainWindow.LabelControlPanel.panel_base.panel_contents.bt_round_gray500.ClickButton()
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis.Click(547, 65)

@when("Basic Analyzer[T] 탭의 Tool box에서 Whole Cell, Nucleus, Nucleolus 체크박스를 클릭한다.")
def step_impl():
  widget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.controlSocket
  widget.CheckBox.ClickButton(cbUnchecked)
  checkBox = widget.CheckBox2
  checkBox.Click(5, 9)
  checkBox.ClickButton(cbUnchecked)
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.tabWidget.qt_tabwidget_stackedwidget.singleRunTab.VizControlPanel.controlSocket.CheckBox3.ClickButton(cbUnchecked)

@when("Preview 패널에서 원하는 TCF 파일을 한 개 선택한다.")
def step_impl():
  tomoAnalysis_ProjectManager_AppUI_MainWindow = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  tomoAnalysis_ProjectManager_AppUI_MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(145, 48)

@when("Mask selector 패널에서 Lipid droplet 버튼을 누르고 Drawing tool 패널의 Paint 버튼을 클릭한 뒤 사이즈를 100으로 키우고 Mask Editor 탭 XY Slice 화면에서 마스크를 추가한다")
def step_impl():
  tomoAnalysis_InterSeg_AppUI_MainWindow = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  widget = tomoAnalysis_InterSeg_AppUI_MainWindow.DrawingToolPanel.panel_base.panel_contents
  pushButton = widget.PaintBtn
  pushButton.ClickButton()
  spinBox = widget.stackedWidget.BrushPage.BrushSizeSpin
  spinBox.qt_spinbox_lineedit.Click(13, 13)
  spinBox.Keys("[BS]1")
  spinBox.wValue = 1
  spinBox.Keys("0")
  spinBox.wValue = 10
  spinBox.Keys("0")
  spinBox.wValue = 100
  tomoAnalysis_InterSeg_AppUI_MainWindow.RenderWindow.Splitter.Widget.Widget.TomoAnalysis_InterSeg_Plugins_InterSegRenderWindow2D.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis.Click(540, 236)
  pushButton.ClickButton()

@when("Mask selector 패널에서 Lipid droplet 아이콘을 한 번 더 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.MaskSelectionPanel.panel_base.panel_contents.lipBtn.ClickButton()

@when("General 패널에서 Apply to Result 버튼을 클릭한다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  scrollArea = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.leftLayout.GeneralPanel.panel_base.panel_contents.SaveLinkBtn.ClickButton()
  Aliases.TomoAnalysis.DeleteDialog.bt_square_line.ClickButton()
@then("Report[T] 탭의 그래프 패널 그래프가 바뀌었는지 확인")
def step_impl():
  Regions.Widget63.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.graphReportPanel.graphWidget.chartView.Widget)

