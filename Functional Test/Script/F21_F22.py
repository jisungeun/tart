﻿import shutil
import os.path
from distutils.dir_util import copy_tree
import time
import glob
import re

@when("Preview 패널에서 FL 정보가 있는 TCF를 선택한다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox2.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(67, 87)

@then("Modality 패널의 FL 체크박스가 비어있는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget2.ModalityPanel.panel_contents.FLCheckBox, "checked", cmpEqual, False)

@when("Modality 패널의 FL 체크박스를 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget2.ModalityPanel.panel_contents.FLCheckBox.ClickButton(cbChecked)

@then("Modality 패널의 FL 체크박스가 체크 됐는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget2.ModalityPanel.panel_contents.FLCheckBox, "checked", cmpEqual, True)
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget2.ModalityPanel.panel_contents.FLCheckBox, "wState", cmpEqual, 1)

@then("3D Visualizer 탭 View 패널 3D, HT 화면에 FL Rendering이 생겼는지 확인")
def step_impl():
  Regions.TomoAnalysis104.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@when("Preset 패널의 FL 탭을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.ClickTab("FL")

@then("Preset 패널 FL 탭의 Blue 체크 박스가 선택 불가능한 상태인지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.FLChannelPanel.panel_contents.widget_3.ch1CheckBox, "Enabled", cmpEqual, False)

@then("Preset 패널 FL 탭의 Blue Opacity 사이즈 바가 조작 불가능한 상태인지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.FLChannelPanel.panel_contents.blueSlider, "wPosition", cmpEqual, 0)

@when("Preset 패널 FL 탭의 Green 체크박스를 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.FLChannelPanel.panel_contents.widget_3.greenCheckBox.ClickButton(cbUnchecked)

@then("3D Visualizer 탭 View 패널에서 초록색 형광의 Rendering이 모두 사라졌는지 확인")
def step_impl():
  Regions.TomoAnalysis105.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@then("Preset 패널의 FL 탭의 Green 체크박스가 체크 해제됐는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.FLChannelPanel.panel_contents.widget_3.greenCheckBox, "checked", cmpEqual, False)

@when("Preset 패널 FL 탭의 Green 체크박스를 다시 한 번 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.FLChannelPanel.panel_contents.widget_3.greenCheckBox.ClickButton(cbChecked)

@then("3D Visualizer 탭 View 패널에서 초록색 형광의 Rendering이 모두 생성됐는지 확인")
def step_impl():
  Regions.TomoAnalysis106.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@then("Preset 패널의 FL 탭의 Green 체크박스가 체크 됐는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.FLChannelPanel.panel_contents.widget_3.greenCheckBox, "checked", cmpEqual, True)

@when("Preset 패널 FL 탭의 Red 체크박스를 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.FLChannelPanel.panel_contents.widget_3.redCheckBox.ClickButton(cbUnchecked)

@then("3D Visualizer 탭 View 패널에서 빨간색 형광의 Rendering이 모두 사라졌는지 확인")
def step_impl():
  Regions.TomoAnalysis107.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@then("Preset 패널의 FL 탭의 Red 체크박스가 체크 해제됐는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.FLChannelPanel.panel_contents.widget_3.redCheckBox, "checked", cmpEqual, False)

@when("Preset 패널 FL 탭의 Red 체크박스를 다시 한 번 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.FLChannelPanel.panel_contents.widget_3.redCheckBox.ClickButton(cbChecked)

@then("3D Visualizer 탭 View 패널에서 빨간색 형광의 Rendering이 모두 생성됐는지 확인")
def step_impl():
  Regions.TomoAnalysis108.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@then("Preset 패널의 FL 탭의 Red 체크박스가 체크 됐는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.FLChannelPanel.panel_contents.widget_3.redCheckBox, "checked", cmpEqual, True)

@when("Preset 패널 FL 탭의 Hide All 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.FLChannelPanel.panel_contents.widget_3.bt_square_gray.Click(87, 13)

@then("3D Visualizer 탭 View 패널에서 형광의 Rendering이 모두 사라졌는지 확인")
def step_impl():
  Regions.TomoAnalysis109.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@then("Preset 패널의 FL 탭의 체크박스가 모두 체크 해제됐는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.FLChannelPanel.panel_contents.widget_3.greenCheckBox, "checked", cmpEqual, False)
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.FLChannelPanel.panel_contents.widget_3.redCheckBox, "checked", cmpEqual, False)

@when("Preset 패널 FL 탭의 Show All 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.FLChannelPanel.panel_contents.widget_3.bt_square_gray2.Click(103, 19)

@then("3D Visualizer 탭 View 패널에서 형광의 Rendering이 모두 나타났는지 확인")
def step_impl():
  Regions.TomoAnalysis110.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@then("Preset 패널의 FL 탭의 체크박스가 모두 체크 됐는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.FLChannelPanel.panel_contents.widget_3.greenCheckBox, "checked", cmpEqual, True)
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.FLChannelPanel.panel_contents.widget_3.redCheckBox, "checked", cmpEqual, True)

@when("Preset 패널 FL 탭의 Green Opacity 사이즈 바를 마우스로 왼쪽 끝까지 움직인다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.FLChannelPanel.panel_contents.greenSlider.wPosition = 0

@then("3D Visualizer 탭 View 패널에서 초록색 형광의 Rendering이 완전히 투명해졌는지 확인")
def step_impl():
  Regions.TomoAnalysis111.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@then("Preset 패널 FL 탭의 Green Opacity 사이즈 바 옆 수치칸에 값이 0으로 바뀌었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.FLChannelPanel.panel_contents.greenSpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "0")

@when("Preset 패널 FL 탭의 Green Opacity 사이즈 바를 마우스로 오른쪽 끝까지 움직인다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.FLChannelPanel.panel_contents.greenSlider.wPosition = 100

@then("3D Visualizer 탭 View 패널에서 초록색 형광의 Rendering이 완전히 진해졌는지 확인")
def step_impl():
  Regions.TomoAnalysis112.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@then("Preset 패널 FL 탭의 Green Opacity 사이즈 바 옆 수치칸에 값이 100으로 바뀌었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.FLChannelPanel.panel_contents.greenSpinBox, "value", cmpEqual, 100)

@when("Preset 패널 FL 탭의 Green Opacity 수치 입력 칸에 70을 입력한다.")
def step_impl():
  spinBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.FLChannelPanel.panel_contents.greenSpinBox
  spinBox.qt_spinbox_lineedit.Drag(34, 10, -38, 3)
  spinBox.Keys("70")
  spinBox.wValue = 70
  spinBox.Keys("[Enter]")
  spinBox.wValue = 70

@then("3D Visualizer 탭 View 패널에서 초록색 형광의 Rendering의 투명도가 바뀌었는지 확인")
def step_impl():
  Regions.TomoAnalysis113.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@when("Preset 패널 FL 탭의 Green Opacity 수치 입력 칸의 위 화살표를 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.FLChannelPanel.panel_contents.greenSpinBox.wValue = 71

@then("3D Visualizer 탭 View 패널에서 초록색 형광의 Rendering이 조금 진해졌는지 확인")
def step_impl():
  Regions.TomoAnalysis114.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@when("Preset 패널 FL 탭의 Green Opacity 수치 입력 칸의 아래 화살표를 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.FLChannelPanel.panel_contents.greenSpinBox.wValue = 70

@then("3D Visualizer 탭 View 패널에서 초록색 형광의 Rendering이 조금 연해졌는지 확인")
def step_impl():
  Regions.TomoAnalysis115.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@when("Preset 패널 FL 탭의 Red Opacity 사이즈 바를 마우스로 왼쪽 끝까지 움직인다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.FLChannelPanel.panel_contents.redSlider.wPosition = 0

@then("3D Visualizer 탭 View 패널에서 빨간색 형광의 Rendering이 완전히 투명해졌는지 확인")
def step_impl():
  Regions.TomoAnalysis116.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@then("Preset 패널 FL 탭의 Red Opacity 사이즈 바 옆 수치칸에 값이 0으로 바뀌었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.FLChannelPanel.panel_contents.redSpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "0")

@when("Preset 패널 FL 탭의 Red Opacity 사이즈 바를 마우스로 오른쪽 끝까지 움직인다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.FLChannelPanel.panel_contents.redSlider.wPosition = 100

@then("3D Visualizer 탭 View 패널에서 빨간색 형광의 Rendering이 완전히 진해졌는지 확인")
def step_impl():
  Regions.TomoAnalysis117.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@then("Preset 패널 FL 탭의 Red Opacity 사이즈 바 옆 수치칸에 값이 100으로 바뀌었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.FLChannelPanel.panel_contents.redSpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "100")

@when("Preset 패널 FL 탭의 Red Opacity 수치 입력 칸에 70을 입력한다.")
def step_impl():
  spinBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.FLChannelPanel.panel_contents.redSpinBox
  spinBox.qt_spinbox_lineedit.Drag(38, 11, -231, -1)
  spinBox.Keys("70")
  spinBox.wValue = 70

@then("3D Visualizer 탭 View 패널에서 빨간색 형광의 Rendering의 투명도가 바뀌었는지 확인")
def step_impl():
  Regions.TomoAnalysis118.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@when("Preset 패널 FL 탭의 Red Opacity 수치 입력 칸의 위 화살표를 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.FLChannelPanel.panel_contents.redSpinBox.wValue = 71

@when("Preset 패널 FL 탭의 Red Opacity 수치 입력 칸의 아래 화살표를 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.FLChannelPanel.panel_contents.redSpinBox.wValue = 70

@when("Preset 패널 FL 탭 하단의 table에서 Green 채널의 Min 셀에 150을 입력하고 키보드의 엔터 키를 누른다.")
def step_impl():
  tableWidget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.FLChannelPanel.panel_contents.channelTableWidget
  tableWidget.ClickCell("2", "Min")
  tableWidget.Keys("1")
  tableWidget.qt_scrollarea_viewport.ExpandingLineEdit.Keys("50[Enter]")

@then("3D Visualizer 탭 View 패널에서 파란색 형광 Rendering이 일부 사라지는지 확인")
def step_impl():
  Regions.TomoAnalysis121.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@when("Preset 패널 FL 탭 하단의 table에서 Green 채널의 Max 셀에 180을 입력하고 키보드의 엔터 키를 누른다.")
def step_impl():
  tableWidget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.FLChannelPanel.panel_contents.channelTableWidget
  tableWidget.ClickCell("2", "Min")
  tableWidget.Keys("1")
  tableWidget.qt_scrollarea_viewport.ExpandingLineEdit.Keys("80")

@then("3D Visualizer 탭 View 패널에서 초록색 형광 Rendering이 바뀌는지 확인")
def step_impl():
  Regions.TomoAnalysis122.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@when("Preset 패널 FL 탭 하단의 table에서 Red 채널의 Min 셀에 150을 입력하고 키보드의 엔터 키를 누른다.")
def step_impl():
  tableWidget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.FLChannelPanel.panel_contents.channelTableWidget
  tableWidget.ClickCell("3", "Min")
  tableWidget.Keys("150")
  tableWidget.qt_scrollarea_viewport.ExpandingLineEdit.Keys("[Enter]")

@then("3D Visualizer 탭 View 패널에서 빨간색 형광 Rendering이 일부 사라지는지 확인")
def step_impl():
  Regions.TomoAnalysis123.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@when("Preset 패널 FL 탭 하단의 table에서 Red 채널의 Max 셀에 180을 입력하고 키보드의 엔터 키를 누른다.")
def step_impl():
  tableWidget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.FLChannelPanel.panel_contents.channelTableWidget
  tableWidget.ClickCell("3", "Min")
  tableWidget.Keys("180")
  tableWidget.qt_scrollarea_viewport.ExpandingLineEdit.Keys("[Enter]")

@then("3D Visualizer 탭 View 패널에서 빨간색 형광 Rendering이 바뀌는지 확인")
def step_impl():
  Regions.TomoAnalysis124.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@when("Modality 패널의 FL 체크박스를 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget2.ModalityPanel.panel_contents.FLCheckBox.ClickButton(cbUnchecked)

@then("Modality 패널의 FL 체크박스가 체크 해제됐는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget2.ModalityPanel.panel_contents.FLCheckBox, "checked", cmpEqual, False)

@then("3D Visualizer 탭 View 패널 3D, HT 화면에 FL Rendering이 사라졌는지 확인")
def step_impl():
  Regions.TomoAnalysis125.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@given("3D Visualizer Modality 패널의 FL 체크박스가 체크 되어있다.")
def step_impl():

  dir = r'E:\Regression_Test\test*.*'
  for f in glob.glob(dir):
    os.remove(f)
  dir1 = r'E:\Regression_Test\*.png'
  for f in glob.glob(dir1):
    os.remove(f)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(88, 31)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\3D_Visualizer\\3D_Visualizer.tcpro")
  frame = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base
  widget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  frame = widget.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base
  scrollArea = frame.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox2.frame.QtObject("TomoAnalysis::ProjectManager::Plugins::LinkableThumbnailWidget", "", 2).QtObject("TC::TCF2DWidget", "", 1).QtObject("panel_contents_image").QtObject("QWidget", "", 1).Click(-1)
  frame.bt_round_tool4.Click(88, 15)
  widget.splitter.mainWidget.MainWindow.TC_TCDockWidget2.ModalityPanel.panel_contents.FLCheckBox.ClickButton(cbChecked)
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.ClickTab("FL")

@when("Preset 패널 FL 탭의 Blue 체크박스를 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.FLChannelPanel.panel_contents.widget_3.ch1CheckBox.Click(10,10)

@then("3D Visualizer 탭 View 패널에서 파란색 형광의 Rendering이 모두 사라졌는지 확인")
def step_impl():
  Regions.TomoAnalysis126.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@then("Preset 패널의 FL 탭의 Blue 체크박스가 체크 해제됐는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.FLChannelPanel.panel_contents.widget_3.ch1CheckBox, "checked", cmpEqual, False)

@then("3D Visualizer 탭 View 패널에서 파란색 형광의 Rendering이 모두 생성됐는지 확인")
def step_impl():
  Regions.TomoAnalysis127.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@then("Preset 패널의 FL 탭의 Blue 체크박스가 체크 됐는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.FLChannelPanel.panel_contents.widget_3.ch1CheckBox, "checked", cmpEqual, True)

@when("Preset 패널 FL 탭의 Blue Opacity 사이즈 바를 마우스로 왼쪽 끝까지 움직인다.")
def step_impl():
  raise NotImplementedError

@then("3D Visualizer 탭 View 패널에서 파란색 형광의 Rendering이 완전히 투명해졌는지 확인")
def step_impl():
  raise NotImplementedError

@then("Preset 패널 FL 탭의 Blue Opacity 사이즈 바 옆 수치칸에 값이 0으로 바뀌었는지 확인")
def step_impl():
  raise NotImplementedError

@when("Preset 패널 FL 탭의 Blue Opacity 사이즈 바를 마우스로 오른쪽 끝까지 움직인다.")
def step_impl():
  raise NotImplementedError

@then("3D Visualizer 탭 View 패널에서 파란색 형광의 Rendering이 완전히 진해졌는지 확인")
def step_impl():
  raise NotImplementedError

@then("Preset 패널 FL 탭의 Blue Opacity 사이즈 바 옆 수치칸에 값이 100으로 바뀌었는지 확인")
def step_impl():
  raise NotImplementedError

@when("Preset 패널 FL 탭의 Blue Opacity 수치 입력 칸에 70을 입력한다.")
def step_impl():
  raise NotImplementedError

@then("3D Visualizer 탭 View 패널에서 파란색 형광의 Rendering의 투명도가 바뀌었는지 확인")
def step_impl():
  raise NotImplementedError

@when("Preset 패널 FL 탭의 Blue Opacity 수치 입력 칸의 위 화살표를 누른다.")
def step_impl():
  raise NotImplementedError

@when("Preset 패널 FL 탭의 Blue Opacity 수치 입력 칸의 아래 화살표를 누른다.")
def step_impl():
  raise NotImplementedError

@when("Preset 패널 FL 탭 하단의 table에서 Blue 채널의 Min 셀에 150을 입력하고 키보드의 엔터 키를 누른다.")
def step_impl():
  tableWidget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.FLChannelPanel.panel_contents.channelTableWidget
  tableWidget.ClickCell("1", "Min")
  tableWidget.Keys("150")
  tableWidget.qt_scrollarea_viewport.ExpandingLineEdit.Keys("[Enter]")

@when("Preset 패널 FL 탭 하단의 table에서 Blue 채널의 Max 셀에 180을 입력하고 키보드의 엔터 키를 누른다.")
def step_impl():
  tableWidget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.FLChannelPanel.panel_contents.channelTableWidget
  tableWidget.ClickCell("1", "Max")
  tableWidget.Keys("180")
  tableWidget.qt_scrollarea_viewport.ExpandingLineEdit.Keys("[Enter]")

@then("3D Visualizer 탭 View 패널에서 파란색 형광 Rendering이 바뀌는지 확인")
def step_impl():
  Delay(10000)
  Regions.TomoAnalysis128.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@when("Viewing tool 패널의 View 드롭다운 버튼에서 TimeStamp 버튼을 누르고 Show TimeStamp 체크박스를 누른다.")
def step_impl():
  
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget4.ViewingToolPanel.panel_contents.bt_tool_round.ClickButtonXY(85, 18)
  Aliases.TomoAnalysis.Menu2.Activate()
  Aliases.TomoAnalysis.Menu2.Click(80, 200)
  Aliases.TomoAnalysis.Menu2.CheckBox.Click()

@then("3D Visualizer 탭 View 패널의 3D 화면에 Time stamp가 생겼는지 확인")
def step_impl():
  Regions.TomoAnalysis129.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@when("Viewing tool 패널의 View 드롭다운 버튼에서 TimeStamp 버튼을 누르고 Change Color 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget4.ViewingToolPanel.panel_contents.bt_tool_round.ClickButtonXY(85, 18)
  Aliases.TomoAnalysis.Menu2.Activate()
  Aliases.TomoAnalysis.Menu2.Click(80, 200)
  Aliases.TomoAnalysis.Menu2.QtMenu.Click("TimeStamp|Change Color")



@when("팝업에서 텍스트 색을 검은색으로 지정하고 OK 버튼을 누른다.")
def step_impl():
  colorDialog = tomoAnalysis.ColorDialog
  wellArray = colorDialog.WellArray
  wellArray.Click(11, 17)
  wellArray.Click(13, 15)
  colorDialog.DialogButtonBox.buttonOk.ClickButton()

@then("3D Visualizer 탭 View 패널의 3D 화면에 Time stamp 텍스트 색이 검은색으로 변경됐는지 확인")
def step_impl():
  Regions.TomoAnalysis130.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)
  
@when("팝업에서 텍스트 색을 흰색으로 지정하고 키보드의 엔터 키를 누른다.")
def step_impl():
  raise NotImplementedError

@then("View 패널의 3D 화면에 Time stamp 텍스트 색이 흰색으로 변경됐는지 확인")
def step_impl():
  raise NotImplementedError

@when("Viewing tool 패널의 View 드롭다운 버튼에서 TimeStamp 버튼을 누르고 Font Size 칸에 30을 입력한다.")
def step_impl():
  raise NotImplementedError

@then("3D Visualizer 탭 View 패널의 3D 화면에 Time stamp 폰트 크기가 변경됐는지 확인")
def step_impl():
  raise NotImplementedError

@when("Viewing tool 패널의 View 드롭다운 버튼에서 TimeStamp 버튼을 누르고 Font Size 칸의 위 화살표를 한 번 누른다.")
def step_impl():
  raise NotImplementedError

@then("3D Visualizer 탭 View 패널의 3D 화면에 Time stamp 폰트 사이즈가 31로 올라갔는지 확인")
def step_impl():
  raise NotImplementedError

@when("Viewing tool 패널의 View 드롭다운 버튼에서 TimeStamp 버튼을 누르고 Font Size 칸의 아래 화살표를 한 번 누른다.")
def step_impl():
  raise NotImplementedError

@then("3D Visualizer 탭 View 패널의 3D 화면에 Time stamp 폰트 사이즈가 30으로 내려갔는지 확인")
def step_impl():
  raise NotImplementedError

@given("3D Visualizer 탭에 TimeLapse 이미지의 rendering이 생성되어 있다.")
def step_impl():

  dir = r'E:\Regression_Test\test*.*'
  for f in glob.glob(dir):
    os.remove(f)
  dir1 = r'E:\Regression_Test\*.png'
  for f in glob.glob(dir1):
    os.remove(f)  
  dir2 = r'E:\Regression_Test\*.mp4'
  for f in glob.glob(dir2):
    os.remove(f)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(88, 31)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\3D_Visualizer\\3D_Visualizer.tcpro")
  tomoAnalysis = Aliases.TomoAnalysis
  widget = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow
  frame = widget.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base
  scrollArea = frame.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 133
  scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox3.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(75, 52)
  frame.bt_round_tool4.Click(37, 19)
  widget.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.MainWindow.dockwidget_inner.Widget.QCanvas2D.scrollArea.qt_scrollarea_viewport.scrollAreaWidgetContents.CanvasWidget.Canvas.Widget.Drag(51, 182, 141, 73)
  tomoAnalysis.ColorDialog.DialogButtonBox.buttonOk.ClickButton()

@when("Player 탭의 타임 바를 마우스로 클릭한 채로 오른쪽 끝까지 옮긴다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.panel_base.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.TCFPlayerPanel.panel_base.progressSlider.wPosition = 17

@then("3D Visualizer 탭 View 패널에서 2D HT 이미지와 3D rendering이 마지막 Time point로 움직였는지 확인")
def step_impl():
  Regions.TomoAnalysis133.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@then("Player 탭의 time point 칸의 값이 마지막 Time point로 바뀌었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.panel_base.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.TCFPlayerPanel.panel_base.progressSpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "17")

@when("Player 탭의 time point 칸에 2를 입력한다.")
def step_impl():
  spinBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.panel_base.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.TCFPlayerPanel.panel_base.progressSpinBox
  spinBox.qt_spinbox_lineedit.Drag(28, 18, -30, 0)
  spinBox.Keys("2[Enter]")
  spinBox.wValue = 2

@then("3D Visualizer 탭 View 패널에서 2D HT 이미지와 3D rendering이 Time point 2로 움직였는지 확인")
def step_impl():
  Regions.TomoAnalysis134.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)
  Regions.TomoAnalysis135.Check(Aliases.TomoAnalysis.mainwindow.TomoAnalysis5)

@then("Player 탭의 타임 바가 Time point 2로 움직였는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.panel_base.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.TCFPlayerPanel.panel_base.progressSpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "2")

@when("Player 탭의 time point 칸의 위 화살표를 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.panel_base.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.TCFPlayerPanel.panel_base.progressSpinBox.wValue = 3

@when("Movie maker 탭의 Add 버튼을 누르고 Timelapse 버튼을 누른다.")
def step_impl():
  raise NotImplementedError

@then("Movie maker 탭에 Timelapse 옵션이 추가되었는지 확인")
def step_impl():
  Regions.qt_scrollarea_viewport68.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.panel_base.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.VideoRecorderWidgetUI.panel_contents.scrollArea.qt_scrollarea_viewport.scrollVideoEdit.editWidget.table_tracks.HeaderView.qt_scrollarea_viewport)

@then("Movie maker 탭에 Time UI가 생겼는지 확인")
def step_impl():
  Regions.qt_scrollarea_viewport78.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.panel_base.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.VideoRecorderWidgetUI.panel_contents.scrollArea.qt_scrollarea_viewport.scrollVideoEdit.editWidget.table_tracks.HeaderView.qt_scrollarea_viewport)

@when("Movie maker 탭 Time UI의 Range 왼쪽 칸에 2를 입력한다.")
def step_impl():
  spinBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.panel_base.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.VideoRecorderWidgetUI.panel_contents.stackedWidget.timePage.timeScrollArea.qt_scrollarea_viewport.timeContents.timeMinSpinBox
  spinBox.qt_spinbox_lineedit.Drag(12, 7, -53, 3)
  spinBox.Keys("2")
  spinBox.wValue = 2
  spinBox.Keys("[Enter]")
  spinBox.wValue = 2

@when("Movie maker 탭 Time UI의 Range 오른쪽 칸에 3를 입력한다.")
def step_impl():
  spinBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.panel_base.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.VideoRecorderWidgetUI.panel_contents.stackedWidget.timePage.timeScrollArea.qt_scrollarea_viewport.timeContents.timeMaxSpinBox
  spinBox.qt_spinbox_lineedit.Drag(15, 11, -49, 3)
  spinBox.Keys("3[Enter]")
  spinBox.wValue = 3


@when("Movie maker 탭에서 Timelapse 효과 옵션 막대의 길이를 {arg}:00까지 늘린다.")
def step_impl(param1):
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.panel_base.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.VideoRecorderWidgetUI.panel_contents.scrollArea
  scrollBar = scrollArea.qt_scrollarea_hcontainer.ScrollBar
  scrollBar.wPosition = 0
  scrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.scrollVideoEdit.editWidget.table_tracks.qt_scrollarea_viewport.TC_RecordActionItem.handle_track_cut_right.Drag(9, 15, 329, -10)

@when("Movie maker 탭의 Play 버튼을 누른다.")
def step_impl():
  raise NotImplementedError

@when("3초 후 Movie maker 탭의 Stop 버튼을 누른다.")
def step_impl():
  raise NotImplementedError

@then("3D Visualizer 탭 View 패널에서 2D HT 이미지와 3D rendering이 움직였는지 확인")
def step_impl():
  raise NotImplementedError

@then("Movie maker 탭의 Stop 버튼을 누르고 3초 뒤에도 이미지가 변하지 않았는지 확인")
def step_impl():
  raise NotImplementedError

@when("Movie maker 탭 Time UI의 Range 왼쪽 칸의 위 화살표를 한 번 누른다.")
def step_impl():
  raise NotImplementedError

@then("Movie maker 탭 Time UI의 Range 왼쪽 칸의 값이 올라갔는지 확인")
def step_impl():
  raise NotImplementedError

@when("Movie maker 탭 Time UI의 Range 왼쪽 칸의 아래 화살표를 한 번 누른다.")
def step_impl():
  raise NotImplementedError

@then("Movie maker 탭 Time UI의 Range 왼쪽 칸의 값이 내려갔는지 확인")
def step_impl():
  raise NotImplementedError

@when("Movie maker 탭 Time UI의 Range 오른쪽 칸의 아래 화살표를 한 번 누른다.")
def step_impl():
  raise NotImplementedError

@then("Movie maker 탭 Time UI의 Range 오른쪽 칸의 값이 내려갔는지 확인")
def step_impl():
  raise NotImplementedError

@when("Movie maker 탭 Time UI의 Range 오른쪽 칸의 위 화살표를 한 번 누른다.")
def step_impl():
  raise NotImplementedError

@then("Movie maker 탭 Time UI의 Range 오른쪽 칸의 값이 올라갔는지 확인")
def step_impl():
  raise NotImplementedError

@when("Viewing tool 패널 View 드롭다운 버튼에서 TimeStamp 버튼을 누르고 Show TimeStamp 체크박스를 누른다.")
def step_impl():
  raise NotImplementedError

@when("Movie maker 탭 Time UI의 Reverse 체크박스를 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.panel_base.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.VideoRecorderWidgetUI.panel_contents.stackedWidget.timePage.timeScrollArea.qt_scrollarea_viewport.timeContents.timeCheckBox.ClickButton(cbChecked)

@when("Movie maker 탭의 Play 버튼을 누르고 3초 뒤 Stop 버튼을 누른다.")
def step_impl():
  raise NotImplementedError

@then("Movie maker 탭 Time UI의 Revere 체크박스가 체크 됐는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.panel_base.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.VideoRecorderWidgetUI.panel_contents.stackedWidget.timePage.timeScrollArea.qt_scrollarea_viewport.timeContents.timeCheckBox, "checked", cmpEqual, True)

@then("3D Visualizer 탭 View 패널 3D 화면의 Time stamp에서 time point가 뒤에서 앞으로 넘어가는지 확인")
def step_impl():
  raise NotImplementedError

@when("Movie maker 탭 Time UI의 Reverse 체크박스를 한번 더 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.panel_base.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.VideoRecorderWidgetUI.panel_contents.stackedWidget.timePage.timeScrollArea.qt_scrollarea_viewport.timeContents.timeCheckBox.ClickButton(cbUnchecked)

@then("Movie maker 탭 Time UI의 Revere 체크박스가 체크 해제됐는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.panel_base.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.VideoRecorderWidgetUI.panel_contents.stackedWidget.timePage.timeScrollArea.qt_scrollarea_viewport.timeContents.timeCheckBox, "checked", cmpEqual, False)

@when("Movie maker 탭의 Record 버튼을 누르고 XY Plane 버튼을 누른다.")
def step_impl():
  raise NotImplementedError

@when("파일 탐색기 팝업에 파일 이름 'A’를 입력하고 저장 경로를 지정한 뒤 저장 버튼을 누른다.")
def step_impl():
  raise NotImplementedError

@then("A동영상 파일이 지정한 경로에 생성되었는지 확인")
def step_impl():
  aqUtils.Delay(15000)
  wndCabinetWClass = Aliases.explorer.wndGenerated_File
  Aliases.explorer.wndShell_TrayWnd.ReBarWindow32.MSTaskSwWClass.MSTaskListWClass.Click(37, 12)
  HWNDView = wndCabinetWClass.Generated_File.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink2.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  UIItem = HWNDView.CtrlNotifySink.ShellView.Item.Regression_Test
  UIItem.Item.Click(74, 7)
  UIItem.Keys("[Enter]")
  aqObject.CheckProperty(Aliases.explorer.wndGenerated_File.Generated_File.DUIViewWndClassName.Item.CtrlNotifySink.ShellView.Item.A_mp42.Item, "Value", cmpEqual, "A.mp4")
  Aliases.explorer.wndGenerated_File.Close()
  
@then("B동영상 파일이 지정한 경로에 생성되었는지 확인")
def step_impl():
  aqUtils.Delay(17000)
  wndCabinetWClass = Aliases.explorer.wndGenerated_File
  Aliases.explorer.wndShell_TrayWnd.ReBarWindow32.MSTaskSwWClass.MSTaskListWClass.Click(37, 12)
  HWNDView = wndCabinetWClass.Generated_File.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink2.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  UIItem = HWNDView.CtrlNotifySink.ShellView.Item.Regression_Test
  UIItem.Item.Click(74, 7)
  UIItem.Keys("[Enter]")
  aqObject.CheckProperty(Aliases.explorer.wndGenerated_File.Generated_File.DUIViewWndClassName.Item.CtrlNotifySink.ShellView.Item.B_mp4.Item2, "Value", cmpEqual, "B.mp4")
  Aliases.explorer.wndGenerated_File.Close()
  
@then("생성된 영상과 3D Visualizer 탭 View 패널의 2D XY HT 영상이 같은지 확인")
def step_impl():
  raise NotImplementedError

@when("Movie maker 탭의 Record 버튼을 누르고 YZ Plane 버튼을 누른다.")
def step_impl():
  raise NotImplementedError

@when("파일 탐색기 팝업에 파일 이름 {arg}를 입력하고 저장 버튼을 누른다.")
def step_impl(param1):
  dlgSaveVideoFile = Aliases.TomoAnalysis.dlgSaveVideoFile
  HWNDView = dlgSaveVideoFile.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  UIItem = HWNDView.CtrlNotifySink2.ShellView.Item.Regression_Test
  UIItem.Item.Click(79, 16)
  UIItem.Keys("[Enter]")
  comboBox = HWNDView.FloatNotifySink.ComboBox
  comboBox.Edit.Drag(279, 7, -53, 3)
  comboBox.SetText(param1)
  dlgSaveVideoFile.btn_S.ClickButton()

@then("생성된 영상과 3D Visualizer 탭 View 패널의 2D YZ HT 영상이 같은지 확인")
def step_impl():
  raise NotImplementedError

@when("Movie maker 탭의 Record 버튼을 누르고 XZ Plane 버튼을 누른다.")
def step_impl():
  raise NotImplementedError

@then("생성된 영상과 3D Visualizer 탭 View 패널의 2D XZ HT 영상이 같은지 확인")
def step_impl():
  raise NotImplementedError

@when("Movie maker 탭의 Record 버튼을 누르고 3D 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.panel_base.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.VideoRecorderWidgetUI.panel_contents.btnScrollArea.qt_scrollarea_viewport.btnContents.bt_square_gray700.Click(44, 8)
  tomoAnalysis.Menu.QtMenu.Click("3D")

@when("Movie maker 탭의 Record 버튼을 누르고 Multi view 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.panel_base.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.VideoRecorderWidgetUI.panel_contents.btnScrollArea.qt_scrollarea_viewport.btnContents.bt_square_gray700.Click(70, 7)
  tomoAnalysis.Menu.QtMenu.Click("Multi-view")

@given("HeLa-Rab5a-Rab7a{arg} TCF가 프로젝트에 불러져 있다.")
def step_impl(param1):
  raise NotImplementedError

@when("Preview 패널에서 HeLa-Rab5a-Rab7a{arg} 데이터를 선택한다.")
def step_impl(param1):
  raise NotImplementedError

@then("3D Visualizer 탭 View 패널에서 초록색 형광 Rendering이 바뀌는지 확인1")
def step_impl():
  raise NotImplementedError

@then("3D Visualizer 탭 View 패널에서 초록색 형광 Rendering이 바뀌는지 확인한다")
def step_impl():
  Regions.TomoAnalysis131.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@when("Player 탭의 time point 칸에 3을 입력한다.")
def step_impl():
  spinBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.panel_base.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.TCFPlayerPanel.panel_base.progressSpinBox
  spinBox.qt_spinbox_lineedit.Drag(27, 8, -59, 2)
  spinBox.Keys("3")
  spinBox.wValue = 3
  spinBox.Keys("[Enter]")
  spinBox.wValue = 3

@then("3D Visualizer 탭 View 패널에서 2D HT 이미지와 3D rendering이 Time point 3으로 움직였는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.panel_base.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.TCFPlayerPanel.panel_base.progressSpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "3")

@then("Player 탭의 타임 바가 Time point 3으로 움직였는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.panel_base.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.TCFPlayerPanel.panel_base.progressSlider, "wPosition", cmpEqual, 3)

@when("Movie maker 탭을 클릭하고 Add 버튼을 누른 뒤 Timelapse 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  TC_AdaptiveTabWidget = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.panel_base.TC_AdaptiveTabWidget
  TC_AdaptiveTabWidget.TC_TabBar.Click(167, 6)
  TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.VideoRecorderWidgetUI.panel_contents.bt_round_gray700.Drag(31, 9, -5, 0)

  Sys.Desktop.MouseY =+ 150
  Sys.Desktop.MouseDown(1,Sys.Desktop.Mousex,Sys.Desktop.Mousey)
  
  tomoAnalysis.Menu.Widget.bt_square_gray700.ClickButton()

@when("파일 탐색기 팝업에 파일 이름 A를 입력하고 저장 버튼을 누른다.")
def step_impl():
  raise NotImplementedError

@when("파일 탐색기 팝업에 파일 이름 B를 입력하고 저장 버튼을 누른다.")
def step_impl():
  raise NotImplementedError

@when("Movie maker 탭을 클릭하고 Add 버튼을 누른 뒤 Timelapse 시간 설정 창에서 add 클릭")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  TC_AdaptiveTabWidget = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget
  TC_AdaptiveTabWidget2 = TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.panel_base.TC_AdaptiveTabWidget
  TC_AdaptiveTabWidget2.TC_TabBar.Click(191, 15)
  TC_AdaptiveTabWidget2.qt_tabwidget_stackedwidget.VideoRecorderWidgetUI.panel_contents.bt_round_gray700.ClickButton()
  Sys.Desktop.MouseY += 110;
  Sys.Desktop.MouseDown(1,Sys.Desktop.Mousex,Sys.Desktop.Mousey)
  tomoAnalysis.Menu.Widget.bt_square_gray700.ClickButton()
  TC_AdaptiveTabWidget.Click(504, 33)
