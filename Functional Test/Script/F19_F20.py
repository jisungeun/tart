﻿import shutil
import os.path
from distutils.dir_util import copy_tree
import time
import glob
import re


@when("Preview 패널에서 TCF를 두 개 선택한다.")
def step_impl():
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea
  scrollBar = scrollArea.qt_scrollarea_vcontainer.ScrollBar
  scrollBar.wPosition = 0
  frame = scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame
  frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(126, 68)
  scrollBar.wPosition = 0
  scrollArea = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea
  scrollArea.qt_scrollarea_vcontainer.ScrollBar.wPosition = 0
  scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox2.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Widget.Click(102, 89)


@when("Toolbox의 3D Visualization 버튼을 누른다.")
def step_impl():
  frame = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base
  frame.tabWidget.Click(662, 2)
  frame.bt_round_tool4.Click(63, 12)

@then("3D Visualizer 탭이 열리는지 확인")
def step_impl():
  Regions.F19_1.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.TC_TabBar)

@then("3D Visualizer 탭 View 패널의 HT 사진과 Project Manager 탭 Preview 패널에서 마지막에 선택한 사진이 같은지 확인")
def step_impl():
  Regions.TomoAnalysis64.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer.Widget.Splitter.Splitter.TomoAnalysis_Viewer_Plugins_ViewerRenderWindow2d.SoQtGLMgr.SoQtGLWidget_InternalWidget.TomoAnalysis)

@then("‘Select tcf files to view’ 라는 내용의 Error 팝업이 나타났는지 확인")
def step_impl():
  Aliases.TomoAnalysis.MessageBox7.qt_msgbox_buttonbox.buttonOk.ClickButton()

@given("3D Visualizer 탭이 열려 있다.")
def step_impl():

  dir = r'E:\Regression_Test\test\test*.*'
  for f in glob.glob(dir):
    os.remove(f)
  dir1 = r'E:\Regression_Test\*.png'
  for f in glob.glob(dir1):
    os.remove(f)
  dir2 = r'E:\*.png'
  for f in glob.glob(dir2):
    os.remove(f)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(88, 31)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\3D_Visualizer\\3D_Visualizer.tcpro")
  frame = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base

  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Click(-1)

  frame.bt_round_tool4.Click(77, 12)
  Regions.AddPicture(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer,"1")


@when("Preset 패널의 TF canvas 위의 하얀색 영역에서 마우스를 클릭한 채로 드래그한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.MainWindow.dockwidget_inner.Widget.QCanvas2D.scrollArea.qt_scrollarea_viewport.scrollAreaWidgetContents.CanvasWidget.Canvas.Widget.Drag(34, 234, 21, 21)

@when("Select Color 팝업에서 원하는 색을 지정하고 OK 버튼을 누른다.")
def step_impl():
  colorDialog = Aliases.TomoAnalysis.ColorDialog
  colorDialog.WellArray.Click(70, 62)
  colorDialog.DialogButtonBox.buttonOk.ClickButton()

@then("TF canvas의 드래그 한 영역에 지정한 색의 TF box가 나타났는지 확인")
def step_impl():
  Regions.Widget36.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.MainWindow.dockwidget_inner.Widget.QCanvas2D.scrollArea.qt_scrollarea_viewport.scrollAreaWidgetContents.CanvasWidget.Canvas.Widget)

@then("3D Visualizer 탭의 View 패널에 Rendering이 생기는지 확인")
def step_impl():
  Regions.AddPicture(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer,"2")
  if not Regions.Compare("1","2",False,False,True,0,lmNone):
    Log.Message("이미지가 바뀜")
  Regions.AddPicture(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer,"2")
  
@then("3D Visualizer 탭 Preset 패널의 parameter들이 생성된 TF box에 따라 바뀌는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.RIMinSpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "1.3416")
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.RIMaxSpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "1.3469")
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.gradMinSpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "1.54")
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.gradMaxSpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "18.51")

@when("Preset 패널의 TF canvas 위의 하얀색 영역에서 마우스를 클릭한 채로 겹치지 않게 드래그한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.MainWindow.dockwidget_inner.Widget.QCanvas2D.scrollArea.qt_scrollarea_viewport.scrollAreaWidgetContents.CanvasWidget.Canvas.Widget.Drag(75, 230, 29, 18)

@when("Select Color 팝업에서 원하는 색을 지정하고 키보드의 엔터 키를 누른다.")
def step_impl():
  wellArray = Aliases.TomoAnalysis.ColorDialog.WellArray
  wellArray.Click(127, 86)
  wellArray.Keys("[NumLock][Enter]")

@then("TF canvas의 드래그 한 영역에 지정한 색의 TF box가 생겼는지 확인")
def step_impl():
  Regions.Widget37.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.MainWindow.dockwidget_inner.Widget.QCanvas2D.scrollArea.qt_scrollarea_viewport.scrollAreaWidgetContents.CanvasWidget.Canvas.Widget)

@then("3D Visualizer 탭의 View 패널에 Rendering이 추가됐는지 확인")
def step_impl():
  Regions.AddPicture(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer,"3")
  if not Regions.Compare("3","2",False,False,True,0,lmNone):
    Log.Message("이미지가 바뀜")

@then("3D Visualizer 탭 Preset 패널의 parameter들이 생성된 TF box에 따라 바뀌었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.RIMinSpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "1.3515")
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.RIMaxSpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "1.3587")
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.gradMinSpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "6.94")
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.gradMaxSpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "21.60")

@when("TF canvas 내의 빈 공간에 마우스 포인터를 가져가서 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.MainWindow.dockwidget_inner.Widget.QCanvas2D.scrollArea.qt_scrollarea_viewport.scrollAreaWidgetContents.CanvasWidget.Canvas.Widget.Click(204, 143)

@then("가장 최근에 추가한 TF box의 테두리에서 녹색 점이 사라졌는지 확인")
def step_impl():
  Regions.Widget38.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.MainWindow.dockwidget_inner.Widget.QCanvas2D.scrollArea.qt_scrollarea_viewport.scrollAreaWidgetContents.CanvasWidget.Canvas.Widget)

@then("3D Visualizer 탭 Preset 패널 하단의 table의  TF box 색 row에서 하이라이트가 제거되었는지 확인")
def step_impl():
  Regions.Widget39.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.MainWindow.dockwidget_inner.Widget.QCanvas2D.scrollArea.qt_scrollarea_viewport.scrollAreaWidgetContents.CanvasWidget.Canvas.Widget)

@when("TF canvas에서 생성되어있는 TF box 한 개를 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.MainWindow.dockwidget_inner.Widget.QCanvas2D.scrollArea.qt_scrollarea_viewport.scrollAreaWidgetContents.CanvasWidget.Canvas.Widget.Click(82, 239)

@then("클릭한 TF box 테두리의 녹색 점이 생성됐는지 확인")
def step_impl():
  Regions.Widget40.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.MainWindow.dockwidget_inner.Widget.QCanvas2D.scrollArea.qt_scrollarea_viewport.scrollAreaWidgetContents.CanvasWidget.Canvas.Widget)

@then("3D Visualizer 탭 Preset 패널 하단 table의 TF box 색 row가 하이라이트 되었는지 확인")
def step_impl():
  Regions.qt_scrollarea_viewport65.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.itemTableWidget.qt_scrollarea_viewport)

@when("3D Visualizer 탭 Preset 패널 하단 table에서 선택되지 않은 TF box 색으로 칠해진 row를 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.itemTableWidget.qt_scrollarea_viewport.Label.Click(32, 26)

@then("클릭한 row 색의 TF box 테두리의 녹색 점이 생성됐는지 확인")
def step_impl():
  Regions.Widget41.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.MainWindow.dockwidget_inner.Widget.QCanvas2D.scrollArea.qt_scrollarea_viewport.scrollAreaWidgetContents.CanvasWidget.Canvas.Widget)

@when("TF box 테두리에 생성된 녹색 점을 클릭한 채로 마우스를 드래그한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.MainWindow.dockwidget_inner.Widget.QCanvas2D.scrollArea.qt_scrollarea_viewport.scrollAreaWidgetContents.CanvasWidget.Canvas.Widget.Drag(56, 236, 16, -19)

@then("TF box의 크기가 달라졌는지 확인")
def step_impl():
  Regions.Widget42.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.MainWindow.dockwidget_inner.Widget.QCanvas2D.scrollArea.qt_scrollarea_viewport.scrollAreaWidgetContents.CanvasWidget.Canvas.Widget)

@then("3D Visualizer 탭 Preset 패널의 Parameter가 수정된 TF box에 따라 바뀌었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.RIMinSpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "1.3416")
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.RIMaxSpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "1.3508")
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.gradMinSpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "1.54")
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.gradMaxSpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "31.62")

@then("3D Visualizer 탭의 View 패널에서 표시되는 Rendering이 달라졌는지 확인")
def step_impl():
  Regions.AddPicture(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer,"4")
  if not Regions.Compare("3","4",False,False,True,0,lmNone):
    Log.Message("이미지가 바뀜")

@then("3D Visualizer 탭 Preset 패널의 Parameter가 수정된 TF box에 맞게 바뀌었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.RIMinSpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "1.3489")
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.RIMaxSpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "1.3580")
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.gradMinSpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "97.19")
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.gradMaxSpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "127.27")

    
@when("선택된 TF box의 내부 영역을 마우스로 클릭한 채로 TF canvas의 위쪽 검은색 영역으로 드래그한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.MainWindow.dockwidget_inner.Widget.QCanvas2D.scrollArea.qt_scrollarea_viewport.scrollAreaWidgetContents.CanvasWidget.Canvas.Widget.Drag(52, 230, 30, -124)

@then("3D Visualizer 탭의 View 패널에서 표시되는 해당 TF box의 Rendering이 사라졌는지 확인")
def step_impl():
  Regions.AddPicture(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer,"5")
  if not Regions.Compare("5","4",False,False,True,0,lmNone):
    Log.Message("이미지가 바뀜")

@when("3D Visualizer 탭 Preset 패널의 Save As 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.VisualizationListPanel.panel_base.bt_round_gray700.ClickButton()

@when("파일 탐색기에 파일 이름과 파일 경로를 입력한다.")
def step_impl():
  HWNDView = Aliases.TomoAnalysis.dlg2.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  UIItemsView = HWNDView.CtrlNotifySink2.ShellView.Item
  UIItem = UIItemsView.Regression_Test
  Aliases.TomoAnalysis.dlg2.DUIViewWndClassName.Item.CtrlNotifySink2.ShellView.Item.Regression_Test.Item.DblClick()
  comboBox = HWNDView.FloatNotifySink.ComboBox
  comboBox.Edit.Click(144, 11)
  Aliases.TomoAnalysis.dlg2.DUIViewWndClassName.Item.CtrlNotifySink2.ShellView.Item.test.Item.DblClick(38, 18)
  comboBox.SetText("test")
  Aliases.TomoAnalysis.dlg2.btn_S.ClickButton()



@then("입력한 폴더에 지정한 이름의 .xml 파일이 생성됐는지 확인")
def step_impl():
  explorer = Aliases.explorer
  explorer.wndShell_TrayWnd.ReBarWindow32.MSTaskSwWClass.MSTaskListWClass.Click(47, 21)
  wndCabinetWClass = explorer.wndGenerated_File
  HWNDView = wndCabinetWClass.Generated_File.DUIViewWndClassName.Item
  tv_ = HWNDView.CtrlNotifySink2.NamespaceTreeControl.tv_
  tv_.MouseWheel(-4)
  tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  HWNDView.CtrlNotifySink.ShellView.Item.Regression_Test.Item.DblClick(62, 12)
  Aliases.explorer.wndGenerated_File.Generated_File.DUIViewWndClassName.Item.CtrlNotifySink.ShellView.Item.test.Item.DblClick(70, 9)
  aqObject.CheckProperty(Aliases.explorer.wndGenerated_File.Generated_File.DUIViewWndClassName.Item.CtrlNotifySink.ShellView.Item.test_xml.Item, "Value", cmpEqual, "test.xml")
  wndCabinetWClass.Close()


@then("3D Visualizer 탭의 Preset 패널 상단 드롭다운 버튼에 지정한 이름의 프리셋이 나타나는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.VisualizationListPanel.panel_base.listComboBox, "wText", cmpEqual, "test")

@when("3D Visualizer 탭 Preset 패널의 Load 버튼을 누른다.")
def step_impl():
  dir1 = r'E:\Regression_Test\test1.xml'
  dir2 = r'E:\Regression_Test\Prefix_File\backup\test1.xml'
  shutil.copy(dir2, dir1)
  
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.VisualizationListPanel.panel_base.bt_round_gray7002.ClickButton()

@when("파일 탐색기에서 .xml 파일을 선택한다.")
def step_impl():
  dlgSelectAPresetFile = Aliases.TomoAnalysis.dlgSelectAPresetFile
  HWNDView = Aliases.TomoAnalysis.dlgSelectAPresetFile.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink2.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  UIItemsView = HWNDView.CtrlNotifySink.ShellView.Item
  UIItem = UIItemsView.Regression_Test
  UIProperty = UIItem.Item
  UIProperty.Click(46, 11)
  UIProperty.Click(46, 11)
  UIItem.Click(178, 6)
  UIProperty.Click(156, 6)
  Aliases.TomoAnalysis.dlgSelectAPresetFile.DUIViewWndClassName.Item.CtrlNotifySink.ShellView.Item.Regression_Test.Item.DblClick()
  UIItemsView.TestProject1.Item.MouseWheel(-6)
  dlgSelectAPresetFile.OpenFile("E:\\Regression_Test\\test1.xml")


@then("3D Visualizer 탭의 Preset 패널 상단 드롭다운 버튼에 불러온 프리셋이 나타나는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.VisualizationListPanel.panel_base.listComboBox, "wText", cmpEqual, "test1")

@then("TF canvas에 프리셋의 TF box가 생성됐는지 확인")
def step_impl():
  Regions.Widget43.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.MainWindow.dockwidget_inner.Widget.QCanvas2D.scrollArea.qt_scrollarea_viewport.scrollAreaWidgetContents.CanvasWidget.Canvas.Widget)

@then("View 패널에 Rendering이 생성됐는지 확인")
def step_impl():
  Regions.TomoAnalysis67.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@when("TF canvas의 하얀색 영역에서 겹치지 않게 TF box를 새로운 색으로 추가한다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.MainWindow.dockwidget_inner.Widget.QCanvas2D.scrollArea.qt_scrollarea_viewport.scrollAreaWidgetContents.CanvasWidget.Canvas.Widget.Drag(133, 207, 36, 38)
  tomoAnalysis.ColorDialog.DialogButtonBox.buttonOk.ClickButton()
  Regions.AddPicture(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer,"1")

@when("3D Visualizer 탭 Preset 패널의 Update 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.VisualizationListPanel.panel_base.bt_round_gray500.ClickButton()

@when("3D Visualizer 탭의 x버튼을 눌러 종료한다.")
def step_impl():
  TC_TabBar = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.TC_TabBar
  TC_TabBar.Click(297, 35)
  TC_TabBar.CloseButton.Click(11, 15)

@when("Preview 패널에서 TCF를 선택하고 다시 3D Visualizer를 연다.")
def step_impl():
  frame = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base
  
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Click(-1)

  frame.bt_round_tool4.Click(77, 12)
  Regions.AddPicture(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer,"1")


@when("3D Visualizer 탭 Preset 패널 상단의 드롭다운 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.VisualizationListPanel.panel_base.listComboBox.DropDown()

@then("3D Visualizer 탭 Preset 패널 상단 드롭다운 버튼에 지정된 경로의 프리셋 목록이 나타나는지 확인")
def step_impl():
  Regions.qt_scrollarea_viewport66.Check(Aliases.TomoAnalysis.ComboBoxPrivateContainer.ComboBoxListView.qt_scrollarea_viewport)

@when("3D Visualizer 탭 Preset 패널 상단 드롭다운 버튼의 목록에서 업데이트한 프리셋을 선택한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.VisualizationListPanel.panel_base.listComboBox.ClickItem("test1")

@then("TF Canvas에 수정한 TF box가 나오는지 확인")
def step_impl():
  Regions.Widget44.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.MainWindow.dockwidget_inner.Widget.QCanvas2D.scrollArea.qt_scrollarea_viewport.scrollAreaWidgetContents.CanvasWidget.Canvas.Widget)

@then("3D Visualizer 탭의 View 패널에 수정한 Rendering이 나오는지 확인")
def step_impl():
  Regions.AddPicture(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer,"2")
  if Regions.Compare("1","2",False,False,True,0,lmNone):
    Log.Message("이미지가 같음")

@when("3D Visualizer 탭 Preset 패널의 드롭다운 버튼에서 프리셋을 선택하고 Delete 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.VisualizationListPanel.panel_base.bt_round_gray7003.ClickButton()

@then("3D Visualizer 탭 Preset 패널의 드롭다운 버튼이 Choose a TF preset 으로 바뀌는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.VisualizationListPanel.panel_base.listComboBox, "wText", cmpEqual, "Choose a TF preset")

@then("삭제한 프리셋이 3D Visualizer 탭 Preset 패널의 드롭다운 버튼에서 사라졌는지 확인")
def step_impl():
  widget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.VisualizationListPanel.panel_base
  widget.Click(196, 28)
  widget.listComboBox.DropDown()
  Regions.qt_scrollarea_viewport67.Check(Aliases.TomoAnalysis.ComboBoxPrivateContainer.ComboBoxListView.qt_scrollarea_viewport)


@then("3D Visualizer 탭의 Rendering과 TF box는 변화 없이 남아 있는지 확인")
def step_impl():
  Regions.AddPicture(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer,"2")
  if Regions.Compare("1","2",False,False,True,0,lmNone):
    Log.Message("이미지가 같음")
  Regions.Widget45.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.MainWindow.dockwidget_inner.Widget.QCanvas2D.scrollArea.qt_scrollarea_viewport.scrollAreaWidgetContents.CanvasWidget.Canvas.Widget)


@given("3D Visualizer 탭에서 TF box가 선택되어 있다.")
def step_impl():

  dir = r'E:\Regression_Test\test*.*'
  for f in glob.glob(dir):
    os.remove(f)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(88, 31)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\3D_Visualizer\\3D_Visualizer.tcpro")
  frame = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base

  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Click(-1)

  frame.bt_round_tool4.Click(77, 12)

  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.MainWindow.dockwidget_inner.Widget.QCanvas2D.scrollArea.qt_scrollarea_viewport.scrollAreaWidgetContents.CanvasWidget.Canvas.Widget.Drag(34, 234, 21, 21)

  colorDialog = Aliases.TomoAnalysis.ColorDialog
  colorDialog.WellArray.Click(70, 62)
  colorDialog.DialogButtonBox.buttonOk.ClickButton()
  Regions.AddPicture(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer,"1")

    
@when("3D Visualizer 탭 Preset 패널의 RI Range 왼쪽 칸에 1.3400을 입력한다.")
def step_impl():
  doubleSpinBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.RIMinSpinBox
  doubleSpinBox.qt_spinbox_lineedit.Drag(47, 5, -138, 2)
  doubleSpinBox.Keys("1.34")
  doubleSpinBox.wValue = 1.3400000000000001
  doubleSpinBox.Keys("00")

@when("3D Visualizer 탭 Preset 패널의 RI Range 오른쪽 칸에 1.3700을 입력한다.")
def step_impl():
  doubleSpinBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.RIMaxSpinBox
  doubleSpinBox.qt_spinbox_lineedit.Drag(47, 11, -132, -12)
  doubleSpinBox.Keys("1.37")
  doubleSpinBox.wValue = 1.3700000000000001
  doubleSpinBox.Keys("00")

@then("선택된 TF box의 x축 길이가 달라졌는지 확인")
def step_impl():
  Regions.Widget46.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.MainWindow.dockwidget_inner.Widget.QCanvas2D.scrollArea.qt_scrollarea_viewport.scrollAreaWidgetContents.CanvasWidget.Canvas.Widget)

@then("3D Visualizer 탭 View 패널에서 Rendering이 달라졌는지 확인")
def step_impl():
  Regions.AddPicture(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer,"2")
  if not Regions.Compare("1","2",False,False,True,0,lmNone):
    Log.Message("이미지가 다름")

@when("3D Visualizer 탭 Preset 패널의 왼쪽 RI Range 수치 입력 칸의 위 화살표 버튼을 한 번 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.RIMinSpinBox.wValue = 1.3401

@then("3D Visualizer 탭 Preset 패널 하단 table의 해당 TF box 색 row에서 RI Min 값이 0.0001만큼 올라갔는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.itemTableWidget.qt_scrollarea_viewport.Label2, "QtText", cmpEqual, "1.3401")

@then("3D Visualizer 탭 Preset 패널의 왼쪽 RI Range 수치 입력 칸의 수치가 0.0001만큼 올라갔는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.RIMinSpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "1.3401")

@when("3D Visualizer 탭 Preset 패널의 오른쪽 RI Range 수치 입력 칸의 아래 화살표 버튼을 한 번 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.RIMaxSpinBox.wValue = 1.3699

@then("3D Visualizer 탭 Preset 패널 하단 table의 해당 TF box 색 row에서 RI Max 값이 0.0001만큼 내려갔는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.itemTableWidget.qt_scrollarea_viewport.Label3, "QtText", cmpEqual, "1.3699")

@then("3D Visualizer 탭 Preset 패널의 오른쪽 RI Range 수치 입력 칸의 수치가 0.0001만큼 내려갔는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.RIMaxSpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "1.3699")

@when("3D Visualizer 탭 Preset 패널의 왼쪽 Grad Range 수치 입력 칸에 마우스 포인터를 대고 마우스 휠을 위로 한 칸 올린다.")
def step_impl():
  widget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents
  doubleSpinBox = widget.gradMinSpinBox
  doubleSpinBox.qt_spinbox_lineedit.Click(31, 12)
  widget.RIMinSpinBox.qt_spinbox_lineedit.MouseWheel(1)
  doubleSpinBox.wValue = 3.1099999999999999

@then("3D Visualizer 탭 Preset 패널의 왼쪽 Grad Range 수치 입력 칸의 수치가 0.05만큼 올라갔는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.gradMinSpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "3.11")

@when("3D Visualizer 탭 Preset 패널의 오른쪽 Grad Range 수치 입력 칸에 마우스 포인터를 대고 마우스 휠을 아래로 한 칸 내린다.")
def step_impl():
  widget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents
  doubleSpinBox = widget.gradMaxSpinBox
  doubleSpinBox.qt_spinbox_lineedit.Click(38, 16)
  widget.RIMaxSpinBox.MouseWheel(-1)
  doubleSpinBox.wValue = 19.85

@then("3D Visualizer 탭 Preset 패널의 오른쪽 Grad Range 수치 입력 칸의 수치가 0.05만큼 내려갔는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.gradMaxSpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "19.85")

@given("3D Visualizer 탭의 View 패널에 Rendering이 생성되어 있다.")
def step_impl():

  dir = r'E:\Regression_Test\test\test*.*'
  for f in glob.glob(dir):
    os.remove(f)
  dir1 = r'E:\Regression_Test\test\*.mp4'
  for f in glob.glob(dir1):
    os.remove(f)
  dir2 = r'E:\Regression_Test\test\*.png'
  for f in glob.glob(dir2):
    os.remove(f)
  dir3 = r'E:\Regression_Test\*.mp4'
  for f in glob.glob(dir3):
    os.remove(f)
  dir4 = r'E:\Regression_Test\*.png'
  for f in glob.glob(dir4):
    os.remove(f)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(88, 31)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\3D_Visualizer\\3D_Visualizer.tcpro")
  frame = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Click(-1)
  frame.bt_round_tool4.Click(77, 12)
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.MainWindow.dockwidget_inner.Widget.QCanvas2D.scrollArea.qt_scrollarea_viewport.scrollAreaWidgetContents.CanvasWidget.Canvas.Widget.Drag(34, 234, 21, 21)
  colorDialog = Aliases.TomoAnalysis.ColorDialog
  colorDialog.WellArray.Click(70, 62)
  colorDialog.DialogButtonBox.buttonOk.ClickButton()
  Regions.AddPicture(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer,"1")

@when("3D Visualizer 탭 Preset 패널의 Overlay 체크박스를 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.overlayCheckBox.Click(5,5)

@then("3D Visualizer 탭 Preset 패널 Overlay 체크박스에 체크표시가 생기는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.overlayCheckBox, "checked", cmpEqual, True)

@then("3D Visualizer 탭 View 패널의 HT 이미지에 Rendering이 생성되는지 확인")
def step_impl():
  Regions.AddPicture(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer,"2")
  if not Regions.Compare("1","2",False,False,True,0,lmNone):
    Log.Message("이미지가 다름")
    
  Regions.AddPicture(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer,"5")

@when("3D Visualizer 탭 Preset 패널의 Opacity 사이즈 바 옆 수치 입력 칸에 0.7를 입력한다.")
def step_impl():
  doubleSpinBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.opacitySpinBox
  doubleSpinBox.qt_spinbox_lineedit.Drag(34, 10, -49, -6)
  doubleSpinBox.Keys("0")
  doubleSpinBox.wValue = 0
  doubleSpinBox.Keys(".7")
  doubleSpinBox.wValue = 0.7
  Regions.AddPicture(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer.Widget,"4")
  
@then("3D Visualizer 탭 View 패널의 Rendering의 불투명도가 달라졌는지 확인")
def step_impl():
  Regions.AddPicture(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer.Widget,"6")
  if not Regions.Compare("6","5",False,False,True,0,lmNone):
    Log.Message("이미지가 다름")
  else :
    Log.Error("이미지가 같음")



@then("3D Visualizer 탭 Preset 패널의 Opacity 사이즈 바의 위치가 입력한 수치에 따라 바뀌었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.opacitySlider, "wPosition", cmpEqual, 70)
  
@then("Preset 패널 Overlay 체크박스에 체크표시가 사라지는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.overlayCheckBox, "checked", cmpEqual, False)

  
@then("3D Visualizer 탭 Preset 패널 Overlay 체크박스 옆 텍스트가 하이라이트 해제되는지 확인")
def step_impl():
  raise NotImplementedError

@then("3D Visualizer 탭 View 패널의 HT 이미지에서 Rendering이 사라지는지 확인")
def step_impl():
  Regions.AddPicture(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer.Widget,"7")
  if not Regions.Compare("4","7",False,False,True,0,lmNone):
    Log.Message("이미지가 다름")
  else :
    Log.Error("이미지가 같음")
    
@when("3D Visualizer 탭 Preset 패널의 Copy 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.bt_square_gary.Click(24, 28)

@when("윈도우의 메모장을 열고 키보드의 Ctrl+V 버튼을 누른다.")
def step_impl():
  folderView = Aliases.explorer.wndProgman.SHELLDLL_DefView.FolderView
  TestedApps.NOTEPAD.Run(1, True)
  edit = Aliases.notepad.wndNotepad.Edit
  edit.Click(223, 237)
  edit.Keys("^v")

@then("3D Visualizer 탭 Preset 하단의 table이 메모장에 붙여넣기 됐는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.notepad.wndNotepad.Edit, "wText", cmpEqual, "RI Min\tRI Max\tOpacity\r\n1.34161\t1.34692\t0.5\r\n")
  notepad = Aliases.notepad
  notepad.wndNotepad.Close()
  notepad.dlg_.Item.CtrlNotifySink.btn_N.ClickButton()

@when("3D Visualizer 탭 Preset 패널의 Save 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.bt_square_primary.ClickButton()

@when("파일 탐색기에 파일 이름과 경로를 지정하고 저장 버튼을 누른다.")
def step_impl():
  dlg = Aliases.TomoAnalysis.dlg3
  HWNDView = dlg.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  UIItemsView = HWNDView.CtrlNotifySink2.ShellView.Item
  UIItemsView.Regression_Test.Item.DblClick(78, 10)
  UIItem = UIItemsView.test
  UIProperty = UIItem.Item
  UIProperty.Click(31, 13)
  UIProperty.Click(31, 13)
  UIItem.Click(139, 11)
  UIProperty.Click(117, 11)
  UIItemsView.Click(153, 399)
  comboBox = HWNDView.FloatNotifySink2.ComboBox
  comboBox.Edit.Drag(69, 4, -148, -2)
  comboBox.SetText("test")
  Aliases.TomoAnalysis.dlg3.btn_S.ClickButton()
  Aliases.TomoAnalysis.dlg3.btn_S.Click()

@then("지정한 경로에 지정한 이름의 .csv 파일이 생성됐는지 확인")
def step_impl():
  explorer = Aliases.explorer
  explorer.wndShell_TrayWnd.ReBarWindow32.MSTaskSwWClass.MSTaskListWClass.Click(25, 7)
  wndCabinetWClass = explorer.wndGenerated_File
  HWNDView = wndCabinetWClass.Generated_File.DUIViewWndClassName.Item
  tv_ = HWNDView.CtrlNotifySink2.NamespaceTreeControl.tv_
  tv_.MouseWheel(-1)
  tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  UIItem = HWNDView.CtrlNotifySink.ShellView.Item.Regression_Test
  UIProperty = UIItem.Item
  UIProperty.Click(113, 4)
  UIProperty.Click(113, 4)
  UIProperty = UIItem.Item2
  UIProperty.Click(43, 3)
  UIProperty.DblClick(43, 3)
  UIItem = Aliases.explorer.wndGenerated_File.Generated_File.DUIViewWndClassName.Item.CtrlNotifySink.ShellView.Item.test
  UIProperty = UIItem.Item
  UIProperty.Click(365, 4)
  UIProperty.Click(365, 4)
  UIItem.Click(387, 4)
  UIProperty.DblClick(365, 4)
  aqObject.CheckProperty(Aliases.explorer.wndGenerated_File.Generated_File.DUIViewWndClassName.Item.CtrlNotifySink.ShellView.Item.test_csv.Item, "Value", cmpEqual, "test.csv")



@then("생성된 .csv 파일과 Preset 패널의 table 내용이 일치하는지 확인")
def step_impl():
  UIItem = Aliases.explorer.wndGenerated_File.Generated_File.DUIViewWndClassName.Item.CtrlNotifySink.ShellView.Item.test_csv
  UIProperty = UIItem.Item
  UIProperty.Click(22, 12)
  UIProperty.Click(22, 12)
  UIImage = UIItem.UIImage
  UIImage.Click(6, 6)
  UIImage.DblClick(6, 6)
  UIImage.Click(6, 6)
  Regions.EXCEL76.Check(Aliases.EXCEL.wndXLMAIN2.XLDESK.EXCEL7)
  Aliases.EXCEL.wndXLMAIN2.Close()
  Aliases.EXCEL.wndNUIDialog.NetUIHWND.Click(165, 65)
  wndtest = Aliases.explorer.wndGenerated_File
  wndtest.Activate()
  UIItem = wndtest.Generated_File.DUIViewWndClassName.Item.CtrlNotifySink.ShellView.Item.test_csv
  UIItem.Item.Click(20, 15)
  UIItem.Keys("[Del]")
  Aliases.explorer.wndGenerated_File.Close()








@when("3D Visualizer 탭 Preset 패널의 Clear 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.bt_square_gary2.Click(20, 18)

@then("TF canvas의 TF box가 모두 사라졌는지 확인")
def step_impl():
  Regions.Widget47.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.MainWindow.dockwidget_inner.Widget.QCanvas2D.scrollArea.qt_scrollarea_viewport.scrollAreaWidgetContents.CanvasWidget.Canvas.Widget)

@then("3D Visualizer 탭 View 패널의 Rendering이 사라졌는지 확인")
def step_impl():
  Regions.TomoAnalysis69.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@given("3D Visualizer 탭이 열려있다.")
def step_impl():

  dir = r'E:\Regression_Test\test*.*'
  for f in glob.glob(dir):
    os.remove(f)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(88, 31)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\3D_Visualizer\\3D_Visualizer.tcpro")
  frame = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Click(-1)
  frame.bt_round_tool4.Click(77, 12)


@when("3D Visualizer 탭 View 패널의 XY 2D HT 이미지 위에 마우스 포인터를 올리고 마우스 휠을 위로 5칸 올린다.")
def step_impl():
  for i in range(5):
    spinBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.zAxisSpinBox
    spinBox.Click(70, 10)



@then("3D Visualizer 탭 View 패널의 XY HT 단층 사진의 Z축이 올라가는지 확인")
def step_impl():
  Regions.TomoAnalysis70.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@when("3D Visualizer 탭 View 패널의 XY 2D HT 이미지 위에 마우스 포인터를 올리고 마우스 휠을 아래로 5칸 내린다.")
def step_impl():  
  for i in range(5):
    spinBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.zAxisSpinBox
    spinBox.Click(70, 17)

@then("3D Visualizer 탭 View 패널의 XY HT 단층 사진의 Z축이 내려가는지 확인")
def step_impl():
  Regions.TomoAnalysis71.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@when("Modality 패널 우상단의 겹쳐진 사각형 아이콘을 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget2.qt_dockwidget_floatbutton.Click(3, 9)

@then("Modality 패널이 팝업창으로 분리되었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.TC_TCDockWidget, "QtText", cmpEqual, "Modality")

@then("Modality 패널의 HT 체크박스가 체크 되어있는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.TC_TCDockWidget.ModalityPanel.panel_contents.HTCheckBox, "checked", cmpEqual, True)

@when("Display 패널 우상단의 겹쳐진 사각형 아이콘을 클릭한다.")
def step_impl():
  raise NotImplementedError

@then("Display 패널이 팝업창으로 분리되었는지 확인")
def step_impl():
  raise NotImplementedError

@when("Viewing tool 패널 우상단의 겹쳐진 사각형 아이콘을 클릭한다.")
def step_impl():
  raise NotImplementedError

@then("Viewing tool 패널이 팝업창으로 분리되었는지 확인")
def step_impl():
  raise NotImplementedError

@when("Screen shot 패널 우상단의 겹쳐진 사각형 아이콘을 클릭한다.")
def step_impl():
  raise NotImplementedError

@then("Screen shot 패널이 팝업창으로 분리되었는지 확인")
def step_impl():
  raise NotImplementedError

@when("Navigator 패널 우상단의 겹쳐진 사각형 아이콘을 클릭한다.")
def step_impl():
  raise NotImplementedError

@then("Navigator 패널이 팝업창으로 분리되었는지 확인")
def step_impl():
  raise NotImplementedError

@when("Preset 패널 우상단의 겹쳐진 사각형 아이콘을 클릭한다.")
def step_impl():
  raise NotImplementedError

@then("Preset 패널이 팝업창으로 분리되었는지 확인")
def step_impl():
  raise NotImplementedError

@when("Modality 팝업의 상단바를 더블 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.TC_TCDockWidget.DblClick(207, 19)

@then("Modality 팝업이 사라지고 3D Visualizer 탭의 원래 자리로 돌아갔는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget2, "floating", cmpEqual, False)

@when("Display 팝업의 상단바를 더블 클릭한다.")
def step_impl():
  raise NotImplementedError

@then("Display 팝업이 3D Visualizer 탭의 원래 자리로 돌아갔는지 확인")
def step_impl():
  raise NotImplementedError

@when("Viewing tool 팝업의 상단바를 더블 클릭한다.")
def step_impl():
  raise NotImplementedError

@then("Viewing tool 팝업이 3D Visualizer 탭의 원래 자리로 돌아갔는지 확인")
def step_impl():
  raise NotImplementedError

@when("Screen shot 팝업의 상단바를 더블 클릭한다.")
def step_impl():
  raise NotImplementedError

@then("Screen shot 팝업이 3D Visualizer 탭의 원래 자리로 돌아갔는지 확인")
def step_impl():
  raise NotImplementedError

@when("Navigator 팝업의 상단바를 더블 클릭한다.")
def step_impl():
  raise NotImplementedError

@then("Navigator 팝업이 3D Visualizer 탭의 원래 자리로 돌아갔는지 확인")
def step_impl():
  raise NotImplementedError

@when("Preset 팝업의 상단바를 더블 클릭한다.")
def step_impl():
  raise NotImplementedError

@then("Preset 팝업이 3D Visualizer 탭의 원래 자리로 돌아갔는지 확인")
def step_impl():
  raise NotImplementedError

@when("Modality 패널 상단의 패널 타이틀 텍스트를 클릭한 채로 드래그한다.")
def step_impl():
  raise NotImplementedError

@when("Display 패널 상단의 패널 타이틀 텍스트를 클릭한 채로 드래그한다.")
def step_impl():
  raise NotImplementedError

@when("Viewing tool 패널 상단의 패널 타이틀 텍스트를 클릭한 채로 드래그한다.")
def step_impl():
  raise NotImplementedError

@when("Screen shot 패널 상단의 패널 타이틀 텍스트를 클릭한 채로 드래그한다.")
def step_impl():
  raise NotImplementedError

@when("Navigator 패널 상단의 패널 타이틀 텍스트를 클릭한 채로 드래그한다.")
def step_impl():
  raise NotImplementedError

@when("Preset 패널 상단의 패널 타이틀 텍스트를 클릭한 채로 드래그한다.")
def step_impl():
  raise NotImplementedError

@when("팝업으로 분리된 모든 패널들의 상단바를 더블 클릭하여 다시 원래 자리로 돌려 보낸다.")
def step_impl():
  raise NotImplementedError

@when("Modality 패널의 타이틀 텍스트를 마우스로 클릭한 채로 Navigator 패널 위로 가져간다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget2.Drag(100, 14, 8, 477)

@when("Display 패널의 타이틀 텍스트를 마우스로 클릭한 채로 Navigator 패널 위로 가져간다.")
def step_impl():
  raise NotImplementedError

@when("Viewing tool 패널의 타이틀 텍스트를 마우스로 클릭한 채로 Navigator 패널 위로 가져간다.")
def step_impl():
  raise NotImplementedError

@when("Screen shot 패널의 타이틀 텍스트를 마우스로 클릭한 채로 Navigator 패널 위로 가져간다.")
def step_impl():
  raise NotImplementedError

@then("3D Visualizer 탭 왼쪽의 패널 하단에 Screen shot, Viewing tool, Display, Modality, Navigator 탭 툴바가 생성되었는지 확인")
def step_impl():
  raise NotImplementedError

@given("3D Visualizer 탭 Display 패널의 3D 체크박스에 체크 되어있다.")
def step_impl():

  dir = r'E:\Regression_Test\test*.*'
  for f in glob.glob(dir):
    os.remove(f)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(88, 31)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\3D_Visualizer\\3D_Visualizer.tcpro")
  frame = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base.tabWidget.qt_tabwidget_stackedwidget.previewTab.ThumbnailPanel.scrollArea.qt_scrollarea_viewport.widget_panel.ThumbnailGroupBox.frame.TomoAnalysis_ProjectManager_Plugins_LinkableThumbnailWidget.TC_TCF2DWidget.panel_contents_image.Click(-1)
  frame.bt_round_tool4.Click(77, 12)

@when("Display 패널의 2D 체크박스를 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget3.DisplayPanel.panel_contents.display2DButton.ClickButton()

@then("Display 패널의 2D 체크박스에 체크 되고 3D 체크박스가 비었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget3.DisplayPanel.panel_contents.display3DButton, "wChecked", cmpEqual, False)
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget3.DisplayPanel.panel_contents.display2DButton, "wChecked", cmpEqual, True)

@then("3D Visualizer 탭 View 패널에 2D MIP 이미지만 출력되는지 확인")
def step_impl():
  Regions.TomoAnalysis72.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@when("Display 패널의 3D 체크박스를 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget3.DisplayPanel.panel_contents.display3DButton.ClickButton()

@then("Display 패널의 3D 체크박스에 체크 되고 2D 체크박스가 비었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget3.DisplayPanel.panel_contents.display3DButton, "wChecked", cmpEqual, True)
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget3.DisplayPanel.panel_contents.display2DButton, "wChecked", cmpEqual, False)

@then("3D Visualizer 탭의 View 패널이 초기 레이아웃으로 돌아왔는지 확인")
def step_impl():
  Regions.panel_base16.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.panel_base)

@given("Viewing tool 패널의 View 드롭다운 버튼에서 Show Orientation marker 버튼이 체크 해제되어 있다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.TC_TCDockWidget4.ViewingToolPanel.panel_contents.bt_tool_round.ClickButtonXY(102, 12)
  aqObject.CheckProperty(Aliases.TomoAnalysis.Menu2.CheckBox, "checked", cmpEqual, False)

@when("Viewing tool 패널의 Layout 드롭다운 버튼에서 2D image top, 3D bottom 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.TC_TCDockWidget4.ViewingToolPanel.panel_contents.bt_tool_round2.ClickButtonXY(126, 17)
  tomoAnalysis.Menu.RadioButton.ClickButton()

@then("View 패널의 레이아웃이 2D image top, 3D bottom으로 변경되었는지 확인")
def step_impl():
  Regions.panel_base17.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@when("Viewing tool 패널의 View 드롭다운 버튼에서 Show Orientation marker 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  widget = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.TC_TCDockWidget4.ViewingToolPanel.panel_contents
  widget.Click(291, 40)
  widget.bt_tool_round.ClickButtonXY(115, 17)
  tomoAnalysis.Menu2.CheckBox.Click(-1)
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.Click(921, 36)


@then("Viewing tool 패널 View 드롭다운 버튼의 Show Orientation marker 버튼이 체크 됐는지 확인")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  widget = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.TC_TCDockWidget4.ViewingToolPanel.panel_contents
  widget.Click(291, 40)
  widget.bt_tool_round.ClickButtonXY(115, 17)
  aqObject.CheckProperty(Aliases.TomoAnalysis.Menu2.CheckBox, "checked", cmpEqual, True)

@then("3D Visualizer 탭 View 패널의 3D 화면에서 정육면체가 생성됐는지 확인")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.Click(921, 36)
  Regions.TomoAnalysis144.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@when("Viewing tool 패널에서 View 드롭다운 버튼의 Show Orientation marker 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  toolButton = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.TC_TCDockWidget4.ViewingToolPanel.panel_contents.bt_tool_round
  toolButton.ClickButtonXY(123, 11)
  tomoAnalysis.Menu2.CheckBox.Click(-1)
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.Click(921, 36)
  
@then("Viewing tool 패널 View 드롭다운 버튼의 Show Orientation marker 버튼이 체크 해제됐는지 확인")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  widget = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.TC_TCDockWidget4.ViewingToolPanel.panel_contents
  widget.Click(291, 40)
  widget.bt_tool_round.ClickButtonXY(115, 17)
  aqObject.CheckProperty(Aliases.TomoAnalysis.Menu2.CheckBox, "checked", cmpEqual, False)
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.Click(921, 36)

@then("3D Visualizer 탭 View 패널의 3D 화면에서 정육면체가 사라졌는지 확인")
def step_impl():
  Regions.TomoAnalysis74.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@when("Viewing tool 패널의 View 드롭다운 버튼에서 Show Boundary box 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  widget = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.TC_TCDockWidget4.ViewingToolPanel.panel_contents
  widget.Click(314, 42)
  toolButton = widget.bt_tool_round
  toolButton.ClickButtonXY(122, 20)

  tomoAnalysis.Menu2.CheckBox2.Click(-1)
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.Click(921, 36)
  
@then("3D Visualizer 탭 View 패널의 3D 화면에 Boundary box가 생성됐는지 확인")
def step_impl():
  Regions.mainwindow4.Check(Regions.CreateRegionInfo(Aliases.TomoAnalysis.mainwindow, 555, 298, 976, 521, False))

@then("Viewing tool 패널 View 드롭다운 버튼의 Show Boundary box 버튼이 체크 됐는지 확인")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  widget = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.TC_TCDockWidget4.ViewingToolPanel.panel_contents
  widget.Click(291, 40)
  widget.bt_tool_round.ClickButtonXY(115, 17)
  aqObject.CheckProperty(tomoAnalysis.Menu2.CheckBox2, "checked", cmpEqual, True)
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.Click(921, 36)


@then("3D Visualizer 탭 View 패널의 3D 화면에 Boundary box가 사라졌는지 확인")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.Click(634, 34)
  Regions.TomoAnalysis76.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.Click(869, 31)

@then("Viewing tool 패널 View 드롭다운 버튼의 Show Boundary box 버튼이 체크 해제됐는지 확인")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  widget = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.TC_TCDockWidget4.ViewingToolPanel.panel_contents
  widget.Click(291, 40)
  widget.bt_tool_round.ClickButtonXY(115, 17) 
  aqObject.CheckProperty(tomoAnalysis.Menu2.CheckBox2, "checked", cmpEqual, False)
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.Click(10 ,10)

@when("Viewing tool 패널의 View 드롭다운 버튼에서 Show Axis grid 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.TC_TCDockWidget4.ViewingToolPanel.panel_contents.bt_tool_round.ClickButtonXY(94, 12)
  Sys.Desktop.Mousey += 110;
  Sys.Desktop.MouseDown(1,Sys.Desktop.Mousex,Sys.Desktop.Mousey)
  tomoAnalysis.Menu.CheckBox.Click(-1)

@then("3D Visualizer 탭 View 패널의 3D 화면에 Axis grid가 생성됐는지 확인")
def step_impl():
  Regions.F20_01.Check(Regions.CreateRegionInfo(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer, 202, 183, 957, 527, False))
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.Click(869, 31)

@then("Viewing tool 패널 View 드롭다운 버튼의 Show Axis grid 버튼이 체크 됐는지 확인")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.TC_TCDockWidget4.ViewingToolPanel.panel_contents.bt_tool_round.ClickButtonXY(94, 12)
  Sys.Desktop.Mousey += 110;
  Sys.Desktop.MouseDown(1,Sys.Desktop.Mousex,Sys.Desktop.Mousey)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.TC_TCDockWidget4.ViewingToolPanel.panel_contents.bt_tool_round.ClickButtonXY(124, 18)
  tomoAnalysis.Menu.QtMenu.Click("Axis grid")
  aqObject.CheckProperty(Aliases.TomoAnalysis.Menu.CheckBox, "QtText", cmpEqual, "Show Axis grid")
  aqObject.CheckProperty(Aliases.TomoAnalysis.Menu.CheckBox, "checked", cmpEqual, True)

@then("3D Visualizer 탭 View 패널의 3D 화면에 Axis grid가 사라졌는지 확인")
def step_impl():
  aqUtils.Delay(3000)
  Regions.TomoAnalysis77.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer, False, False, 1000, 25)
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.Click(869, 31)

@then("Viewing tool 패널 View 드롭다운 버튼의 Show Axis grid 버튼이 체크 해제됐는지 확인")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.TC_TCDockWidget4.ViewingToolPanel.panel_contents.bt_tool_round.ClickButtonXY(53, 14)
  tomoAnalysis.Menu.QtMenu.Click("Axis grid")
  aqObject.CheckProperty(Aliases.TomoAnalysis.Menu.CheckBox, "QtText", cmpEqual, "Show Axis grid")
  aqObject.CheckProperty(Aliases.TomoAnalysis.Menu.CheckBox, "checked", cmpEqual, True)


@when("Screen shot 패널 2D view 탭의 Upsampling 수치 입력 칸에 {arg}을 입력하고 Capture 버튼을 누른다.")
def step_impl(param1):
  widget = NameMapping.Sys.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget5.ScreenShotWidgetUI.panel_contents.tabWidget.qt_tabwidget_stackedwidget.view2DTab
  spinBox = widget.sliceUpsampleSpin
  spinBox.Keys("1")
  spinBox.wValue = 1
  widget.bt_round_gray700.ClickButton()

@when("파일 탐색기에서 이름은 그대로 두고 경로만 지정한 뒤 저장 버튼을 누른다.")
def step_impl():
  dlgSaveScreenShot = Aliases.TomoAnalysis.dlgSaveScreenShot
  HWNDView = dlgSaveScreenShot.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  UIItem = HWNDView.CtrlNotifySink2.ShellView.Item.Regression_Test
  UIItem.Item.Click(2, 12)
  UIItem.Keys("[Enter]")
  dlgSaveScreenShot.btn_S.Drag(14, 4, 5, -1)

@when("Screen shot 패널 2D view 탭의 Upsampling 수치 입력 칸에서 위 화살표를 두 번 누르고 Capture 버튼을 누른다.")
def step_impl():
  widget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.TC_TCDockWidget5.ScreenShotWidgetUI.panel_contents.tabWidget.qt_tabwidget_stackedwidget.view2DTab
  widget.sliceUpsampleSpin.wValue = 3
  widget.bt_round_gray7002.ClickButton()

@when("파일 탐색기에서 경로는 그대로 두고 이름을 변경한 뒤 저장 버튼을 누른다.")
def step_impl():
  dlgSaveScreenShot = Aliases.TomoAnalysis.dlgSaveScreenShot
  comboBox = dlgSaveScreenShot.DUIViewWndClassName.Item.FloatNotifySink.ComboBox
  comboBox.Edit.Click(248, 7)
  comboBox.SetText("20200611.164640.571.Hep G2-105_XY_plan.png")
  dlgSaveScreenShot.btn_S.ClickButton()

@then("지정한 경로에 서로 사이즈만 다른 .png XY 스크린 샷 파일이 두 개 생성되었는지 확인")
def step_impl():
  explorer = Aliases.explorer
  explorer.wndShell_TrayWnd.ReBarWindow32.MSTaskSwWClass.MSTaskListWClass.Click(27, 12)
  HWNDView = explorer.wndGenerated_File.Generated_File.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink2.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  UIItem = HWNDView.CtrlNotifySink.ShellView.Item.Regression_Test
  UIItem.Item.Click(132, 17)
  UIItem.Keys("[Enter]")
  aqObject.CheckProperty(Aliases.explorer.wndGenerated_File.Generated_File.DUIViewWndClassName.Item.CtrlNotifySink.ShellView.Item.Hep_G2_105_XY_plan_png.Item, "Value", cmpEqual, "244KB")
  aqObject.CheckProperty(Aliases.explorer.wndGenerated_File.Generated_File.DUIViewWndClassName.Item.CtrlNotifySink.ShellView.Item.Hep_G2_105_XY_plane_png.Item, "Value", cmpEqual, "44KB")
  Aliases.explorer.wndGenerated_File.Close()

@when("Screen shot 패널 2D view 탭의 All 체크박스에 체크한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.TC_TCDockWidget5.ScreenShotWidgetUI.panel_contents.tabWidget.qt_tabwidget_stackedwidget.view2DTab.allRadioButton.ClickButton()

@when("Screen shot 패널 2D view 탭의 Capture 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.TC_TCDockWidget5.ScreenShotWidgetUI.panel_contents.tabWidget.qt_tabwidget_stackedwidget.view2DTab.bt_round_gray700.ClickButton()

@when("파일 탐색기에서 파일 경로를 지정하고 저장 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.dlg4.btn_.ClickButton()

@when("팝업에서 Replace 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.widget_popup.bt_square_primary.ClickButton()

@then("지정한 경로에 .png 스크린 샷 파일이 3개 있는지 확인")
def step_impl():
  explorer = Aliases.explorer
  explorer.wndShell_TrayWnd.ReBarWindow32.MSTaskSwWClass.MSTaskListWClass.Click(27, 12)
  HWNDView = explorer.wndGenerated_File.Generated_File.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink2.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  UIItem = HWNDView.CtrlNotifySink.ShellView.Item.Regression_Test
  UIItem.Item.Click(132, 17)
  UIItem.Keys("[Enter]")
  aqObject.CheckProperty(Aliases.explorer.wndGenerated_File.Generated_File.DUIViewWndClassName.Item.CtrlNotifySink.ShellView.Item.Hep_G2_105_XY_plane_png.Item2, "Value", cmpEqual, "20200611.164640.571.Hep G2-105_XY_plane.png")
  aqObject.CheckProperty(Aliases.explorer.wndGenerated_File.Generated_File.DUIViewWndClassName.Item.CtrlNotifySink.ShellView.Item.Hep_G2_105_XZ_plane_png.Item, "Value", cmpEqual, "20200611.164640.571.Hep G2-105_XZ_plane.png")
  aqObject.CheckProperty(Aliases.explorer.wndGenerated_File.Generated_File.DUIViewWndClassName.Item.CtrlNotifySink.ShellView.Item.Hep_G2_105_YZ_plane_png.Item, "Value", cmpEqual, "20200611.164640.571.Hep G2-105_YZ_plane.png")
  Aliases.explorer.wndGenerated_File.Close()
  
@when("Screen shot 패널의 3D view 탭을 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget5.ScreenShotWidgetUI.panel_contents.tabWidget.TC_TabBar.Click(153, 26)

@then("Screen shot 패널에 3D view 탭 전용 UI가 나타남을 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget5.ScreenShotWidgetUI.panel_contents.tabWidget.qt_tabwidget_stackedwidget.view3DTab, "Visible", cmpEqual, True)

@when("Preset 패널의 TF canvas에 TF box를 생성하여 View 패널의 3D 화면에 rendering을 만든다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.TransferFunctionPanel.panel_base.panel_contents.MainWindow.dockwidget_inner.Widget.QCanvas2D.scrollArea.qt_scrollarea_viewport.scrollAreaWidgetContents.CanvasWidget.Canvas.Widget.Drag(28, 205, 106, 38)
  Aliases.TomoAnalysis.ColorDialog.DialogButtonBox.buttonOk.ClickButton()

@when("Screen shot 패널 3D view 탭의 Capture 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget5.ScreenShotWidgetUI.panel_contents.tabWidget.qt_tabwidget_stackedwidget.view3DTab.bt_round_gray700.ClickButton()

@when("파일 탐색기에 경로만 지정하고 저장 버튼을 누른다.")
def step_impl():
  dlgSaveScreenshot = Aliases.TomoAnalysis.dlgSaveScreenshot2
  HWNDView = dlgSaveScreenshot.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  UIItem = HWNDView.CtrlNotifySink2.ShellView.Item.Regression_Test
  UIItem.Item.Click(111, 15)
  UIItem.Keys("[Enter]")
  dlgSaveScreenshot.btn_S.ClickButton()

@then("지정한 경로에 .png 3D 스크린 샷 파일이 생겼는지 확인")
def step_impl():
  explorer = Aliases.explorer
  explorer.wndShell_TrayWnd.ReBarWindow32.MSTaskSwWClass.MSTaskListWClass.Click(23, 19)
  HWNDView = explorer.wndGenerated_File.Generated_File.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink2.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  UIItem = HWNDView.CtrlNotifySink.ShellView.Item.Regression_Test
  UIProperty = UIItem.Item
  UIProperty.Click(101, 8)
  UIProperty.Click(101, 8)
  UIItem.Item2.Click(89, 15)
  UIItem.Keys("[Enter]")
  aqObject.CheckProperty(Aliases.explorer.wndGenerated_File.Generated_File.DUIViewWndClassName.Item.CtrlNotifySink.ShellView.Item.Hep_G2_105_3D_screenshot_png.Item, "Value", cmpEqual, "20200611.164640.571.Hep G2-105_3D_screenshot.png")

@then("촬영 시점의 View 패널 3D 화면과 저장된 스크린 샷 파일이 일치하는지 확인")
def step_impl():
  UIItem = Aliases.explorer.wndGenerated_File.Generated_File.DUIViewWndClassName.Item.CtrlNotifySink.ShellView.Item.Hep_G2_105_3D_screenshot_png
  UIItem.Item.Click(170, 6)
  UIItem.Keys("[Enter]")

  Regions.F20_5.Check(Regions.CreateRegionInfo(Aliases.App.Item2, 529, 196, 862, 695, False), False, False, 19969, 8)
  Aliases.ApplicationFrameHost.wndApplicationFrameWindow3.Close()
  Aliases.explorer.wndGenerated_File.Close()
 

@when("Screen shot 패널 3D view 탭의 All plane 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget5.ScreenShotWidgetUI.panel_contents.tabWidget.qt_tabwidget_stackedwidget.view3DTab.bt_round_gray7002.ClickButton()

@when("파일 탐색기에서 경로만 지정하고 저장 버튼을 누른다.")
def step_impl():
  dlg = Aliases.TomoAnalysis.dlg4
  HWNDView = dlg.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  HWNDView.CtrlNotifySink2.ShellView.Item.Regression_Test.Item.Click(63, 6)
  dlg.btn_.ClickButton()

@then("지정한 경로에 .png 3D 스크린 샷 파일 3장이 생성되었는지 확인")
def step_impl():
  explorer = Aliases.explorer
  explorer.wndShell_TrayWnd.ReBarWindow32.MSTaskSwWClass.MSTaskListWClass.Click(30, 4)
  HWNDView = explorer.wndGenerated_File.Generated_File.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink2.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  UIItem = Aliases.explorer.wndGenerated_File.Generated_File.DUIViewWndClassName.Item.CtrlNotifySink.ShellView.Item.Regression_Test
  UIItem.Item.Click(131, 8)
  UIItem.Keys("[Enter]")
  aqObject.CheckProperty(Aliases.explorer.wndGenerated_File.Generated_File.DUIViewWndClassName.Item.CtrlNotifySink.ShellView.Item.Hep_G2_105_3D_View2_png.Item, "Value", cmpEqual, "20200611.164640.571.Hep G2-105_3D_View2.png")
  aqObject.CheckProperty(Aliases.explorer.wndGenerated_File.Generated_File.DUIViewWndClassName.Item.CtrlNotifySink.ShellView.Item.Hep_G2_105_3D_View1_png.Item, "Value", cmpEqual, "20200611.164640.571.Hep G2-105_3D_View1.png")
  aqObject.CheckProperty(Aliases.explorer.wndGenerated_File.Generated_File.DUIViewWndClassName.Item.CtrlNotifySink.ShellView.Item.Hep_G2_105_3D_View3_png.Item, "Value", cmpEqual, "20200611.164640.571.Hep G2-105_3D_View3.png")
  Aliases.explorer.wndGenerated_File.Close()


@when("Screen shot 패널의 multi view 탭을 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget5.ScreenShotWidgetUI.panel_contents.tabWidget.TC_TabBar.Click(274, 25)

@then("Screen shot 패널에 multi view 탭 전용 UI가 나타남을 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget5.ScreenShotWidgetUI.panel_contents.tabWidget.qt_tabwidget_stackedwidget.viewMultiTab, "QtText", cmpEqual, "")
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget5.ScreenShotWidgetUI.panel_contents.tabWidget.qt_tabwidget_stackedwidget.viewMultiTab, "Visible", cmpEqual, True)

@when("Screen shot 패널 multi view 탭의 Capture 버튼을 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget5.ScreenShotWidgetUI.panel_contents.tabWidget.qt_tabwidget_stackedwidget.viewMultiTab.bt_round_gray700.ClickButton()

@when("파일 탐색기에서 경로를 설정하고 저장 버튼을 누른다.")
def step_impl():
  dlgSaveScreenShot = Aliases.TomoAnalysis.dlgSaveScreenShot
  HWNDView = dlgSaveScreenShot.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  UIItem = HWNDView.CtrlNotifySink2.ShellView.Item.Regression_Test
  UIItem.Item2.Click(41, 6)
  UIItem.Keys("[Enter]")
  dlgSaveScreenShot.btn_S.ClickButton()

@then(".png multi-view 스크린 샷 파일이 설정한 경로에 생성됐는지 확인")
def step_impl():
  explorer = Aliases.explorer
  explorer.wndShell_TrayWnd.ReBarWindow32.MSTaskSwWClass.MSTaskListWClass.Click(30, 4)
  wndRegression_Test = Aliases.explorer.wndGenerated_File
  wndRegression_Test.Activate()
  HWNDView = wndRegression_Test.Generated_File.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink2.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  UIItem = HWNDView.CtrlNotifySink.ShellView.Item.Regression_Test
  UIItem.Item.Click(78, 18)
  UIItem.Keys("[Enter]")
  scrollBar = Aliases.explorer.wndGenerated_File.Generated_File.DUIViewWndClassName.Item.CtrlNotifySink.ShellView.Item.CtrlNotifySink2.ScrollBar
  aqObject.CheckProperty(Aliases.explorer.wndGenerated_File.Generated_File.DUIViewWndClassName.Item.CtrlNotifySink.ShellView.Item.Hep_G2_105_multi_view_screenshot_png.Item, "Value", cmpEqual, "20200611.164640.571.Hep G2-105_multi-view_screenshot.png")


@then(".png 파일이 촬영 시점의 View 패널 화면과 일치하는지 확인")
def step_impl():
  UIItem = Aliases.explorer.wndGenerated_File.Generated_File.DUIViewWndClassName.Item.CtrlNotifySink.ShellView.Item.Hep_G2_105_multi_view_screenshot_png
  UIItem.Item.Click(227, 4)
  UIItem.Keys("[Enter]")
  Regions.F20_3.Check(Regions.CreateRegionInfo(Aliases.App.Item2, 386, 195, 1147, 697, False))
  Aliases.ApplicationFrameHost.wndApplicationFrameWindow4.Close()
  Aliases.explorer.wndGenerated_File.Close()
  
@when("Screen shot 패널의 2D view 탭을 클릭한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.TC_TCDockWidget5.ScreenShotWidgetUI.panel_contents.tabWidget.TC_TabBar.Click(66, 31)

@then("Screen shot 패널에 2D view 탭 전용 UI가 나타남을 확인")
def step_impl():
  Regions.F20_6.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.TC_TCDockWidget5)

@when("Navigator 패널의 Z axis LOCATION 수치 입력 칸에 10을 입력한다.")
def step_impl():
  doubleSpinBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.zPhySpinBox
  doubleSpinBox.Click(12, 16)
  doubleSpinBox.qt_spinbox_lineedit.Drag(0, 12, 29, -3)
  doubleSpinBox.Keys("10")
  doubleSpinBox.wValue = 10
  doubleSpinBox.Keys("[Enter]")
  doubleSpinBox.wValue = 10

@then("Navigator 패널의 XY Plane 사이즈 바와 수치가 변경되었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.zAxisSlider, "wPosition", cmpEqual, 52)

@then("3D Visualizer 탭 View 패널의 2D XY HT 이미지가 Z축 10의 위치로 바뀌었는지 확인")
def step_impl():
  Regions.TomoAnalysis78.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@when("Navigator 패널의 Z axis LOCATION 수치 입력 칸의 위 화살표를 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.zPhySpinBox.wValue = 10.19

@then("Navigator 패널의 XY Plane의 수치가 {arg} 올라갔는지 확인")
def step_impl(param1):
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.zAxisSpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "53")

@when("Navigator 패널의 Z axis LOCATION 수치 입력 칸의 아래 화살표를 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.zAxisSpinBox.wValue = 52

@then("Navigator 패널의 XY Plane의 수치가 {arg} 내려갔는지 확인")
def step_impl(param1):
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.zAxisSpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "52")

@when("Navigator 패널의 XY plane 사이즈 바를 마우스로 왼쪽 끝까지 움직인다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.zAxisSlider.wPosition = 2

@then("Navigator 패널의 XY plane 사이즈 바 옆의 수치가 2로 바뀌었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.zAxisSpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "2")

@then("Navigator 패널의 Z axis LOCATION 수치가 0.38로 바뀌었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.zPhySpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "0.38")

@then("3D Visualizer 탭 View 패널의 2D XY HT 이미지가 {arg} Slice 위치로 바뀌었는지 확인")
def step_impl(param1):
  Regions.TomoAnalysis79.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@when("Navigator 패널의 XY plane 사이즈 바를 마우스로 오른쪽 끝까지 움직인다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.zAxisSlider.wPosition = 210

@then("Navigator 패널의 XY plane 사이즈 바 옆의 수치가 210으로 바뀌었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.zAxisSpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "210")

@then("Navigator 패널의 Z axis LOCATION 수치가 39.86으로 바뀌었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.zPhySpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "39.86")

@when("Navigator 패널의 XY plane 사이즈 바 옆 수치 칸에 150을 입력한다.")
def step_impl():
  spinBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.zAxisSpinBox
  spinBox.qt_spinbox_lineedit.Drag(30, 15, -40, 0)
  spinBox.Keys("150")
  spinBox.wValue = 150
  spinBox.Keys("[Enter]")
  spinBox.wValue = 150

@then("Navigator 패널의 Z axis LOCATION 수치가 28.47로 바뀌었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.zPhySpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "28.47")

@when("Navigator 패널의 XY plane 사이즈 바 옆 수치 칸의 위 화살표를 한 번 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.zAxisSpinBox.wValue = 151

@then("Navigator 패널의 Z axis LOCATION 수치가 {arg} 올라갔는지 확인")
def step_impl(param1):
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.zPhySpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "28.66")

@then("3D Visualizer 탭 View 패널의 2D XY HT 이미지가 한 Slice 올라갔는지 확인")
def step_impl():
  Regions.TomoAnalysis80.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@when("Navigator 패널의 XY plane 사이즈 바 옆 수치 칸의 아래 화살표를 한 번 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.zAxisSpinBox.wValue = 150

@then("Navigator 패널의 Z axis LOCATION 수치가 {arg} 내려갔는지 확인")
def step_impl(param1):
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.zPhySpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "28.47")

@then("3D Visualizer 탭 View 패널의 2D XY HT 이미지가 한 Slice 내려갔는지 확인")
def step_impl():
  Regions.TomoAnalysis81.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@when("Navigator 패널의 X axis LOCATION 수치 입력 칸에 10을 입력한다.")
def step_impl():
  doubleSpinBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.xPhySpinBox
  doubleSpinBox.qt_spinbox_lineedit.Drag(0, 10, 64, 8)
  doubleSpinBox.Keys("10")
  doubleSpinBox.wValue = 10
  doubleSpinBox.Keys("[Enter]")
  doubleSpinBox.wValue = 10

@then("Navigator 패널의 YZ Plane 사이즈 바와 수치가 변경되었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.xAxisSlider, "wPosition", cmpEqual, 105)
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.xAxisSpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "105")

@then("3D Visualizer 탭 View 패널의 2D YZ HT 이미지가 {arg} 위치로 바뀌었는지 확인")
def step_impl(param1):
  Regions.TomoAnalysis82.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@when("Navigator 패널의 X axis LOCATION 수치 입력 칸의 위 화살표를 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.xPhySpinBox.wValue = 10.09

@then("Navigator 패널의 YZ Plane의 수치가 {arg} 올라갔는지 확인")
def step_impl(param1):
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.xAxisSpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "106")
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.xAxisSpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "106")

@when("Navigator 패널의 X axis LOCATION 수치 입력 칸의 아래 화살표를 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.xPhySpinBox.wValue = 10

@then("Navigator 패널의 YZ Plane의 수치가 {arg} 내려갔는지 확인")
def step_impl(param1):
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.xAxisSpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "105")

@when("Navigator 패널의 YZ plane 사이즈 바를 마우스로 왼쪽 끝까지 움직인다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.xAxisSlider.wPosition = 6

@then("Navigator 패널의 YZ plane 사이즈 바 옆의 수치가 6으로 바뀌었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.xAxisSpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "6")

@then("Navigator 패널의 X axis LOCATION 수치가 0.57로 바뀌었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.xPhySpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "0.57")

@then("3D Visualizer 탭 View 패널의 2D YZ HT 이미지가 {arg} Slice 위치로 바뀌었는지 확인")
def step_impl(param1):
  Regions.TomoAnalysis83.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@when("Navigator 패널의 YZ plane 사이즈 바를 마우스로 오른쪽 끝까지 움직인다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.xAxisSlider.wPosition = 648

@then("Navigator 패널의 YZ plane 사이즈 바 옆의 수치가 648로 바뀌었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.xAxisSpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "648")

@then("Navigator 패널의 X axis LOCATION 수치가 61.33으로 바뀌었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.xPhySpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "61.33")

@then("View 패널의 2D YZ HT 이미지가 {arg} Slice 위치로 바뀌었는지 확인")
def step_impl(param1):
  Regions.TomoAnalysis84.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@when("Navigator 패널의 YZ plane 사이즈 바 옆 수치 칸에 200을 입력한다.")
def step_impl():
  spinBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.xAxisSpinBox
  spinBox.Keys("200")
  spinBox.wValue = 200

@then("Navigator 패널의 X axis LOCATION 수치가 18.93으로 바뀌었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.xPhySpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "18.93")

@when("Navigator 패널의 YZ plane 사이즈 바 옆 수치 칸의 위 화살표를 한 번 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.xAxisSpinBox.wValue = 201

@then("Navigator 패널의 X axis LOCATION 수치가 {arg} 올라갔는지 확인")
def step_impl(param1):
  raise NotImplementedError

@then("View 패널의 2D YZ HT 이미지가 한 Slice 올라갔는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.xAxisSpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "201")

@when("Navigator 패널의 YZ plane 사이즈 바 옆 수치 칸의 아래 화살표를 한 번 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.xAxisSpinBox.wValue = 200

@then("Navigator 패널의 X axis LOCATION 수치가 {arg} 내려갔는지 확인")
def step_impl(param1):
  raise NotImplementedError

@then("View 패널의 2D YZ HT 이미지가 한 Slice 내려갔는지 확인")
def step_impl():
  Regions.TomoAnalysis92.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@when("Navigator 패널의 Y axis LOCATION 수치 입력 칸에 10을 입력한다.")
def step_impl():
  doubleSpinBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.yPhySpinBox
  doubleSpinBox.qt_spinbox_lineedit.Drag(2, 13, 27, 0)
  doubleSpinBox.Keys("1")
  doubleSpinBox.wValue = 1
  doubleSpinBox.Keys("0")
  doubleSpinBox.wValue = 10
  doubleSpinBox.Keys("[Enter]")
  doubleSpinBox.wValue = 10
  doubleSpinBox.Keys("[Enter]")
  doubleSpinBox.wValue = 10

@then("Navigator 패널의 XZ Plane 사이즈 바와 수치가 변경되었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.yAxisSlider, "wPosition", cmpEqual, 105)
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.yAxisSpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "105")

@then("3D Visualizer 탭 View 패널의 2D XZ HT 이미지가 {arg} 위치로 바뀌었는지 확인")
def step_impl(param1):
  raise NotImplementedError

@when("Navigator 패널의 Y axis LOCATION 수치 입력 칸의 위 화살표를 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.yPhySpinBox.wValue = 10.09

@then("Navigator 패널의 XZ Plane의 수치가 {arg} 올라갔는지 확인")
def step_impl(param1):
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.yAxisSpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "106")

@when("Navigator 패널의 Y axis LOCATION 수치 입력 칸의 아래 화살표를 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.yPhySpinBox.wValue = 10

@then("Navigator 패널의 XZ Plane의 수치가 올라갔는지 확인")
def step_impl():
  raise NotImplementedError

@when("Navigator 패널의 XZ plane 사이즈 바를 마우스로 왼쪽 끝까지 움직인다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.yAxisSlider.wPosition = 6

@then("Navigator 패널의 XZ plane 사이즈 바 옆의 수치가 6으로 바뀌었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.yAxisSpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "6")

@then("Navigator 패널의 Y axis LOCATION 수치가 0.57로 바뀌었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.yPhySpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "0.57")

@then("3D Visualizer 탭 View 패널의 2D XZ HT 이미지가 {arg} Slice 위치로 바뀌었는지 확인")
def step_impl(param1):
  raise NotImplementedError

@when("Navigator 패널의 XZ plane 사이즈 바를 마우스로 오른쪽 끝까지 움직인다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.yAxisSlider.wPosition = 648

@then("Navigator 패널 XZ plane 사이즈 바 옆의 수치가 648로 바뀌었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.yAxisSpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "648")

@then("Navigator 패널의 Y axis LOCATION 수치가 61.33으로 바뀌었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.yPhySpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "61.33")

@when("Navigator 패널의 XZ plane 사이즈 바 옆 수치 칸에 200을 입력한다.")
def step_impl():
  spinBox = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.yAxisSpinBox
  spinBox.qt_spinbox_lineedit.Drag(28, 19, -33, 2)
  spinBox.Keys("20")
  spinBox.wValue = 20
  spinBox.Keys("0")
  spinBox.wValue = 200

@then("Navigator 패널의 XZ plane 사이즈 바의 위치가 바뀌었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.yAxisSlider, "wPosition", cmpEqual, 200)
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.yAxisSlider, "wPosition", cmpEqual, 200)

@then("Navigator 패널의 Y axis LOCATION 수치가 18.93로 바뀌었는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.yPhySpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "18.93")
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.xPhySpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "18.93")
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.xPhySpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "18.93")

@when("Navigator 패널의 XZ plane 사이즈 바 옆 수치 칸의 위 화살표를 한 번 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.yAxisSpinBox.wValue = 201

@then("Navigator 패널의 Y axis LOCATION 수치가 {arg} 올라갔는지 확인")
def step_impl(param1):
  raise NotImplementedError

@then("3D Visualizer 탭 View 패널의 2D XZ HT 이미지가 한 Slice 올라갔는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer, "WndCaption", cmpEqual, "TomoAnalysis")
  Regions.TomoAnalysis97.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@when("Navigator 패널의 XZ plane 사이즈 바 옆 수치 칸의 아래 화살표를 한 번 누른다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.yAxisSpinBox.wValue = 200

@then("Navigator 패널의 Y axis LOCATION 수치가 {arg} 내려갔는지 확인")
def step_impl(param1):
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.yPhySpinBox, "wValue", cmpEqual, 18.93)
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.yPhySpinBox, "wValue", cmpEqual, 18.93)


@then("3D Visualizer 탭 View 패널의 2D XZ HT 이미지가 한 Slice 내려갔는지 확인")
def step_impl():
  Regions.TomoAnalysis98.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@when("Modality 패널의 HT 체크박스를 체크 해제한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget2.ModalityPanel.panel_contents.HTCheckBox.ClickButton(cbUnchecked)

@then("3D Visualizer 탭 View 패널의 2D XY, YZ, XZ HT 이미지에 아무런 변화가 없는지 확인")
def step_impl():
  Regions.TomoAnalysis99.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)
  Regions.TomoAnalysis100.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)
  Regions.TomoAnalysis101.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@when("Display 패널에서 2D 체크박스에 체크한다.")
def step_impl():
  Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget3.DisplayPanel.panel_contents.display2DButton.ClickButton()

@when("Navigator 패널에서 XY Plane의 위 화살표 버튼을 한 번 누른다.")
def step_impl():
  raise NotImplementedError

@then("Navigator 패널 XY Plane의 위 화살표 버튼이 눌리지 않고 View 패널에 아무런 변화가 없는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.zAxisSpinBox, "Enabled", cmpEqual, False)
  Regions.TomoAnalysis102.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)
  Regions.TomoAnalysis103.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@given("Movie maker 탭에 Slice 효과 옵션이 추가되어 있다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  TC_AdaptiveTabWidget = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.panel_base.TC_AdaptiveTabWidget
  TC_AdaptiveTabWidget.TC_TabBar.Click(151, 8)
  TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.RecordVideoPanel.panel_contents.bt_round_gray700.ClickButton()
  tomoAnalysis.Menu.QtMenu.Click("Slice")

@when("Movie maker 탭의 Record 버튼을 누르고 Multi-view 버튼을 누른다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.panel_base.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.RecordVideoPanel.panel_contents.btnScrollArea.qt_scrollarea_viewport.btnContents.bt_square_gray700.Click(47, 27)
  tomoAnalysis.Menu.QtMenu.Click("Multi-view")

@when("파일의 저장 경로와 이름을 지정하고 저장한다.")
def step_impl():
  dlgSaveVideoFile = Aliases.TomoAnalysis.dlgSaveVideoFile
  HWNDView = dlgSaveVideoFile.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  UIItem = HWNDView.CtrlNotifySink2.ShellView.Item.Regression_Test
  UIItem.Item.Click(108, 15)
  UIItem.Keys("[Enter]")
  dlgSaveVideoFile.btn_S.ClickButton()

@then("지정한 경로에 지정한 이름으로 동영상이 만들어졌는지 확인")
def step_impl():
  aqUtils.Delay(7000)
  explorer = Aliases.explorer
  explorer.wndShell_TrayWnd.ReBarWindow32.MSTaskSwWClass.MSTaskListWClass.Click(30, 10)
  wndCabinetWClass = explorer.wndGenerated_File
  HWNDView = wndCabinetWClass.Generated_File.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink2.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  UIItem = HWNDView.CtrlNotifySink.ShellView.Item.Regression_Test
  UIItem.Item.Click(91, 15)
  UIItem.Keys("[Enter]")
  aqObject.CheckProperty(Aliases.explorer.wndGenerated_File.Generated_File.DUIViewWndClassName.Item.CtrlNotifySink.ShellView.Item.Hep_G2_105_Multi_view_mp4.Item, "Value", cmpEqual, "20200611.164640.571.Hep G2-105_Multi-view.mp4")
  wndCabinetWClass.Close()

@then("생성된 영상과 View 패널 전체 화면의 영상이 같은지 확인")
def step_impl():
  aqUtils.Delay(7000)
  explorer = Aliases.explorer
  explorer.wndShell_TrayWnd.ReBarWindow32.MSTaskSwWClass.MSTaskListWClass.Click(30, 10)
  wndCabinetWClass = explorer.wndGenerated_File
  HWNDView = wndCabinetWClass.Generated_File.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink2.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  UIItem = HWNDView.CtrlNotifySink.ShellView.Item.Regression_Test
  UIItem.Item.Click(68, 14)
  UIItem.Keys("[Enter]")
  aqObject.CheckProperty(Aliases.explorer.wndGenerated_File.Generated_File.DUIViewWndClassName.Item.CtrlNotifySink.ShellView.Item.Hep_G2_105_Multi_view_mp4.Item2, "Value", cmpEqual, "632KB")
  explorer = Aliases.explorer
  wndCabinetWClass = explorer.wndGenerated_File
  wndCabinetWClass.Close()

@given("Movie maker 탭에 Orbit 효과 옵션이 추가되어 있다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  TC_AdaptiveTabWidget = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.panel_base.TC_AdaptiveTabWidget
  TC_AdaptiveTabWidget.TC_TabBar.Click(212, 14)
  TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.RecordVideoPanel.panel_contents.bt_round_gray700.ClickButton()
  tomoAnalysis.Menu.QtMenu.Click("Orbit")

@when("Movie maker 탭 Orbit UI에서 Start Angle 수치 입력 칸에 270을 입력하고 Record 버튼을 누른 뒤 Multi-view 버튼을 눌러 저장한다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  widget = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.panel_base.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.RecordVideoPanel.panel_contents
  spinBox = widget.stackedWidget.orbitPage.orbitScrollArea.qt_scrollarea_viewport.orbitContents.angleSpinBox
  spinBox.qt_spinbox_lineedit.Drag(24, 10, -33, 7)
  spinBox.Keys("27")
  spinBox.wValue = 27
  spinBox.Keys("0")
  spinBox.wValue = 270
  widget.btnScrollArea.qt_scrollarea_viewport.btnContents.bt_square_gray700.Click(31, 12)
  tomoAnalysis.Menu.QtMenu.Click("Multi-view")
  dlgSaveVideoFile = tomoAnalysis.dlgSaveVideoFile
  HWNDView = dlgSaveVideoFile.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  UIItem = HWNDView.CtrlNotifySink2.ShellView.Item.Regression_Test
  UIItem.Item.Click(107, 9)
  UIItem.Keys("[Enter]")
  dlgSaveVideoFile.btn_S.ClickButton()

@then("Record 된 영상에서 3D Rendering의 회전 시작 각이 달라졌는지 확인")
def step_impl():
  explorer = Aliases.explorer
  explorer.wndShell_TrayWnd.ReBarWindow32.MSTaskSwWClass.MSTaskListWClass.Click(43, 15)
  wndCabinetWClass = explorer.wndGenerated_File
  HWNDView = wndCabinetWClass.Generated_File.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink2.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  UIItem = HWNDView.CtrlNotifySink.ShellView.Item.Regression_Test
  UIItem.Item.Click(34, 1)
  UIItem.Keys("[Enter]")
  aqObject.CheckProperty(Aliases.explorer.wndGenerated_File.Generated_File.DUIViewWndClassName.Item.CtrlNotifySink.ShellView.Item.Hep_G2_105_Multi_view_mp4.Item2, "Value", cmpEqual, "3,088KB")
  wndCabinetWClass.Close()

@when("Movie maker 탭 Orbit UI에서 Orbit 칸에 180을 입력한다.")
def step_impl():
  lineEdit = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.panel_base.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.RecordVideoPanel.panel_contents.stackedWidget.orbitPage.orbitScrollArea.qt_scrollarea_viewport.orbitContents.orbitLineEdit
  lineEdit.Drag(52, 19, -103, -1)
  lineEdit.SetText("180")

@when("Movie maker 탭에서 Reverse 체크박스를 체크하고 Record 버튼을 누른 뒤 Multi-view 버튼을 눌러 저장한다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  widget = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.panel_base.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.RecordVideoPanel.panel_contents
  widget.stackedWidget.orbitPage.orbitScrollArea.qt_scrollarea_viewport.orbitContents.orbitCheckBox.ClickButton(cbChecked)
  widget.btnScrollArea.qt_scrollarea_viewport.btnContents.bt_square_gray700.Click(53, 22)
  tomoAnalysis.Menu.QtMenu.Click("Multi-view")
  dlgSaveVideoFile = tomoAnalysis.dlgSaveVideoFile
  HWNDView = dlgSaveVideoFile.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  UIItem = HWNDView.CtrlNotifySink2.ShellView.Item.Regression_Test
  UIProperty = UIItem.Item
  UIProperty.Click(101, 8)
  UIProperty.Click(101, 8)
  UIItem.Click(145, 5)
  UIItem.Keys("[Enter]")
  dlgSaveVideoFile.btn_S.ClickButton()

@then("Record 된 영상에서 3D Rendering의 회전 각이 시계 방향 180도로 설정되었는지 확인")
def step_impl():
  explorer = Aliases.explorer
  explorer.wndShell_TrayWnd.ReBarWindow32.MSTaskSwWClass.MSTaskListWClass.Click(30, 10)
  HWNDView = explorer.wndGenerated_File.Generated_File.DUIViewWndClassName.Item
  tv_ = HWNDView.CtrlNotifySink2.NamespaceTreeControl.tv_
  tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  UIItem = HWNDView.CtrlNotifySink.ShellView.Item.Regression_Test
  UIProperty = UIItem.Item
  UIProperty.Click(72, 18)
  tv_.Keys("[Enter]")
  UIProperty.Click(78, 14)
  UIItem.Keys("[Enter]")
  aqObject.CheckProperty(Aliases.explorer.wndGenerated_File.Generated_File.DUIViewWndClassName.Item.CtrlNotifySink.ShellView.Item.Hep_G2_105_Multi_view_mp4.Item2, "Value", cmpEqual, "3,153KB")
  Aliases.explorer.wndGenerated_File.Close()

@when("Movie maker 탭 Orbit UI에서 T 체크박스에 체크하고 Record 버튼을 누른 뒤 Multi-view 버튼을 눌러 저장한다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  widget = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.panel_base.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.RecordVideoPanel.panel_contents
  widget.stackedWidget.orbitPage.orbitScrollArea.qt_scrollarea_viewport.orbitContents.tRadioButton.ClickButton()
  widget.btnScrollArea.qt_scrollarea_viewport.btnContents.bt_square_gray700.Click(53, 23)
  tomoAnalysis.Menu.QtMenu.Click("Multi-view")
  dlgSaveVideoFile = tomoAnalysis.dlgSaveVideoFile
  HWNDView = dlgSaveVideoFile.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  UIItem = HWNDView.CtrlNotifySink2.ShellView.Item.Regression_Test
  UIItem.Item.Click(88, 15)
  UIItem.Keys("[Enter]")
  dlgSaveVideoFile.btn_S.ClickButton()

@then("Record 된 영상에서 3D Rendering이 사선 방향으로 진동하는지 확인")
def step_impl():
  explorer = Aliases.explorer
  explorer.wndShell_TrayWnd.ReBarWindow32.MSTaskSwWClass.MSTaskListWClass.Click(33, 19)
  HWNDView = explorer.wndGenerated_File.Generated_File.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink2.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  UIItemsView = HWNDView.CtrlNotifySink.ShellView.Item 
  UIItem = UIItemsView.Regression_Test
  UIItem.Item.Click(98, 15)
  UIItem.Keys("[Enter]")
  aqObject.CheckProperty(Aliases.explorer.wndGenerated_File.Generated_File.DUIViewWndClassName.Item.CtrlNotifySink.ShellView.Item.Hep_G2_105_Multi_view_mp4.Item2, "Value", cmpEqual, "3,051KB")
  Aliases.explorer.wndGenerated_File.Close()

@when("Movie maker 탭 Slice UI의 Range 왼쪽 칸에 100을 입력한다.")
def step_impl():
  widget = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.panel_base.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.RecordVideoPanel.panel_contents.stackedWidget.slicePage.sliceScrollArea.qt_scrollarea_viewport.sliceContents
  widget.sliceMaxSpinBox.qt_spinbox_lineedit.Click(28, 6)
  spinBox = widget.sliceMinSpinBox
  lineEdit = spinBox.qt_spinbox_lineedit
  lineEdit.Click(14, 11)
  lineEdit.Drag(14, 11, -25, 1)
  spinBox.Keys("100")
  spinBox.wValue = 100

@when("Movie maker 탭 Slice UI의 Range 오른쪽 칸에 150을 입력하고 Record 버튼을 누른 뒤 Multi-view 버튼을 눌러 저장한다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  widget = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.panel_base.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.RecordVideoPanel.panel_contents
  spinBox = widget.stackedWidget.slicePage.sliceScrollArea.qt_scrollarea_viewport.sliceContents.sliceMaxSpinBox
  spinBox.qt_spinbox_lineedit.Drag(23, 11, -60, 4)
  spinBox.Keys("15")
  spinBox.wValue = 15
  spinBox.Keys("0")
  spinBox.wValue = 150
  widget.btnScrollArea.qt_scrollarea_viewport.btnContents.bt_square_gray700.Click(67, 21)
  tomoAnalysis.Menu.QtMenu.Click("Multi-view")
  dlgSaveVideoFile = tomoAnalysis.dlgSaveVideoFile
  HWNDView = dlgSaveVideoFile.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  UIItem = HWNDView.CtrlNotifySink2.ShellView.Item.Regression_Test
  UIItem.Item.Click(87, 10)
  UIItem.Keys("[Enter]")
  dlgSaveVideoFile.btn_S.ClickButton()

@then("Record 된 영상에서 2D XY HT 이미지의 Z축이 아래에서 위로 움직이는지 확인")
def step_impl():
  explorer = Aliases.explorer
  explorer.wndShell_TrayWnd.ReBarWindow32.MSTaskSwWClass.MSTaskListWClass.Click(40, 11)
  HWNDView = explorer.wndGenerated_File.Generated_File.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink2.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  UIItem = HWNDView.CtrlNotifySink.ShellView.Item.Regression_Test
  UIItem.Item.Click(94, 12)
  UIItem.Keys("[Enter]")
  aqObject.CheckProperty(Aliases.explorer.wndGenerated_File.Generated_File.DUIViewWndClassName.Item.CtrlNotifySink.ShellView.Item.Hep_G2_105_Multi_view_mp4.Item2, "Value", cmpEqual, "636KB")
  explorer = Aliases.explorer
  wndCabinetWClass = explorer.wndGenerated_File
  wndCabinetWClass.Close()


@when("Movie maker 탭 Slice UI에서 Reverse 체크박스를 클릭하고 Record 버튼을 누른 뒤 Multi-view 버튼을 눌러 저장한다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  widget = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.panel_base.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.RecordVideoPanel.panel_contents
  widget.stackedWidget.slicePage.sliceScrollArea.qt_scrollarea_viewport.sliceContents.sliceCheckBox.ClickButton(cbChecked)
  widget.btnScrollArea.qt_scrollarea_viewport.btnContents.bt_square_gray700.Click(55, 26)
  tomoAnalysis.Menu.QtMenu.Click("Multi-view")
  dlgSaveVideoFile = tomoAnalysis.dlgSaveVideoFile
  HWNDView = dlgSaveVideoFile.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  UIItemsView = HWNDView.CtrlNotifySink2.ShellView.Item
  UIItemsView.Regression_Test_backup.Item.Click(87, 0)
  UIItem = UIItemsView.Regression_Test
  UIItem.Item.Click(90, 12)
  UIItem.Keys("[Enter]")
  dlgSaveVideoFile.btn_S.ClickButton()

@then("Record 된 영상에서  2D XY HT 이미지의 Z축이 위에서 아래로 움직이는지 확인")
def step_impl():
  explorer = Aliases.explorer
  explorer.wndShell_TrayWnd.ReBarWindow32.MSTaskSwWClass.MSTaskListWClass.Click(40, 19)
  HWNDView = explorer.wndGenerated_File.Generated_File.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink2.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  UIItem = HWNDView.CtrlNotifySink.ShellView.Item.Regression_Test
  UIItem.Item.Click(110, 7)
  UIItem.Keys("[Enter]")
  aqObject.CheckProperty(Aliases.explorer.wndGenerated_File.Generated_File.DUIViewWndClassName.Item.CtrlNotifySink.ShellView.Item.Hep_G2_105_Multi_view_mp4.Item2, "Value", cmpEqual, "642KB")
  explorer = Aliases.explorer
  wndCabinetWClass = explorer.wndGenerated_File
  wndCabinetWClass.Close()
  
@when("Movie maker 탭 Slice UI의 Window 드롭다운 버튼에서 XZ Plane을 누르고 Record 버튼을 누른 뒤 Multi-view 버튼을 눌러 저장한다.")
def step_impl():
  tomoAnalysis = Aliases.TomoAnalysis
  widget = tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.panel_base.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.RecordVideoPanel.panel_contents
  widget.stackedWidget.slicePage.sliceScrollArea.qt_scrollarea_viewport.sliceContents.sliceWindowComboBox.ClickItem("XZ Plane")
  widget.btnScrollArea.qt_scrollarea_viewport.btnContents.bt_square_gray700.Click(32, 19)
  tomoAnalysis.Menu.QtMenu.Click("Multi-view")
  dlgSaveVideoFile = tomoAnalysis.dlgSaveVideoFile
  HWNDView = dlgSaveVideoFile.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  UIItem = HWNDView.CtrlNotifySink2.ShellView.Item.Regression_Test
  UIItem.Item.Click(78, 7)
  UIItem.Keys("[Enter]")
  dlgSaveVideoFile.btn_S.ClickButton()
  aqUtils.Delay(10000)

@then("Record 된 영상에서  2D XZ HT 이미지의 Z축이 움직이는지 확인")
def step_impl():
  explorer = Aliases.explorer
  explorer.wndShell_TrayWnd.ReBarWindow32.MSTaskSwWClass.MSTaskListWClass.Click(28, 13)
  HWNDView = explorer.wndGenerated_File.Generated_File.DUIViewWndClassName.Item
  HWNDView.CtrlNotifySink2.NamespaceTreeControl.tv_.ClickItem("|바탕 화면|내 PC|DATADISK1 (E:)")
  UIItem = HWNDView.CtrlNotifySink.ShellView.Item.Regression_Test
  UIItem.Item.Click(97, 11)
  UIItem.Keys("[Enter]")
  aqObject.CheckProperty(Aliases.explorer.wndGenerated_File.Generated_File.DUIViewWndClassName.Item.CtrlNotifySink.ShellView.Item.Hep_G2_105_Multi_view_mp4.Item2, "Value", cmpEqual, "450KB")
  explorer = Aliases.explorer
  wndCabinetWClass = explorer.wndGenerated_File
  wndCabinetWClass.Close()
  

@then("3D Visualizer TA종료")
def step_impl():
  TC_TabBar = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.TC_TabBar
  TC_TabBar.Click(297, 35)
  TC_TabBar.CloseButton.Click(11, 15)
  Aliases.TomoAnalysis.mainwindow.Close()
  aqUtils.Delay(3000)


@then("3D Visualizer 탭 왼쪽의 패널 하단에 Modality, Navigator 탭 툴바가 생성되었는지 확인")
def step_impl():
  Regions.TabBar.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TabBar)

@then("3D Visualizer 탭 View 패널의 2D XY HT 이미지가 2Slice 위치로 바뀌었는지 확인")
def step_impl():
  Regions.TomoAnalysis85.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)


@then("3D Visualizer 탭 View 패널의 2D XY HT 이미지가 210Slice 위치로 바뀌었는지 확인")
def step_impl():
  Regions.TomoAnalysis88.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@then("3D Visualizer 탭 View 패널의 2D XY HT 이미지가 150Slice 위치로 바뀌었는지 확인")
def step_impl():
  Regions.TomoAnalysis87.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@then("3D Visualizer 탭 View 패널의 2D YZ HT 이미지가 6Slice 위치로 바뀌었는지 확인")
def step_impl():
  Regions.TomoAnalysis89.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@then("View 패널의 2D YZ HT 이미지가 648Slice 위치로 바뀌었는지 확인")
def step_impl():
  Regions.TomoAnalysis90.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@then("View 패널의 2D YZ HT 이미지가 200Slice 위치로 바뀌었는지 확인")
def step_impl():
  Regions.TomoAnalysis91.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@then("Navigator 패널의 X axis LOCATION 수치가 0.10올라갔는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.xPhySpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "19.02")

@then("Navigator 패널의 X axis LOCATION 수치가 0.09내려갔는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.xPhySpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "18.93")

@then("3D Visualizer 탭 View 패널의 2D XZ HT 이미지가 10위치로 바뀌었는지 확인")
def step_impl():
  Regions.TomoAnalysis93.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@then("3D Visualizer 탭 View 패널의 2D XZ HT 이미지가 6Slice 위치로 바뀌었는지 확인")
def step_impl():
  Regions.TomoAnalysis94.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@then("3D Visualizer 탭 View 패널의 2D XZ HT 이미지가 648Slice 위치로 바뀌었는지 확인")
def step_impl():
  Regions.TomoAnalysis95.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@then("3D Visualizer 탭 View 패널의 2D XZ HT 이미지가 200Slice 위치로 바뀌었는지 확인")
def step_impl():
  Regions.TomoAnalysis96.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.panel_base.panel_contents.TomoAnalysis_Viewer_Plugins_OIVViewer)

@then("Navigator 패널의 Y axis LOCATION 수치가 0.10올라갔는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.yPhySpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "19.02")

@then("Navigator 패널의 XZ Plane의 수치가 내려갔는지 확인")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget6.NavigatorPanel.panel_contents.yAxisSpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "105")

@given("TCF가 프로젝트에 있다.")
def step_impl():

  dir = r'E:\Regression_Test\test*.*'
  for f in glob.glob(dir):
    os.remove(f)
  dir1 = r'E:\Regression_Test\*.png'
  for f in glob.glob(dir1):
    os.remove(f)
  tomoAnalysis = Aliases.TomoAnalysis
  tomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.operationPanel.panel_base.widget_panel.SelectProjectPanel.bt_round_operation2.Click(88, 31)
  dlgOpenProject = tomoAnalysis.dlgOpenProject
  dlgOpenProject.OpenFile("E:\\Regression_Test\\Prefix_File\\3D_Visualizer\\3D_Visualizer.tcpro")
  frame = Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.operationFrame.operationSplitter.previewPanel.panel_base

@then("3D Visualizer 탭 View 패널에서 빨간색 형광의 Rendering의 투명도가 바뀌었는지 확인1")
def step_impl():
  aqObject.CheckProperty(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.splitter.mainWidget.MainWindow.TC_TCDockWidget.panel_base.TabWidget.qt_tabwidget_stackedwidget.FLChannelPanel.panel_contents.redSpinBox.qt_spinbox_lineedit, "wText", cmpEqual, "71")
  Regions.TomoAnalysis119.Check(Aliases.TomoAnalysis.mainwindow.TomoAnalysis6)

@then("3D Visualizer 탭 View 패널에서 빨간색 형광의 Rendering의 투명도가 바뀌었는지 확인2")
def step_impl():
  Regions.TomoAnalysis120.Check(Aliases.TomoAnalysis.mainwindow.TomoAnalysis6)

@when("Navigator 패널에 흰색 테두리가 생기면 마우스를 놓는다.")
def step_impl():
  Regions.TabBar1.Check(Aliases.TomoAnalysis.mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.TabBar)

@when("Screen shot 패널 3D view 탭의 All axis 버튼을 누른다.")
def step_impl():
  tomoAnalysis_mainwindow = Aliases.TomoAnalysis.mainwindow
  tomoAnalysis_mainwindow.TC_AdaptiveTabWidget.qt_tabwidget_stackedwidget.MainWindow.mainSplitter.splitter.mainWidget.MainWindow.TC_TCDockWidget5.ScreenShotWidgetUI.panel_contents.tabWidget.qt_tabwidget_stackedwidget.view3DTab.bt_round_gray7003.ClickButton()

