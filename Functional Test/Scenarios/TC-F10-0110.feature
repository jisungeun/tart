﻿"Feature: BA AI Segmentation Single RUN

  Scenario: 생성된 3D 마스크의 시야를 초기 시점으로 설정
Given Basic Analyzer 탭의 Mask Viewing panel에 마스크가 생성되어 있다.

When 3D 마스크 캔버스의 우상단의 톱니바퀴 아이콘을 클릭한다.

And View 버튼을 클릭한다.

And View의 Reset View 버튼을 클릭한다.

Then 처음 마스크가 생성되었을 때의 초기 시점으로 마스크가 보이는지 확인