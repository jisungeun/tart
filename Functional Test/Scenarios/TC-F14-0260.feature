﻿Feature: BAT AI Segmentation Single RUN

  Scenario: BAT Single Run 탭의 Hide Result 버튼을 눌러 Measurement 팝업을 없앰
Given Basic Analyzer[T] Single Run 탭에서 Measurement 팝업이 열려 있다.

When Basic Analyzer[T] 탭의 Hide Result 버튼을 누른다.

Then Basic Analyzer[T] 탭의 Hide Result 버튼이 Show Result 버튼으로 바뀌었는지 확인