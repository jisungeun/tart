﻿Feature: BA AI Segmentation Single RUN

  Scenario: BA 탭의 Processing mode 패널에서, Basic Measurement의 table에서 preset을 ‘Customize’로 설정했을 때 값을 직접 입력
Given Basic Analyzer Single Run 탭이 열려 있다.

When Processing mode 패널 Basic Measurement의 table에서 모든 column의 Preset 드롭다운 버튼을 Customize로 선택한다.

And Processing mode 패널에서 Baseline RI row의 cell을 클릭하고 키보드로 ‘12345’를 입력하고 키보드의 엔터 키를 누른다.

And Processing mode 패널에서 RII row의 cell을 클릭하고 키보드로 ‘12345’를 입력하고 키보드의 엔터 키를 누른다.

Then 네 column에서 모두 Baseline RI 값과 RII값이 ‘12345’로 변경되는지 확인