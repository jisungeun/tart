﻿Feature: 3D Visualizer; HT Image Setup and Screenshot

  Scenario: Movie maker 탭의 Record 버튼을 이용해 효과 옵션을 입힌 Multi-view 영상을 저장
Given 3D Visualizer 탭의 View 패널에 Rendering이 생성되어 있다.

And Movie maker 탭에 Slice 효과 옵션이 추가되어 있다.

When Movie maker 탭의 Record 버튼을 누르고 Multi-view 버튼을 누른다.

And 파일의 저장 경로와 이름을 지정하고 저장한다.

Then 지정한 경로에 지정한 이름으로 동영상이 만들어졌는지 확인

And 생성된 영상과 View 패널 전체 화면의 영상이 같은지 확인

Then 3D Visualizer TA종료