﻿Feature: BA AI Segmentation Single RUN

  Scenario: BA 탭의 Processing mode 패널에서, Basic Measurement의 table의 ‘Protein’ preset 값을 확인
Given Basic Analyzer Single Run 탭이 열려 있다.

When Processing mode 패널  Whole cell column의 Preset 드롭다운 버튼에서 Protein을 선택한다.

And Processing mode 패널  Nucleus column의 Preset 드롭다운 버튼에서 Protein을 선택한다.

And Processing mode 패널  Nucleolus column의 Preset 드롭다운 버튼에서 Protein을 선택한다.

And Processing mode 패널 Lipid droplet column의 Preset 드롭다운 버튼에서 Protein을 선택한다.

Then 네 column에서 모두 Baseline RI 값이 1.3337로, RII값이 0.19로 입력되는지 확인한다.