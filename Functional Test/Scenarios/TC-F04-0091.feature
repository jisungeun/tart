﻿Feature: Create Hypercube

  Scenario: 엔터 키를 이용하여 Rename Hypercube 버튼을 눌러 Hypercube의 이름을 변경 
Given Hypercube가 생성되어 있다.

When Playground canvas의 Hypercube 아이콘을 우클릭한다.

And Rename Hypercube 버튼을 누른다.

And 팝업에서 Hypercube의 이름을 지정하고 키보드의 엔터 키를 누른다.

Then Project Explorer 패널에서 Hypercube 이름이 변경됨을 확인

And Playground canvas에서 Hypercube 이름이 변경됨을 확인