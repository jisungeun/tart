﻿Feature: BA AI Segmentation Single RUN

  Scenario: Show Result 버튼을 눌러 Measurement 팝업을 띄움
Given 주어진 TCF에 대해 위, 아래 Execute 버튼을 모두 눌러 작업을 수행한 Basic Analyzer 탭이 열려 있다.

And Measurement 팝업이 닫혀 있다.

When Show Result 버튼을 누른다.

Then Measurement 팝업이 생기는지 확인

And Show Result 버튼이 Hide Result 버튼으로 바뀌었는지 확인