﻿Feature: BAT Report[T]

  Scenario: Report[T] 탭에서 그래프의 y축 Min 값을 설정
Given Report[T] 탭에 그래프가 그려져 있다.

When Report[T] 탭에서 그래프의 Min 값을 입력하는 창에 원하는 수치를 넣고 키보드의 엔터 키를 누른다.

Then Report[T] 탭에서 그래프의 y축 최솟값이 설정된 값으로 바뀌었는지 확인