﻿Feature: Mask Editor; From empty mask

  Scenario: Mask Editor에 새로운 ID를 생성/설정
Given 빈 TA가 열려있다.   
  
Given Mask Editor 탭이 열려 있다.

When ID manager 패널의 New 버튼을 누른다.

And ID 입력 칸에 아무것도 입력하지 않고 OK 버튼을 누른다.

Then “Empty or invalid playground name'이라는 경고창이 나타나는지 확인

When ID manager 패널의 New 버튼을 누른다.

And ID 입력 칸에 ‘test’를 입력하고 OK 버튼을 누른다.

Then ID manager 패널 드롭다운 버튼에서 test 라는 ID가 생겼는지 확인

When ID manager 패널의 New 버튼을 누르고 팝업에서 ID 입력 칸에 ‘test’를 입력하고 OK 버튼을 누른다.

Then “ID already exist!” 라는 경고창이 나타나는지 확인

And ID manager 패널의 드롭다운 버튼에서 ‘test’ ID가 한 개만 존재하는지 확인

When ID manager 패널의 New 버튼을 누르고 팝업에서 Cancel 버튼을 누른다.

Then “ID is invalid!” 라는 경고창이 나타나는지 확인

Then TA종료