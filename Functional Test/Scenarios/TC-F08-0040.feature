﻿Feature: PEP/UI features

  Scenario: Project Explorer Panel에서 TCF를 프로젝트에서 삭제
Given 빈 TA가 열려있다.   
  
Given 프로젝트에 TCF가 있다.

When Preview 패널에서 TCF를 우클릭 한다.

And Create Simple Playground 버튼을 누른다.

Then Playground canvas에 ‘여섯 자리 숫자_여섯 자리 숫자’ 의 동일한 이름으로 Hypercube와 cube 아이콘이 생성됨을 확인

And Playground setup 패널에 ‘여섯 자리 숫자_여섯 자리 숫자’ 의 이름으로 playground가 새로 생성되었는지 확인

And  Operation Sequence가 Select Application으로 넘어감을 확인

Then TA종료