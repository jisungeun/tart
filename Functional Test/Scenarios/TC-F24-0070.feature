﻿Feature: Mask Editor; From empty mask

  Scenario: Mask Editor 탭 FL tool 패널에서 Ch0, 1 FL 보기
Given 빈 TA가 열려있다.   
  
Given Blue FL을 가진 TCF로 Mask Editor 탭이 열려 있다.

When FL tool 패널의 Ch0 버튼을 누른다.

Then Mask Editor의 2D Slice 세 화면에 Blue FL이 나타났는지 확인

When FL tool 패널에서 Ch1 버튼을 누른다.

Then Mask Editor의 2D Slice 세 화면에 Blue FL이 사라지고 Green FL이 나타났는지 확인

And FL tool 패널의 Channel 박스에서 Ch0의 체크가 사라지고 Ch1의 체크가 나타났는지 확인

When FL tool 패널의 None 버튼을 누른다.

Then Mask Editor의 2D Slice 세 화면에 FL이 사라졌는지 확인

When FL tool 패널에서 Ch0 버튼을 누른다.

And FL tool 패널의 Channel 박스 아래 사이즈 바를 마우스로 클릭한 채로 오른쪽 끝으로 드래그한다.

Then FL tool 패널 사이즈 바 옆 값이 1.00으로 바뀌었는지 확인

And Mask Editor 탭 2D Slice 화면의 FL 렌더링이 선명해졌는지 확인

When FL tool 패널의 Channel 박스 아래의 Color 버튼을 누르고 팝업에서 흰색을 선택한 후 OK 버튼을 누른다.

Then Mask Editor 탭 2D Slice 화면의 Green FL이 흰색으로 바뀌었는지 확인


Then TA종료