﻿Feature: Mask Editor; From empty mask

  Scenario: Preview 패널에서 TCF를 선택하고 Mask editor를 실행Preview 패널에서 TCF를 선택하고 Mask editor를 실행
Given 빈 TA가 열려있다.   
  
Given TCF파일이 들어있는 프로젝트가 열려있다.

When Preview 패널에서 원하는 TCF 파일을 한 개 선택한다.

And Toolbox의 Mask editor 버튼을 누른다.

And General 패널의 Apply to Result 버튼이 비활성화 되었는지 확인

And Mask selector 패널이 비활성화 되었는지 확인

When General 패널의 Switch window 버튼을 누른다.

Then XY Slice 화면과 YZ Slice 화면의 위치가 바뀌었는지 확인

When General 패널의 Switch window 버튼을 누른다.

Then YZ Slice 화면과 XZ Slice 화면의 위치가 바뀌었는지 확인

When General 패널의 Switch window 버튼을 누른다.

Then 가운데로 3D Mask 화면이, 오른쪽은 위부터 XY, YZ, XZ Slice 화면이 오도록 바뀌었는지 확인

When General 패널의 Switch window 버튼을 누른다.

Then 3D Mask 화면과 XY Slice 화면의 위치가 바뀌었는지 확인

When Label tools 패널의 Label picker 버튼을 누른다.

Then “Failed to activate Picking Tool” 경고창이 뜨는지 확인

When Drawing tool 패널의 Add 버튼을 누른다.

Then “Failed to activate add tool” 경고창이 뜨는지 확인

When Clean tool 패널의 Cleaning-2D plane 버튼을 누른다.

Then “Failed Clean current slice” 경고창이 뜨는지 확인

When Semi-auto tool 패널의 Divide 버튼을 누른다.

Then “Failed to activate divide tool” 경고창이 뜨는지 확인

Then TA종료