﻿Feature: BAT Application Parameter

  Scenario: RI Thresholding을 프로세서로 선택했을 때, Auto lower Threshold 체크박스 체크
Given BAT Application Parameter에서 RI Thresholding이 프로세서로 선택되어 있다.

And BAT Application Parameter 패널의 Auto lower threshold 체크박스가 비어있다.

When BAT Application Parameter 패널의 Auto lower Threshold 체크박스를 클릭한다.

Then BAT Application Parameter 패널의 Auto Threshold 탭에 ‘Select Algorithm’ 문구와 알고리즘 드롭다운 버튼이 나타나는지 확인

And BAT Application Parameter 패널의 Auto lower Threshold 체크박스가 하이라이트 되며 체크 되는지 확인