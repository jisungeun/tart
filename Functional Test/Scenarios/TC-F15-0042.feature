﻿Feature: BAT Batch RUN setting

  Scenario: BAT Batch Run setting 탭에서 개별 TCF가 하이라이트 되어 있을 때 UI의 빈 공간을 클릭하여 하이라이트 해제
Given Basic Analyzer[T]의 Batch Run setting 탭의 개별 TCF가 하이라이트 되어 있다.

When Basic Analyzer[T] Batch Run setting 탭의 아무런 UI도 없는 빈 공간을 클릭한다.

Then Batch Run setting 탭의 하이라이트 된 TCF에서 하이라이트가 해제되었는지 확인

And Batch Run setting 탭에서 TCF의 하이라이트 되어 있던 부분에 점선 박스가 생겼는지 확인

And Batch Run setting 탭에서 TCF의 체크 유무는 유지되었는지 확인