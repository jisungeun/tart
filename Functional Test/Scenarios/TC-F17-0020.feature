﻿Feature: TC-F17-0020

  Scenario: BAT 탭에서 RI 마스크 보기
Given 빈 TA가 열려있다.   
  
Given Basic Analyzer[T] 탭에서 RI Thresholding이 프로세서로 선택되어 있다.

When Basic Analyzer[T] 탭에서 UL Threshold 탭의 Execute 버튼을 누른다.

Then Basic Analyzer[T] Single Run 탭의 Mask Viewing panel 3D 화면과 2D 화면에 RI 마스크가 생성되었는지 확인

When 마우스를 커서 모양이 바뀌는 Basic Analyzer[T] 탭의 Mask Viewing panel 3D 마스크 캔버스 위로 가져가 클릭한 채로 마우스를 움직인다.

Then Basic Analyzer[T] 탭 Mask Viewing panel에 생성된 3D 마스크와 정육면체가 회전하는지 확인

When 3D 마스크 캔버스 좌하단 정육면체의 위쪽 변을 누른다.

Then 해당 변이 정면으로 오도록 정육면체와 마스크의 방향이 움직였는지 확인

When 3D 마스크 캔버스 좌하단 정육면체의 Left면을 누른다.

Then Left 면이 정면으로 오도록 정육면체와 마스크의 방향이 움직였는지 확인

When 3D 마스크 캔버스 정육면체의 Left 면의 오른쪽 위 점을 클릭한다.

Then 해당 점이 정면으로 오도록 정육면체와 마스크의 방향이 움직였는지 확인

When 마우스를 커서 모양이 바뀌는 Basic Analyzer[T] 탭의 Mask Viewing panel 3D 마스크 캔버스 위로 가져가 마우스 휠을 위로 두 칸 올린다.

Then Basic Analyzer[T] 탭 Mask Viewing panel에 생성된 3D 마스크가 두 단위 확대됨을 확인

When 마우스를 커서 모양이 바뀌는 Basic Analyzer[T] 탭 Mask Viewing panel 3D 마스크 캔버스 위로 가져가 마우스 휠을 아래로 두 칸 내린다.

Then Basic Analyzer[T] 탭 Mask Viewing panel에 생성된 3D 마스크가 두 단위 축소됨을 확인

When Basic Analyzer[T] 탭 Mask Viewing panel 상단 Mask 툴박스의 Opacity 칸에 ‘.5’를 입력한다.

Then Basic Analyzer[T] 탭 Mask Viewing panel의 마스크들이 입력 값에 맞게 투명해짐을 확인

When Basic Analyzer[T] 탭 Mask Viewing panel의 HT 이미지 캔버스 위에 마우스 포인터를 올리고 마우스 휠을 위로 5칸 올린다.

Then Basic Analyzer[T] 탭의 Mask Viewing panel HT 단층 사진의 Z축이 올라가는지 확인

When Basic Analyzer[T] 탭 Mask Viewing panel의 HT 이미지 캔버스 위에 마우스 포인터를 올리고 마우스 휠을 아래로 5칸 내린다.

Then Basic Analyzer[T] Single Run 탭의 Mask Viewing panel HT 단층 사진의 Z축이 내려가는지 확인

When Basic Analyzer[T] 탭 Basic Measurement의 table의 parameter를 설정하고 Basic Measurement 탭의 Execute 버튼을 누른다.

Then Basic Analyzer[T] 탭 위에서 프로세싱 바 팝업이 사라지고 Measurement 팝업이 생기는지 확인한다.

When Basic Analyzer[T] 탭의 Movie player 패널에서 setting한 time points 중 하나로 넘어간다.

Then Basic Analyzer[T] 탭 위에 Measurement 팝업이 생기는지 확인

And 새로 생긴 Basic Analyzer[T] 탭 Measurement 팝업의 table의 Time Point column 값이 원래 열려 있던 Measurement 팝업의 table의 Time Point column 값과 다른지 확인

When Basic Analyzer[T] 탭 위 Measurement 팝업의 Export 버튼을 누른다.

And 파일 탐색기 팝업에서 취소 버튼을 누른다.

Then ‘Failed to save CSV’ 라는 내용의 에러 메세지 팝업이 뜨는지 확인

And TA종료