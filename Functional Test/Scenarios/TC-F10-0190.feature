﻿Feature: BA AI Segmentation Single RUN

  Scenario: Measurement 팝업의 각 row 색깔이 cell mask의 색과 동일
Given Basic Analyzer 탭의 Mask Viewing panel에 마스크가 생성되어 있다.

When Basic Measurement의 table의 parameter를 설정하고 Basic Measurement 탭의 Execute 버튼을 누른다. 

Then Measuerment 팝업의 각 row가 하이라이트 된 색이 cell mask 색과 동일한지 확인