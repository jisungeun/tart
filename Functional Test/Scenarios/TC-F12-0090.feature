﻿Feature: BA RI Thresholding Single Run

  Scenario: BA 탭에서 프로세서가 RI Thresholding일 때, Auto lower Threshold 체크박스 체크 후 알고리즘 Entropy 선택
Given Basic Analyzer 탭에서 Auto lower Threshold 체크박스가 체크되어 있다.

When Basic Analyzer 탭의 Select Algorithm 드롭다운 버튼에서 Entropy를 선택한다.

Then Basic Analyzer 탭 Processing mode 패널 Auto Threshold 탭의 Select Algorithm 버튼에 Entropy가 선택되었는지 확인