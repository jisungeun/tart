﻿Feature: BA RI Thresholding Single Run

  Scenario: BA 탭에서 프로세서가 RI Thresholding일 때, Use Labeling 체크박스 체크 후 Largest Label 옵션을 선택
Given Basic Analyzer 탭에서 Use Labeling 체크박스가 체크되어 있다.

When Basic Analyzer 탭 Processing mode 패널의 Select Option 드롭다운 버튼을 누르고 Largest Label 버튼을 누른다.

Then Basic Analyzer 탭 Processing mode 패널의 Select Option 버튼이 Largest Label 옵션으로 변경되었는지 확인 