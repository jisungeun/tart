﻿Feature: BAT Application Parameter

  Scenario: RI Thresholding을 프로세서로 선택했을 때, UL Threshold 탭에서 윗 화살표 버튼을 눌러 lower threshold 수치를 올리기
Given BAT Application Parameter에서 RI Thresholding이 프로세서로 선택되어 있다.

When BAT Application Parameter 패널의 ‘Assign lower threshold’ 수치 칸 옆의 윗 방향 화살표를 한 번 누른다.

Then BAT Application Parameter 패널의 ‘Assign lower threshold’의 수치가 1.3501로 올라가는지 확인