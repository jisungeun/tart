﻿Feature: BAT Application Parameter

  Scenario: AI Segmentation을 프로세서로 선택했을 때, Basic Measurement의 table의 ‘Hemoglobin’ preset 값을 확인
Given BAT Application Parameter에서 AI Segmentation이 프로세서로 선택되어 있다.

When BAT Application Parameter 패널 Whole cell column의 Preset 드롭다운 버튼에서 Hemoglobin을 선택한다.

And BAT Application Parameter 패널 Nucleus column의 Preset 드롭다운 버튼에서 Hemoglobin을 선택한다.

And BAT Application Parameter 패널 Nucleolus column의 Preset 드롭다운 버튼에서 Hemoglobin을 선택한다.

And BAT Application Parameter 패널 Lipid droplet column의 Preset 드롭다운 버튼에서 Hemoglobin을 선택한다.

Then BAT Application Parameter 패널 Basic Measurement의 table의 네 column에서 모두 Baseline RI 값이 1.3337로, RII값이 0.15로 입력되는지 확인