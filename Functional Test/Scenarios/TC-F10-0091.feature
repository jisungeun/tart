﻿Feature: BA AI Segmentation Single RUN

  Scenario: Mask Viewing panel의 3D 마스크 위에 마우스를 올리고 마우스 휠을 조작하여 마스크를 축소
Given Basic Analyzer 탭의 Mask Viewing panel에 3D 마스크가 생성되어 있다.

When 마우스를 커서 모양이 바뀌는 3D 마스크 캔버스 위로 가져가 마우스 휠을 아래로 내린다.

Then 3D 마스크가 축소됨을 확인