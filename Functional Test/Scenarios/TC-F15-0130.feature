﻿Feature: BAT Batch RUN setting

  Scenario: BAT Application Parameter에서 AI Segmentation으로 프로세서를 지정하고 Single RUN을 돌리면 Processing mode의 기본 값이 AI Segmentation으로 설정
Given BAT Application Parameter에서 프로세서로 AI Segmentation이 선택되어 있다.

When BAT Application Parameter의 Batch RUN 버튼을 누른다.

Then Basic Analyzer[T] 탭의 Batch Run setting 탭에서 Processing mode 패널의 맨 위 드롭다운 버튼이 AI Segmentation으로 설정됐는지 확인