﻿Feature: Link Data to Cube

  Scenario: cube에 TCF를 연결 해제할 때 Playground canvas에서 cube의 아이콘이 변경

Given TCF가 cube에 연결되어 있다.

When cube에 연결된 TCF를 모두 연결 해제한다.

Then Playground canvas에서 TCF containing Cube 아이콘이 Empty Cube 아이콘으로 바뀌는지 확인 