﻿Feature: 3D Visalizer; HT+FL Image

  Scenario: 3D Visualizer에서 FL Rendering이 생성되어 있을 때, Preset 패널의 FL 탭에서 Blue FL rendering을 삭제
Given 빈 TA가 열려있다.   
  
Given 3D Visualizer Modality 패널의 FL 체크박스가 체크 되어있다.

When Preset 패널 FL 탭의 Blue 체크박스를 클릭한다.

Then 3D Visualizer 탭 View 패널에서 파란색 형광의 Rendering이 모두 사라졌는지 확인

And Preset 패널의 FL 탭의 Blue 체크박스가 체크 해제됐는지 확인

When Preset 패널 FL 탭의 Blue 체크박스를 클릭한다.

Then 3D Visualizer 탭 View 패널에서 파란색 형광의 Rendering이 모두 생성됐는지 확인

And Preset 패널의 FL 탭의 Blue 체크박스가 체크 됐는지 확인

When Preset 패널 FL 탭 하단의 table에서 Blue 채널의 Max 셀에 180을 입력하고 키보드의 엔터 키를 누른다.

When Preset 패널 FL 탭 하단의 table에서 Blue 채널의 Min 셀에 150을 입력하고 키보드의 엔터 키를 누른다.

Then 3D Visualizer 탭 View 패널에서 파란색 형광 Rendering이 바뀌는지 확인

Then 3D Visualizer TA종료