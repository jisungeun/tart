﻿Feature: BAT Application Parameter

  Scenario: RI Thresholding을 프로세서로 선택했을 때, UL Threshold 탭에서 값을 직접 입력하여 수치를 설정하기
Given BAT Application Parameter에서 RI Thresholding이 프로세서로 선택되어 있다.

When BAT Application Parameter 패널의 ‘Assign lower threshold’ 사이즈 바 옆의 칸에 '1'을 입력한다.

And BAT Application Parameter 패널의 ‘Assign upper threshold’ 사이즈 바 옆의 칸에 '2'를 입력한다.

Then BAT Application Parameter 패널의 ‘Assign lower threshold’ 사이즈 바와 ‘Assign upper threshold’ 사이즈 바가 각각 왼쪽 끝과 오른쪽 끝으로 이동했는지 확인