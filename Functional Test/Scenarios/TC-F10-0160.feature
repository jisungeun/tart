﻿"Feature: BA AI Segmentation Single RUN

  Scenario: Mask Viewing panel의 HT 이미지 캔버스의 Level window를 조절
Given Basic Analyzer Single Run 탭이 열려 있다.

When HT 이미지 캔버스의 우상단의 톱니바퀴 아이콘을 클릭한다.

And Level Window 버튼을 클릭한다.

And Data Range 사이즈 바의 왼쪽 버튼을 마우스로 오른쪽 끝까지 움직인다.

Then HT 이미지가 어둡게 변하는지 확인

And 사이즈 바 밑의 왼쪽 수치가 오른쪽 수치와 14320으로 같아졌는지 확인