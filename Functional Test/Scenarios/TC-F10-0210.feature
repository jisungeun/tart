﻿Feature: BA AI Segmentation Single RUN

  Scenario: Measurement 팝업에서 tabel 셀을 클릭 시 해당 셀이 포함된 row가 전체 하이라이트
Given Basic Analyzer 탭에서 Measurement 팝업이 열려 있다.

When 테이블의 셀 하나에 마우스 포인터를 가져가서 클릭한다.

Then 클릭한 테이블 셀이 포함된 row 전체가 파란색으로 하이라이트 됨을 확인