﻿Feature: Set Playground

  Scenario: Playground setup 패널에서 Playground를 설정
  
Given 빈 TA가 열려있다.  
  
Given 프로젝트에 Playground가 두 개 이상 생성되어 있다.

When Playground Setup 패널에서 선택되어 있지 않은 Playground를 클릭한다.

Then Playground canvas 패널 상단에 선택된 Playground의 이름이 나타나는지 확인

When playground setup 패널에서 이름을 변경하고 싶은 playground를 선택하고 Rename 버튼을 누른다.

And Playground의 이름을 변경하는 팝업에서 변경할 이름을 입력하고 OK 버튼을 누른다.

Then Playground setup 패널에서 playground의 이름이 변경되었는지 확인

When playground setup 패널에서 삭제할 playground를 선택하고 Delete 버튼을 누른다.

And Playground의 이름을 삭제하는 팝업의 Delete 버튼을 누른다.

Then Playground setup 패널에서 playground가 삭제되었는지 확인
  
Then TA종료