﻿"""Feature: BAT AI Segmentation Single RUN

  Scenario: BAT Single Run 탭 이름 옆의 x 버튼을 눌러 BAT Single Run 탭을 종료
Given Basic Analyzer[T] Single Run 탭이 열려 있다.

When Application tab 바의 Basic Analyzer[T] 탭 이름 옆의 x 버튼을 클릭한다.

Then Basic Analyzer[T] Single Run 탭이 종료되는지 확인