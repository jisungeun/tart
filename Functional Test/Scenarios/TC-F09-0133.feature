﻿Feature: BA Application Parameter

  Scenario: RI Thresholding을 프로세서로 선택했을 때, Labeling 탭에서 화살표 버튼을 눌러 ‘Assign Size of Neglectable Particle’ 수치를 내리기
Given BA Application Parameter에서 Use Labeling 체크박스가 체크되어 있다.

When ‘Assign Size of Neglectable Particle’ 수치 칸 옆의 아래 방향 화살표를 한 번 누른다.

Then ‘Assign Size of Neglectable Particle’ 값이 24.9999로 바뀌는지 확인