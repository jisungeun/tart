﻿Feature: BAT AI Segmentation Single RUN

  Scenario: BAT Single Run 탭에서 Mask Viewing panel의 3D 마스크 위에 마우스를 올리고 마우스 휠을 조작하여 마스크를 축소
Given Basic Analyzer[T] Single Run 탭의 Mask Viewing panel에 3D 마스크가 생성되어 있다.

When 마우스를 커서 모양이 바뀌는 Basic Analyzer[T] 탭 Mask Viewing panel 3D 마스크 캔버스 위로 가져가 마우스 휠을 아래로 두 칸 내린다.

Then Basic Analyzer[T] 탭 Mask Viewing panel에 생성된 3D 마스크가 두 단위 축소됨을 확인