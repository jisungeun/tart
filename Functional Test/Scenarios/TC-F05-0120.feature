﻿Feature: Create Cube

  Scenario: Copy Cube 버튼을 눌러 Cube를 복사할 때 그 Cube가 속해있는 Hypercube가 복사
Given Cube가 생성되어 있다.

When Playground canvas의 Cube 아이콘을 우클릭한다.

And Copy Cube 버튼을 누른다.

Then Project Explorer 패널에서 '(Cube가 속해 있는 hypercube의 원본 Hypercube 이름)_copy’의 Hypercube가 생성됨을 확인

And Playground canvas에서 ‘(원본 Hypercube 이름)_copy’의 Hypercube가 생성됨을 확인.