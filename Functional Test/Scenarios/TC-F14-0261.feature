﻿Feature: BAT Single Run 탭의 Hide Result 버튼을 눌러 Measurement 팝업을 없앤 후 Movie player 탭에서 time points setting 패널에서 설정한 다른 time point로 화면을 움직이면 Measurement 탭이 자동 생성 

  Scenario:BAT Single Run 탭의 Hide Result 버튼을 눌러 Measurement 팝업을 없앤 후 Movie player 탭에서 time points setting 패널에서 설정한 다른 time point로 화면을 움직이면 Measurement 탭이 자동 생성 
Given Basic Analyzer[T] Single Run 탭에서 Measurement 팝업이 열려 있다.

When Basic Analyzer[T] 탭의 Movie player 패널에서 setting한 time points 중 하나로 넘어간다.

Then Basic Analyzer[T] 탭 위에 Measurement 팝업이 생기는지 확인

And 새로 생긴 Basic Analyzer[T] 탭 Measurement 팝업의 table의 Time Point column 값이 원래 열려 있던 Measurement 팝업의 table의 Time Point column 값과 다른지 확인