﻿"Feature: BA RI Thresholding Single Run

  Scenario: RI Thresholding을 프로세서로 선택하고 Single RUN 버튼을 눌러 나오는 팝업에서 프로세싱 할 TCF를 선택하고 Single RUN 버튼을 클릭하여 BA 탭 열기
Given BA Application Parameter에서 프로세서로 RI Thresholding이 선택되어 있다.

When BA Application Parameter 패널의 Single RUN 버튼을 누른다.

And Single RUN 팝업에서 App이 연결된 hypercube 안의 TCF 중 Single RUN 하고 싶은 TCF를 선택하고 팝업 안의 Single RUN 버튼을 누른다.

Then Basic Analyzer 탭이 열리는지 확인

And Application tab 바에서 Project Manager 탭 이름의 하이라이트가 사라지고 Basic Analyzer 탭 이름에 하이라이트가 생겼는지 확인

And Single Run 탭 이름에 하이라이트가 생겼는지 확인