﻿Feature: Import Data

  Scenario: Preview 패널에서 TCF를 여러 개 선택/선택 해제
  
Given 빈 TA가 열려있다.

Given TCF의 썸네일이 보이는 Preview 패널이 열려 있다.

When 원하는 TCF 파일의 썸네일을 두 개 순서대로 누른다.

Then 두 개의 TCF 썸네일이 동시에 선택되는지 확인

When 원하는 TCF 파일의 썸네일을 두 개 순서대로 누른다.

Then 두 개의 TCF 썸네일이 선택 해제됐는지 확인

When 원하는 TCF 파일들 중 가장 윗 줄의 가장 왼쪽 파일을 선택한다.

And 원하는 TCF 파일들 중 가장 아랫 줄의 가장 오른쪽 파일을 키보드의 Shift 키와 함께 누른다.

Then 선택한 두 개의 파일 사이에 위치한 모든 TCF 파일들이 선택되었는지 확인

When Preview 패널에서 Deselect all 버튼을 누른다.

Then 선택되었던 모든 TCF가 선택 해제되었는지 확인

When Preview 패널에서 Select all 버튼을 누른다.

Then 불러진 모든 TCF가 선택되었는지 확인

Then TA종료