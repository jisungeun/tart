﻿Feature: BA RI Thresholding Single Run

  Scenario: BA 탭에서 프로세서가 RI Thresholding일 때, Labeling 탭 펼치기
Given Basic Analyzer 탭에서 RI Thresholding이 프로세서로 선택되어 있다.

And Basic Analyzer 탭의 Labeling 버튼 맨 오른쪽의 화살표가 아래 방향(▽)이다.

When Basic Analyzer 탭의 Labeling 버튼을 누른다.

Then Basic Analyzer 탭 Labeling 버튼 밑의 체크박스가 펼쳐지는지 확인

And Basic Analyzer 탭의 Labeling 버튼 맨 오른쪽의 화살표가 윗 방향(△)으로 바뀌는지 확인