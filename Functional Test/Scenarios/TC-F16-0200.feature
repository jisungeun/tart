﻿Feature: BAT Report[T]

  Scenario: Report[T] 탭 Data Table 패널 오른쪽의 화살표 버튼(>)을 눌러 Data Table 탭 화면 전체를 오른쪽으로 이동
 Given 2개 이상의 cube를 batch run 한 결과의 Report[T] 탭이 열려 있다. 

When Report[T] 탭의 Data Table 패널 오른쪽의 화살표 버튼(>)을 누른다.

Then Report[T] 탭의 Data table 패널에서 스크롤 바를 민 것처럼 화면에 표시되는 Data Table 패널 전체가 오른쪽으로 이동함을 확인

When Report[T] 탭의 Data Table 패널 왼쪽의 화살표 버튼(<)을 누른다.

Then Report[T] 탭의 Data table 패널에서 스크롤 바를 민 것처럼 화면에 표시되는 Data Table 패널 전체가 왼쪽으로 이동함을 확인