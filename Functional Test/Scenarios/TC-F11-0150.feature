﻿"Feature: BA Batch RUN

  Scenario: Report 탭 이름 옆의 x 버튼을 눌러 Report 탭을 종료
Given Report 탭이 열려 있다.

When Application tab 바의 Report 탭 이름 옆의 x 버튼을 클릭한다.

Then Report 탭이 종료되는지 확인