﻿Feature: BAT Report[T]

  Scenario: Report[T] 탭 Data Table 패널에 출력된 개별 data tabe 하단부의 스크롤 바를 움직여 data munging
Given 2개 이상의 cube를 batch run 한 결과의 Report[T] 탭이 열려 있다.

And Report[T] 탭 Data Table 패널에 출력된 개별 data table 하단부에 스크롤 바가 생성되어 있다.

When Report[T] 탭의 Data table 하단의 스크롤 바를 마우스로 클릭한 상태로 움직인다.

Then Report[T] 탭의 Data table 패널에서 각 데이터 table이 화면에 보이는 영역이 달라지는지 확인