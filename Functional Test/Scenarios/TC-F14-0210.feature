﻿Feature: BAT AI Segmentation Single RUN

  Scenario: BAT Single Run 탭의 Mask Viewing panel 상단의 Mask 툴박스에서 Opacity를 수치 입력으로 설정하여 마스크의 불투명도를 조절
Given Basic Analyzer[T] Single Run 탭의 Mask Viewing panel에 3D 마스크가 생성되어 있다.

When Basic Analyzer[T] 탭 Mask Viewing panel 상단 Mask 툴박스의 Opacity 칸에 ‘.5’를 입력한다.

Then Basic Analyzer[T] 탭 Mask Viewing panel의 마스크들이 입력 값에 맞게 투명해짐을 확인