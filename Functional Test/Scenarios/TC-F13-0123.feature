﻿Feature: BAT Application Parameter

  Scenario: RI Thresholding을 프로세서로 선택했을 때, Use Labeling 체크박스 체크 후 마우스 휠을 이용해 Multi Labels 옵션을 선택
Given BAT Application Parameter에서 Use Labeling 체크박스가 체크되어 있다.

And BAT Application Parameter 패널의 Select Option 버튼에 Largest Label이 선택되어 있다.

When BAT Application Parameter 패널의 Select Option 드롭다운 버튼에 마우스 포인터를 올리고 마우스 휠을 위로 올린다.

Then BAT Application Parameter 패널의 Select Option 버튼이 Multi Labels 옵션으로 변경되었는지 확인