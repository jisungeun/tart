﻿Feature: BAT AI Segmentation Single RUN

  Scenario: BAT Single Run 탭에서 Mask Viewing panel의 3D 마스크 캔버스의 배경 색을 그라데이션으로 변경
Given Basic Analyzer[T] Single Run 탭이 열려 있다.

When Basic Analyzer[T] 탭 Mask Viewing panel의 3D 마스크 캔버스의 우상단의 톱니바퀴 아이콘을 클릭하고 Background Option 버튼을 클릭한 뒤  Gradient 버튼을 클릭한다.

Then Basic Analyzer[T] 탭 Mask Viewing panel의 3D 마스크 캔버스의 배경 색이 Gradient로 변경됨을 확인