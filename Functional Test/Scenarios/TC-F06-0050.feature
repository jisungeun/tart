﻿Feature: Link Data to Cube

  Scenario: List 패널에서 "<" 버튼을 이용해 TCF를 cube에서 연결 해제
	
Given TCF가 cube에 연결되어 있다.

When List 패널의 오른쪽 table에서 cube에서 연결 해제하고 싶은 TCF를 선택한다.

And List 패널의 < 버튼을 누른다.

Then Project Explorer 패널에서 cube에서 TCF가 연결 해제되었는지 확인

And List 패널의 오른쪽 table에서 cube에 연결 해제된 TCF row가 삭제되었는지 확인