﻿Feature: BAT AI Segmentation Single RUN

  Scenario: BAT Single Run 탭의 Time points setting 패널에서 end 칸에 마우스 포인터를 대고 마우스 휠을 올려 time point 끝 값을 올림
Given Basic Analyzer[T] 탭의 Single run 탭이 열려 있다.

And Basic Analyzer[T] 탭의 Time points setting 패널 end 칸에 '3’이 입력되어 있다.

When Basic Analyzer[T] 탭의 Time points setting 패널에서 end 칸에 마우스 포인터를 대고 마우스 휠을 위로 한 칸 올린다.

Then Basic Analyzer[T] 탭 end 칸의 숫자가 1만큼 올라갔는지 확인

And Basic Analyzer[T] 탭 Time points setting 패널의 Selected time points에 나타나는 끝 숫자가 1만큼 올라갔는지 확인