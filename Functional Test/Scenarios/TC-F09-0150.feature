﻿Feature: BA Application Parameter

  Scenario: RI Thresholding을 프로세서로 선택했을 때, Basic Measurement의 table이 default 값으로 세팅
Given Playground canvas에서 Basic Analyzer 아이콘이 선택되어 있다.

When RI Thresholding을 프로세서로 선택한다.

Then Basic Measurement의 table의 Preset row가 Protein으로 선택되어 있는지 확인