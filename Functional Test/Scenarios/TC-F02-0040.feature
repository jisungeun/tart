﻿Feature: Import Data

  Scenario: List 패널에서 프로젝트에 불러진 TCF를 확인
  
  Given 프로젝트가 열려있다.

  When Import 버튼을 누른다.

  And TCF파일이 있는 폴더를 선택한다.

  And List 패널을 누른다.

  Then List 패널의 왼쪽 table에 불러진 TCF의 Data/Name/HT/FL/BF column의 정보가 누락 없이 나타나 있는지 확인

  And List 패널의 왼쪽 table에 불러진 TCF의  Cube/Hypercube column이 비어있는지 확인