﻿Feature: BA Batch RUN

  Scenario: Inter-Cube mean comparison panel에 Table을 표시
Given Report 탭의 Inter-Cube mean comparison panel에 graph가 열려 있다.

When Inter-Cube mean comparison panel에서 ‘Table' 텍스트를 클릭한다.

Then Inter-Cube mean comparison panel에 graph 대신 table이 나타나는지 확인

And Inter-Cube mean comparison panel의 Graph 글자의 하이라이트가 꺼지고 Table 글자가 하이라이트 됨을 확인