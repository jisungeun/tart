﻿Feature: Import Data

  Scenario: List 패널에서 Ctrl 키를 이용하여 TCF를 여러 개 선택
Given TCF가 불러진 List 패널이 열려 있다.

When 컨트롤 키를 누른 상태로 원하는 TCF 파일을 여러 개 선택한다.

Then 여러 개의 TCF 파일이 선택되었는지 확인