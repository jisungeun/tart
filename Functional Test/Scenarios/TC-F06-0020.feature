﻿Feature: Link Data to Cube

  Scenario: Link to Cube 버튼을 이용하여 Cube에 TCF를 연결
  
Given 빈 TA가 열려있다.   
  
Given TCF와 Cube 불러진 List 패널이 열려 있다.

When List 패널의 왼쪽 table에서 cube에 연결하고 싶은 TCF를 선택하고 List 패널의 > 버튼을 누른다.

Then Project Explorer 패널에서 cube에 TCF가 연결되었는지 확인

When List 패널의 Exclusive 체크박스를 체크한다.

Then List 패널의 오른쪽 table에 존재하는 데이터가 왼쪽 table에서 제거됨을 확인

When List 패널의 오른쪽 table에서 cube에서 연결 해제하고 싶은 TCF를 선택하고 List 패널의 "<" 버튼을 누른다.

Then List 패널의 오른쪽 table에서 cube에 연결 해제된 TCF row가 삭제되었는지 확인

Then TA종료