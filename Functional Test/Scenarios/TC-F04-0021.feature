﻿Feature: Create Hypercube

  Scenario: Create Hypercube 탭에서 엔터 키를 이용하여 Hypercube를 생성
Given Create Hypercube 탭이 열려있다.

When default name 외의 Hypercube의 이름을 직접 지정한다.

And  Hypercube 탭에서 키보드의 엔터 키를 누른다.

Then Project Explorer 패널에 생성된 Hypercube가 표시됨을 확인