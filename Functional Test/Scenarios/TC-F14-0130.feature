﻿Feature: BAT AI Segmentation Single RUN

  Scenario: BAT Single Run 탭에서 AI Inference 탭의 Execute 버튼을 클릭하여 Mask Viewing panel에 Mask를 생성
Given Basic Analyzer[T] Single Run 탭에서 AI Segmentation이 선택되어 있다.

When Basic Analyzer[T] 탭의 AI Inference 탭의 Execute 버튼을 클릭한다.

And 프로세싱 바 팝업이 생기면 진행이 완료되고 팝업이 사라질 때 까지 기다린다.

Then setting한 time points에서 Basic Analyzer[T] 탭의 Mask Viewing panel에 마스크가 생성되었는지 확인

And Basic Analyzer[T] 탭 Mask Viewing panel 상단에 Cell Instance, Whole Cell, Nucleus, Nucleolus, Lipid droplet 체크박스와 Cell-wise filter 스위치, Opacity 조절 버튼이 있는 툴박스가 생성되었는지 확인

And Basic Analyzer[T] 탭 Processing mode 패널 AI Inference 탭의 Execute 버튼이 하이라이트 해제되었는지 확인

And Basic Analyzer[T] 탭 Processing mode 패널 Basic Measurement 탭의 Execute 버튼이 하이라이트 되었는지 확인 