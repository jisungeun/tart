﻿Feature: Import Data

  Scenario: Preview 패널에서 프로젝트에 불러진 TCF 파일이 저장된 폴더 이름을 확인
Given 프로젝트가 열려있다.

When Import 버튼을 누른다.

And TCF파일이 있는 폴더를 선택한다.

Then Preview 패널의 제일 왼쪽 썸네일의 윗쪽에 TCF가 저장된 폴더의 이름이 나오는지 확인