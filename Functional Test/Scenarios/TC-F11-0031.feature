﻿Feature: Batch RUN

  Scenario: Report 탭의 Select Prameter 패널을 Volume으로 변경
Given Report 탭이 열려 있다.

When Report 탭 Select Prameter 패널에서 Volume 체크박스에 체크한다.

Then Report 탭 Select Parameter 패널이 Volume에 체크됐는지 확인

And Report 탭의 Volume 글자가 하이라이트 됐는지 확인

And Inter-Cube mean comparison panel의 그래프 y축이 Volume으로 되어 있는지 확인

And Graph of individual cell 패널의 y축이 Volume으로 되어 있는지 확인