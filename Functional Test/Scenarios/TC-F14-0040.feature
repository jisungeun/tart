﻿Feature: BAT AI Segmentation Single RUN

  Scenario: BAT Single Run 탭의 Time points setting 패널에서 end 칸에 수치를 입력하여 time point 끝 값을 setting
Given Basic Analyzer[T] 탭의 Single run 탭이 열려 있다.

When Basic Analyzer[T] 탭의 Time points setting 패널에서 end 칸에 수치를 입력한다.

Then Basic Analyzer[T] 탭 end 칸의 숫자가 입력한 수치로 변경되는지 확인

And Basic Analyzer[T] 탭 Time points setting 패널의 Selected time points에 나타나는 숫자가 모두 end 값보다 작은지 확인