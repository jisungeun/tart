﻿Feature: BA Batch RUN

  Scenario: Report 탭 Data Table 패널에서 data munging
Given 빈 TA가 열려있다. 
  
Given 2개 이상의 cube를 RI Thresholding으로 batch run 한 결과의 Report 탭이 열려 있다.

When File Change 팝업에서 Accept 버튼을 누른다.

When 프로세싱 바 팝업이 전부 진행될 때 까지 기다린다.

Given Report 탭 Data Table 패널에 출력된 개별 data table 하단부에 스크롤 바가 생성되어 있다.

When data table의 스크롤 바를 마우스로 클릭한 상태로 오른쪽으로 움직인다.

Then 각 데이터 table이 화면에 보이는 영역이 달라지는지 확인

When Data Table 패널 오른쪽의 화살표 버튼(>)을 누른다.

Then 스크롤 바를 민 것처럼 화면에 표시되는 Data Table 패널 전체가 오른쪽으로 이동함을 확인

Then 리포트와 TA종료