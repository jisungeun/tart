﻿Feature: BAT Batch RUN setting

  Scenario: BAT Batch Run setting 탭의 Processing mode 패널의 설정 값 변경이 불가
Given Basic Analyzer[T] 탭 Batch Run setting 탭의 Processing mode 패널에서 AI Segmentation이 프로세서로 선택되어 있다.

When Batch Run setting 탭 Processing mode 패널의 가장 위에 있는 드롭다운 버튼을 클릭한다.

And Batch Run setting 탭 Processing mode 패널 Basic Measurement 탭 table의 셀을 클릭한다.

Then 동작들에 대해 Batch Run setting 탭의 Processing mode 탭에서 아무런 변화가 없는지 확인