﻿Feature: PEP/UI features

  Scenario: Operation sequence 패널에서 다음 단계를 위한 조건을 만족하지 않았을 경우 오른쪽 화살표(>) 버튼을 눌러도 다음 단계로 진행되지 않음
Given Operation sequence 패널에서 현재 단계의 해야 하는 작업을 모두 완료하지 않은 상태로 해당 탭이 열려 있다.

When Operation sequence 패널의 > 버튼을 누른다.

Then 현재 화면에서 변화가 없음을 확인