﻿Feature: Mask Editor; From empty mask

  Scenario: Mask control 패널에서 각 마스크 오른쪽의 색깔 버튼을 클릭하고 .msk 파일 생성
Given 빈 TA가 열려있다.   
  
Given Mask Editor 탭에서 ‘test’ ID가 선택되어 있다.

When Mask control 패널 Cell Instance 마스크의 오른쪽 회색 버튼을 클릭한다.

Then Mask control 패널 Cell Instance 오른쪽 버튼이 파란색으로 바뀌었는지 확인

And Mask Selector 패널에서 Cell Instance 아이콘이 활성화 되었는지 확인

When Mask selector 패널의 Cell Instance 버튼을 누른다.

And Drawing tool 패널의 Paint 버튼을 클릭한다.

And Drawing tool 패널 오른쪽의 Size 입력 칸에 마우스 포인터를 올리고 마우스 휠을 위로 다섯 칸 올린다.

Then Drawing tool 패널 오른쪽의 Size 칸 값이 10이 되었는지 확인

When Mask Editor 탭 XY Slice 화면의 왼쪽 위 부분을 클릭한다.

Then Mask Editor 탭 XY Slice 화면 클릭한 위치에 Cell instance 마스크가 찍혔는지 확인

When Mask control 패널 Whole cell 마스크의 오른쪽 회색 버튼을 클릭한다.

And Mask selector 패널의 Whole cell 버튼을 누른다.

And Drawing tool 패널의 Paint 버튼을 누르고 Mask Editor 탭 XY Slice 화면의 왼쪽 아래 부분을 클릭한다.

Then Mask Editor 탭 XY Slice 화면 클릭한 위치에 Whole cell 마스크가 찍혔는지 확인

When Mask control 패널 Nucleus 마스크의 오른쪽 회색 버튼을 클릭한다.

And Mask selector 패널의 Nucleus 버튼을 누른다.

And Drawing tool 패널의 Paint 버튼을 누르고 Mask Editor 탭 XY Slice 화면의 오른쪽 위 부분을 클릭한다.

Then Mask Editor 탭 XY Slice 화면 클릭한 위치에 Nucleus 마스크가 찍혔는지 확인

When Mask control 패널 Nucleolus 마스크의 오른쪽 회색 버튼을 클릭한다.

And Mask selector 패널의 Nucleolus 버튼을 누른다.

And Drawing tool 패널의 Paint 버튼을 누르고 Mask Editor 탭 XY Slice 화면의 오른쪽 아래 부분을 클릭한다.

Then Mask Editor 탭 XY Slice 화면 클릭한 위치에 Nucleolus 마스크가 찍혔는지 확인

When Mask control 패널 Lipid droplet 마스크의 오른쪽 회색 버튼을 클릭한다.

And Mask selector 패널의 Lipid droplet 버튼을 누른다.

And Drawing tool 패널의 Paint 버튼을 누르고 Mask Editor 탭 XY Slice 화면의 중앙 부분을 클릭한다.

Then Mask Editor 탭 XY Slice 화면 클릭한 위치에 Lipid droplet 마스크가 찍혔는지 확인

When Mask selector 패널의 Cell Instance 버튼을 누른다.

Then Mask Editor 탭 XY Slice 화면의 왼쪽 위 부분에 점으로 찍힌 마스크가 나타났는지 확인

When Mask selector 패널의 Whole cell 버튼을 누른다.

Then Mask Editor 탭 XY Slice 화면의 왼쪽 아래 부분에 점으로 찍힌 마스크가 나타났는지 확인

When Mask selector 패널의 Nucleus 버튼을 누른다.

Then Mask Editor 탭 XY Slice 화면의 오른쪽 위 부분에 점으로 찍힌 마스크가 나타났는지 확인

When Mask selector 패널의 Nucleolus 버튼을 누른다.

Then Mask Editor 탭 XY Slice 화면의 오른쪽 아래 부분에 점으로 찍힌 마스크가 나타났는지 확인

When Mask selector 패널의 Lipid droplet 버튼을 누른다.

Then Mask Editor 탭 XY Slice 화면의 중앙 부분에 점으로 찍힌 마스크가 나타났는지 확인

Then TA종료
