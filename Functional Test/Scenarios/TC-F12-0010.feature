﻿Feature: 	BA RI Thresholding Single Run

  Scenario: Project Manager 탭 Application Parameter 패널의 설정이 BA 탭 Processing mode 패널에 유지
Given 빈 TA가 열려있다.   
  
Given BA Application Parameter에서 프로세서로 RI Thresholding이 선택되어 있다.

When ‘Assign lower threshold’ 의 사이즈 바를 마우스로 왼쪽 끝까지 움직인다.

And BA Application Parameter 패널의 Single RUN 버튼을 누른다.

And 팝업에서 App이 연결된 hypercube 안의 TCF 중 Single RUN 하고 싶은 TCF를 선택한다.

Then BA 탭에서 Processing mode 패널의 맨 위 드롭다운 버튼이 RI Thresholding으로 설정됐는지 확인

And Processing mode 패널 UL Threshold 탭의 Assign lower threshold 값이 1.0010으로 설정되어 있는지 확인

Then 리포트와 TA종료