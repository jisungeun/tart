﻿Feature: Create Cube

  Scenario: Delete Cube 버튼을 눌러 Cube를 삭제
Given Cube가 생성되어 있다.

When Playground canvas의 Cube 아이콘을 우클릭한다.

And Delete Cube 버튼을 누른다.

And 팝업에서 Delete 버튼을 누른다.

Then Project Explorer 패널에서 Cube가 삭제됨을 확인

And Playground canvas에서 Cube 아이콘이 삭제됨을 확인