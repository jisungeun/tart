﻿Feature: BAT Report[T]

  Scenario: Select Parameter 패널에서 Dry mass 선택
Given Report[T] 탭이 열려 있다.

When Report[T] 탭 Select Parameter 패널에서 Dry mass 버튼을 누른다.

Then Report[T] 탭 Select Parameter 패널의 Dry mass 체크박스로 체크가 옮겨졌는지 확인

And Report[T] 탭 Select Parameter 패널의 Dry mass 글자가 하이라이트 됐는지 확인

And Report[T] 탭 Select Parameter 패널의 Dry mass 글자와 체크박스 외의 다른 글자와 체크박스는 모두 하이라이트 해제됐는지 확인