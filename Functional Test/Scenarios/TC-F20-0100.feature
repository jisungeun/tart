﻿Feature: 3D Visualizer; HT Image Setup and Screenshot

  Scenario: Movie maker 탭에서 Slice 효과를 추가했을 때, Slice UI의 Range 칸에 수치를 입력
Given 3D Visualizer 탭의 View 패널에 Rendering이 생성되어 있다.

And Movie maker 탭에 Slice 효과 옵션이 추가되어 있다.

When Movie maker 탭 Slice UI의 Range 왼쪽 칸에 100을 입력한다.

And Movie maker 탭 Slice UI의 Range 오른쪽 칸에 150을 입력하고 Record 버튼을 누른 뒤 Multi-view 버튼을 눌러 저장한다.

Then Record 된 영상에서 2D XY HT 이미지의 Z축이 아래에서 위로 움직이는지 확인

Then 3D Visualizer TA종료