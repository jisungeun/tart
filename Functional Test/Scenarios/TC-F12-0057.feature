﻿Feature: BA RI Thresholding Single Run

  Scenario: BA 탭에서 프로세서가 RI Thresholding일 때, UL Threshold 탭에서 마우스 휠을 이용하여 수치를 올리기
Given Basic Analyzer Single Run 탭의 Processing mode 패널의 제일 위 드롭다운 버튼이 RI Thresholding으로 선택되어 있다.

When Basic Analyzer Single Run 탭의 ‘Assign lower threshold’의 UI에 마우스 포인터를 대고 마우스 휠을 위로 한 칸 올린다.

And Basic Analyzer Single Run 탭의 ‘Assign upper threshold’ 의 UI에 마우스 포인터를 대고 마우스 휠을 위로 한 칸 올린다.

Then Basic Analyzer Single Run 탭의 ‘Assign lower threshold’가 1.3501로, ‘Assign upper threshold’ 가 1.4401로 올라가는지 확인