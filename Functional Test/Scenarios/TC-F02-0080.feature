﻿Feature: Import Data

  Scenario: List 패널에서 TCF들을 각 column의 오름차순으로 정렬
Given TCF가 불러진 List 패널이 열려 있다.

When TCF들을 오름차순으로 정렬하고 싶은 column의 table header를 한 번 누른다.

Then column의 헤더에 삼각형(위 화살표) 모양이 생겼는지 확인

And 해당 변수에 대해 오름차순으로 List가 정렬되었는지 확인