﻿Feature: BAT Report[T]

  Scenario: Report[T] 탭 Data Table 패널에 출력된 개별 data tabe 하단부의 스크롤 바를 움직여 data munging
Given 빈 TA가 열려있다.   
  
Given 2개 이상의 cube를 RI Thresholding으로 batch run 한 결과의 Report[T] 탭이 열려 있다.

And Report[T] 탭 Data Table 패널에 출력된 개별 data table 하단부에 스크롤 바가 생성되어 있다.

When Report[T] 탭의 Data table 하단의 스크롤 바를 마우스로 클릭한 상태로 움직인다.

Then Report[T] 탭의 Data table 패널에서 각 데이터 table이 화면에 보이는 영역이 달라지는지 확인

When Report[T] 탭의 Data Table 패널 오른쪽의 화살표 버튼(>)을 누른다.

Then Report[T] 탭의 Data table 패널에서 스크롤 바를 민 것처럼 화면에 표시되는 Data Table 패널 전체가 오른쪽으로 이동함을 확인

When Report[T] 탭의 Data Table 패널의 저장할 table 위의 Export 버튼을 누른다.

And 파일 탐색기에서 파일 저장 경로와 이름을 지정하고 저장 버튼을 누른다.

Then 해당 저장 경로에 저장한 csv 파일이 Data Table 패널의 table과 같은 내용인지 확인

And TA종료