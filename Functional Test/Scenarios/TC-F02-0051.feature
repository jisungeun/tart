﻿Feature: Import Data

  Scenario: List 패널에서 File Information 버튼을 눌러 TCF의 정보를 확인
Given TCF파일이 들어있는 프로젝트가 열려있다.

When List 패널에서 원하는 TCF 파일을 한 개 선택한다.

And File Information 버튼을 누른다.

Then Information 팝업이 뜨는지 확인