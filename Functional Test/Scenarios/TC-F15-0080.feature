﻿Feature: BAT Batch RUN setting

  Scenario: BAT Batch Run setting 탭의 Time points setting 패널에서 custom 체크박스에 체크
Given Basic Analyzer[T] 탭의 Batch Run setting 탭이 열려 있다.

And Batch Run setting 탭의 Time points setting 패널의 custom 체크 박스가 비어 있다.

When Batch Run setting 탭의 Time points setting 패널에서 custom 체크박스에 체크한다.

Then Batch Run setting 탭 Time points setting 패널의 custom 체크박스와 글자가 하이라이트 되는지 확인

And Batch Run setting 탭 Time points setting 패널의 custom 체크 박스에 체크 표시가 생기는지 확인

And Batch Run setting 탭 Time points setting 패널의 Selected time points에 직접 숫자를 입력할 수 있게 되는지 확인