﻿Feature: Link Data to Cube

  Scenario: Link Data to Cube 탭으로 이동 시 Toolbox의 Link to Cube, Unlink from Cube 두 버튼이 하이라이트
Given 빈 TA가 열려있다. 
  
Given Operation sequence 패널의 Link Data to Cube 탭 화면이 열려 있다.

And cube에 TCF가 2개 연결되어 있다.

When cube에 연결되어 있는 TCF를 한 개만 선택하고 Toolbox의 Unlink from Cube 버튼을 누른다.

Then Project Explorer 패널에서 TCF가 연결 해제되었는지 확인

And Operation sequance 패널이 Select Application 탭으로 자동으로 이동하는지 확인

When Cube에 연결된 TCF를 모두 연결 해제한다.

Then Operation sequence 패널의 Select Application 탭 화면이 Link Data to Cube 탭으로 다시 돌아감을 확인

Then TA종료