﻿Feature: 	Create Hypercube

  Scenario: Hypercube를 선택 해제 시 Preview 패널에서 표시하는 TCF가 달라짐
	
Given Hypercube가 선택되어 있다.

When Hypercube 아이콘 바깥의Playground canvas 빈 공간 아무 곳이나 누른다.

Then Preview 패널의 Title이 Playground 이름으로 바뀜을 확인 

And Hypercube만 존재하는 Preview 패널에 표시되는 TCF가 Playground 내의 전체 TCF로 바뀜을 확인