﻿Feature: BA AI Segmentation Single RUN

  Scenario: Multi-layered mask를 on 했을 때 각 마스크의 체크박스를 해제
Given 빈 TA가 열려있다.   
  
Given Basic Analyzer 탭의 Mask Viewing panel에 마스크가 생성되어 있다.

When Basic Analyzer 탭의 Tool box에서 Multi-layered mask 버튼을 클릭한다.

And Basic Analyzer 탭의 Tool box에서 All 체크박스를 클릭한다.

Then Mask Viewing panel 3D 화면에 네가지 mask가 모두 생성되었는지 확인 

When Basic Analyzer 탭의 Tool box에서 Hide Mask 체크박스를 클릭한다. 

Then Mask Viewing panel의 2D화면과 3D 화면에 마스크가 없는지 확인

When Hide Mask 체크박스를 선택 해제한 뒤, Whole cell, Nucleolus, Lipid droplet 체크박스를 선택 해제한다. 

Then Mask Viewing panel 3D 화면에 Nucleolus 마스크만 남아있는지 확인

Then TA종료