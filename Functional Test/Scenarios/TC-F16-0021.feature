﻿Feature: BAT Report[T]

  Scenario: Report[T] 탭의 Select data 패널에서 Run 된 Cube에 링크된 개별 TCF의 체크박스 목록 접기
Given Report[T] 탭이 열려 있다.

And Report[T] 탭 Select Data 패널의 All 체크박스 아래에 Batch Run 된 data cube와 그 안의 개별 TCF의 체크박스 목록이 펼쳐져 있다.

When Report[T] 탭 Select Data 패널의 cube 체크박스 왼쪽의 아래 방향 화살표(▽)를 누른다.

Then Report[T] 탭에서 All 체크박스와 Batch Run 된 data cube들의 이름과 체크박스가 모두 정상 출력 되는지 확인

And Report[T] 탭에서 화살표 버튼을 누른 Cube 체크박스 아래에 있던 해당 Cube에 링크된 TCF의 체크박스 목록이 사라지는지 확인

And Report[T] 탭 Cube 체크박스 왼쪽의 화살표가 옆 방향(▷)으로 바뀌었는지 확인