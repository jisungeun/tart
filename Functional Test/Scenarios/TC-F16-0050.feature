﻿Feature: BAT Report[T]

  Scenario: Report[T] 탭의 Select data 패널에서 개별 TCF 체크박스에 체크
Given Report[T] 탭이 열려 있다.

And Report[T] 탭 Select Data 패널의 개별 TCF 체크박스가 비어있다.

When Report[T] 탭 Select Data 패널의 개별 TCF 체크박스를 누른다.

Then Report[T] 탭 Select Data 패널 개별 TCF 체크박스에 체크되었는지 확인