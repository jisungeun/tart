﻿Feature: TC-F17-0010

  Scenario: BAT Single Run 탭으로 설정이 유지, Processing mode 패널에서 프로세서를 변경
Given 빈 TA가 열려있다. 
  
Given BAT Application Parameter패널이 열려 있다.

When BAT Application Parameter 패널에서 프로세서로 RI Thresholding을 선택하고 Assign lower threshold에 1을 입력한다.

And BAT Application Parameter 패널의 Single RUN 버튼을 누르고 팝업에서 데이터를 선택한다.

Then Basic Analyzer[T] 탭의 Processing mode 패널에서 Assign lower threshold 값이 1.0000인지 확인

When Basic Analyzer[T] 탭 Processing mode 패널의 제일 위 드롭다운 버튼에서 AI Segmentation을 선택한다.

Then Basic Analyzer[T] 탭 Single run 탭의 Processing mode 패널에 AI Segmentation 모드의 UI가 나타남을 확인

When Basic Analyzer[T] 탭 Processing mode 패널의 제일 위 드롭다운 버튼에서 RI Threholding을 선택한다.

Then Basic Analyzer[T] 탭 Single run 탭의 Processing mode 패널에 RI Thresholding 모드의 UI가 나타남을 확인

And Basic Analyzer[T]에서 Time points setting 패널이 없고 Notice 패널이 나타나는지 확인

Then TA종료