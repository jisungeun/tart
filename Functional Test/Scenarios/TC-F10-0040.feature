﻿Feature: BA AI Segmentation Single RUN

  Scenario: Single run 하기 전 Project Manager 탭의 Application Parameter 패널에서 설정한 값이 BA 탭 Processing mode 패널의 Basic Measurement 탭의 설정 값으로 넘어옴
Given BA Application Parameter에서 프로세서로 AI Segmentation이 선택되어 있다.

When Single RUN 버튼을 누른다.

And 팝업에서 App이 연결된 hypercube 안의 TCF 중 Single RUN 하고 싶은 TCF를 선택한다.

Then Project Manager 탭의 BA Application Parameter 패널과 Basic Analyzer 탭의 Processing mode 패널의 두 Basic Measurement 값이 서로 일치하는지 확인