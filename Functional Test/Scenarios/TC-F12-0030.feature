﻿Feature: BA RI Thresholding Single Run

  Scenario: 	BA 탭에서 프로세서가 RI Thresholding일 때, Entropy / Moment preserving 알고리즘으로 Auto lower Threshold 설정

Given 빈 TA가 열려있다.   
  
Given Basic Analyzer 탭에서 RI Thresholding이 프로세서로 선택되어 있다.

And Basic Analyzer 탭 Auto threshold 탭의 Auto lower threshold 체크박스가 체크되어 있지 않다.

When Basic Analyzer 탭의 Auto lower Threshold 체크박스를 클릭한다.

Then Basic Analyzer 탭의 Auto lower Threshold 체크박스 밑에 ‘Select Algorithm’ 문구와 알고리즘 드롭다운 버튼, 하이라이트 된 Execute 버튼이 나타나는지 확인

When Basic Analyzer 탭의 Select Algorithm 드롭다운 버튼에서 Entropy 알고리즘을 선택하고 Auto Threshold 탭의 Execute 버튼을 누른다.

Then Basic Analyzer 탭의 UL Threshold 탭에서 값과 사이즈 바가 Entropy 알고리즘에 의해 자동으로 바뀌었는지 확인

When Basic Analyzer 탭의 Select Algorithm 드롭다운 버튼에서 Moment Preserving 알고리즘을 선택하고 Auto Threshold 탭의 Execute 버튼을 누른다.

Then Basic Analyzer 탭의 UL Threshold 탭에서 값과 사이즈 바가 Moment Preserving 방식에 의해 자동으로 바뀌었는지 확인

Then TA종료