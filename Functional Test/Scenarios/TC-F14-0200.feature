﻿Feature: BAT AI Segmentation Single RUN

  Scenario: BAT Single Run 탭의 Mask Viewing panel 상단의 Mask 툴박스에서 Cell-wise filter 스위치를 켜 TCF 안의 서로 다른 cell을 구별
Given Basic Analyzer[T] Single Run 탭의 Mask Viewing panel에 3D 마스크가 생성되어 있다.

And Basic Analyzer[T] 탭의 Mask Viewing panel 상단 Mask 툴박스에 Whole Cell이 체크되어 있다.

And Basic Analyzer[T] 탭 Mask Viewing panel 상단 Mask 툴박스의 Cell-wise filter 스위치가 꺼져 있다.

When Basic Analyzer[T] 탭 Mask Viewing panel 상단 Mask 툴박스의 Cell-wise filter 스위치를 켠다.

Then Basic Analyzer[T] 탭 Mask Viewing panel에서 각각의 cell의 마스크가 다른 색으로 표시되는지 확인

And Basic Analyzer[T] 탭 Mask 툴박스의 Cell-wise filter 스위치 버튼이 하이라이트 됐는지 확인

And Basic Analyzer[T] 탭 Mask 툴박스의 Cell-wise filter 스위치 버튼이 오른쪽으로 움직였는지 확인