﻿Feature: Link Data to Cube

  Scenario: 엔터 키와 Unlink from Cube 버튼을 이용하여 TCF를 Cube에서 연결 해제
Given TCF가 cube에 연결되어 있다.

When cube에 연결되어 있는 TCF를 선택한다.

And Toolbox의 Unlink from Cube 버튼을 누른다.

And Unlink from Cube 팝업에서 키보드의 엔터 키를 누른다.

Then Project Explorer 패널에서 cube에서 TCF가 연결 해제되었는지 확인

And Operation sequence 패널의 Link to Cube 탭에서 TCF가 연결 해제되었는지 확인