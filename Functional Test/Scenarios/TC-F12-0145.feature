﻿Feature: BA RI Thresholding Single Run

  Scenario: BA 탭에서 프로세서가 RI Thresholding일 때, Labeling 탭에서 마우스 휠을 이용하여 ‘Assign Size of Neglectable Particle’ 수치를 내림
Given Basic Analyzer 탭에서 Use Labeling 체크박스가 체크되어 있다.

When Basic Analyzer 탭의 ‘Assign Size of Neglectable Particle’ 의 UI에 마우스 포인터를 대고 마우스 휠을 아래로 한 칸 내린다.

Then Basic Analyzer 탭의 ‘Assign Size of Neglectable Particle’ 값이 24.9999로 바뀌는지 확인