﻿Feature: Report History

  Scenario: 프로젝트가 열려있을 때 Report history 기능을 수행
  
Given 빈 TA가 열려있다.   
  
Given “F23RH“프로젝트가 열려있다.

When Toolbox의 Report history 버튼을 누른다.

And  Result History 팝업 왼쪽의 리스트에서 선택되어있지 않은 다른 리포트를 클릭한다.

Then  Result History 팝업의 Playground canvas와 Application Parameter가 변경되었는지 확인

When Result History 팝업의 Load Playground 버튼을 누른다.

Then Result History 팝업 왼쪽의 리스트에서 선택한 Playground가 Project Manager 탭의 Playground canvas 패널에 불러졌는지 확인

When Result History 팝업의 Open Report App 버튼을 누른다.

Then TA에 선택한 Report 탭이 나타났는지 확인

Then TA종료