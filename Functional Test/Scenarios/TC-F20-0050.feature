﻿Feature: 3D Visualizer; HT Image Setup and Screenshot

  Scenario: Navigator 패널에서 각 Plane의 Slice 이미지를 조정
Given 빈 TA가 열려있다.   
  
Given 3D Visualizer 탭이 열려 있다.

When Navigator 패널의 Z axis LOCATION 수치 입력 칸에 10을 입력한다.

Then Navigator 패널의 XY Plane 사이즈 바와 수치가 변경되었는지 확인

And 3D Visualizer 탭 View 패널의 2D XY HT 이미지가 Z축 10의 위치로 바뀌었는지 확인

When Navigator 패널의 Z axis LOCATION 수치 입력 칸의 위 화살표를 누른다.

Then Navigator 패널의 XY Plane의 수치가 1 올라갔는지 확인

When Navigator 패널의 Z axis LOCATION 수치 입력 칸의 아래 화살표를 누른다.

Then Navigator 패널의 XY Plane의 수치가 1 내려갔는지 확인

When Navigator 패널의 XY plane 사이즈 바를 마우스로 왼쪽 끝까지 움직인다.

Then Navigator 패널의 XY plane 사이즈 바 옆의 수치가 2로 바뀌었는지 확인

And Navigator 패널의 Z axis LOCATION 수치가 0.38로 바뀌었는지 확인

And 3D Visualizer 탭 View 패널의 2D XY HT 이미지가 2Slice 위치로 바뀌었는지 확인

When Navigator 패널의 XY plane 사이즈 바를 마우스로 오른쪽 끝까지 움직인다.

Then Navigator 패널의 XY plane 사이즈 바 옆의 수치가 210으로 바뀌었는지 확인

And Navigator 패널의 Z axis LOCATION 수치가 39.86으로 바뀌었는지 확인

And 3D Visualizer 탭 View 패널의 2D XY HT 이미지가 210Slice 위치로 바뀌었는지 확인

When Navigator 패널의 XY plane 사이즈 바 옆 수치 칸에 150을 입력한다.

Then Navigator 패널의 Z axis LOCATION 수치가 28.47로 바뀌었는지 확인

And 3D Visualizer 탭 View 패널의 2D XY HT 이미지가 150Slice 위치로 바뀌었는지 확인

When Navigator 패널의 XY plane 사이즈 바 옆 수치 칸의 위 화살표를 한 번 누른다.

Then Navigator 패널의 Z axis LOCATION 수치가 0.19 올라갔는지 확인

And 3D Visualizer 탭 View 패널의 2D XY HT 이미지가 한 Slice 올라갔는지 확인

When Navigator 패널의 XY plane 사이즈 바 옆 수치 칸의 아래 화살표를 한 번 누른다.

Then Navigator 패널의 Z axis LOCATION 수치가 0.19 내려갔는지 확인

And 3D Visualizer 탭 View 패널의 2D XY HT 이미지가 한 Slice 내려갔는지 확인

When Navigator 패널의 X axis LOCATION 수치 입력 칸에 10을 입력한다.

Then Navigator 패널의 YZ Plane 사이즈 바와 수치가 변경되었는지 확인

And 3D Visualizer 탭 View 패널의 2D YZ HT 이미지가 10 위치로 바뀌었는지 확인

When Navigator 패널의 X axis LOCATION 수치 입력 칸의 위 화살표를 누른다.

Then Navigator 패널의 YZ Plane의 수치가 1 올라갔는지 확인

When Navigator 패널의 X axis LOCATION 수치 입력 칸의 아래 화살표를 누른다.

Then Navigator 패널의 YZ Plane의 수치가 1 내려갔는지 확인

When Navigator 패널의 YZ plane 사이즈 바를 마우스로 왼쪽 끝까지 움직인다.

Then  Navigator 패널의 YZ plane 사이즈 바 옆의 수치가 6으로 바뀌었는지 확인

And Navigator 패널의 X axis LOCATION 수치가 0.57로 바뀌었는지 확인

And 3D Visualizer 탭 View 패널의 2D YZ HT 이미지가 6Slice 위치로 바뀌었는지 확인

When Navigator 패널의 YZ plane 사이즈 바를 마우스로 오른쪽 끝까지 움직인다.

Then  Navigator 패널의 YZ plane 사이즈 바 옆의 수치가 648로 바뀌었는지 확인

And Navigator 패널의 X axis LOCATION 수치가 61.33으로 바뀌었는지 확인

And View 패널의 2D YZ HT 이미지가 648Slice 위치로 바뀌었는지 확인

When Navigator 패널의 YZ plane 사이즈 바 옆 수치 칸에 200을 입력한다.

Then Navigator 패널의 X axis LOCATION 수치가 18.93으로 바뀌었는지 확인

And View 패널의 2D YZ HT 이미지가 200Slice 위치로 바뀌었는지 확인

When Navigator 패널의 YZ plane 사이즈 바 옆 수치 칸의 위 화살표를 한 번 누른다.

Then Navigator 패널의 X axis LOCATION 수치가 0.10올라갔는지 확인

And View 패널의 2D YZ HT 이미지가 한 Slice 올라갔는지 확인

When Navigator 패널의 YZ plane 사이즈 바 옆 수치 칸의 아래 화살표를 한 번 누른다.

Then Navigator 패널의 X axis LOCATION 수치가 0.09내려갔는지 확인

And View 패널의 2D YZ HT 이미지가 한 Slice 내려갔는지 확인

When Navigator 패널의 Y axis LOCATION 수치 입력 칸에 10을 입력한다.

Then Navigator 패널의 XZ Plane 사이즈 바와 수치가 변경되었는지 확인

And 3D Visualizer 탭 View 패널의 2D XZ HT 이미지가 10위치로 바뀌었는지 확인

When Navigator 패널의 Y axis LOCATION 수치 입력 칸의 위 화살표를 누른다.

Then Navigator 패널의 XZ Plane의 수치가 1 올라갔는지 확인

When Navigator 패널의 Y axis LOCATION 수치 입력 칸의 아래 화살표를 누른다.

Then Navigator 패널의 XZ Plane의 수치가 내려갔는지 확인

When Navigator 패널의 XZ plane 사이즈 바를 마우스로 왼쪽 끝까지 움직인다.

Then Navigator 패널의 XZ plane 사이즈 바 옆의 수치가 6으로 바뀌었는지 확인

And Navigator 패널의 Y axis LOCATION 수치가 0.57로 바뀌었는지 확인

And 3D Visualizer 탭 View 패널의 2D XZ HT 이미지가 6Slice 위치로 바뀌었는지 확인

When Navigator 패널의 XZ plane 사이즈 바를 마우스로 오른쪽 끝까지 움직인다.

Then Navigator 패널 XZ plane 사이즈 바 옆의 수치가 648로 바뀌었는지 확인

And Navigator 패널의 Y axis LOCATION 수치가 61.33으로 바뀌었는지 확인

And 3D Visualizer 탭 View 패널의 2D XZ HT 이미지가 648Slice 위치로 바뀌었는지 확인

When Navigator 패널의 XZ plane 사이즈 바 옆 수치 칸에 200을 입력한다.

Then Navigator 패널의 XZ plane 사이즈 바의 위치가 바뀌었는지 확인

And Navigator 패널의 Y axis LOCATION 수치가 18.93로 바뀌었는지 확인

And 3D Visualizer 탭 View 패널의 2D XZ HT 이미지가 200Slice 위치로 바뀌었는지 확인

When Navigator 패널의 XZ plane 사이즈 바 옆 수치 칸의 위 화살표를 한 번 누른다.

Then Navigator 패널의 Y axis LOCATION 수치가 0.10올라갔는지 확인

And 3D Visualizer 탭 View 패널의 2D XZ HT 이미지가 한 Slice 올라갔는지 확인

When Navigator 패널의 XZ plane 사이즈 바 옆 수치 칸의 아래 화살표를 한 번 누른다.

Then Navigator 패널의 Y axis LOCATION 수치가 0.09 내려갔는지 확인

And 3D Visualizer 탭 View 패널의 2D XZ HT 이미지가 한 Slice 내려갔는지 확인

When Modality 패널의 HT 체크박스를 체크 해제한다.

And Navigator 패널의 XY plane 사이즈 바를 마우스로 오른쪽 끝까지 움직인다.

And Navigator 패널의 YZ plane 사이즈 바를 마우스로 오른쪽 끝까지 움직인다.

And Navigator 패널의 XZ plane 사이즈 바를 마우스로 오른쪽 끝까지 움직인다.

Then 3D Visualizer 탭 View 패널의 2D XY, YZ, XZ HT 이미지에 아무런 변화가 없는지 확인

When Display 패널에서 2D 체크박스에 체크한다.

Then Navigator 패널 XY Plane의 위 화살표 버튼이 눌리지 않고 View 패널에 아무런 변화가 없는지 확인

Then 3D Visualizer TA종료