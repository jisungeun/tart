﻿Feature: 3D Visualizer; HT Image Setup and Screenshot

  Scenario: Movie maker 탭에서 Orbit 효과를 추가했을 때, Orbit UI에서 회전하는 각도와 방향을 설정
Given 3D Visualizer 탭의 View 패널에 Rendering이 생성되어 있다.

And Movie maker 탭에 Orbit 효과 옵션이 추가되어 있다.

When Movie maker 탭 Orbit UI에서 Orbit 칸에 180을 입력한다.

And Movie maker 탭에서 Reverse 체크박스를 체크하고 Record 버튼을 누른 뒤 Multi-view 버튼을 눌러 저장한다.

Then Record 된 영상에서 3D Rendering의 회전 각이 시계 방향 180도로 설정되었는지 확인

Then 3D Visualizer TA종료