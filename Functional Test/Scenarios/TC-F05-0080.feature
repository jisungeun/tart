﻿Feature: Create Cube

  Scenario: Cube가 선택되어 있을 때 Playground canvas의 다른 공간을 눌러 cube의 선택을 해제
Given Cube가 선택되어 있다.

When Cube 아이콘 바깥의Playground canvas 아무 영역이나 누른다.

Then Playground canvas 위의 Active cube 아이콘이 Normal cube 아이콘으로 바뀜을 확인