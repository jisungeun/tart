﻿Feature: BAT AI Segmentation Single RUN

  Scenario: BAT Single Run 탭의 Time points setting 패널에서 Apply time points 버튼을 눌러 process할 time points를 적용
Given Basic Analyzer[T] 탭의 Single run 탭이 열려 있다.

When Time points setting 패널에서 time points를 지정하고 Apply time points 버튼을 누른다.

Then Apply time points 버튼의 하이라이트가 해제됐는지 확인

And Processing mode 패널의 AI Inference 탭의 Execute 버튼이 하이라이트 되었는지 확인