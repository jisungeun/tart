﻿Feature: BA Application Parameter

  Scenario: RI Thresholding을 프로세서로 선택했을 때, Use Labeling 체크박스 체크 해제
Given BA Application Parameter에서 Use Labeling 체크박스가 체크되어 있다.

When Use Labeling 체크박스를 클릭한다.

Then ‘Select Option’ 문구와 옵션 드롭다운 버튼이 사라지는지 확인

And ‘Assign Size of Neglectable Particle’ 문구와 사이즈 바, 수치 입력 칸이 사라지는지 확인

And Use Labeling 체크박스가  체크 해제 되는지 확인