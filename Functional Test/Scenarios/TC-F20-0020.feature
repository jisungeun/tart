﻿Feature: 3D Visualizer; HT Image Setup and Screenshot

  Scenario: Display 패널에서 2D/3D 모드 변경
Given 빈 TA가 열려있다.   
  
Given 3D Visualizer 탭 Display 패널의 3D 체크박스에 체크 되어있다.

When Display 패널의 2D 체크박스를 클릭한다.

Then Display 패널의 2D 체크박스에 체크 되고 3D 체크박스가 비었는지 확인

And 3D Visualizer 탭 View 패널에 2D MIP 이미지만 출력되는지 확인

When Display 패널의 3D 체크박스를 클릭한다.

Then Display 패널의 3D 체크박스에 체크 되고 2D 체크박스가 비었는지 확인

And 3D Visualizer 탭의 View 패널이 초기 레이아웃으로 돌아왔는지 확인

Then 3D Visualizer TA종료