﻿Feature: BAT AI Segmentation Single RUN

  Scenario: BAT Single Run 탭의 Movie player에 time point를 직접 입력하여 화면에 보이는 이미지의 time point를 설정
Given Basic Analyzer[T] 탭의 Single run 탭이 열려 있다.

When Basic Analyzer[T] 탭의 Movie player 타임 바 오른쪽의 숫자 입력 칸에 '4'을 입력한다.

Then Basic Analyzer[T] 탭의 Mask Viewing panel에 나타나는 이미지가 time point 4의 이미지로 바뀌었는지 확인

And Basic Analyzer[T] 탭의 Movie player 타임 바 커서가 오른쪽으로 조금 움직였는지 확인