﻿Feature: 2DIG Batch Run

  Scenario: 2D Image Gallery 탭의 스크롤을 조작
Given 빈 TA가 열려있다.   
  
Given 3개의 cube에 대해 2D Image Gallery 탭이 열려 있다.

When 2D Image Gallery 탭 오른쪽의 스크롤 바를 마우스로 클릭한 상태로 내린다.

Then 2D Image Gallery 탭 화면이 내려갔는지 확인

When 2D Image Gallery 탭 오른쪽의 스크롤 바를 마우스로 클릭한 상태로 올린다.

Then 2D Image Gallery 탭 화면이 올라갔는지 확인

When 2D Image Gallery 탭 화면에서 마우스 휠을 아래로 한 칸 내린다.

Then 2D Image Gallery 탭 화면이 내려갔는지 확인

When 2D Image Gallery 탭 화면에서 마우스 휠을 위로 한 칸 올린다.

Then 2D Image Gallery 탭 화면이 올라갔는지 확인

Then TA종료