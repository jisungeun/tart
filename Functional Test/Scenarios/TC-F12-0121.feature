﻿Feature: BA RI Thresholding Single Run

  Scenario: BA 탭에서 프로세서가 RI Thresholding일 때, Use Labeling 체크박스 체크 해제
Given Basic Analyzer 탭에서 Use Labeling 체크박스가 체크되어 있다.

When Basic Analyzer 탭의 Use Labeling 체크박스를 클릭한다.

Then Basic Analyzer 탭 Processing mode 패널에서 ‘Select Option’ 문구와 옵션 드롭다운 버튼, ‘Assign Size of Neglectable Particle’ 문구와 사이즈 바, 수치 입력 칸이 사라지는지 확인

And Basic Analyzer 탭의 Use Labeling 체크박스가 체크 해제 되는지 확인

And Basic Analyzer 탭의 Labeling 탭의 Execute 버튼이 하이라이트 해제되는지 확인