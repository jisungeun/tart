﻿Feature: BA AI Segmentation Single RUN

  Scenario: BA 탭 Processing mode 패널의 드롭다운 버튼에서 AI Segmentation으로 변경
Given Basic Analyzer Single Run 탭의 Processing mode 패널의 제일 위 드롭다운 버튼이 RI Thresholding으로 선택되어 있다,

When Processing mode 패널의 드롭다운 버튼을 클릭한다.

And Single Run 탭의 AI Segmentation을 선택한다.

Then Basic Analyzer Single Run 탭의 Processing mode 패널에 AI Segmentation의 UI가 나타나는지 확인