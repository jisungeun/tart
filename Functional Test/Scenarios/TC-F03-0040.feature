﻿Feature: Set Playground

  Scenario: Playground setup 패널에서 생성된 Playground를 확인
Given Set playground의 Playground 이름을 지정하는 탭이 열려있다.

When Playground의 이름을 지정한다.

And Set Playground패널의 OK 버튼을 누른다.

Then Project Explorer 패널에 지정한 이름의 playground가 생성됐는지 확인