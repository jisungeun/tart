﻿Feature: BA Application Parameter

  Scenario: RI Thresholding을 프로세서로 선택했을 때, UL Threshold 탭에서 값을 직접 입력하여 수치를 설정하기
Given BA Application Parameter에서 RI Thresholding이 프로세서로 선택되어 있다.

When ‘Assign lower threshold’ 사이즈 바 옆의 칸에 값을 입력한다.

And ‘Assign upper threshold’ 사이즈 바 옆의 칸에 값을 입력한다.

Then 입력한 값 대로 수치가 변경되었는지 확인

And 사이즈 바가 값에 따라 이동했는지 확인