﻿Feature: BAT Batch RUN setting

  Scenario:체크된 BAT Batch Run setting 탭에서 All 체크박스를 클릭하여 체크 해제
Given Basic Analyzer[T]의 Batch Run setting 탭의 All 체크박스가 체크 되어있다.

When Basic Analyzer[T] Batch Run setting 탭의 All 체크박스를 클릭한다.

Then Basic Analyzer[T] Batch Run setting 탭의 All 체크박스가 체크 해제됐는지 확인

And Basic Analyzer[T] Batch Run setting 탭의 All 체크박스의 글씨가 하이라이트 해제됐는지 확인

And Basic Analyzer[T] Batch Run setting 탭의 cube 체크박스가 모두 체크 해제됐는지 확인

And Basic Analyzer[T] Batch Run setting 탭의 cube 이름이 모두 하이라이트 해제됐는지 확인

And Basic Analyzer[T] Batch Run setting 탭에서 모든 TCF 파일의 체크박스가 체크 해제됐는지 확인