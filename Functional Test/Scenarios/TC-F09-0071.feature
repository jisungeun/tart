﻿Feature: BA Application Parameter

  Scenario: RI Thresholding을 프로세서로 선택했을 때, Auto Threshold 탭 펼치기
Given BA Application Parameter에서 RI Thresholding이 프로세서로 선택되어 있다.

And Auto Threshold 버튼 맨 오른쪽의 화살표가 아래 방향(▽)이다.

When Auto Threshold 버튼을 누른다.

Then Auto Threshold 밑의 체크박스가 펼쳐지는지 확인

And Auto Threshold 버튼 맨 오른쪽의 화살표가 윗 방향(△)으로 바뀌는지 확인