﻿Feature: BA AI Segmentation Single RUN

  Scenario: Mask Viewing panel의 HT 이미지 캔버스 위에 마우스 포인터를 올리고 마우스 휠을 조작하여 단층 사진 보기
Given Basic Analyzer 탭의 Mask Viewing panel에 마스크가 생성되어 있다.

And Mask 툴박스의 Whole Cell 체크박스가 체크 되어있다.

When Mask Viewing panel의 HT 이미지 캔버스 위에 마우스 포인터를 올리고 마우스 휠을 위로 5칸 올린다.

Then Basic Analyzer 탭의 Mask Viewing panel HT 단층 사진의 Z축이 올라가는 지 확인

When Mask Viewing panel의 HT 이미지 캔버스 위에 마우스 포인터를 올리고 마우스 휠을 아래로 5칸 내린다.

Then Basic Analyzer 탭의 Mask Viewing panel HT 단층 사진의 Z축이 내려가는 지 확인