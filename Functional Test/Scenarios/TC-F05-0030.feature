﻿Feature: Create Cube

  Scenario: Playground canvas에서 Cube의 Rename / Copy / Delete
Given 빈 TA가 열려있다.  

Given Cube가 생성되어 있다.

When Playground canvas의 Cube 아이콘을 우클릭 하고 Rename Cube 버튼을 누른다.

And 팝업에서 Cube의 이름을 지정하고 OK 버튼을 누른다.

And Playground canvas의 Cube 아이콘을 우클릭 하고 Copy Cube 버튼을 누른다.

Then Project Explorer 패널에서 ‘(원본 Hypercube 이름)_copy’의 Hypercube가 생성됨을 확인

When Playground canvas의 cube 아이콘을 우클릭 하고 Delete Cube 버튼을 누른다.

And 팝업에서 Delete 버튼을 누른다.

Then Playground canvas에서 Cube 아이콘이 삭제됨을 확인

Then TA종료