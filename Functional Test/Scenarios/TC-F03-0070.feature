﻿Feature: Set Playground

  Scenario: Playground setup 패널에서 Delete 버튼을 눌러 playground를 삭제
Given 하나 이상의 Playground가 생성되어 있다. 

When playground setup 패널에서 삭제할 playground를 선택한다.

And Delete 버튼을 누른다.

And Playground의 이름을 삭제하는 패널의 Delete 버튼을 누른다.

Then Project Explorer 패널에서 playground가 삭제되었는지 확인

And Playground setup 패널에서 playground가 삭제되었는지 확인