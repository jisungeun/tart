﻿Feature: 3D Visualizer; HT Image Data Rendering

  Scenario: Preview 패널에서 TCF를 여러 개 선택하고 3D Visualization 버튼을 누르면 제일 마지막에 선택한 파일 한 개만 열림
  
Given 빈 TA가 열려있다.   
  
Given TCF가 프로젝트에 있다.

When Preview 패널에서 TCF를 두 개 선택한다.

And Toolbox의 3D Visualization 버튼을 누른다.

Then 3D Visualizer 탭이 열리는지 확인

And 3D Visualizer 탭 View 패널의 HT 사진과 Project Manager 탭 Preview 패널에서 마지막에 선택한 사진이 같은지 확인

Then TA종료