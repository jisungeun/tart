﻿Feature: BA AI Segmentation Single RUN

  Scenario: BA 탭이 열렸을 때 AI Inference 탭의 Execute 버튼이 하이라이트
Given BA Application Parameter에서 프로세서로 AI Segmentation이 선택되어 있다.

When Single RUN 버튼을 누른다.

And 팝업에서 App이 연결된 hypercube 안의 TCF 중 Single RUN 하고 싶은 TCF를 선택한다.

Then AI Inference 탭의 Execute 버튼이 하이라이트 되었는지 확인