﻿"Feature: BA Batch RUN

  Scenario: Report 탭 Data Table 패널의 table을 .csv 파일로 저장
Given Report 탭이 열려 있다.

When Data Table 패널의 저장할 table 위의 Export 버튼을 누른다.

And 파일 탐색기에 파일 저장 경로와 파일 이름을 지정하고 저장 버튼을 누른다.

Then 해당 저장 경로에 지정한 이름의 table csv 파일이 생성되었는지 확인

And 저장한 table이 Data Table 패널의 table과 같은 내용인지 확인