﻿Feature: Create Cube

  Scenario: Rename Cube 버튼을 눌러 Cube의 이름을 변경
Given Cube가 생성되어 있다.

When Playground canvas의 Cube 아이콘을 우클릭한다.

And Rename Cube 버튼을 누른다.

And 팝업에서 Cube의 이름을 지정하고 OK 버튼을 누른다.

Then Project Explorer 패널에서 Cube 이름이 변경됨을 확인

And Playground canvas에서 Cube 이름이 변경됨을 확인