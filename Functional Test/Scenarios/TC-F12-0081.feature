﻿Feature: BA RI Thresholding Single Run

  Scenario: BA 탭에서 프로세서가 RI Thresholding일 때, Auto lower Threshold 체크박스 체크 해제
Given Basic Analyzer 탭에서 Auto lower Threshold 체크박스가 체크되어 있다.

When Basic Analyzer 탭의 Auto lower Threshold 체크박스를 클릭한다.

Then Basic Analyzer 탭의 Auto lower Threshold 체크박스 밑의 ‘Select Algorithm’ 문구와 알고리즘 드롭다운 버튼, Execute 버튼이 사라지는지 확인

And Basic Analyzer 탭의 Auto lower Threshold 체크박스가 체크 해제 되는지 확인

And Basic Analyzer 탭의 UL Threshold 탭의 Execute 버튼이 하이라이트 되었는지 확인