﻿Feature: Report History

  Scenario: Report History 삭제
Given 빈 TA가 열려있다.   
  
Given “F23RH“프로젝트가 열려있다.

When Toolbox의 Report history 버튼을 선택한다.

And  Result History 팝업 왼쪽의 리스트에서 선택되어있지 않은 다른 리포트를 선택하고 Delete History 버튼을 클릭한다. 

And  Delete History 팝업에 Delete 버튼을 누른다.

When Project Manager 탭의  Playground canvas에서 Hypercube1에 연결된 BA 아이콘을 선택하고 Batch Run을 클릭한다.

And Report 탭의 오른쪽 그래프에서 막대 아무거나 하나를 클릭하고 Edit Mask 버튼을 누른다..  

And Mask Editor 탭의 General 패널에서 Apply to Result 버튼을 누른다.

And Delete History 팝업 창에서 Delete 버튼을 클릭한다. 

And Project Manager 탭의 Tool box에서 Report History를 클릭한다.

Then Report History 팝업창의 리스트에 History 파일이 있는지 확인

Then TA종료

