﻿Feature: BAT AI Segmentation Single RUN

  Scenario: BAT Single Run 탭의 Processing mode 패널에서, Basic Measurement의 table의 ‘Lipid’ preset 값을 확인

Given Basic Analyzer[T] Single Run 탭의 Processing mode 패널에서 프로세서가 AI Segmentation으로 선택되어 있다.

When Basic Analyzer[T] 탭 Processing mode 패널 Whole cell column의 Preset 드롭다운 버튼에서 Lipid를 선택한다.

And Basic Analyzer[T] 탭 Processing mode 패널 Nucleus column의 Preset 드롭다운 버튼에서 Lipid를 선택한다.

And Basic Analyzer[T] 탭 Processing mode 패널 Nucleolus column의 Preset 드롭다운 버튼에서 Lipid를 선택한다.

And Basic Analyzer[T] 탭 Processing mode 패널 Lipid droplet column의 Preset 드롭다운 버튼에서 Lipid를 선택한다.

Then Basic Analyzer[T] 탭 Processing mode 패널 table의 네 column에서 모두 Baseline RI 값이 1.3337로, RII값이 0.135로 입력되는지 확인