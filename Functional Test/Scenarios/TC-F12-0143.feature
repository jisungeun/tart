﻿Feature: BA RI Thresholding Single Run

  Scenario: BA 탭에서 프로세서가 RI Thresholding일 때, Labeling 탭에서 화살표 버튼을 눌러 ‘Assign Size of Neglectable Particle’ 수치를 내림
Given Basic Analyzer 탭에서 Use Labeling 체크박스가 체크되어 있다.

When Basic Analyzer 탭의 ‘Assign Size of Neglectable Particle’ 수치 칸 옆의 아래 방향 화살표를 한 번 누른다.

Then Basic Analyzer 탭의 ‘Assign Size of Neglectable Particle’ 값이 24.9999로 바뀌는지 확인