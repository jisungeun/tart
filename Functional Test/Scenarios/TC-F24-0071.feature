﻿Feature: Mask Editor; From empty mask

  Scenario: Mask Editor 탭 FL tool 패널에서 Ch2로 Red FL 보기
Given 빈 TA가 열려있다.   
  
Given Red FL을 가진 TCF로 Mask Editor 탭이 열려 있다.

When FL tool 패널의 Ch2 버튼을 누른다.

Then Mask Editor의 2D Slice 세 화면에 Red FL이 나타났는지 확인

When FL tool 패널의 Channel 박스 아래의 Color 버튼을 누르고 팝업에서 흰색을 선택한 후 키보드의 엔터 키를 누른다.

Then Mask Editor 탭 2D Slice 화면의 Red FL이 흰색으로 바뀌었는지 확인

Then TA종료