﻿Feature: BAT Batch RUN setting

  Scenario: BAT  Batch Run setting 탭에서 TCF에 Time points를 적용하고 Execute Whole 버튼을 눌러 Batch run 시행
Given Basic Analyzer[T] 탭 Batch Run setting 탭의 Processing mode 패널에서 AI Segmentation이 프로세서로 선택되어 있다.

When Batch Run setting 탭 리스트에 존재하는 모든 TCF에 Time points를 설정하고 Processing mode 패널의 Execute Whole 버튼을 누른다.

And Batch Run setting 탭 위에 프로세싱 바 팝업이 나타나면 진행이 완료될 때 까지 기다린다.

Then 프로세싱 바 팝업과 Basic Analyzer[T] 탭이 사라지는 지 확인

And Report[T] 탭이 화면에 나타나는지 확인

And Report[T] 탭의 All 체크박스에 체크 되어있는지 확인

And Report[T] 탭에 app을 적용한 hypercube의 데이터에 따른 table과 비어있는 graph 캔버스가 각 패널에 출력 되었는지 확인

And Application tab 바에서 Report[T] 탭 이름에 하이라이트가 생겼는지 확인 