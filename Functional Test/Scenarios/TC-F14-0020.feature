﻿Feature: BAT AI Segmentation Single RUN

  Scenario: BAT Single Run 탭의 Mask Viewing panel 상단의 Mask 툴박스에서 마스크 선택
Given 빈 TA가 열려있다.   
  
Given Basic Analyzer[T] Single Run 탭의 Mask Viewing panel에 3D 마스크가 생성되어 있다.

When Basic Analyzer[T] 탭의 Mask Viewing panel 상단 Mask 툴박스의 Nucleus 체크박스에 체크한다.

Then Basic Analyzer[T] 탭의 Mask Viewing panel의 Cell의 Nucleus 영역에 마스크가 입혀졌는지 확인

When Basic Analyzer[T] 탭의 Mask Viewing panel 상단 Mask 툴박스의 Whole Cell 체크박스에 체크한다.

Then Basic Analyzer[T] 탭의 Mask Viewing panel의 Cell의 전체 영역에 마스크가 입혀졌는지 확인

When Basic Analyzer[T] 탭의 Mask Viewing panel 상단 Mask 툴박스의 Nucleolus 체크박스에 체크한다.

Then Basic Analyzer[T] 탭의 Mask Viewing panel의 Cell의 Nucleolus 영역에 마스크가 입혀졌는지 확인

When Basic Analyzer[T] 탭의 Mask Viewing panel 상단 Mask 툴박스의 Lipid droplet 체크박스에 체크한다.

Then Basic Analyzer[T] 탭의 Mask Viewing panel의 Cell의 Lipid droplet 영역에 마스크가 입혀졌는지 확인

When Basic Analyzer[T] 탭의 Mask Viewing panel 상단 Mask 툴박스에서 Multi-layered mask 스위치를 키고 All 체크박스를 클릭한다.

Then Mask Viewing panel의 3D 화면에 네가지 mask가 모두 보이는지 확인

When Basic Analyzer[T] 탭의 Tool box에서 Hide Mask 체크박스를 클릭한다. 

Then Mask Viewing panel 2D화면과 3D 화면에 마스크가 보이지 않는지 확인

When Hide Mask 체크박스를 선택 해제한 뒤, Nucleus, Nucleolus, Lipid droplet 체크박스를 선택 해제한다. 

Then Basic Analyzer[T] 탭의 Mask Viewing panel 3D 화면에 Whole Cell 마스크만 남아있는지 확인

Then TA종료