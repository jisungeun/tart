﻿Feature: Create Cube

  Scenario: Cube를 선택 시 Preview 패널에서 표시하는 TCF가 달라짐
Given 한 개 이상의 cube가 생성되어 있다.

When Playground canvas 위의 Normal cube 아이콘을 누른다.

Then Preview 패널의 Title이 선택한 cube 이름으로 바뀌며 표시되는 TCF도 cube 내의 데이터로 바뀜을 확인