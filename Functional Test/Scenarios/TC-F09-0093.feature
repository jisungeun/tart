﻿Feature: BA Application Parameter

  Scenario: RI Thresholding을 프로세서로 선택했을 때, Auto lower Threshold 체크박스 체크 시 마우스 휠을 이용해 Entropy 알고리즘을 선택
Given BA Application Parameter에서 Auto lower Threshold 체크박스가 체크되어 있다.

And BA Application Parameter 패널 Auto Threshold 탭의 Select Algorithm 버튼에 Otsu가 선택되어 있다.

When Select Algorithm 드롭다운 버튼에 마우스 포인터를 올리고 마우스 휠을 아래로 한 칸 내린다.

Then BA Application Parameter 패널 Auto Threshold 탭의 Select Algorithm 버튼에 Entropy가 선택되었는지 확인