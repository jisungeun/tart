﻿Feature: BAT Application Parameter

  Scenario: Basic Analyzer[T] app을 적용했을 때, Application Parameter 패널의 SELECT PROCESSOR에서 AI Segmentation으로 변경
	
Given Playground canvas에서 Basic Analyzer[T] 아이콘이 선택되어 있다.

And BAT Application Parameter 패널에 RI Thresholding이 선택되어 있다.

When BAT Application Parameter 패널 SELECT PROCESSOR의 드롭다운 버튼을 클릭한다.

And BAT Application Parameter 패널의 AI Segmentation을 선택한다.

Then BAT Application Parameter의 UI가 AI Segmentation UI로 바뀌는지 확인