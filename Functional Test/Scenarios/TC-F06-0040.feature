﻿Feature: Link Data to Cube

  Scenario: List 패널에서 > 버튼을 이용해 TCF를 cube에 연결
Given Cube가 존재하는 List 패널이 열려 있다.

When List 패널의 왼쪽 table에서 cube에 연결하고 싶은 TCF를 선택한다.

And List 패널의 > 버튼을 누른다.

Then Project Explorer 패널에서 cube에 TCF가 연결되었는지 확인 한다.

And List 패널의 오른쪽 table에서 cube에 연결된 TCF row가 생성되었는지 확인