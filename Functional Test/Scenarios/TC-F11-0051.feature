﻿Feature: BA Batch RUN

  Scenario: Inter-Cube mean comparison panel에 Graph를 표시
Given Report 탭의 Inter-Cube mean comparison panel에 table이 열려 있다.

When Inter-Cube mean comparison panel에서 Graph 글자를 클릭한다.

Then Inter-Cube mean comparison panel에 table 대신 graph가 나타나는지 확인

And Inter-Cube mean comparison panel의 Table 글자의 하이라이트가 꺼지고 Graph 글자가 하이라이트 됨을 확인