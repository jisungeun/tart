﻿Feature: Create Hyperube

  Scenario: Copy Hypercube 버튼을 눌러 Hypercube를 복사
Given Hypercube가 생성되어 있다.

When Playground canvas의 Hypercube 아이콘을 우클릭한다.

And Copy Hypercube 버튼을 누른다.

Then Project Explorer 패널에서 ‘(원본 Hypercube 이름)_copy’의 Hypercube가 생성됨을 확인

And Playground canvas에서 ‘(원본 Hypercube 이름)_copy’의 Hypercube가 생성됨을 확인