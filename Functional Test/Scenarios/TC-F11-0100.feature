﻿"""Feature: BA Batch RUN

  Scenario: Graph of individual cell 패널의 그래프를 저장
Given Report 탭이 열려 있다.

When Graph of individual cell 패널의 Capture 버튼을 누른다.

And 파일 탐색기에서 경로와 파일 이름을 지정하고 저장 버튼을 누른다.

Then 해당 저장 경로에 지정한 이름을 가진 Inter-Cube mean comparison panel의 그래프의 png 파일이 생성되었는지 확인

And 캡쳐된 그래프가 Graph of individual cell 패널의 그래프와 같은 내용인지 확인