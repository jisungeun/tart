﻿Feature: 3D Visualizer; HT Image Data Rendering

  Scenario: Preset의 Copy, Save 버튼으로 TF canvas에 생성된 TF box의 정보를 Copy, Save, Clear
Given 빈 TA가 열려있다.   
  
Given 3D Visualizer 탭의 View 패널에 Rendering이 생성되어 있다.

When 3D Visualizer 탭 Preset 패널의 Copy 버튼을 누른다.

And 윈도우의 메모장을 열고 키보드의 Ctrl+V 버튼을 누른다.

Then 3D Visualizer 탭 Preset 하단의 table이 메모장에 붙여넣기 됐는지 확인

When 3D Visualizer 탭 Preset 패널의 Save 버튼을 누른다.

And 파일 탐색기에 파일 이름과 경로를 지정하고 저장 버튼을 누른다.

Then 지정한 경로에 지정한 이름의 .csv 파일이 생성됐는지 확인

And 생성된 .csv 파일과 Preset 패널의 table 내용이 일치하는지 확인

When 3D Visualizer 탭 Preset 패널의 Clear 버튼을 누른다.

Then TF canvas의 TF box가 모두 사라졌는지 확인

And 3D Visualizer 탭 View 패널의 Rendering이 사라졌는지 확인

Then 3D Visualizer TA종료