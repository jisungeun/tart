﻿Feature: BAT Application Parameter

  Scenario: RI Thresholding을 프로세서로 선택했을 때, Basic Measurement 탭 접기
Given BAT Application Parameter에서 RI Thresholding이 프로세서로 선택되어 있다.

And BAT Application Parameter 패널의 Basic Measurement 버튼 맨 오른쪽의 화살표가 윗 방향(△)이다.

When BAT Application Parameter 패널 Basic Measurement 버튼을 클릭한다.

Then BAT Application Parameter 패널 Basic Measurement 밑 UI가 접히는지 확인

And BAT Application Parameter 패널의 Basic Measurement 버튼의 맨 오른쪽 화살표가 아래 방향(▽)으로 바뀌었는지 확인