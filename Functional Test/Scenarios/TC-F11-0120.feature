﻿"Feature: BA Batch RUN

  Scenario: Report 탭 Data Table 패널 오른쪽의 화살표 버튼(>)을 눌러 Data Table 탭 화면 전체를 오른쪽으로 이동
Given 2개 이상의 cube를 batch run 한 결과의 Report 탭이 열려 있다. 

When Data Table 패널 오른쪽의 화살표 버튼(>)을 누른다.

Then 스크롤 바를 민 것처럼 화면에 표시되는 Data Table 패널 전체가 오른쪽으로 이동함을 확인