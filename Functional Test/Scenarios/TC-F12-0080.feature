﻿Feature: BA RI Thresholding Single Run

  Scenario: BA 탭에서 프로세서가 RI Thresholding일 때, Auto lower Threshold 체크박스 체크
Given Basic Analyzer 탭에서 RI Thresholding이 프로세서로 선택되어 있다.

And Basic Analyzer 탭 Auto threshold 탭의 Auto lower threshold 체크박스가 체크되어 있지 않다.

When Basic Analyzer 탭의 Auto lower Threshold 체크박스를 클릭한다.

Then Basic Analyzer 탭의 Auto lower Threshold 체크박스 밑에 ‘Select Algorithm’ 문구와 알고리즘 드롭다운 버튼, 하이라이트 된 Execute 버튼이 나타나는지 확인

And Basic Analyzer 탭의 Auto lower Threshold 체크박스가 하이라이트 되며 체크 되는지 확인

And Basic Analyzer 탭의 UL Threshold 탭의 Execute 버튼이 하이라이트 해제 되었는지 확인