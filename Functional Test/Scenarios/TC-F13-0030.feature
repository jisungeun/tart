﻿Feature: BAT Application Parameter

  Scenario: AI Segmentation을 프로세서로 선택했을 때, Basic Measurement의 table이 default 값으로 세팅
Given Playground canvas에서 Basic Analyzer[T] 아이콘이 선택되어 있다.

When BAT Application Parameter 패널의 맨 위 드롭다운 버튼에서 AI Segmentation을 프로세서로 선택한다.

Then BAT Application Parameter 패널 Basic Measurement의 table의 Preset row가 왼쪽부터 순서대로 Protein, Protein, Protein, Lipid로 선택되어 있는지 확인