﻿Feature: BAT AI Segmentation Single RUN

  Scenario: BAT Single Run 탭의 Movie player에 마우스를 대고 마우스 휠을 내려 화면에 보이는 이미지의 time point를 내리기
Given Basic Analyzer[T] 탭의 Single run 탭이 열려 있다.

And Basic Analyzer[T] 탭의 Mask Viewing panel에 time point 17의 이미지가 나타나있다.

When Basic Analyzer[T] 탭의 Movie player 패널에 마우스 포인터를 대고 마우스 휠을 아래로 20 칸 내린다.

Then Basic Analyzer[T] 탭의 Mask Viewing panel에 나타나는 이미지가 time point 1의 이미지로 바뀌었는지 확인

And Basic Analyzer[T] 탭의 Movie player 타임 바 커서의 위치가 왼쪽 끝으로 움직였는지 확인

And Basic Analyzer[T] 탭의 Movie player 타임 바 오른쪽의 숫자가 1로 내려갔는지 확인