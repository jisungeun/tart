﻿Feature: PEP/UI features

  Scenario: Project Explorer Panel의 각 항목 왼쪽의 화살표를 눌러 하위 항목 숨기기
Given Project Explorer Panel에 한 개 이상의 항목이 생성되어 있다.

And 항목 왼쪽 끝에 위치한 화살표가 아래 방향(▽)이다.

When 접고 싶은 항목의 왼쪽 끝에 위치한 화살표를 클릭한다.

Then 화살표 방향이 아래 방향(▽)에서 오른쪽 방향(▷)으로 바뀌었는지 확인

And 해당 항목 이하 수준의 모든 항목들이 숨겨졌는지 확인 