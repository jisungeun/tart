﻿Feature: BAT Application Parameter 

  Scenario: Basic Analyzer[T] AI Segmantation의 Application Parameter 패널 설정
Given 빈 TA가 열려있다.   
  
Given Playground canvas에서 Basic Analyzer[T] 아이콘이 선택되어 있다.

When BAT Application Parameter 패널 SELECT PROCESSOR의 드롭다운 버튼을 클릭하고 RI Thresholding을 선택한다.

Then BAT Application Parameter의 UI가 RI Thresholding UI로 바뀌는지 확인

When BAT Application Parameter 패널 SELECT PROCESSOR의 드롭다운 버튼에 마우스 포인터를 대고 마우스 휠을 위로 한 칸 올린다.

Then BAT Application Parameter의 UI가 AI Segmentation UI로 바뀌는지 확인

And BAT Application Parameter 패널 Basic Measurement의 table의 Preset row가 왼쪽부터 순서대로 Protein, Protein, Protein, Lipid로 선택되어 있는지 확인

When BAT Application Parameter 패널 Whole cell column의 Preset 드롭다운 버튼에서 Hemoglobin을 선택한다.

Then BAT Application Parameter 패널 Basic Measurement의 table의 Whole cell column에서 RII값이 0.15로 입력되는지 확인

When BAT Application Parameter 패널 Nucleus column의 Preset 드롭다운 버튼에서 Lipid를 선택한다.

Then BAT Application Parameter 패널 Basic Measurement의 table의 Nucleus column에서 RII값이 0.135로 입력되는지 확인

When BAT Application Parameter 패널 Basic Measurement의 table에서 Nucleolus column의 Preset 드롭다운 버튼을 Customize로 선택한다.

And BAT Application Parameter 패널 Basic Measurement의 table의 RII row의 cell을 클릭하고 키보드로 ‘12345’를 입력하고 키보드의 엔터 키를 누른다.

Then BAT Application Parameter 패널 Basic Measurement의 table의 Nucleolus column에서 RII값이 ‘12345’로 변경되는지 확인

When BAT Application Parameter 패널 Lipid droplet column의 Preset 드롭다운 버튼에서 Protein을 선택한다.

Then BAT Application Parameter 패널 Basic Measurement의 table의 Lipid droplet column에서 RII값이 0.19로 입력되는지 확인

Then TA종료