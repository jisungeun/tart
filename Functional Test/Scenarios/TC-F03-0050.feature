﻿Feature: Set Playground

  Scenario: Playground가 생성되면 Operation sequence의 화면이 다음 단계로 자동 진행
Given Set playground의 Playground 이름을 지정하는 탭이 열려있다.

When Playground의 이름을 지정한다.

And Playground의 이름을 설정하는 패널의 OK 버튼을 누른다.

Then Operation sequence의 화면이 Set Playground 탭에서 Create Hypercube 탭으로 자동으로 넘어가는지 확인