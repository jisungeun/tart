﻿Feature: PEP/UI features

  Scenario: Project Explorer Panel에서 Project 이름을 엔터 키를 이용해 재설정
Given 프로젝트가 열려있다.

When Rename Project 버튼을 누른다.

And 팝업에서 변경할 이름을 입력하고 키보드의 엔터 키를 누른다.

Then Project Explorer Panel에서 프로젝트 이름의 변경을 확인