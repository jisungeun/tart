﻿Feature: BA AI Segmentation Single RUN

  Scenario: Measurement 팝업의 데이터를 .csv 파일로 Export 취소 시 에러 메세지 출력
Given Basic Analyzer 탭에서 Measurement 팝업이 열려 있다.

When Measurement 팝업의 Export 버튼을 누른다.

And 파일 탐색기에서 취소 버튼을 누른다.

Then ‘Failed to save CSV’ 라는 내용의 에러 메세지 팝업이 뜨는지 확인