﻿Feature: PEP/UI features

  Scenario: Project Explorer Panel 하단부의 겹쳐진 위 방향 화살표 버튼을 눌러 Project Explorer Panel에서 개별 TCF파일 숨기기
Given Project Explorer Panel에 한 개 이상의 항목이 생성되어 있다.

When Project Explorer Panel 최하단부에 위치한 위 방향 겹쳐진 화살표 버튼(︽)을 클릭한다.

Then Project Explorer Panel에서 cube 단위까지의 항목들이 펼쳐졌는지 확인

And Project Explorer Panel에서 cube 단위 이하의 개별 TCF 파일들이 숨겨졌는지 확인