﻿Feature: BAT AI Segmentation Single RUN

  Scenario: BAT Single Run 탭에서 setting한 time points에만 마스크가 생성됨
Given Basic Analyzer[T] Single Run 탭의 데이터 1, 2, 3 time point에 마스크가 생성되어 있다.

When Basic Analyzer[T] 탭의 Movie player에서 time point를 4에서 17까지 움직인다.

Then Basic Analyzer[T] 탭의 Mask Viewing panel에서 마스크가 생성되지 않았음을 확인