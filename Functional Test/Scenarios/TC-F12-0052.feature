﻿Feature: BA RI Thresholding Single Run 

  Scenario: 	BA 탭에서 프로세서가 RI Thresholding일 때, UL Threshold 탭에서 값을 직접 입력하여 수치를 설정하기
Given Basic Analyzer Single Run 탭의 Processing mode 패널의 제일 위 드롭다운 버튼이 RI Thresholding으로 선택되어 있다.

When Basic Analyzer Single Run 탭의 ‘Assign lower threshold’ 사이즈 바 옆의 칸에 '1'을 입력한다.

And Basic Analyzer Single Run 탭의 ‘Assign upper threshold’ 사이즈 바 옆의 칸에 '2'를 입력한다.

Then ‘Assign lower threshold’ 사이즈 바와 ‘Assign upper threshold’ 사이즈 바가 각각 왼쪽 끝과 오른쪽 끝으로 이동했는지 확인