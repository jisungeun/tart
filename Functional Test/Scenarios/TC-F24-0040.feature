﻿Feature: Mask Editor; From empty mask

  Scenario: Paint 버튼으로 Cell instance, Whole cell 마스크를 그리고 임시 저장 후 .msk 파일로 저장하기
Given 빈 TA가 열려있다.   
  
Given Mask Editor 탭에서 ‘test’ ID가 선택되어 있다.

When Mask control 패널 Cell Instance와 Whole cell 마스크의 오른쪽 회색 버튼을 클릭한다.

And Mask selector 패널 Cell instance 아이콘을 누른다.

And Drawing tool 패널의 Fill 버튼을 누른다.

And Mask Editor 탭 중앙의 XY Slice 화면을 클릭한다.

Then Mask Editor 탭에서 클릭한 slice에 Cell instance 마스크가 채워졌는지 확인

When Mask selector 패널 Whole cell 아이콘을 누른다.

And Drawing tool 패널의 Fill 버튼을 누른다.

And Mask Editor 탭 중앙의 XY Slice 화면에 마우스 포인터를 대고 클릭한다.

Then Mask Editor 탭에서 마우스 포인터로 클릭한 slice에 Whole cell 마스크가 생겼는지 확인

When Mask selector 패널 Whole cell 아이콘을 누른다.

Then Mask selector 화면에 Whole cell 마스크가 사라졌는지 확인

When General 패널의 Mask file download 버튼을 누르고 Organ Mask와 Whole cell에 체크한 뒤 Save 버튼을 누른다.

And 파일의 경로와 이름을 지정한다.

Then 지정한 파일의 경로에 .msk 형식의 Multi-Layer 마스크 파일이 생성됐는지 확인

When Mask selector 패널에서 Cell instance 버튼을 누른 뒤 General 패널의 Mask file download 버튼을 누르고 Cell Instance Mask에 체크한 후 Save 버튼을 누른다.

And 파일의 이름과 경로를 지정한다.

Then 지정한 파일의 경로에 .msk 형식의 Multi-Label 마스크 파일이 생성됐는지 확인

Then TA종료