﻿Feature: BA AI Segmentation Single RUN

  Scenario: Mask Viewing panel의 3D 마스크를 마우스로 클릭한 채로 움직이면 마스크가 회전
Given Basic Analyzer 탭의 Mask Viewing panel에 3D 마스크가 생성되어 있다.

When 마우스를 커서 모양이 바뀌는 3D 마스크 캔버스 위로 가져가 클릭한 채로 마우스를 움직인다.

Then 생성된 3D 마스크가 회전하는지 확인

