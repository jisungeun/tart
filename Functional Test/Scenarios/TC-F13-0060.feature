﻿Feature: BAT Application Parameter

  Scenario: RI Thresholding을 프로세서로 선택했을 때, UL Threshold 탭에서 ‘Assign lower threshold’ 사이즈 바를 움직여 수치를 설정하기
Given BAT Application Parameter에서 RI Thresholding이 프로세서로 선택되어 있다.

When BAT Application Parameter 패널의 ‘Assign lower threshold’ 의 사이즈 바를 마우스로 왼쪽 끝까지 움직인다.

Then BAT Application Parameter 패널의 ‘Assign lower threshold’ 사이즈 바 옆의 수치가 1.0010으로 바뀌었는지 확인

When BAT Application Parameter 패널의 ‘Assign lower threshold’ 의 사이즈 바를 마우스로 오른쪽 끝까지 움직인다2.

Then BAT Application Parameter 패널의 ‘Assign lower threshold’ 사이즈 바가 ‘Assign upper threshold’ 값을 넘지 않는 1.4384까지만 올라가는지 확인