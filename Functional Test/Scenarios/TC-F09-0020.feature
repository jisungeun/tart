﻿Feature: 	BA Application Parameter

  Scenario: Basic Analyzer RI Thresholding의 Application parameter 패널 설정
Given 빈 TA가 열려있다.   
  
Given BA Application Parameter에서 RI Thresholding이 프로세서로 선택되어 있다.

When ‘Assign lower threshold’ 의 사이즈 바를 마우스로 오른쪽 끝까지 움직인다.

Then ‘Assign lower threshold’ 사이즈 바가 ‘Assign upper threshold’ 값을 넘지 않는 값 까지만 올라가는지 확인

And ‘Assign upper threshold’ 사이즈 바 옆의 칸에 '2'를 입력한다.

Then 입력한 값 대로 수치가 변경되었는지 확인

When Auto lower Threshold 체크박스를 클릭한다.

Then ‘Select Algorithm’ 문구와 알고리즘 드롭다운 버튼이 나타나는지 확인

When Select Algorithm 드롭다운 버튼에서 Entropy를 클릭한다.

Then BA Application Parameter 패널 Auto Threshold 탭의 Select Algorithm 버튼에 Entropy가 선택되었는지 확인

When Select Algorithm 드롭다운 버튼에 마우스 포인터를 올리고 마우스 휠을 아래로 한 칸 내린다.

Then BA Application Parameter 패널 Auto Threshold 탭의 Select Algorithm 버튼에 Moments Preserving이 선택되었는지 확인

When Auto lower Threshold 체크박스를 한 번 더 클릭한다.

Then ‘Select Algorithm’ 문구와 알고리즘 드롭다운 버튼이 사라지는지 확인

When Use Labeling 체크박스를 클릭한다.

Then ‘Select Option’ 문구와 옵션 드롭다운 버튼과 ‘Assign Size of Neglectable Particle’ 문구와 사이즈 바, 수치 입력 칸이 나타나는지 확인

When BA Application Parameter 패널의 Select Option 드롭다운 버튼을 누르고 Largest Label 버튼을 누른다.

Then BA Application Parameter 패널의 Select Option 버튼이 Largest Label 옵션으로 변경되었는지 확인

When Select Option 드롭다운 버튼에 마우스 포인터를 올리고 마우스 휠을 위로 올린다.

Then BA Application Parameter 패널의 Select Option 버튼이 Multi Labels 옵션으로 변경되었는지 확인

When ‘Assign Size of Neglectable Particle’ 수치 칸 옆의 윗 방향 화살표를 한 번 누른다.

Then ‘Assign Size of Neglectable Particle’ 값이 25.0001로 바뀌는지 확인

When Use Labeling 체크박스를 클릭한다.

Then ‘Select Option’ 문구와 옵션 드롭다운 버튼과 ‘Assign Size of Neglectable Particle’ 문구와 사이즈 바, 수치 입력 칸이 사라지는지 확인

Then TA종료