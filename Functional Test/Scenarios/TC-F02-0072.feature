﻿Feature: TC-F02-0072

  Scenario: List 패널에서 Shift 키를 이용하여 TCF를 묶음 선택
	
Given TCF가 불러진 List 패널이 열려 있다.

When List의 가장 위에 있는 TCF 파일을 누른다.

And 키보드의 Shift를 누른 상태로 List의 가장 아래에 있는 TCF파일을 누른다.

Then 클릭한 두 개의 파일 사이의 모든 TCF 파일이 선택되었는지 확인