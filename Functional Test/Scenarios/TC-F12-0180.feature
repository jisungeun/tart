﻿Feature: BA RI Thresholding Single Run

  Scenario: BA 탭에서 프로세서가 RI Thresholding일 때, Basic Measurement 탭의 Execute 버튼을 눌러 Cell 정량 분석 결과 출력
Given Basic Analyzer 탭의 Mask Viewing panel에 RI 마스크가 생성되어 있다.

When  Basic Analyzer 탭 Basic Measurement의 table의 default column의 Preset 드롭다운 버튼을 ‘Protein’으로 설정하고 Basic Measurement 탭의 Execute 버튼을 누른다.

Then Basic Analyzer 탭에 Measurement 팝업이 생기는지 확인한다.