﻿Feature: Select Application

  Scenario: Hypercube에 Basic Analyzer 앱 연결 시 Application Parameter 패널의 UI 변경
Given Hypercube에 BA app이 연결되어 있지 않다.

When BA app과 적용 시킬 hypercube를 선택한다.

And Select Application패널에 OK 버튼을 누른다.

Then Application Parameter 패널의 UI가 BA 버전으로 바뀌는지 확인