﻿Feature: BAT AI Segmentation Single RUN

  Scenario: BAT Single Run 탭의 Movie player에 마우스를 대고 마우스 휠을 올려 화면에 보이는 이미지의 time point를 올리기
Given Basic Analyzer[T] 탭의 Single run 탭이 열려 있다.

When Basic Analyzer[T] 탭의 Movie player 패널에 마우스 포인터를 대고 마우스 휠을 위로 20 칸 올린다.

Then Basic Analyzer[T] 탭의 Mask Viewing panel에 나타나는 이미지가 time point 17의 이미지로 바뀌었는지 확인

And Basic Analyzer[T] 탭의 Movie player 타임 바 커서의 위치가 오른쪽 끝으로 움직였는지 확인

And Basic Analyzer[T] 탭의 Movie player 타임 바 오른쪽의 숫자가 17로 올라갔는지 확인