﻿Feature: BAT Application Parameter

  Scenario: RI Thresholding을 프로세서로 선택했을 때, Use Labeling 체크박스 체크 후 Multi Labels 옵션을 선택
Given BAT Application Parameter에서 Use Labeling 체크박스가 체크되어 있다.

When BAT Application Parameter 패널의 Select Option 드롭다운 버튼에서 Multi Labels 버튼을 누른다.

Then BAT Application Parameter 패널의 Select Option 버튼이 Multi Labels 옵션으로 변경되었는지 확인 