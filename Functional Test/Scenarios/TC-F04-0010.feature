﻿Feature: 	Create Hypercube

  Scenario: Playground canvas에서 Hypercube 아이콘을 클릭하여 hypercube를 선택/선택 해제
  
Given 빈 TA가 열려있다.  
  
Given 한 개 이상의 hypercube가 생성되어 있다.

When Playground canvas 위의 Normal hypercube 아이콘을 누른다.

Then Playground canvas 위의 Normal hypercube 아이콘이 Active hypercube 아이콘으로 바뀜을 확인

And Preview 패널의 Title이 선택한 hypercube 이름으로 바뀌며 표시되는 TCF도 Hypercube 내의 데이터로 바뀜을 확인

When Hypercube 아이콘 바깥의 Playground canvas 아무 영역이나 누른다.

Then Playground canvas 위의 Active hypercube 아이콘이 Normal hypercube 아이콘으로 바뀜을 확인

And Preview 패널의 Title이 Playground 이름으로 바뀜을 확인

And Preview 패널에 표시되는 TCF가 Playground 내의 전체 TCF로 바뀜을 확인

Then TA종료