﻿Feature: BA RI Thresholding Single Run

  Scenario: BA 탭에서 프로세서가 RI Thresholding일 때, Auto lower Threshold 체크박스 체크 시 마우스 휠을 이용해 Moments Preserving 알고리즘을 선택
Given Basic Analyzer 탭에서 Auto lower Threshold 체크박스가 체크되어 있다.

And Basic Analyzer 탭의 Processing mode 패널 Auto Threshold 탭의 Select Algorithm 버튼에 Entropy가 선택되어 있다.

When Basic Analyzer 탭의 Select Algorithm 드롭다운 버튼에 마우스 포인터를 올리고 마우스 휠을 아래로 내린다.

Then Basic Analyzer 탭 Processing mode 패널 Auto Threshold 탭의 Select Algorithm 버튼에 Moments Preserving이 선택되었는지 확인