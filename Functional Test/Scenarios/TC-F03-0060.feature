﻿Feature: Set Playground

  Scenario: Playground setup 패널에서 Rename 버튼을 눌러 playground의 이름을 변경
Given 하나 이상의 Playground가 생성되어 있다.

When playground setup 패널에서 이름을 변경하고 싶은 playground를 선택한다.

And Rename 버튼을 누른다.

And 변경할 이름을 지정한다.

And Playground의 이름을 변경하는 패널의 OK 버튼을 누른다.

Then Project Explorer 패널에서 playground의 이름이 변경되었는지 확인

And Playground setup 패널에서 playground의 이름이 변경되었는지 확인