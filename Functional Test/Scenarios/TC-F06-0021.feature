﻿Feature: Link Data to Cube

  Scenario: 엔터 키와 Link to Cube 버튼을 이용하여 Cube에 TCF를 연결
Given Cube와 TCF가 생성되어 있다.

When 연결하고 싶은 TCF를 한 개 이상 선택한다.

And Toolbox의 Link to Data 버튼을 누른다.

And 팝업에서 경로를 지정하고 키보드의 엔터 키를 누른다.

Then Project Explorer 패널에서 cube에 TCF가 연결되었는지 확인

And Operation sequence 패널의 Link to Cube 탭에서 TCF가 연결되었는지 확인

And Preview/List 패널에서 데이터가 연결되었음이 표시되었는지 확인