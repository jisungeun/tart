﻿Feature: BA RI Thresholding Single Run

  Scenario: BA 탭에서 프로세서가 RI Thresholding일 때, UL Threshold 탭에서 ‘Assign upper threshold’ 사이즈 바를 움직여 수치를 설정하기
Given Basic Analyzer Single Run 탭의 Processing mode 패널의 제일 위 드롭다운 버튼이 RI Thresholding으로 선택되어 있다.

When Basic Analyzer Single Run 탭의 ‘Assign upper threshold’ 의 사이즈 바를 마우스로 왼쪽 끝까지 움직인다.

Then Basic Analyzer Single Run 탭의 ‘Assign upper threshold’ 사이즈 바가 ‘Assign lower threshold’ 값을 넘지 않는 1.3584까지만 내려가는지 확인

When Basic Analyzer Single Run 탭의 ‘Assign upper threshold’ 의 사이즈 바를 마우스로 오른쪽 끝까지 움직인다.

Then Basic Analyzer Single Run 탭의 ‘Assign upper threshold’ 사이즈 바 옆의 수치가 2.0000으로 바뀌었는지 확인