﻿"""Feature: BAT AI Segmentation Single RUN

  Scenario: BAT Single Run 탭의 Mask Viewing panel의 HT 이미지 캔버스의 Level window를 조절
Given Basic Analyzer[T] Single Run 탭이 열려 있다.

When Basic Analyzer[T] 탭 HT 이미지 캔버스의 우상단의 톱니바퀴 아이콘을 클릭한 뒤 Level Window 버튼을 클릭하고 Data Range 사이즈 바의 왼쪽 버튼을 마우스로 오른쪽 끝까지 움직인다.

Then Basic Analyzer[T] 탭 Mask Viewing panel에서 HT 이미지가 어둡게 변하는지 확인

And Basic Analyzer[T] 탭 Level Window 사이즈 바 밑의 왼쪽 수치가 오른쪽 수치와 14441로 같아졌는지 확인