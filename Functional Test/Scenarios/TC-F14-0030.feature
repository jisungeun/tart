﻿Feature: BAT AI Segmentation Single RUN

  Scenario: BAT Single Run 탭의 Time points setting 패널에서 start 칸에 수치를 입력하여 time point 시작 값을 setting
Given Basic Analyzer[T] 탭의 Single run 탭이 열려 있다.

When Basic Analyzer[T] 탭의 Time points setting 패널에서 start 칸에 '2'를 입력한다.

Then Basic Analyzer[T] 탭 Time points setting 패널의 Selected time points에 나타나는 시작 숫자가 '2'로 바뀌었는지 확인