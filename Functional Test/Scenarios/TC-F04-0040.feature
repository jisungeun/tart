﻿Feature: Create Hypercube

  Scenario: Hypercube를 생성시 Operation sequence의 화면이 자동으로 다음 단계로 진행
Given Create Hypercube 탭이 열려있다.

When Hypercube의 이름을 지정한다.

And Create Hypercube 탭의 OK 버튼을 누른다.

Then Operation sequence의 화면이 Create Hypercube 탭에서 Create Cube 탭으로 자동으로 넘어감을 확인