﻿Feature: BAT AI Segmentation Single RUN

  Scenario: BAT Single Run 탭의 Mask Viewing panel 상단의 Mask 툴박스에서 Cell Instance 마스크를 출력
Given Basic Analyzer[T] Single Run 탭의 Mask Viewing panel에 3D 마스크가 생성되어 있다.

And Basic Analyzer[T] 탭의 Mask Viewing panel 상단 Mask 툴박스에 Whole Cell이 체크되어 있다.

When Basic Analyzer[T] 탭의 Mask Viewing panel 상단 Mask 툴박스의 Cell Instance 체크박스에 체크한다.

Then Basic Analyzer[T] 탭의 Mask Viewing panel의 Cell Instance 영역에 마스크가 입혀졌는지 확인