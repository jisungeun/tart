﻿"""Feature: BA AI Segmentation Single RUN

  Scenario: 이전에 Single run 돌려본 TCF를 다시 Single run 돌리면 Basic Analyzer 탭에 Mask와 Measurement 팝업이 같이 열림
Given BA Application Parameter에서 프로세서로 AI Segmentation이 선택되어 있다.

When Single RUN 버튼을 누른다.

And 팝업에서 App이 연결된 hypercube 안의 TCF 중 이미 Single run 해 본 적이 있는 TCF를 선택한다.

Then Basic Analyzer에서 Mask와 Measurement 팝업이 바로 열리는지 확인