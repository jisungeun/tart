﻿Feature: BAT Application Parameter

  Scenario:Basic Analyzer[T] app을 적용했을 때, Application Parameter 패널의 SELECT PROCESSOR에서 마우스 휠을 이용하여 RI Thresholding으로 변경
Given Playground canvas에서 Basic Analyzer[T] 아이콘이 선택되어 있다.

And BAT Application Parameter 패널에 AI Segmentation이 선택되어 있다.

When BAT Application Parameter 패널 SELECT PROCESSOR의 드롭다운 버튼에 마우스 포인터를 대고 마우스 휠을 아래로 내린다.

Then BAT Application Parameter 패널 SELECT PROCESSOR에서 RI Thresholding이 선택되는지 확인

And Application Parameter의 UI가 RI Thresholding UI로 바뀌는지 확인 