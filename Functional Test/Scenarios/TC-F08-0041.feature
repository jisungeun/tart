﻿Feature: PEP/UI features

  Scenario: Project Explorer Panel에서 TCF를 엔터 키를 이용하여 프로젝트에서 삭제
Given 프로젝트에 TCF가 불러져 있다.

When Project Explorer Panel의 삭제하고 싶은 TCF 폴더에 마우스 포인터를 대고 우클릭후 Remove TCF 버튼을 누른다.

And  Remove TCF Panel의 키보드의 엔터 키를 누른다.

Then Project Explorer Panel에서 삭제한 TCF 폴더가 사라졌는지 확인

And Preview/List 패널에서 해당 폴더의 TCF가 삭제됐는지 확인