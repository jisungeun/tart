﻿"Feature: BA Batch RUN

  Scenario: Report 탭 Data Table 패널에서 table 셀을 클릭 시 해당 셀이 포함된 row가 전체 하이라이트
Given Report 탭이 열려 있다.

When Data Table 패널 테이블의 셀 하나에 마우스 포인터를 가져가서 클릭한다.

Then 클릭한 테이블 셀이 포함된 row 전체가 파란색으로 하이라이트 됨을 확인

And Data Table 패널의 헤더가 모두 파란색으로 하이라이트 됨을 확인