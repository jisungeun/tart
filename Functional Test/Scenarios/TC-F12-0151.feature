﻿Feature: BA RI Thresholding Single Run

  Scenario: BA 탭에서 프로세서가 RI Thresholding일 때, Labeling 탭에서 Execute 버튼을 눌러 Largest Label 방식으로 마스크를 수정
Given Basic Analyzer 탭에서 Use Labeling 체크박스가 체크되어 있다.

When Basic Analyzer 탭 Processing mode 패널의 Option에서 Largest Label을 선택하고 Labeling 탭의 Execute 버튼을 누른다.

Then Basic Analyzer 탭 Mask Viewing panel에서 가장 큰 크기의 mask만 남는 방식으로 mask가 수정되었는지 확인