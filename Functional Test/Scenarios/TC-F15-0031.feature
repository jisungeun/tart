﻿Feature: BAT Batch RUN setting

  Scenario: BAT Batch Run setting 탭의 단일 cube 체크박스에 체크 해제하여 cube에 링크된 TCF를 일괄 체크 해제
Given Basic Analyzer[T]의 Batch Run setting 탭의 단일 cube 체크박스가 체크 되어 있다.

When Basic Analyzer[T] Batch Run setting 탭의 단일 cube 체크박스 혹은 cube 이름을 클릭한다.

Then Basic Analyzer[T] Batch Run setting 탭의 단일 cube 체크박스가 체크 해제됐는지 확인

And Basic Analyzer[T] Batch Run setting 탭의 cube 체크박스 뒤의 cube 이름이 하이라이트 해제됐는지 확인

And Basic Analyzer[T] Batch Run setting 탭에서 cube에 링크된 TCF 파일의 체크박스가 체크 해제됐는지 확인