﻿Feature: BA RI Thresholding Single Run

  Scenario: BA 탭에서 마스크가 생성되었을 때 Mask Viewing panel 상단의 툴박스에서 Whole Cell 마스크를 출력
Given Basic Analyzer 탭의 Mask Viewing panel에 3D RI 마스크가 생성되어 있다.

When Basic Analyzer Single Run 탭 Mask Viewing panel 상단 Mask 툴박스의 Whole Cell 체크박스에 체크한다.

Then Mask Viewing panel의 3D 마스크 캔버스에서 Cell 전체에 RI 마스크가 입혀졌는지 확인

And Mask Viewing panel의 HT 사진의 Cell 영역 전체에 RI 마스크가 입혀졌는지 확인