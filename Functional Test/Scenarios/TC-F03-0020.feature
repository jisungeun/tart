﻿Feature: 	Set Playground

  Scenario: Set Playground 탭에서 Playground를 다시 새로 만들 때 default name이 자동 생성 
Given 프로젝트에 Playground가 생성되어 있다.

When Set Playground 탭을 연다.

And New Playground 버튼을 누른다.

Then Playground 이름을 지정하는 칸에 ‘현재 시간-pc ID’ 로 default name이 설정되었는지 확인2