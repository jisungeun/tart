﻿Feature: BA AI Segmentation Single RUN

  Scenario: Mask Viewing panel 상단의 Mask 툴박스에서 Cell-wise filter 스위치를 켜 TCF 안의 서로 다른 cell을 구별
Given Basic Analyzer 탭의 Mask Viewing panel에 3D Whole Cell 마스크가 생성되어 있다.

When Mask Viewing panel 상단 Mask 툴박스의 Cell-wise filter 스위치를 켠다.

Then TCF에서 각각의 cell의 마스크가 다른 색으로 표시되는지 확인

And  Mask 툴박스의 Cell-wise filter 스위치 버튼이 하이라이트 됐는지 확인

And Mask 툴박스의 Cell-wise filter 스위치 버튼이 오른쪽으로 움직였는지 확인