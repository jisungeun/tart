﻿Feature: BAT Report[T]

  Scenario: Parameter와 Organelle을 각각 Concentration와 Nuleolus으로 설정하고 Draw Graph 버튼을 눌러 그래프 출력
Given Report[T] 탭이 열려 있다.

When Report[T] 탭의 Select data 패널에서 All에 체크한다.

And Report[T] 탭의 Select Parameter 패널에서 Concentration, Select Organelle 패널에서 Nuleolus를 선택하고 Draw Graph 버튼을 누른다.

Then Report[T] 탭의 Graph 캔버스 패널에 dot plot이 Time points마다 그려졌는지 확인

And Report[T] 탭의 Graph 캔버스 패널에 line plot이 그려졌는지 확인

And 그래프 title이 Nuleolus인지 확인

And 그래프 y축이 Concentration인지 확인