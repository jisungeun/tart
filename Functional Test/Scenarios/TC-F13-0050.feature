﻿Feature: BAT Application Parameter

  Scenario: RI Thresholding을 프로세서로 선택했을 때, UL Threshold 탭 접기
Given BAT Application Parameter에서 RI Thresholding이 프로세서로 선택되어 있다.

And BAT Application Parameter 패널의 UL Threshold 버튼 맨 오른쪽의 화살표가 윗 방향(△)이다.

When BAT Application Parameter 패널의 UL Threshold 버튼을 누른다.

Then BAT Application Parameter 패널의 UL Threshold 밑의 UI가 접히는지 확인

And BAT Application Parameter 패널의 UL Threshold 버튼 맨 오른쪽의 화살표가 아래 방향(▽)으로 바뀌는지 확인