﻿"""Feature: BAT AI Segmentation Single RUN

  Scenario: BAT Single Run 탭 Measurement 팝업에서 tabel 셀을 클릭 시 해당 셀이 포함된 row가 전체 하이라이트
Given Basic Analyzer[T] Single Run 탭에서 Measurement 팝업이 열려 있다.

When Basic Analyzer[T] 탭 위의 Measurement 팝업 테이블의 셀 하나에 마우스 포인터를 가져가서 클릭한다.

Then 클릭한 Basic Analyzer[T] 탭 위 Measurement 팝업 테이블 셀이 포함된 row 전체가 파란색으로 하이라이트 됨을 확인