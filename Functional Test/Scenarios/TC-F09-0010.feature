﻿Feature: BA Application Parameter

  Scenario: Basic Analyzer AI Segmentation의 Application parameter 패널 설정
  
Given 빈 TA가 열려있다.   

Given Playground canvas에서 Basic Analyzer 아이콘이 선택되어 있다.

And Application Parameter 패널에 AI Segmentation이 선택되어 있다.

When Application Parameter 패널 SELECT PROCESSOR의 드롭다운 버튼을 클릭한다.

And Application Parameter 패널의 RI Thresholding을 선택한다.

Then Application Parameter의 UI가 달라지는지 확인

When Application Parameter 패널 SELECT PROCESSOR의 드롭다운 버튼에 마우스 포인터를 대고 마우스 휠을 위로 올린다.

Then Application Parameter의 UI가 AI Segmentation의 것으로 달라지는지 확인

And Basic Measurement의 table의 Preset row가 왼쪽부터 순서대로 Protein, Protein, Protein, Lipid로 선택되어 있는지 확인

When Application Parameter 패널 Whole cell column의 Preset 드롭다운 버튼에서 Hemoglobin을 선택한다.

Then Application Parameter Whole cell column에서 RII값이 0.15로 입력되는지 확인

When Application Parameter 패널 Nucleus column의 Preset 드롭다운 버튼에서 Lipid를 선택한다.

Then Application Parameter Nucleus column에서 RII값이 0.135로 입력되는지 확인

When Application Parameter 패널 Nucleolus column의 Preset 드롭다운 버튼에서 Customize를 선택한다.

And Application Parameter 패널 Nucleolus column에서 row의 cell을 클릭하고 키보드로 ‘12345’를 입력하고 키보드의 엔터 키를 누른다.

Then Nucleolus column에서 RII값이 ‘12345’로 변경되는지 확인

And Application Parameter 패널 Lipid droplet column의 Preset 드롭다운 버튼에서 Protein을 선택한다.

Then Application Parameter Lipid droplet column에서 RII값이 0.19로 입력되는지 확인

Then TA종료