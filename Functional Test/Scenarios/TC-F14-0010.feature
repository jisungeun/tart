﻿Feature: BAT AI Segmentation Single RUN

  Scenario: BAT Single Run 탭의 Movie player에서 time point 설정
Given 빈 TA가 열려있다.   
  
Given Basic Analyzer[T] 탭의 Single run 탭이 열려 있다.

When Basic Analyzer[T] 탭의 Movie player 타임 바 오른쪽의 숫자 칸 옆의 위 화살표 버튼을 한 번 누른다.

Then Basic Analyzer[T] 탭의 Mask Viewing panel 화면에 나타나는 이미지가 time point 2의 이미지로 바뀌었는지 확인

And Basic Analyzer[T] 탭의 Movie player 타임 바 오른쪽의 숫자가 2로 올라갔는지 확인

When Basic Analyzer[T] 탭의 Time points setting 패널에서 start 칸 옆의 위 화살표 버튼을 한 번 누른다.

Then Basic Analyzer[T] 탭 start 칸의 숫자가 1만큼 올라갔는지 확인

When Basic Analyzer[T] 탭의 Time points setting 패널에서 end 칸 옆의 아래 화살표 버튼을 한 번 누른다.

Then Basic Analyzer[T] 탭 end 칸의 숫자가 1만큼 내려갔는지 확인

When Basic Analyzer[T] 탭의 Time points setting 패널에서 interval 칸에 마우스 포인터를 대고 마우스 휠을 위로 한 칸 올린다.

Then Basic Analyzer[T] 탭 interval 칸의 숫자가 1만큼 올라갔는지 확인

And Basic Analyzer[T] 탭 Time points setting 패널의 Selected time points에 나타나는 수열의 공차가 1만큼 올라갔는지 확인

When Basic Analyzer[T] 탭의 Time points setting 패널에서 custom 체크박스에 체크한다.

And Basic Analyzer[T] 탭 Time points setting 패널의 Selected time points 칸에 time points를 1, 2, 3을 입력한다.

Then Basic Analyzer[T] 탭 Time points setting 패널의 Selected time points 칸의 숫자가 입력한 대로 바뀌는지 확인

Then TA종료