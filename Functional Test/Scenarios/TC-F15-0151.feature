﻿Feature: BAT Batch RUN setting

  Scenario: BAT  Batch Run setting 탭에서 모든 TCF에 Time points를 적용하지 않고 Execute Whole 버튼을 누르면 에러 팝업이 출력
Given Basic Analyzer[T] 탭이 열려있지 않다.

When BAT Application Parameter 패널의 Batch RUN 버튼을 누른다.

And Basic Analyzer[T] 탭 Batch Run setting 탭이 열리면 Processing mode 패널의 Execute Whole 버튼을 누른다.

Then “Set time steps for all image first!” 라는 에러 메세지 팝업이 나타나는지 확인