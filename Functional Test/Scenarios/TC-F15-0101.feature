﻿Feature: BAT Batch RUN setting

  Scenario: BAT Batch Run setting 탭의 Time points setting 패널에서 Apply time points 버튼을 눌러 선택한 TCF에 Time point를 적용
Given Basic Analyzer[T] 탭의 Batch Run setting 탭이 열려 있다.

When Batch Run setting 탭 Time points setting 패널에서 Time points를 세팅한다.

And Batch Run setting 탭에서 Time point를 적용할 TCF 파일을 선택하여 하이라이트 한다.

And Batch Run setting 탭의 빈 공간을 클릭한다.

And Batch Run setting 탭 Time points setting 패널의 Appply time points 버튼을 누른다.

Then Batch Run setting 탭의 점선 박스가 표시된 TCF의 Selected time points column에서 세팅한 Time points가 적용되었는지 확인