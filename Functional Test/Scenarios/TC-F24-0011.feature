﻿Feature: Mask Editor; From empty mask

  Scenario: TCF를 선택하지 않고 Mask editor를 실행하면 경고창이 출력
Given 빈 TA가 열려있다. 
  
Given TCF파일이 들어있는 프로젝트가 열려있다.

And Preview, List 패널에서 어떤 TCF도 선택되어 있지 않다.

When Toolbox의 Mask editor 버튼을 누른다.

Then 경고창이 생기며 Mask Editor 탭이 열리지 않는지 확인

Then TA종료