﻿Feature: BA AI Segmentation Single RUN

  Scenario: BA 탭 Processing mode 패널의 드롭다운 버튼에서 RI Thresholding으로 마우스 휠을 이용하여 변경
Given Basic Analyzer Single Run 탭의 Processing mode 패널의 제일 위 드롭다운 버튼이 AI Segmentation으로 선택되어 있다.

When Processing mode 패널의 드롭다운 버튼에 마우스 포인터를 올리고 마우스 휠을 아래로 내린다.

Then Processing mode 패널의 드롭다운 버튼이 RI Thresholding으로 변경되는지 확인

And Basic Analyzer Single Run 탭의 Processing mode 패널에 RI Thresholding의 UI가 나타나는지 확인