﻿Feature: 3D Visualizer; HT Image Data Rendering

  Scenario: View 패널의 2D HT 이미지 위에서 마우스 휠을 조작하여 단층 사진 보기 
Given 빈 TA가 열려있다.   
  
Given 3D Visualizer 탭이 열려있다.

When 3D Visualizer 탭 View 패널의 XY 2D HT 이미지 위에 마우스 포인터를 올리고 마우스 휠을 위로 5칸 올린다. 

Then 3D Visualizer 탭 View 패널의 XY HT 단층 사진의 Z축이 올라가는지 확인

When 3D Visualizer 탭 View 패널의 XY 2D HT 이미지 위에 마우스 포인터를 올리고 마우스 휠을 아래로 5칸 내린다. 

Then 3D Visualizer 탭 View 패널의 XY HT 단층 사진의 Z축이 내려가는지 확인

Then 3D Visualizer TA종료
