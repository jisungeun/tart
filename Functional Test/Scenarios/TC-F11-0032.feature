﻿Feature: BA Batch RUN

  Scenario: Report 탭의 Select Prameter 패널을 Surface area으로 변경
Given Report 탭이 열려 있다.

When Report 탭 Select Prameter 패널에서 Surface area 체크박스에 체크한다.

Then Report 탭 Select Parameter 패널이 Surface area에 체크됐는지 확인

And Report 탭의 Surface area 글자가 하이라이트 됐는지 확인

And Inter-Cube mean comparison panel의 그래프 y축이 Surface area로 되어 있는지 확인

And Graph of individual cell 패널의 y축이 Surface area로 되어 있는지 확인