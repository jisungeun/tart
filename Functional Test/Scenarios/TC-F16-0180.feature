﻿Feature: BAT Report[T]

  Scenario: Report[T] 탭의 그래프 패널의 그래프를 저장
Given Report[T] 탭에 그래프가 그려져 있다.

When Report[T] 탭의 그래프 패널의 Export graph 버튼을 누른다.

And 파일 탐색기에서 파일 경로와 파일 이름을 지정하고 저장 버튼을 누른다.

Then 해당 저장 경로에 지정한 이름의 png 파일이 생성되었는지 확인

And 저장된 그래프가 그래프 패널의 그래프와 같은 내용인지 확인

And Report[T] 탭의 그래프 패널에서 지정한 Min, Max값이 표시되었는지 확인

And 저장된 그래프의 색이 지정한 색으로 표시되었는지 확인