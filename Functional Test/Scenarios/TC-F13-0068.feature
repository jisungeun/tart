﻿Feature: BAT Application Parameter

  Scenario: RI Thresholding을 프로세서로 선택했을 때, UL Threshold 탭에서 마우스 휠을 이용하여 수치를 내리기
Given BAT Application Parameter에서 RI Thresholding이 프로세서로 선택되어 있다.

When BAT Application Parameter 패널의 ‘Assign lower threshold’의 UI에 마우스 포인터를 대고 마우스 휠을 아래로 한 칸 내린다.

And BAT Application Parameter 패널의 ‘Assign upper threshold’ 의 UI에 마우스 포인터를 대고 마우스 휠을 아래로 한 칸 내린다.

Then BAT Application Parameter 패널의 ‘Assign lower threshold’가 1.3499로, ‘Assign upper threshold’가 1.4399로 각각 내려가는지 확인