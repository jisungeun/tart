﻿Feature: BA RI Thresholding Single Run

  Scenario: BA 탭에서 프로세서가 RI Thresholding일 때, Basic Measurement의 table의 ‘Protein’ preset 값을 확인
Given Basic Analyzer Single Run 탭의 Processing mode 패널의 제일 위 드롭다운 버튼이 RI Thresholding으로 선택되어 있다.

When Basic Analyzer 탭 Basic Measurement 탭의 table에서 default column의 Preset 드롭다운 버튼에서 Protein을 선택한다.

Then Basic Analyzer 탭 Basic Measurement 탭의 table에서 Baseline RI 값이 1.3337로, RII값이 0.19로 입력되는지 확인