﻿Feature: Mask Editor; After BA Single Run

  Scenario: Multi-cell의 AI system 마스크를 수정
Given 빈 TA가 열려있다.   
  
Given Mask Editor 탭에 2개의 Cell이 있는 TCF의 AI Segmentation cell instance 마스크가 ID ‘test’로 Copy되어 있다.

When Mask control 패널의 체크박스 5개를 모두 클릭한다.

Then Mask Editor 탭의 2D, 3D 화면에서 모든 마스크가 multi-layered mask로 표현됐는지 확인

When Mask selector에서 Cell instance 아이콘을 클릭한다.

Then Mask control 패널의 체크박스가 모두 체크 해제됐는지 확인

And Label tools 패널의 Label picker 버튼을 누르고 Mask Editor의 초록색 마스크를 클릭한다.

Then Lable tools 패널 Label 칸의 값이 2로 바뀌었는지 확인

When Label tools 패널의 아래 화살표 버튼을 누른다.

Then Lable tools 패널 Label 칸의 값이 1로 바뀌었는지 확인

When Lable tools 패널 Label 칸에 '2’를 키보드로 입력한 뒤 Label tools 패널의 Label picker 버튼을 누른다.

Then Lable picker 버튼이 비활성화 되었는지 확인

And Drawing tool 패널에서 Paint 버튼을 클릭하고 Size를 100으로 키운다.

And Label tools 패널의 Add 글씨 버튼을 누르고 Mask Editor 탭의 XY Slice 화면에서 기존의 마스크와 겹치지 않게 마우스를 클릭한다.

Then Mask Editor 탭의 XY Slice 화면에 노란색 마스크가 그려졌는지 확인

When Clean tool의 Clean selected label 버튼을 누르고 Mask Editor XY Slice 화면의 초록색 마스크를 클릭한다.

Then Mask Editor XY Slice 화면에서 초록색 마스크가 완전히 삭제되었는지 확인

And Mask Editor 3D 화면에 초록색 마스크가 존재하는지 확인

When History 패널에서 Undo 버튼을 누른다.

And Clean tool의 Flush selected label 버튼을 누르고 Mask Editor XY Slice 화면의 초록색 마스크를 클릭한다.

Then Mask Editor의 XY Slice 화면과 3D 화면에서 초록색 마스크가 완전히 삭제되었는지 확인

When History 패널에서 Undo 버튼을 누른다.

And Clean tool의 Clean Slice 버튼을 누르고 Mask Editor XY Slice 화면의 빨간색 마스크를 클릭한다.

Then Mask Editor XY Slice 화면에서 모든 마스크가 완전히 삭제되었는지 확인

And Mask Editor 3D 화면에 마스크가 존재하는지 확인

When History 패널에서 Undo 버튼을 누른다.

And Clean tool의 Flush Slice 버튼을 누르고 Mask Editor XY Slice 화면의 빨간색 마스크를 클릭한다.

Then Mask Editor의 XY Slice 화면과 3D 화면에서 모든 마스크가 완전히 삭제되었는지 확인

Then TA종료