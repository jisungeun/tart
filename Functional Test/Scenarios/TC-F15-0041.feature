﻿Feature: BAT Batch RUN setting

  Scenario: BAT Batch Run setting 탭의 개별 TCF 체크박스에 체크 해제
Given Basic Analyzer[T]의 Batch Run setting 탭의 개별 TCF 체크박스가 체크 되어 있다.

When Basic Analyzer[T] Batch Run setting 탭의 단일 TCF 체크박스를 클릭한다.

Then Basic Analyzer[T] Batch Run setting 탭의 단일 TCF 체크박스가 체크 해제됐는지 확인

And Basic Analyzer[T] Batch Run setting 탭의 파일 list에서 체크한 TCF의 row가 하이라이트 됐는지 확인

And Basic Analyzer[T] Batch Run setting 탭의 파일 list에서 체크한 TCF가 포함된 cube의 헤더가 전부 하이라이트 됐는지 확인