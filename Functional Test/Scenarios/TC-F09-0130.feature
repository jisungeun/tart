﻿Feature: BA Application Parameter

  Scenario: RI Thresholding을 프로세서로 선택했을 때, Labeling 탭에서 사이즈 바를 움직여 ‘Assign Size of Neglectable Particle’ 수치를 설정하기
Given BA Application Parameter에서 Use Labeling 체크박스가 체크되어 있다.

When ‘Assign Size of Neglectable Particle’ 밑의 사이즈 바를 마우스로 왼쪽 끝으로 움직인다.

Then 사이즈 바 옆의 수치가 0.1001로 변경되었는지 확인

When ‘Assign Size of Neglectable Particle’ 밑의 사이즈 바를 마우스로 오른쪽 끝으로 움직인다.

Then 사이즈 바 옆의 수치가 100.0000으로 변경되었는지 확인