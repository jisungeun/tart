﻿Feature: Create Cube

  Scenario: 같은 이름의 Cube를 새로 생성하려 했을 때 생성되지 않음
Given 빈 TA가 열려있다.
  
Given ‘CubeName’ 이라는 이름의 Cube가 생성되어 있다.

When 같은 Playground에서 Creat Cube 탭을 연다.

And 새로 생성할 cube의 이름을 ‘CubeName’ 으로 지정하고 경로를 지정한다.

And Create Cube 탭의 OK 버튼을 누른다.

Then 에러 팝업이 나타나며 cube가 생성되지 않음을 확인

Then TA종료