﻿Feature: BAT AI Segmentation Single RUN

  Scenario: BAT Single Run 탭의 Processing mode 패널에서 AI Segmentation 프로세서를 선택
Given Basic Analyzer[T] 탭의 Single run 탭이 열려 있다.

When Basic Analyzer[T] 탭 Processing mode 패널 상단의 드롭다운 버튼에서 AI Segmentation을 선택한다.

Then Basic Analyzer[T] 탭 Single run 탭의 Processing mode 패널에 AI Segmentation 모드의 UI가 나타남을 확인

And Basic Analyzer[T] 탭에 Time points setting 패널이 있는지 확인