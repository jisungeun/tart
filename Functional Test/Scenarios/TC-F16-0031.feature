﻿Feature: BAT Report[T]

  Scenario: Report[T] 탭의 Select data 패널에서 All 체크박스에 체크 해제
Given Report[T] 탭이 열려 있다.

And Report[T] 탭 Select Data 패널의 All 체크박스가 체크 되어있다.

When Report[T] 탭 Select Data 패널의 All 체크박스를 누른다.

And Report[T] 탭에서 All 체크박스 옆 화살표 버튼을 눌러 Cube 체크박스 리스트를 펼친다.

And Report[T] 탭에서 Cube 체크박스 옆 화살표 버튼을 눌러 개별 TCF 체크박스 리스트를 펼친다.

Then Report[T] 탭 Select Data 패널 All 체크박스에 체크 해제되었는지 확인

And Report[T] 탭에서 Cube 체크박스와 개별 TCF의 체크박스가 모두 체크 해제되었는지 확인