﻿Feature: BA AI Segmentation Single RUN

  Scenario: BA의 Measurement 팝업
Given 빈 TA가 열려있다.   
  
Given Basic Analyzer 탭의 Mask Viewing panel에 마스크가 생성되어 있다.

When Basic Measurement의 table의 parameter를 설정하고 Basic Measurement 탭의 Execute 버튼을 누른다.

And Measurement 팝업의 Export 버튼을 누른다.

And 파일 탐색기에서 취소 버튼을 누른다.

Then ‘Failed to save CSV’ 라는 내용의 에러 메세지 팝업이 뜨는지 확인

And 에러창이 뜬 TA 삭제