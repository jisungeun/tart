﻿Feature: PEP/UI features

  Scenario: Project Explorer Panel에서 TCF를 프로젝트에서 삭제
Given 빈 TA가 열려있다.   
  
Given 프로젝트에 TCF가 불러져 있다.

When Project Explorer Panel의 삭제하고 싶은 TCF 폴더에 마우스 포인터를 대고 우클릭한다.

And Remove TCF 버튼을 누른다.

And 팝업에서 Yes 버튼을 누른다.

Then Preview/List 패널에서 해당 폴더의 TCF가 삭제됐는지 확인

Then TA종료