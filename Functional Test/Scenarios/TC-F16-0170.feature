﻿Feature:  Report[T] 탭에서 그래프의 dot, line 색을 변경

  Scenario: Report[T] 탭에서 그래프의 dot, line 색을 변경
Given Report[T] 탭에 그래프가 그려져 있다.

When Report[T]의 그래프 패널 상단에서 cube 이름 옆의 드롭다운 버튼을 누르고 변경할 색을 선택한다.

Then Report[T] 탭의 그래프 패널에서 변경한 cube의 그래프 색이 바뀌었는지 확인

And Report[T] 탭의 그래프 패널에서 변경한 드롭다운 버튼의 색이 바뀌었는지 확인