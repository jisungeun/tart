﻿Feature: Create Hypercube

  Scenario: Create Hypercube 탭에서 Hypercube를 생성
Given 빈 TA가 열려있다.  
  
Given Hypercube가 생성되어 있다.

When Playground canvas의 Hypercube 아이콘을 우클릭하고 Rename Hypercube 버튼을 누른다.

And 팝업에서 Hypercube의 이름을 지정하고 OK 버튼을 누른다.

And Playground canvas의 Hypercube 아이콘을 우클릭 하고 Copy Hypercube 버튼을 누른다.

Then Playground canvas에서 ‘(원본 Hypercube 이름)_copy’의 Hypercube가 생성됨을 확인

When Playground canvas의 Copy된 Hypercube 아이콘을 우클릭 하고 Delete Hypercube 버튼을 누른다.

And 팝업의 Delete 버튼을 누른다.

Then Playground canvas에서 Hypercube 아이콘이 삭제됨을 확인

Then TA종료