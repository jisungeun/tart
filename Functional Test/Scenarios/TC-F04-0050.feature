﻿Feature: Create Hypercube

  Scenario: Playground canvas에서 Hypercube 아이콘을 클릭하여 hypercube를 선택
Given 한 개 이상의 hypercube가 생성되어 있다.

When Playground canvas 위의 Normal hypercube 아이콘을 누른다.

Then Playground canvas 위의 Normal hypercube 아이콘이 Active hypercube 아이콘으로 바뀜을 확인