﻿Feature: Import Data

  Scenario: Preview 패널에서 TCF 썸네일을 클릭하여 TCF를 여러 개 선택
  
Given TCF의 썸네일이 보이는 Preview 패널이 열려 있다.

When 원하는 TCF 파일의 썸네일을 1개 이상 누른다.

Then 1개 이상의 TCF 썸네일이 선택되는지 확인