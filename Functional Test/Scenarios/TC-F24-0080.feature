﻿Feature:  Mask Editor; From empty mask

  Scenario: 빈 Label 추가 후 Cell instance 마스크 그리기
Given 빈 TA가 열려있다.   
  
Given Mask Editor 탭에서 ‘test’ ID가 선택되어 있다.

When Mask control 패널 Cell Instance와 Whole cell 마스크의 오른쪽 회색 버튼을 클릭한다.

And Mask selector 패널 Cell instance 아이콘을 누른다.

And Drawing tool 패널의 Paint 버튼을 클릭하고 사이즈를 100으로 설정한 뒤 XY slice 화면을 클릭한다.

And Label tools 패널의 Add 글씨 버튼을 누른다.

Then Label tools 패널의 Label 값이 2로 바뀌었는지 확인

When Mask Editor 탭 XY Slice 화면에서 빨간 마스크와 겹치지 않게 화면을 클릭한다.

Then Mask Editor 탭에 녹색 마스크가 생성됐는지 확인

When Mask selector에서 Whole cell 아이콘을 누른다.

When Label tools 패널에서 Add 버튼을 누른다.

Then “Failed to add label” 이라는 경고창이 나타났는지 확인

When 경고창의 OK 버튼을 누르고 Mask Selector 패널에서 Cell instance 버튼을 누른다.

And Label tools 패널의 Label picker 버튼을 누른다.

And Mask Editor 탭에 생성된 녹색 마스크를 한 번 누른다.

Then Label tools 패널의 Label 값이 2로 바뀌었는지 확인

When Label tools 패널에서 활성화된 Label picker 버튼을 누른다.

And Drawing tool 패널에서 Erase 버튼을 누르고 XY Slice 화면의 빨간색 마스크를 클릭한다.

Then XY Slice 화면의 빨간색 마스크가 남아 있는지 확인(스크린 샷)

When Mask Editor 탭 XY Slice 화면의 초록색 마스크를 클릭한다.

Then XY Slice 화면에서 초록색 마스크가 지워졌는지 확인

When History 패널의 Undo 버튼을 누르고 Label tools 패널의 Merge label 버튼을 누른다.

And Mask Editor 탭의 빨간색, 녹색 마스크를 하나씩 누른다.

And Label tools 패널에서 활성화된 Merge label 버튼을 누른다.

Then Mask Editor에서 선택한 두 마스크가 모두 빨간색 마스크로 바뀌었는지 확인

Then TA종료

