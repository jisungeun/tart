﻿Feature: BAT Application Parameter

  Scenario: RI Thresholding을 프로세서로 선택했을 때, Basic Measurement의 table의 ‘Lipid’ preset 값을 확인
Given BAT Application Parameter에서 RI Thresholding이 프로세서로 선택되어 있다.

When BAT Application Parameter 패널 Basic Measurement 탭 table의 default column의 Preset 드롭다운 버튼에서 Lipid를 선택한다.

Then BAT Application Parameter 패널 Basic Measurement 탭 table에서 Baseline RI 값이 1.3337로, RII값이 0.135로 입력되는지 확인