﻿Feature: Link Data to Cube

  Scenario: List 패널에서 Exclusive 체크박스 체크 시 오른쪽 table에 존재하는 데이터가 왼쪽 table에서 제거됨
Given TCF가 cube에 연결되어 있다.

And List 패널이 열려 있다.

And Exclusive 체크박스가 비어있다.

When Exclusive 체크박스를 체크한다.

Then List 패널의 오른쪽 table에 존재하는 데이터가 왼쪽 table에서 제거됨을 확인