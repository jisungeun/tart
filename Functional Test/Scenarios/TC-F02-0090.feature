﻿Feature: Import Data

  Scenario: Preview 패널에서 Select all 버튼을 눌러 불러온 TCF를 모두 선택
Given Project에 TCF가 한 개 이상 불러져 있다.

When Preview 패널에서 Select all 버튼을 누른다.

Then 불러진 모든 TCF가 선택되었는지 확인