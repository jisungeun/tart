﻿Feature: D Visualizer HT+FL

  Scenario: 3D Visualizer application을 이용하여 HT+FL 데이터에서 이미지 파일과 영상 파일을 Export
Given 빈 TA가 열려있다.

When Select Project 탭의 Open 버튼을 누른다.

And “S03_3DVisualizerD” 프로젝트 파일을 선택한다.

Then “S03_3DVisualizerD” 프로젝트가 TA에 열렸는지 확인

When Preview 패널에서 원하는 HT+FL TCF 파일을 한 개 선택한다.

And Toolbox의 File Information 버튼을 누른다.

Then 선택한 HT+FL TCF의 Information 팝업이 열렸는지 확인

When Information 팝업 이미지 부분 상단의 FL 버튼을 누른다.

Then Information 팝업의 이미지가 HT+FL 사진으로 바뀌었는지 확인

When Information 팝업의 X 표시를 누른다.

And Preview 패널에서 HT+FL TCF를 한 개 선택한다.

And Toolbox의 3D visualization 버튼을 누른다.

Then 선택한 HT+FL TCF로 3D Visualizer 탭이 열렸는지 확인

When Preset 패널 TF canvas 밑의 Heatmap 체크박스를 누른다.

Then Preset 패널의 TF canvas가 Heatmap으로 변경되었는지 확인

When Modality 패널의 FL 체크박스를 클릭한다.

Then View 패널 3D 화면에 HT+FL Rendering이 생겼는지 확인

And View 패널 HT 화면에 형광 표지가 입혀졌는지 확인

When Preset 패널 상단의 Choose a TF preset 드롭다운 버튼을 누른다.

And 드롭다운에서 Rab5a_FL을 선택한다.

Then Preset 패널 상단 드롭다운 버튼에 Rab5a_FL 텍스트가 나타나는지 확인

And TF canvas에 Rab5a_FL preset의 TF box가 생성됐는지 확인

And View 패널에 Rab5a_FL preset의 Rendering이 생성됐는지 확인

When Screen shot 패널에서 multi view 탭의 Capture 버튼을 누른다.

And 파일 탐색기에서 이름과 경로를 지정한다.

Then 지정한 경로에 .png 파일이 지정한 이름으로 만들어졌는지 확인

And .png 파일이 촬영 시점의 View 화면과 동일한지 확인

When Movie maker 탭의 Add 버튼을 클릭하고 Slice 버튼을 클릭한다.

Then Movie maker 탭에 Slice 옵션이 추가되었는지 확인

When 마우스 포인터 모양이 바뀌면 마우스를 클릭한 채로 Slice 옵션 막대 길이를 2초까지 늘린다.

Then Movie maker 탭의 Slice 효과 막대의 재생 시간이 2초로 늘어났는지 확인

When Movie maker 탭 Slice UI의 Range 왼쪽 칸에 60을 입력한다.

And Movie maker 탭 Slice UI의 Range 오른쪽 칸에 155을 입력한다.

And Movie maker 탭의 Record 버튼을 누르고 Multi-view 버튼을 누른다.

And 파일 탐색기에 파일 이름과 저장 경로를 입력한다.

Then 지정한 경로에 동영상이 지정한 이름으로 생성되었는지 확인

When Application tab 바의 3D Visualizer 탭 이름 옆의 x 버튼을 클릭한다.

Then 3D Visualizer 탭이 종료되는지 확인