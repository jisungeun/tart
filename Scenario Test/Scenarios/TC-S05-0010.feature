﻿Feature: Basic Analyzer RI Thresholding

  Scenario: Basic Analyzer의 RI Thresholding을 이용하여 샘플의 RI에 기반한 마스크를 만들고 측정 값을 Export
Given 빈 TA가 열려있다.

When Select Project 탭의 Open 버튼을 누른다.

And “S05_BasicAnalyzerRIT” 프로젝트 파일을 선택한다.

Then “S05_BasicAnalyzerRIT” 프로젝트가 TA에 열렸는지 확인

When BA Application Parameter 패널의 제일 위 드롭다운 버튼에서 RI Thresholding을 클릭한다.

And BA Application Parameter 패널의 Single RUN 버튼을 누른다.

And 팝업에서 HeLa-Rab5a-Rab7a-003 파일을 더블클릭한다.

Then Basic Analyzer 탭이 나타났는지 확인

When Basic Analyzer 탭의 Processing mode에서 Auto lower threshold 체크박스를 클릭한다.

Then Basic Analyzer 탭 Auto lower Threshold 체크박스 밑에 ‘Select Algorithm’ 문구와 알고리즘 드롭다운 버튼, 하이라이트 된 Execute 버튼이 나타나는지 확인

When Basic Analyzer 탭 Auto Threshold 탭의 Execute 버튼을 누른다.

Then Basic Analyzer 탭의 Processing mode에서 Assign upper threshold의 값이 2.0000으로, Assign lower threshold의 값이 1.3482로 바뀌었는지 확인

When Basic Analyzer 탭의 UL Threshold 탭의 Execute 버튼을 누른다.

Then Basic Analyzer 탭 Mask Viewing panel에 마스크가 생성되었는지 확인

And Basic Analyzer 탭 Mask Viewing panel 상단에 Cell Instance, Whole Cell 체크박스와 Cell-wise filter 스위치, Opacity 조절 버튼이 있는 툴박스가 생성되었는지 확인

When Basic Analyzer 탭의 Processing mode에서 Use Labeling 체크박스를 클릭한다.

Then Basic Analyzer 탭의 ‘Select Option’ 문구와 옵션 드롭다운 버튼, ‘Assign Size of Neglectable Particle’ 문구와 사이즈 바, 수치 입력 칸이 나타나는지 확인

And Basic Analyzer 탭의 ‘Select Option’ 드롭다운 버튼에 기본 값으로 Multi Labels가 선택되었는지 확인

And Basic Analyzer 탭 Labeling 탭의 Execute 버튼이 하이라이트 되는지 확인

When Basic Analyzer 탭 Labeling 탭의 Execute 버튼을 누른다.

Then Basic Analyzer 탭 Mask Viewing panel에서 작은 크기의 mask가 없어지는 방식으로 mask가 수정되었는지 확인

And Basic Analyzer 탭 Mask Viewing panel에서 붙어있지 않은 마스크들이 다른 색으로 구별되는지 확인

When Basic Analyzer 탭 Processing mode 패널 Basic Measurement 탭의 Execute 버튼을 누른다.

Then Basic Analyzer 탭에 RI Thresholding에 대한 Measurement 팝업이 생기는지 확인한다.

When Measurement 팝업에서 Export 버튼을 누른다.

And 파일 탐색기에 저장 경로와 파일 이름을 입력하고 저장 버튼을 누른다.

Then 파일 탐색기 지정한 경로에 지정한 이름으로 생성된 .csv 파일의 데이터가 Measurement 탭의 데이터와 같은지 확인한다.

When Basic Analyzer 탭의 Apply Parameter 버튼을 클릭한다.

Then Basic Analyzer 탭의 Processing mode 패널과 Project Manager 탭의 BA Application Parameter 패널의 두 Basic Measurement 값이 일치하는지 확인

And 화면의 Basic Analyzer 탭이 Project Manager 탭으로 넘어갔는지 확인

When BA Application Parameter 패널에서 Batch Run 버튼을 누른다.

And File changes 팝업 Flush 탭의 Accept 버튼을 누른다.

And 프로세싱 바 팝업의 진행이 완료된다.

Then Report 탭이 나타났는지 확인

When Application tab 바의 Report 탭 이름 옆의 x 버튼을 클릭한다.

Then Report 탭이 종료되는지 확인